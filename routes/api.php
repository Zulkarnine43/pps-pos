<?php

use App\Http\Resources\UserResource;
use App\Http\Resources\SupplierResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Laravue\Faker;
use \App\Laravue\JsonResponse;
use \App\Laravue\Acl;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Api')->group(function () {
    Route::post('auth/login', 'AuthController@login');
    Route::group(['middleware' => 'auth:sanctum'], function () {
        // get super user dashboard data
        Route::get('/super-user/data', 'SuperUserDashboard@index');
        // Auth routes
        Route::get('auth/user', 'AuthController@user');
        Route::post('auth/logout', 'AuthController@logout');
        Route::put('/user/active/{id}', 'UserController@active');
        Route::put('/user/suspend/{id}', 'UserController@suspend');
        Route::get('/user', function (Request $request) {
            return new UserResource($request->user());
        });

        

        Route::post('get-model-data', 'SuperUserDashboard@getDatas');

        Route::apiResource('users', 'UserController')->middleware('permission:' . Acl::PERMISSION_USER_MANAGE);
        Route::get('get-user-role/{id}', 'UserController@getUserRole')->middleware('permission:' . Acl::PERMISSION_USER_MANAGE);
        Route::post('users/get-image', 'UserController@getImage');
        // Custom routes
        Route::put('users/{user}', 'UserController@update');
        Route::get('users/{user}/permissions', 'UserController@permissions')->middleware('permission:' . Acl::PERMISSION_PERMISSION_MANAGE);
        Route::put('users/{user}/permissions', 'UserController@updatePermissions')->middleware('permission:' . Acl::PERMISSION_PERMISSION_MANAGE);
        Route::get('roles/{role}/permissions', 'RoleController@permissions')->middleware('permission:' . Acl::PERMISSION_PERMISSION_MANAGE);
        // country route
        // Route::get('/countries', 'CountryController@activeCountry');
        Route::apiResource('countries', 'CountryController');
        // state route
        Route::get('/states/{country_id}', 'StateController@stateByCountry');
        Route::apiResource('states', 'StateController');
        // district route
        Route::get('/districts/{state_id}', 'DistrictController@districtByState');
        Route::apiResource('districts', 'DistrictController');
        // city route
        Route::get('cities/{district_id}', 'CityController@cityByDistrict');
        Route::apiResource('cities', 'CityController');
        // suppliers route
        Route::apiResource('suppliers', 'SupplierController');
        Route::put('suppliers/inactive/{supplier}', 'SupplierController@inActive');
        Route::put('suppliers/active/{supplier}', 'SupplierController@active');
        Route::post('suppliers/get-image', 'SupplierController@getImage');
        Route::post('suppliers/trade-licence', 'SupplierController@tradeLicence');
        Route::post('suppliers/tin', 'SupplierController@getTin');
        Route::post('suppliers/bin', 'SupplierController@getBin');
        Route::post('suppliers/nid', 'SupplierController@getNid');
        // clients routs
        Route::apiResource('clients', 'ClientController');
        Route::put('clients/inactive/{client}', 'ClientController@inActive');
        Route::put('clients/active/{client}', 'ClientController@active');
        Route::post('clients/get-image', 'ClientController@getImage');
        
        // subcontractors routes
        Route::apiResource('subcontractors', 'SubcontractorController');
        Route::put('subcontractors/inactive/{subcontractor}', 'SubcontractorController@inActive');
        Route::put('subcontractors/active/{subcontractor}', 'SubcontractorController@active');
        Route::get('active/subcontractors', 'SubcontractorController@activeSubcontractors');
        Route::post('subcontractors/get-image', 'SubcontractorController@getImage');
        Route::post('subcontractors/trade-licence', 'SubcontractorController@tradeLicence');
        Route::post('subcontractors/tin', 'SubcontractorController@getTin');
        Route::post('subcontractors/bin', 'SubcontractorController@getBin');
        Route::post('subcontractors/nid', 'SubcontractorController@getNid');
        Route::put('subcontractors/bulk-edit/{subcontractor_ids}', 'SubcontractorController@bulkEdit');
        // department routes
        Route::apiResource('departments', 'DepartmentController');
        // employee routes
        Route::apiResource('employees', 'EmployeeController');
        Route::post('employees/get-image', 'EmployeeController@getImage');
        Route::put('employee/active/{id}', 'EmployeeController@active');
        Route::put('employee/inactive/{id}', 'EmployeeController@inactive');
        // Api resource routes
        Route::apiResource('roles', 'RoleController')->middleware('permission:' . Acl::PERMISSION_PERMISSION_MANAGE);
        Route::apiResource('permissions', 'PermissionController')->middleware('permission:' . Acl::PERMISSION_PERMISSION_MANAGE);
        Route::apiResource('permission-prefix', 'PermissionPrefixController')->middleware('permission:' . Acl::PERMISSION_PERMISSION_MANAGE);
        // product routes
        Route::apiResource('products', 'ProductController');
        Route::put('product/unmute/{product}', 'ProductController@unmute');
        Route::put('product/mute/{product}', 'ProductController@mute');
        Route::delete('product-meta/delete/{product}', 'ProductController@destroy');
        Route::post('products/get-image', 'ProductController@getImage');

        // purchases routes
        Route::apiResource('purchases', 'PurchaseController');
        Route::get('/active/items', 'SuperUserDashboard@activeItems');
        Route::get('/get/purchase-items/{id}', 'PurchaseController@getItems');
        Route::put('purchase/unmute/{purchase}', 'PurchaseController@unmute');
        Route::put('purchase/mute/{purchase}', 'PurchaseController@mute');
        Route::put('purchase/receive/{purchase}', 'PurchaseController@receivePurchase');
        Route::post('purchase-report','PurchaseController@purchase_report');
        // purchases requisition routes
        Route::apiResource('purchaseRequisitions', 'PurchaseRequisitionController');
        Route::apiResource('purchaseOrders', 'PurchaseOrderController');

        // settings routes
        // warehouse routes
        Route::apiResource('warehouses', 'WarehouseController');
        Route::get('warehouse/get-inventory-products/{id}', 'WarehouseController@getInventoryProducts');
        Route::put('warehouse/unmute/{warehouse}', 'WarehouseController@unmute');
        Route::put('warehouse/mute/{warehouse}', 'WarehouseController@mute');
        // Stock routes
        Route::get('stock/list', 'StockController@index');
        // Transfer routes
        Route::apiResource('transfers', 'TransferController');
        // Delivery routes
        Route::apiResource('deliveries', 'DeliveryController');
        Route::get('inventory/get-deliveries', 'TransferController@getDeliveries');
        // entrypin route
        Route::get('entry-pin/check/{pin}', 'EntryPinController@checkPin');
        Route::get('entry-pin/get-pin', 'EntryPinController@getPin');
        Route::get('entry-pin/update/{id}', 'EntryPinController@show');
        Route::put('entry-pin/update/{id}', 'EntryPinController@update');
        // category routes
        
        Route::apiResource('categories', 'CategoryController');
        Route::put('categories/unmute/{category}', 'CategoryController@unmute');
        Route::put('categories/mute/{category}', 'CategoryController@mute');
        Route::post('categories/get-image', 'CategoryController@getImage');
        // brand routes
        Route::apiResource('brands', 'BrandController');
        Route::put('brand/unmute/{brand}', 'BrandController@unmute');
        Route::put('brand/mute/{brand}', 'BrandController@mute');
        Route::post('brands/get-image', 'BrandController@getImage');
        // unit routes
        Route::apiResource('units', 'UnitController');
        Route::put('unit/unmute/{unit}', 'UnitController@unmute');
        Route::put('unit/mute/{unit}', 'UnitController@mute');
        // project_type route
        Route::apiResource('project-type', 'ProjectTypeController');
        Route::put('project-type/unmute/{id}', 'ProjectTypeController@unmute');
        Route::put('project-type/mute/{id}', 'ProjectTypeController@mute');
        Route::delete('project-type-meta/delete/{id}', 'ProjectTypeController@destroy');
        Route::get('active/project-type', 'ProjectTypeController@getActiveItems');
        //project type active stage route
        Route::get('project-type-stage/{type_id}', 'TypeStageController@getTypeStage');
        Route::get('/get-current-stage/{id}', 'ProjectStageController@getCurrentStage');
        // activity log routes
        Route::get('activity-log', 'ActivityLogController@index');
        // project_type estimate route
        Route::apiResource('project-type-estimate', 'ProjectTypeEstimateController');
        Route::put('project-type-estimate/unmute/{id}', 'ProjectTypeEstimateController@unmute');
        Route::put('project-type-estimate/mute/{id}', 'ProjectTypeEstimateController@mute');
        Route::delete('project-type-estimate-item/delete/{id}', 'ProjectTypeEstimateController@itemDelete');
        // project estimate route
        Route::apiResource('projects', 'ProjectController');
        Route::get('projects/project-type/{id}', 'ProjectController@getProjectsByProjectType');
        Route::post('projects/import', 'ProjectController@import');
        Route::post('projects/imported-file/save', 'ProjectController@saveFile');
        // Project Bulk Edit Route
        Route::post('projects/bulk-edit', 'ProjectController@bulkEdit');
        Route::post('projects/bulk-edit-address', 'ProjectController@bulkEditAddress');
        Route::post('projects/edit-from-list/{id}', 'ProjectController@editFromList');
        Route::post('projects/sineboard/{project_ids}', 'ProjectController@getSineboard');
        Route::apiResource('project-estimate', 'ProjectEstimateController');
        Route::get('get/project-estimate/{pid}', 'ProjectEstimateController@projectEstimations');
        Route::apiResource('project-subcontracts', 'ProjectSubcontractController');
        Route::get('get-all-subcontract', 'ProjectSubcontractController@getAllSubcontract');
        Route::get('get-project-subcontract/{pid}', 'ProjectSubcontractController@getProSub');
        Route::put('subcontract-done/{id}', 'ProjectSubcontractController@done');
        Route::post('subcontracts/bulk-store', 'ProjectSubcontractController@bulkStore');
        // Project inventory Route
        Route::get('project/get-inventory-products/{id}', 'ProjectController@getInventoryProducts');
        //    work order route
        Route::apiResource('work-order', 'WorkOrderController');
        Route::apiResource('work-order-item', 'WorkOrderItemController');
        // settings route
        Route::get('settings/{id}', 'SettingsController@show');

        // update office info
        Route::apiResource('office-info', 'OfficeInfoController');

        // update pos setting
        Route::apiResource('pos-setting', 'PosSettingController');

        // print paper setting
        Route::apiResource('print-paper-size', 'PrintPaperSizeController');

        // update pos setting
        Route::apiResource('payment-method', 'PaymentMethodController');
        // project stage route
        Route::apiResource('project-stage', 'ProjectStageController');
        Route::post('stages/get-image/{pid}/{sid}', 'ProjectStageController@getImage');
        Route::get('get/project-stage/{id}', 'ProjectStageController@getStage');
        Route::get('stage/current/images/{pid}/{sid}', 'ProjectImageController@getImage');
        // Route::get('stage/images/{pid}/{sid}', 'ProjectImageController@getImage');
        Route::post('stage/go-next/{pid}/{next_id}/{sid}', 'ProjectStageController@goNext');
        Route::delete('project-image/delete/{pid}/{sid}/{imgId}', 'ProjectStageController@deleteImage');
        Route::apiResource('project-requisition', 'ProjectRequisitionController');
        Route::post('project-requisition/import', 'ProjectRequisitionController@import');
        Route::post('project-requisition/set-site-code/{id}', 'ProjectRequisitionController@setSiteCode');

        Route::apiResource('project-status', 'ProjectStatusController');
        Route::get('get-all-project-status', 'ProjectStatusController@getAll');
        Route::apiResource('new-project-assign', 'NewProjectAssignController');
        Route::apiResource('new-project-assign-item', 'NewProjectAssignItemController');
        Route::put('assaigned-project/remove/{id}', 'NewProjectAssignItemController@destroy');

        Route::apiResource('project-dirasa', 'ProjectDirasaController');
        Route::post('project-dirasa/set-site-code/{id}', 'ProjectDirasaController@setSiteCode');
        // client price list route
        Route::apiResource('client-price-list', 'ClientPriceListController');
        // subcontractor price list routes
        Route::apiResource('subcontractor-price-list', 'SubcontractorPriceListController');
        // subcontractor price list routes
        Route::apiResource('quotation', 'QuotationController');
        // warehouse-inventory routes
        Route::apiResource('warehouse-inventory', 'WarehouseInventoryController');
        Route::get('check-product-in-warehouse/{product_id}', 'WarehouseInventoryController@checkProductInWarehouse');
        // purchase-item routes
        Route::apiResource('purchase-item', 'PurchaseItemController');

        // sale routes
        Route::apiResource('sales', 'SaleController');
        Route::post('sales-report','SaleController@sale_report');
        Route::apiResource('sale-register', 'SaleRegisterController');
        Route::get('check-opened-sale-register', 'SaleRegisterController@checkOpenedRegister');
        Route::post('close-sale-register', 'SaleRegisterController@closeRegister');
        // bill routes
        Route::apiResource('client-bill', 'ClientBillController');
        Route::post('client-bill/save-draft', 'ClientBillController@saveDraft');
        Route::post('client-bill/receive-bills', 'ClientBillController@receiveBill');
        // client bill routes
        Route::apiResource('client-topsheet', 'ClientTopSheetController');
        Route::put('client-topsheet/remove/{id}', 'ClientTopSheetController@remove');
        Route::get('client-topsheet/receive/{id}', 'ClientTopSheetController@view');
        Route::get('get-all-canceled-bill', 'ClientTopSheetController@getCanceledBills');
        Route::put('client-topsheet/receive/{id}', 'ClientTopSheetController@update');
        Route::post('client-topsheet/set-br-number/{id}', 'ClientBillController@setBRNumber');
        Route::post('client-topsheet/set-bs-number/{id}', 'ClientBillController@setBSNumber');

        Route::get('get-ready-for-bill', 'ClientBillController@getReadyForBill');

        // subcontract bill routes
        Route::apiResource('subcontractor-bill', 'SubcontractorBillController');
        Route::put('subcontractor-bill/approved/{id}', 'SubcontractorBillController@approved');
        Route::post('subcontractor-bill/checked', 'SubcontractorBillController@checked');
        // subcontract bill item routes
        Route::apiResource('subcontractor-bill-items', 'SubcontractorBillItemController');
        Route::get('get-all-subcontractor-canceled-bill', 'SubcontractorBillItemController@getCanceledBills');
        //  get table last id
        Route::get('get-table-last-id/{model}', 'SuperUserDashboard@getTableLastID');
        /**
         * @var		mixed	route BANKLIST
         * khalif
         */
        Route::apiResource('bank', 'BankController');
        Route::put('bank/unmute/{bank}', 'BankController@unmute');
        Route::put('bank/mute/{bank}', 'BankController@mute');
        // Route::post('brands/get-image', 'BrandController@getImage');
        // Chart Of Account  
        Route::apiResource('chart-of-account', 'ChartOfAccountController');

        Route::apiResource('groups', 'GroupController');
        Route::get('get-all-groups', 'GroupController@getAll');
        Route::apiResource('parent-group', 'ParentGroupController');
        Route::apiResource('default-group', 'DefaultGroupController');
        Route::get('get-all-default-groups', 'DefaultGroupController@getAll');
        Route::apiResource('ledgers', 'LedgerController');
        Route::get('get-all-ledgers', 'GroupController@getAll');
        Route::apiResource('journa-item-cost-center', 'JournalItemCostCenterController');

        Route::apiResource('journals', 'JournalController');
        Route::get('get-all-journals', 'GroupController@getAll');
        Route::delete('delete-journal-item/{id}', 'JournalItemController@destroy');
        Route::delete('delete-journal-item-cost-center/{id}', 'JournalItemCostCenterController@destroy');

        Route::post('cost-center-report', 'JournalItemCostCenterController@viewReport');

        Route::post('ledger-report', 'LedgerController@viewReport');

        Route::post('trail-balance-report', 'LedgerController@viewTrailBalanceReport');
    });
});
