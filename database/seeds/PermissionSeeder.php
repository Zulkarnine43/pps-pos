<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            ['name' => 'view menu element ui', 'guard_name' => 'api'],
            ['name' =>  'view menu permission', 'guard_name' => 'api'],
            ['name' =>  'view menu components', 'guard_name' => 'api'],
            ['name' =>  'view menu charts', 'guard_name' => 'api'],
            ['name' =>  'view menu nested routes', 'guard_name' => 'api'],
            ['name' =>  'view menu table', 'guard_name' => 'api'],
            ['name' =>  'view menu administrator', 'guard_name' => 'api'],
            ['name' =>  'view menu theme', 'guard_name' => 'api'],
            ['name' =>  'view menu clipboard', 'guard_name' => 'api'],
            ['name' =>  'view menu excel', 'guard_name' => 'api'],
            ['name' =>  'view menu zip', 'guard_name' => 'api'],
            ['name' =>  'view menu pdf', 'guard_name' => 'api'],
            ['name' =>  'view menu i18n', 'guard_name' => 'api'],
            ['name' =>  'manage user', 'guard_name' => 'api'],
            ['name' =>  'manage article', 'guard_name' => 'api'],
            ['name' =>  'manage permission', 'guard_name' => 'api'],
            ['name' =>  'manage supplier', 'guard_name' => 'api'],
            ['name' =>  'manage client', 'guard_name' => 'api'],
            ['name' =>  'manage subcontractor', 'guard_name' => 'api'],
            ['name' =>  'view menu product', 'guard_name' => 'api'],
            ['name' =>  'view menu category', 'guard_name' => 'api'],
            ['name' => 'view menu brand', 'guard_name' => 'api'],
            ['name' =>  'view menu unit', 'guard_name' => 'api'],
            ['name' =>  'manage category', 'guard_name' => 'api'],
            ['name' =>  'view purchase', 'guard_name' => 'api'],
            ['name' =>  'manage brand', 'guard_name' => 'api'],
            ['name' =>  'view menu inventory', 'guard_name' => 'api'],
            ['name' =>  'manage unit', 'guard_name' => 'api'],
            ['name' =>  'manage product', 'guard_name' => 'api'],
            ['name' =>  'view menu purchase', 'guard_name' => 'api'],
            ['name' =>  'view menu settings', 'guard_name' => 'api'],
            ['name' =>  'manage warehouse', 'guard_name' => 'api'],
            ['name' => 'manage purchase', 'guard_name' => 'api'],
            ['name' =>  'view suppliers list', 'guard_name' => 'api'],
            ['name' =>  'add supplier', 'guard_name' => 'api'],
            ['name' =>  'update supplier', 'guard_name' => 'api'],
            ['name' =>  'view clients list', 'guard_name' => 'api'],
            ['name' =>  'add client', 'guard_name' => 'api'],
            ['name' =>  'update client', 'guard_name' => 'api'],
            ['name' =>  'view subcontractors list', 'guard_name' => 'api'],
            ['name' =>  'add subcontractor', 'guard_name' => 'api'],
            ['name' =>  'update subcontractor', 'guard_name' => 'api'],
            ['name' =>  'view brands list', 'guard_name' => 'api'],
            ['name' =>  'add brand', 'guard_name' => 'api'],
            ['name' =>  'update brand', 'guard_name' => 'api'],
            ['name' =>  'view categories list', 'guard_name' => 'api'],
            ['name' =>  'add category', 'guard_name' => 'api'],
            ['name' =>  'update category', 'guard_name' => 'api'],
            ['name' =>  'view units list', 'guard_name' => 'api'],
            ['name' =>  'add unit', 'guard_name' => 'api'],
            ['name' =>  'update unit', 'guard_name' => 'api'],
            ['name' =>  'view warehouses list', 'guard_name' => 'api'],
            ['name' =>  'edit warehouse', 'guard_name' => 'api'],
            ['name' =>  'update warehouse', 'guard_name' => 'api'],
            ['name' => 'view purchases list', 'guard_name' => 'api'],
            ['name' =>  'add purchase', 'guard_name' => 'api'],
            ['name' =>  'update purchase', 'guard_name' => 'api'],
            ['name' =>  'view users list', 'guard_name' => 'api'],
            ['name' =>  'update user', 'guard_name' => 'api'],
            ['name' =>  'add user', 'guard_name' => 'api'],
            ['name' =>  'manage export', 'guard_name' => 'api'],
            ['name' =>  'manage activity', 'guard_name' => 'api'],
            ['name' =>  'view menu projects', 'guard_name' => 'api'],
            ['name' =>  'manage project type', 'guard_name' => 'api'],
            ['name' =>  'add project type', 'guard_name' => 'api'],
            ['name' =>  'update project type', 'guard_name' => 'api'],
            ['name' =>  'add project type meta', 'guard_name' => 'api'],
            ['name' =>  'add product', 'guard_name' => 'api'],
            ['name' =>  'update product', 'guard_name' => 'api'],
            ['name' =>  'update entry pin', 'guard_name' => 'api'],
            ['name' =>  'manage project type estimate', 'guard_name' => 'api'],
            ['name' =>  'add project type estimate', 'guard_name' => 'api'],
            ['name' =>  'update project type estimate', 'guard_name' => 'api'],
            ['name' =>  'add project', 'guard_name' => 'api'],
            ['name' =>  'update project', 'guard_name' => 'api'],
            ['name' =>  'manage project', 'guard_name' => 'api'],
            ['name' =>  'view project list', 'guard_name' => 'api'],
            ['name' =>  'view project', 'guard_name' => 'api'],
            ['name' =>  'add project subcontract', 'guard_name' => 'api'],
            ['name' => 'update project subcontract', 'guard_name' => 'api'],
            ['name' =>  'view invoice', 'guard_name' => 'api'],
            ['name' =>  'generate invoice', 'guard_name' => 'api'],
            ['name' =>  'print invoice', 'guard_name' => 'api'],
            ['name' =>  'generate work order', 'guard_name' => 'api'],
            ['name' =>  'add department', 'guard_name' => 'api'],
            ['name' =>  'update department', 'guard_name' => 'api'],
            ['name' =>  'manage department', 'guard_name' => 'api'],
            ['name' =>  'print agreement', 'guard_name' => 'api'],
            ['name' =>  'update user image', 'guard_name' => 'api'],
            ['name' => 'update user information', 'guard_name' => 'api']
        ];

        DB::table('permissions')->insert($permissions);
    }
}
