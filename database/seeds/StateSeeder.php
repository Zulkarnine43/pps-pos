<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $states = array(
            array('id' => '1','country_id' => '19','name' => 'Chattagram'),
            array('id' => '2','country_id' => '19','name' => 'Rajshahi'),
            array('id' => '3','country_id' => '19','name' => 'Khulna'),
            array('id' => '4','country_id' => '19','name' => 'Barisal'),
            array('id' => '5','country_id' => '19','name' => 'Sylhet'),
            array('id' => '6','country_id' => '19','name' => 'Dhaka'),
            array('id' => '7','country_id' => '19','name' => 'Rangpur'),
            array('id' => '8','country_id' => '19','name' => 'Mymensingh')
        );
        DB::table('states')->insert($states);
    }
}
