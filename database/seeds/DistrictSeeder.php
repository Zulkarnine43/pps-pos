<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DistrictSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $districts = array(
            array('id' => '1','state_id' => '1','name' => 'Comilla'),
            array('id' => '2','state_id' => '1','name' => 'Feni'),
            array('id' => '3','state_id' => '1','name' => 'Brahmanbaria'),
            array('id' => '4','state_id' => '1','name' => 'Rangamati'),
            array('id' => '5','state_id' => '1','name' => 'Noakhali'),
            array('id' => '6','state_id' => '1','name' => 'Chandpur'),
            array('id' => '7','state_id' => '1','name' => 'Lakshmipur'),
            array('id' => '8','state_id' => '1','name' => 'Chattogram'),
            array('id' => '9','state_id' => '1','name' => 'Coxsbazar'),
            array('id' => '10','state_id' => '1','name' => 'Khagrachhari'),
            array('id' => '11','state_id' => '1','name' => 'Bandarban'),
            array('id' => '12','state_id' => '2','name' => 'Sirajganj'),
            array('id' => '13','state_id' => '2','name' => 'Pabna'),
            array('id' => '14','state_id' => '2','name' => 'Bogura'),
            array('id' => '15','state_id' => '2','name' => 'Rajshahi'),
            array('id' => '16','state_id' => '2','name' => 'Natore'),
            array('id' => '17','state_id' => '2','name' => 'Joypurhat'),
            array('id' => '18','state_id' => '2','name' => 'Chapainawabganj'),
            array('id' => '19','state_id' => '2','name' => 'Naogaon'),
            array('id' => '20','state_id' => '3','name' => 'Jashore'),
            array('id' => '21','state_id' => '3','name' => 'Satkhira'),
            array('id' => '22','state_id' => '3','name' => 'Meherpur'),
            array('id' => '23','state_id' => '3','name' => 'Narail'),
            array('id' => '24','state_id' => '3','name' => 'Chuadanga'),
            array('id' => '25','state_id' => '3','name' => 'Kushtia'),
            array('id' => '26','state_id' => '3','name' => 'Magura'),
            array('id' => '27','state_id' => '3','name' => 'Khulna'),
            array('id' => '28','state_id' => '3','name' => 'Bagerhat'),
            array('id' => '29','state_id' => '3','name' => 'Jhenaidah'),
            array('id' => '30','state_id' => '4','name' => 'Jhalakathi'),
            array('id' => '31','state_id' => '4','name' => 'Patuakhali'),
            array('id' => '32','state_id' => '4','name' => 'Pirojpur'),
            array('id' => '33','state_id' => '4','name' => 'Barisal'),
            array('id' => '34','state_id' => '4','name' => 'Bhola'),
            array('id' => '35','state_id' => '4','name' => 'Barguna'),
            array('id' => '36','state_id' => '5','name' => 'Sylhet'),
            array('id' => '37','state_id' => '5','name' => 'Moulvibazar'),
            array('id' => '38','state_id' => '5','name' => 'Habiganj'),
            array('id' => '39','state_id' => '5','name' => 'Sunamganj'),
            array('id' => '40','state_id' => '6','name' => 'Narsingdi'),
            array('id' => '41','state_id' => '6','name' => 'Gazipur'),
            array('id' => '42','state_id' => '6','name' => 'Shariatpur'),
            array('id' => '43','state_id' => '6','name' => 'Narayanganj'),
            array('id' => '44','state_id' => '6','name' => 'Tangail'),
            array('id' => '45','state_id' => '6','name' => 'Kishoreganj'),
            array('id' => '46','state_id' => '6','name' => 'Manikganj'),
            array('id' => '47','state_id' => '6','name' => 'Dhaka'),
            array('id' => '48','state_id' => '6','name' => 'Munshiganj'),
            array('id' => '49','state_id' => '6','name' => 'Rajbari'),
            array('id' => '50','state_id' => '6','name' => 'Madaripur'),
            array('id' => '51','state_id' => '6','name' => 'Gopalganj'),
            array('id' => '52','state_id' => '6','name' => 'Faridpur'),
            array('id' => '53','state_id' => '7','name' => 'Panchagarh'),
            array('id' => '54','state_id' => '7','name' => 'Dinajpur'),
            array('id' => '55','state_id' => '7','name' => 'Lalmonirhat'),
            array('id' => '56','state_id' => '7','name' => 'Nilphamari'),
            array('id' => '57','state_id' => '7','name' => 'Gaibandha'),
            array('id' => '58','state_id' => '7','name' => 'Thakurgaon'),
            array('id' => '59','state_id' => '7','name' => 'Rangpur'),
            array('id' => '60','state_id' => '7','name' => 'Kurigram'),
            array('id' => '61','state_id' => '8','name' => 'Sherpur'),
            array('id' => '62','state_id' => '8','name' => 'Mymensingh'),
            array('id' => '63','state_id' => '8','name' => 'Jamalpur'),
            array('id' => '64','state_id' => '8','name' => 'Netrokona')
        );

        DB::table('districts')->insert($districts);
    }
}
