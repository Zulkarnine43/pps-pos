<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionPrefix extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission_prefixes = [
            ['name' => 'permission'],
            ['name' => 'administrator'],
            ['name' => 'user'],
            ['name' => 'supplier'],
            ['name' => 'client'],
            ['name' => 'subcontractor'],
            ['name' => 'product'],
            ['name' => 'category'],
            ['name' => 'unit'],
            ['name' => 'brand'],
            ['name' => 'purchase'],
            ['name' => 'inventory'],
            ['name' => 'settings'],
            ['name' => 'warehouse'],
            ['name' => 'export'],
            ['name' => 'activity'],
            ['name' => 'project'],
            ['name' => 'project type'],
            ['name' => 'project type meta'],
            ['name' => 'entry pin'],
            ['name' => 'project type estimate'],
            ['name' => 'project subcontract'],
            ['name' => 'invoice'],
            ['name' => 'work order'],
            ['name' => 'department'],
            ['name' => 'agreement'],
            ['name' => 'project estimation'],
            ['name' => 'topsheet'],
            ['name' => 'requisition'],
            ['name' => 'city'],
            ['name' => 'country'],
            ['name' => 'state'],
            ['name' => 'district'],
            ['name' => 'signboard'],
        ];

        DB::table('permission_prefixes')->insert($permission_prefixes);
    }
}
