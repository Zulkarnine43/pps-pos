<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfers', function (Blueprint $table) {
            $table->id();
            $table->date('transfer_date');
            $table->unsignedInteger('created_by');
            $table->string('ref_number')->nullable();
            $table->string('gatepass_number')->nullable();
            $table->unsignedTinyInteger('transfer_type');
            $table->unsignedInteger('from');
            $table->unsignedInteger('to');
            $table->unsignedTinyInteger('status')->default(0);
            $table->string('transfered_by')->nullable();
            $table->string('transfered_by_phone')->nullable();
            $table->text('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfers');
    }
}
