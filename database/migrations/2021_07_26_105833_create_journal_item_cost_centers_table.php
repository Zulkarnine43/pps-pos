<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJournalItemCostCentersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journal_item_cost_centers', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('journal_item_id');
            $table->unsignedInteger('project_type_id');
            $table->unsignedInteger('project_id');
            $table->string('site_code');
            $table->unsignedDouble('amount');
            $table->string('cost_center_type');
            $table->unsignedTinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('journal_item_cost_centers');
    }
}
