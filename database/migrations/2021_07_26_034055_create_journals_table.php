<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJournalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journals', function (Blueprint $table) {
            $table->id();
            $table->string('voucher_no');
            $table->unsignedInteger('ledger_id');
            $table->string('ledger_name');
            $table->unsignedDouble('net_amount')->default(0);
            $table->date('trans_date');
            $table->unsignedInteger('created_by');
            $table->string('branch_name');
            $table->text('narration')->nullable();
            $table->unsignedTinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('journals');
    }
}
