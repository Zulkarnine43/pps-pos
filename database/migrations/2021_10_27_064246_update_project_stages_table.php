<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateProjectStagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_stages', function (Blueprint $table) {
            $table->string('status')->default('waiting')->change();
            $table->renameColumn('status', 'bill_status');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_stages', function (Blueprint $table) {
            $table->renameColumn('bill_status', 'status')->nullable();
            $table->unsignedInteger('bill_status')->default(0)->change();
        });
    }
}
