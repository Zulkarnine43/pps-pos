<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->id();
            $table->string('sale_id')->unique();
            $table->date('sale_date');
            $table->float('discount')->default(0);
            $table->string('tax')->default(0);
            $table->string('vat')->default(0);
            $table->unsignedDouble('grand_total', 16,2)->default(0);
            $table->unsignedInteger('sale_by');
            $table->unsignedInteger('customer_id');
            $table->unsignedInteger('sale_register_id');
            $table->unsignedInteger('warehouse_id');
            $table->string('note')->nullable();
            $table->unsignedTinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
