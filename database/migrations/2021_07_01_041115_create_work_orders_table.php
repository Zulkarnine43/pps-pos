<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_orders', function (Blueprint $table) {
            $table->id();
            
            $table->unsignedInteger('subcontractor_id')->nullable();
            $table->unsignedInteger('supplier_id')->nullable();
            $table->text('task_details')->nullable();
            $table->text('delivery_point')->nullable();
            $table->text('delivery_address')->nullable();
            $table->text('delivery_contract_number')->nullable();
            $table->text('supply_item')->nullable();
            $table->text('payment_terms')->nullable();
            $table->text('payment_methods')->nullable();
            $table->text('remarks')->nullable();
            $table->string('order_no')->unique();
            $table->string('contract_ref')->nullable();
            $table->unsignedInteger('department_id');
            $table->unsignedFloat('payable_amount');
            $table->date('issue_date')->nullable();
            $table->date('finished_date')->nullable();
            $table->date('deadline')->nullable();
            $table->string('work_order_for');
            $table->string('status')->default('created');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_orders');
    }
}
