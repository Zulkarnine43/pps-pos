<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectSubcontractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_subcontracts', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('project_id');
            $table->unsignedInteger('subcontractor_id');
            $table->unsignedInteger('project_stage_id');
            $table->text('task_details');
            $table->string('code')->nullable();
            $table->string('order_no')->nullable();
            $table->string('contract_ref')->nullable();
            $table->unsignedInteger('department_id');
            $table->integer('payable_amount');
            $table->date('issue_date')->nullable();
            $table->date('finished_date')->nullable();
            $table->string('deadline');
            $table->unsignedTinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_subcontracts');
    }
}
