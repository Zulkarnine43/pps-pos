<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectTypeEstimateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_type_estimate_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('project_type_estimate_id');
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('unit_id');
            $table->unsignedInteger('quantity')->default(0);
            $table->unsignedInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_type_estimate_items');
    }
}
