<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsToPurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchases', function (Blueprint $table) {
          $table->integer('purchase_type')->after('ref_number')->nullable();
          $table->unsignedInteger('purchase_order_id')->after('ref_number')->nullable();
          $table->unsignedInteger('project_id')->after('warehouse_id')->nullable();
          $table->unsignedInteger('warehouse_id')->nullable()->change();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchases', function (Blueprint $table) {
          $table->dropColumn('purchase_order_id','purchase_type','project_id');
          $table->unsignedInteger('warehouse_id')->nullable(false)->change();
        });
    }
}
