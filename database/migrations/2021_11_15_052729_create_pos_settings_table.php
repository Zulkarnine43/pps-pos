<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePosSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pos_settings', function (Blueprint $table) {
            $table->id();
            $table->string('shop_name');
            $table->string('address');
            $table->string('office_phone');
            $table->string('mushak');
            $table->string('tax')->default(0);
            $table->string('vat')->default(0);
            $table->string('vat_registration_number')->nullable();
            $table->string('tax_registration_number')->nullable();
            $table->unsignedInteger('customer_id');
            $table->unsignedInteger('warehouse_id');
            $table->text('bill_footer');
            $table->string('logo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pos_settings');
    }
}
