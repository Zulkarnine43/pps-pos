<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectDirasasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_dirasas', function (Blueprint $table) {
            $table->id();
            $table->string('pdc_number')->unique();
            $table->date('dirasa_date');
            $table->unsignedInteger('project_type_id');
            $table->unsignedInteger('quantity')->default(1);
            $table->text('details')->nullable();
            $table->unsignedInteger('requisition_by')->nullable();
            $table->string('prs_number')->nullable();
            $table->unsignedInteger('client_id')->nullable();
            $table->string('department_name')->nullable();
            $table->string('name')->nullable();
            $table->string('nid_number')->nullable();
            $table->unsignedInteger('country_id')->default(19);
            $table->unsignedInteger('state_id')->nullable();
            $table->unsignedInteger('district_id')->nullable();
            $table->unsignedInteger('city_id')->nullable();
            $table->string('location')->nullable();
            $table->string('bn_address')->nullable();
            $table->string('phone')->nullable();
            $table->string('gps')->nullable();
            $table->string('remark')->nullable();
            $table->string('site_code')->nullable();
            $table->unsignedTinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_dirasas');
    }
}
