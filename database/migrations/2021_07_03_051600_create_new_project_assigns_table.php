<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewProjectAssignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_project_assigns', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('department_id');
            $table->string('reference_number');
            $table->unsignedInteger('subcontractor_id');
            $table->unsignedInteger('prepared_by');
            $table->date('prepared_date');
            $table->unsignedInteger('checked_by')->nullable();
            $table->date('checked_date')->nullable();
            $table->unsignedInteger('approved_by')->nullable();
            $table->date('approved_date')->nullable();
            $table->string('status')->default('prepared');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_project_assigns');
    }
}
