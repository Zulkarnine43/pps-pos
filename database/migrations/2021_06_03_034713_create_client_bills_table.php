<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_bills', function (Blueprint $table) {
            $table->id();
            $table->string('bill_id')->unique();
            $table->unsignedInteger('client_id')->nullable();
            $table->string('bill_status')->default('created');
            $table->text('payment_details')->nullable();
            $table->unsignedInteger('supervised_by');
            $table->unsignedInteger('prepared_by');
            $table->date('prepared_date')->useCurrent()->nullable();
            $table->date('send_date')->useCurrent()->nullable();
            $table->date('estimated_payment_date')->nullable();
            $table->string('work_order_id')->nullable();
            $table->string('site_code')->nullable();
            $table->string('bill_submission_number')->nullable();
            $table->date('bill_submission_date')->nullable();
            $table->string('bill_received_number')->nullable();
            $table->date('bill_received_date')->nullable();
            $table->text('subject')->nullable();
            $table->string('bill_format')->nullable();
            $table->text('msg1')->nullable();
            $table->text('msg2')->nullable();
            $table->text('to')->nullable();
            $table->text('from')->nullable();
            $table->string('uom')->nullable();
            $table->unsignedFloat('vat')->nullable();
            $table->unsignedFloat('total_cheque_amount')->nullable();
            $table->unsignedFloat('total_bill_amount')->nullable();
            $table->string('attached')->nullable();
            $table->unsignedTinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_bills');
    }
}
