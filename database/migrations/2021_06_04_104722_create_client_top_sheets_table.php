<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientTopSheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_top_sheets', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('client_bill_id');
            $table->unsignedInteger('project_id');
            $table->unsignedInteger('stage_id')->nullable();
            $table->unsignedDouble('amount');
            $table->unsignedDouble('vat')->default(0);
            $table->unsignedDouble('tax')->default(0);
            $table->unsignedDouble('sd')->default(0);
            $table->unsignedDouble('deduction')->default(0);
            $table->unsignedDouble('cheque_amount')->default(0);
            $table->text('remarks')->nullable();
            $table->text('details')->nullable();
            $table->string('status')->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_top_sheets');
    }
}
