<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseRequisitionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_requisitions', function (Blueprint $table) {
          $table->id();
          $table->string('prn');
          $table->date('requisition_date');
          $table->date('delivery_date')->nullable();
          $table->string('requisition_by')->nullable();
          $table->unsignedTinyInteger('requisition_type');
          $table->unsignedBigInteger('project_id')->nullable();
          $table->unsignedBigInteger('warehouse_id')->nullable();
          $table->string('status')->nullable();
          $table->text('note')->nullable();
          $table->unsignedBigInteger('checked_by')->nullable();
          $table->date('checked_at')->nullable();
          $table->unsignedBigInteger('approved_by')->nullable();
          $table->date('approved_at')->nullable();
          $table->unsignedBigInteger('created_by');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_requisitions');
    }
}
