<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubcontractorBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subcontractor_bills', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('subcontractor_id')->nullable();
            $table->string('bill_id')->unique();
            $table->string('payment_send_number')->nullable();
            $table->date('payment_date')->nullable();
            $table->string('bill_status')->default('received');
            $table->unsignedInteger('received_by');
            $table->date('entry_date')->useCurrent()->nullable();
            $table->unsignedInteger('checked_by')->nullable();
            $table->date('checked_date')->useCurrent()->nullable();
            $table->unsignedInteger('approved_by')->nullable();
            $table->date('approved_date')->useCurrent()->nullable();
            $table->string('submited_by');
            $table->string('submited_by_phone_number');
            $table->unsignedFloat('total_receive_amount')->default(0);
            $table->unsignedFloat('total_approve_amount')->default(0);
            $table->unsignedTinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subcontractor_bills');
    }
}
