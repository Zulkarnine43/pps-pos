<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            // $table->string('work_order_id')->nullable();
            $table->string('site_code')->unique()->nullable();
            $table->integer('project_amount')->nullable();
            // $table->float('agreement_amount')->nullable();
            // $table->float('project_other_expense')->nullable(0);
            // $table->unsignedInteger('project_type');
            // $table->unsignedInteger('client_id');
            $table->unsignedInteger('country_id')->nullable();
            $table->unsignedInteger('state_id')->nullable();
            $table->unsignedInteger('district_id')->nullable();
            $table->unsignedInteger('city_id')->nullable();
            // $table->unsignedInteger('quantity')->default(1);
            // $table->unsignedDouble('unit_price')->default(0);
            $table->text('location')->nullable();
            // $table->text('bn_address')->nullable();
            // $table->text('gps')->nullable();
            $table->text('details')->nullable();
            $table->unsignedInteger('coordinator')->comment('Users table ID')->nullable();
            // $table->unsignedInteger('stage_id')->nullable();
            $table->date('start_date')->nullable();
            $table->date('deadline')->nullable();
            $table->date('finished_date')->nullable();
            $table->date('approved_date')->nullable();
            // $table->date('signboard_collection_date')->nullable();
            // $table->string('signboard_collected_by')->nullable();
            // $table->text('remark')->nullable();
            // $table->date('power_of_attorney')->nullable();
            $table->unsignedInteger('project_status_id')->default(1);
            $table->unsignedInteger('prepaired_by');
            // $table->unsignedInteger('project_dirasa_id')->nullable();
            // $table->unsignedInteger('project_requisition_id')->nullable();
            // $table->unsignedInteger('new_project_assign_id')->nullable();
            // $table->unsignedInteger('file_number')->nullable();
            // $table->unsignedInteger('balance_amount', 16,4)->nullable();
            // $table->string('sd_date')->nullable();
            $table->unsignedTinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
