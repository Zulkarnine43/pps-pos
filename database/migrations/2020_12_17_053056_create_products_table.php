<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('brand_id');
            $table->unsignedInteger('unit_id');
            $table->string('name');
            $table->string('code')->unique()->nullable();
            $table->string('sku')->unique()->nullable();
            $table->unsignedDouble('cost',16,2)->default(0);
            $table->unsignedDouble('price', 16.2)->default(0);
            $table->unsignedDouble('promotion_price', 16,2)->default(0);
            $table->string('alert_quantity')->nullable();
            $table->string('image')->nullable();
            $table->string('description')->nullable();
            $table->unsignedTinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
