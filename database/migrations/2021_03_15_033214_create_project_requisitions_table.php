<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectRequisitionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_requisitions', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('prs_number')->unique();
            $table->string('phone');
            $table->string('requisition_date');
            $table->string('nid_number');
            $table->unsignedInteger('country_id')->default(19);
            $table->unsignedInteger('state_id')->nullable();
            $table->unsignedInteger('district_id')->nullable();
            $table->unsignedInteger('city_id')->nullable();
            $table->string('location')->nullable();
            $table->string('bn_address')->nullable();
            $table->string('gps')->nullable();
            $table->string('site_code')->nullable();
            $table->unsignedInteger('project_type_id');
            $table->unsignedInteger('requisition_by');
            $table->unsignedInteger('quantity')->default(1);
            $table->text('details')->nullable();
            $table->text('remark')->nullable();
            $table->string('status')->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_requisitions');
    }
}
