<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deliveries', function (Blueprint $table) {
            $table->id();
            $table->date('delivery_date');
            $table->unsignedInteger('created_by');
            $table->string('gatepass_number')->nullable();
            $table->unsignedTinyInteger('delivery_type');
            $table->unsignedInteger('from');
            $table->unsignedInteger('to');
            $table->unsignedTinyInteger('status')->default(0);
            $table->string('delivered_by')->nullable();
            $table->string('delivered_by_phone')->nullable();
            $table->text('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deliveries');
    }
}
