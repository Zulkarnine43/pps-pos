<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_order_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('work_order_id');
            $table->unsignedInteger('project_id')->nullable();
            $table->unsignedInteger('project_stage_id')->nullable();
            $table->unsignedFloat('work_order_amount')->default(0);
            $table->unsignedFloat('quantity')->default(0);
            $table->unsignedFloat('unit_price')->default(0);
            $table->text('remarks')->nullable();
            $table->text('details')->nullable();
            $table->string('status')->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_order_items');
    }
}
