<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewProjectAssignItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_project_assign_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('new_project_assign_id');
            $table->unsignedInteger('project_id')->nullable();
            $table->unsignedInteger('client_id');
            $table->unsignedInteger('project_type_id');
            $table->text('details')->nullable();
            $table->string('remark')->nullable();
            $table->unsignedInteger('quantity')->default(1);
            $table->unsignedTinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_project_assign_items');
    }
}
