<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateWorkOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('work_orders', function (Blueprint $table) {
            $table->text('warranty_terms')->nullable();
            $table->text('delay_terms')->nullable();
            $table->text('other_terms')->nullable();
            $table->text('scope_of_work')->nullable();
            $table->text('service_scope')->nullable();
            $table->text('equipment_scope')->nullable();
            $table->text('contract_referance')->nullable();
            $table->renameColumn('finished_date', 'po_issuance_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('work_orders', function (Blueprint $table) {
            $table->renameColumn('po_issuance_date', 'finished_date')->nullable();
            $table->dropColumn(['warranty_terms', 'delay_terms' , 'other_terms', 'scope_of_work' ,'service_scope' ,'equipment_scope' ,'contract_referance']);
        });
    }
}
