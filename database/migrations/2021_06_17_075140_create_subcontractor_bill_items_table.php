<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubcontractorBillItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subcontractor_bill_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('subcontractor_bill_id');
            $table->unsignedInteger('project_id');
            $table->unsignedInteger('work_order_id');
            $table->unsignedInteger('stage_id')->nullable();
            $table->unsignedInteger('project_subcontract_id')->nullable();
            $table->unsignedDouble('amount')->default(0);
            $table->unsignedDouble('sd')->default(0);
            $table->unsignedDouble('deduction')->default(0);
            $table->unsignedDouble('cheque_amount')->default(0);
            $table->text('payment_details')->nullable();
            $table->text('remarks')->nullable();
            $table->string('status')->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subcontractor_bill_items');
    }
}
