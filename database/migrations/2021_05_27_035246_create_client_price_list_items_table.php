<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientPriceListItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_price_list_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('client_price_list_id');
            $table->unsignedInteger('product_id');
            $table->unsignedFloat('price')->default(0);
            $table->unsignedFloat('quantity')->default(1);
            $table->unsignedDouble('discount')->default(0);
            $table->unsignedDouble('total')->default(0);
            $table->unsignedTinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_price_list_items');
    }
}
