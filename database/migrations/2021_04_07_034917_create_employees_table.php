<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('nid_number')->unique();
            $table->string('email')->unique();
            $table->string('phone')->unique();
            $table->string('employee_id')->unique()->nullable();
            $table->string('office_phone')->unique()->nullable();
            $table->string('emergency_phone')->nullable();
            $table->string('designation')->nullable();
            $table->string('blood_group')->nullable();
            $table->string('joining_date')->nullable();
            $table->string('gender')->nullable();
            $table->string('image')->nullable();
            $table->string('employee_status')->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
