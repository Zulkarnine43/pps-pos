<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdatePosSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pos_settings', function (Blueprint $table) {
            $table->unsignedInteger('print_paper_size_id')->nullable();
            $table->string('vat_status')->default('excluding_vat');
            $table->string('currency')->default('USD');
            $table->string('sale_id_prefix')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pos_settings', function (Blueprint $table) {
            $table->dropColumn(['print_paper_size_id', 'vat_status', 'currency', 'sale_id_prefix']);
        });
    }
}
