<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsToProjectEstimatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_estimates', function (Blueprint $table) {
          $table->float('purchase_requisition_quantity')->nullable();
          $table->float('purchase_order_quantity')->nullable();
          $table->float('purchase_quantity')->nullable();
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_estimates', function (Blueprint $table) {            
          $table->dropColumn('purchase_requisition_quantity','purchase_order_quantity','purchase_quantity');
        });
    }
}
