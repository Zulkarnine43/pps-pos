<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientPriceListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_price_lists', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('client_id');
            $table->unsignedInteger('user_id');
            $table->date('entry_date');
            $table->date('delivery_date')->nullable();
            $table->unsignedTinyInteger('status')->default(0);
            $table->string('discount_type')->nullable();
            $table->unsignedDouble('discount')->default(0);
            $table->unsignedDouble('shipping_cost')->default(0);
            $table->unsignedDouble('grand_total')->default(0);
            $table->text('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_price_lists');
    }
}
