<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateClientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->dropColumn(['trade_licence', 'TIN', 'BIN', 'NID', 'district']);
            $table->string('tax_number')->nullable();
            $table->string('city', 255)->nullable()->change();
            $table->unsignedInteger('country')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->string('trade_licence')->nullable();
            $table->string('TIN')->nullable();
            $table->string('BIN')->nullable();
            $table->string('NID')->nullable();
            $table->string('country')->nullable();
            $table->unsignedInteger('district')->nullable();
            $table->dropColumn(['tax_number']);
            $table->unsignedInteger('city')->nullable()->change();
        });
    }
}
