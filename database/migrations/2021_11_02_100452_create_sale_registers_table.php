<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSaleRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_registers', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('open_by');
            $table->unsignedInteger('close_by')->nullable();
            $table->timestamp('open_date_time');
            $table->timestamp('close_date_time')->nullable();
            $table->unsignedDouble('opening_balance', 16,2)->default(0);
            $table->unsignedDouble('closing_balance', 16,2)->default(0);
            $table->string('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_registers');
    }
}
