<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJournalItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journal_items', function (Blueprint $table) {
            $table->id();
            $table->string('type');
            $table->unsignedInteger('ledger_id');
            $table->string('ledger_name');
            $table->unsignedInteger('journal_id');
            $table->unsignedDouble('debit_amount')->default(0);
            $table->unsignedDouble('credit_amount')->default(0);
            $table->boolean('cost_center')->default(0);
            $table->unsignedTinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('journal_items');
    }
}
