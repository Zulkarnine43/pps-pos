<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->id();
            $table->date('entry_date');
            $table->unsignedInteger('created_by');
            $table->string('ref_number')->nullable();
            $table->string('warehouse_id');
            $table->string('status')->default('ordered');
            $table->string('supplier_id');
            $table->string('attachment')->nullable();
            $table->float('discount', 16,4)->nullable();
            $table->string('discount_type')->nullable();
            $table->unsignedInteger('shipping_cost', 16,4)->nullable();
            $table->float('grand_total', 16,4);
            $table->text('note')->nullable();
            $table->string('payment_condition')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}
