<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PosSettingResource;
use App\Laravue\Models\PosSetting;
use Illuminate\Http\Request;
use Validator;

class PosSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\PosSetting  $posSetting
     * @return \Illuminate\Http\Response
     */
    public function show(PosSetting $posSetting)
    {
        return new PosSettingResource($posSetting);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\PosSetting  $posSetting
     * @return \Illuminate\Http\Response
     */
    public function edit(PosSetting $posSetting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\PosSetting  $posSetting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PosSetting $posSetting)
    {
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules($isNew = true, $ledger = null),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            $posSetting->warehouse_id = $request->warehouse_id;
            $posSetting->customer_id = $request->customer_id;
            $posSetting->vat = $request->vat;
            $posSetting->tax = $request->tax;
            $posSetting->vat_registration_number = $request->vat_registration_number;
            $posSetting->tax_registration_number = $request->tax_registration_number;
            $posSetting->shop_name = $request->shop_name;
            $posSetting->office_phone = $request->office_phone;
            $posSetting->address = $request->address;
            $posSetting->mushak = $request->mushak;
            $posSetting->bill_footer = $request->bill_footer;
            $posSetting->logo = $request->logo;
            $posSetting->print_paper_size_id = $request->print_paper_size_id;
            $posSetting->vat_status = $request->vat_status;
            $posSetting->currency = $request->currency;
            $posSetting->sale_id_prefix = $request->sale_id_prefix;
            $posSetting->print_page_top_margin = $request->print_page_top_margin;
            $posSetting->print_page_bottom_margin = $request->print_page_bottom_margin;
            $posSetting->print_page_left_margin = $request->print_page_left_margin;
            $posSetting->print_page_right_margin = $request->print_page_right_margin;
            $posSetting->save();

            return new PosSettingResource($posSetting);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\PosSetting  $posSetting
     * @return \Illuminate\Http\Response
     */
    public function destroy(PosSetting $posSetting)
    {
        //
    }

    private function getValidationRules($isNew = true, $ledger)
    {
        return [
            'warehouse_id' => 'required',
            'customer_id' => 'required',
            'vat' => 'required',
            'tax' => 'required',
        ];
    }
}
