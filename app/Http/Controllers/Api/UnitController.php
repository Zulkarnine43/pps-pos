<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UnitResource;
use App\Laravue\Models\Unit;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Validator;

class UnitController extends Controller
{
    const ITEM_PER_PAGE = 100;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $unitQuery = Unit::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        // search query
        if (!empty($keyword)) {
            $unitQuery->where('name', 'LIKE', '%' . $keyword . '%');
        }

        return UnitResource::collection($unitQuery->orderBy('id', 'desc')->paginate($limit));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  get validation rules from getValidationRules method
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {

            // create new unit

            $unit = Unit::create([
                'name' => $request->name,
                'code' => $request->code,
            ]);
            return new UnitResource($unit);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function show(Unit $unit)
    {
        return new UnitResource($unit);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function edit(Unit $unit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Unit $unit)
    {
        // check requested unit
        if ($unit === null) {
            return response()->json(['error' => 'Unit not found'], 404);
        }

        $validator = Validator::make($request->all(), $this->getValidationRules(false));
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            //  update unit data
            $unit->name = $request->get('name');
            $unit->code = $request->get('code');
            $unit->save();
            return new UnitResource($unit);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Unit $unit)
    {
        //
    }

    /**
     * unmute unit.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	public
     * @param	request	$request
     * @param	unit   	$unit
     * @return	mixed
     */
    public function unmute(Request $request, Unit $unit){
        if ($unit === null) {
            return response()->json(['error' => 'unit not found'], 404);
        }

        $unit->status = 0;

        $unit->save();
        return new UnitResource($unit);
    }

    /**
     * mute unit.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	public
     * @param	request	$request
     * @param	unit   	$unit
     * @return	mixed
     */
    public function mute(Request $request, Unit $unit){
        if ($unit === null) {
            return response()->json(['error' => 'unit not found'], 404);
        }
        $unit->status = 1;

        $unit->save();
        return new UnitResource($unit);
    }

    /**
     * getValidationRules.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	private
     * @param	boolean	$isNew	Default: true
     * @return	array
     */
    private function getValidationRules($isNew = true)
    {
        return [
            'name' => 'required',
            'code' => $isNew ? 'nullable|unique:units' : 'nullable',
        ];
    }
}
