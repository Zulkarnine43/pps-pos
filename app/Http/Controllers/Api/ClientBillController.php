<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Laravue\Models\ClientBill;
use Illuminate\Support\Arr;
use App\Http\Resources\ClientBillResource;
use App\Http\Resources\ProjectStageResource;
use App\Laravue\Models\ClientTopSheet;
use App\Laravue\Models\ProjectStage;
use Illuminate\Http\Request;
use Validator;
use Auth;
use Illuminate\Support\Str;

class ClientBillController extends Controller
{
    const ITEM_PER_PAGE = 100;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $clientBillQuery = ClientBill::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        $client_id = Arr::get($searchParams, 'client_id', '');
        
        $bill_status = Arr::get($searchParams, 'bill_status', '');
        $date = Arr::get($searchParams, 'date', '');
        // search query
        if (!empty($keyword)) {
            $clientBillQuery->where('bill_id', 'LIKE', '%' . $keyword . '%')
                            ->orWhere('bill_submission_number','LIKE', '%' . $keyword . '%')
                            ->orWhere('bill_received_number', 'LIKE', '%' . $keyword . '%');
        }
        if (!empty($client_id)) {
            $clientBillQuery->where('client_id', $client_id);
        }

        if (!empty($date)) {
            $clientBillQuery->where('prepared_date', 'LIKE', '%' . $date . '%')
                            ->orwhere('send_date', 'LIKE', '%' . $date . '%')
                            ->orwhere('estimated_payment_date', 'LIKE', '%' . $date . '%')
                            ->orwhere('bill_submission_date', 'LIKE', '%' . $date . '%')
                            ->orwhere('bill_received_date', 'LIKE', '%' . $date . '%');
        }
        if (!empty($bill_status)) {
            if($bill_status !== 'all'){
                $clientBillQuery->where('bill_status', $bill_status);
            }
        }

        return ClientBillResource::collection($clientBillQuery->orderBy('id', 'DESC')->paginate($limit));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {          
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            $bill = new ClientBill();
            $bill->bill_id  = $request->bill_id;
            $bill->client_id = $request->client_id;
            $bill->prepared_date = Str::replaceFirst('date: ', '', $request->prepared_date);
            $bill->send_date = Str::replaceFirst('date: ', '', $request->send_date);
            $bill->prepared_by = Auth::id();
            $this->saveBill($request, $bill);
        }
    }

    public function receiveBill(Request $request){
        $bills = $request->bills;
        foreach($bills as $bill){
            $id = $bill['id'];
            $b = ClientBill::find($id);
            $b->payment_date = $request->payment_date;
            $b->sd = $bill['sd'];
            $b->vat = $bill['vat'];
            $b->tax = $bill['tax'];
            $b->deduction = $bill['deduction'];
            $b->cheque_amount = $bill['cheque_amount'];
            $b->bill_status = 'received';
            $b->save();
        }

        return new ClientBillResource($b);
    }

    public function saveBill($request, $bill){
            
        $bill->work_order_id = $request->work_order_id;
        $bill->supervised_by = $request->supervised_by;
        $bill->payment_details = $request->payment_details;
        $bill->bill_received_number = $request->bill_received_number;
        $bill->subject = $request->subject;
        $bill->bill_format = $request->bill_format;
        $bill->msg1 = $request->msg1;
        $bill->msg2 = $request->msg2;
        $bill->vat = $request->vat;
        $bill->uom = $request->uom;
        $bill->from = $request->from;
        $bill->to = $request->to;
        $bill->total_bill_amount = $request->total_bill_amount;
        $bill->total_cheque_amount = $request->total_cheque_amount;
        $bill->attached = $request->attached;
        $bill->site_code = $request->site_code;
        if(count($request->BillRows) > 0){
            $bill->save();
            $bills = $request->BillRows;
            foreach($bills as $item){
                if(isset($item['id'])){
                    $oldCts = ClientTopSheet::find($item['id']);
                    $this->saveClientTopSheet($oldCts, $item, $request);
                } else {
                    $cts = new ClientTopSheet();
                    $cts->client_bill_id = $bill->id;
                    $cts->project_id = $item['project_id'];
                    $cts->stage_id = $item['stage_id'];
                    $this->saveClientTopSheet($cts, $item, $request);
                }
                
            }
        }
        
        return new ClientBillResource($bill);
    }

    public function saveClientTopSheet($cts,$item, $request){
        $cts->amount = $item['amount'];
        if(isset($request->vat)){
            $cts->vat = (($item['amount'] * $request->vat)/100);
        } else {
            $cts->vat = $item['vat'];
        }
        $cts->tax = $item['tax'];
        $cts->sd = $item['sd'];
        $cts->deduction = $item['deduction'];
        $cts->cheque_amount = $item['cheque_amount'];
        $cts->remarks = $item['remarks'];
        $cts->details = $item['details'];
        $cts->save();

        $this->updateProjectStage($cts);
    }

    public function updateProjectStage($cts){
        $totalBillAmount = 0;
        $projectStage = ProjectStage::where('id', $cts->stage_id)->with('clientBill')->first();
        if($projectStage){
            foreach($projectStage->clientBill as $bill){
                $totalBillAmount += $bill->amount;
            };
            $projectStage->bill_status = 'done';
            if($projectStage->stage_amount > $totalBillAmount){
                $projectStage->bill_status = 'partial';
            }
            $projectStage->save();
        };
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\ClientBill  $clientBill
     * @return \Illuminate\Http\Response
     */
    public function show(ClientBill $clientBill)
    {
        return new ClientBillResource($clientBill);
    }


    public function saveDraft(Request $request){
        $bill = new ClientBill();
        $bill->bill_status = 'draft';
        $bill->bill_id  = $request->bill_id;
        $bill->client_id = $request->client_id;
        $bill->prepared_date = Str::replaceFirst('date: ', '', $request->prepared_date);
        $bill->send_date = Str::replaceFirst('date: ', '', $request->send_date);
        $bill->prepared_by = Auth::id();
        $this->saveBill($request, $bill);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\ClientBill  $clientBill
     * @return \Illuminate\Http\Response
     */
    public function edit(ClientBill $clientBill)
    {
        //
    }

    public function getReadyForBill(){
        $stageBills = ProjectStage::whereNotNull('finished_date')->where('bill_status', '!=','done')->get();
        return ProjectStageResource::collection($stageBills);
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\ClientBill  $clientBill
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClientBill $clientBill)
    {
        if ($clientBill === null) {
            return response()->json(['error' => 'Client Bill not found'], 404);
        } else {

            if($clientBill->bill_status === 'draft'){
                if($request->mood && $request->mood === 'edit'){
                    $clientBill->bill_status = 'created';
                }
            }
            
            if($request->mood && $request->mood === 'receive'){
                $clientBill->bill_status = 'received';
            }
            $this->saveBill($request, $clientBill);
            
            return new ClientBillResource($clientBill);
        }
    }

    public function setBRNumber(Request $request, $id){
        $bill = ClientBill::find($id);
        $bill->bill_received_number = $request->bill_received_number;
        $bill->bill_received_date = $request->bill_received_date;
        $bill->bill_status = 'pending';
        $bill->save();

        return new ClientBillResource($bill);
    }

    public function setBSNumber(Request $request, $id){
        $bill = ClientBill::find($id);
        $bill->bill_submission_number = $request->bill_submission_number;
        $bill->bill_submission_date = $request->bill_submission_date;
        $bill->estimated_payment_date = $request->estimated_payment_date;
        $bill->bill_status = 'pending';
        $bill->save();

        return new ClientBillResource($bill);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\ClientBill  $clientBill
     * @return \Illuminate\Http\Response
     */
    public function destroy(ClientBill $clientBill)
    {
        if($clientBill === null){
            return response()->json(['error' => 'Client bill not found'], 404);
        } else {
            $clientBill->bill_status = 'canceled';
            $clientBill->save();

            return new ClientBillResource($clientBill);
        }
    }

    /**
     * getValidationRules.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	private
     * @param	boolean	$isNew	Default: true
     * @return	array
     */
    private function getValidationRules($isNew = true)
    {
        return [
            'client_id' => 'required',
        ];
    }
}
