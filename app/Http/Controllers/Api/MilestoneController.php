<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\MilestoneResource;
use App\Laravue\Models\Milestone;
use App\Laravue\Models\Wbc;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Validator;

class MilestoneController extends Controller
{
    const ITEM_PER_PAGE = 100;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $milestoneQuery = Milestone::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        // search query
        if (!empty($keyword)) {
            $milestoneQuery->where('name', 'LIKE', '%' . $keyword . '%');
        }

        return MilestoneResource::collection($milestoneQuery->orderBy('id', 'desc')->paginate($limit));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  get validation rules from getValidationRules method
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules($isNew = true, $milestone=null),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {

            // create new Department
            
            $milestone = Milestone::create([
                'name' => $request->name,
                'work_done' => $request->work_done,
            ]);
            $wbcs = $request->wbcDatas;
            foreach($wbcs as $wbc){
                $wb = new Wbc();
                $wb->milestone_id = $milestone->id;
                $wb->name = $wbc['name'];
                $wb->work_done = $wbc['work_done'];
                $wb->save();
            }

            return new MilestoneResource($milestone);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\Milestone  $milestone
     * @return \Illuminate\Http\Response
     */
    public function show(Milestone $milestone)
    {
        return new MilestoneResource($milestone);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\Milestone  $milestone
     * @return \Illuminate\Http\Response
     */
    public function edit(Milestone $milestone)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\Milestone  $milestone
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Milestone $milestone)
    {
        //  get validation rules from getValidationRules method
        if ($milestone === null) {
            return response()->json(['error' => 'Parent Group not found'], 404);
        }

        $validator = Validator::make($request->all(), $this->getValidationRules($isNew = false, $milestone));
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            $milestone->name = $request->name;
            $milestone->work_done = $request->work_done;
            $milestone->save();
            $wbcs = $request->wbcDatas;
            foreach($wbcs as $wbc){
                $wb = Wbc::find($wbc['id']);
                $wb->name = $wbc['name'];
                $wb->work_done = $wbc['work_done'];
                $wb->save();
            }
            return new MilestoneResource($milestone);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\Milestone  $milestone
     * @return \Illuminate\Http\Response
     */
    public function destroy(Milestone $milestone)
    {
        //
    }

    /**
     * getValidationRules.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	private
     * @param	boolean	$isNew	Default: true
     * @return	array
     */
    private function getValidationRules($isNew, $milestone)
    {
        return [
            
            'name' => $isNew ? 'required|unique:milestones' : 'required|unique:milestones,name,'.$milestone->id,
        ];
    }
}
