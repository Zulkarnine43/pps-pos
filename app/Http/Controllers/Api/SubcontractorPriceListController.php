<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Laravue\Models\SubcontractorPriceList;
use App\Laravue\Models\SubcontractorPriceListItems;
use App\Http\Resources\SubcontractorPriceListResource;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Validator;
use Auth;

class SubcontractorPriceListController extends Controller
{
    const ITEM_PER_PAGE = 100;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $splQuery = SubcontractorPriceList::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        // search query
        if (!empty($keyword)) {
            $splQuery->where('subcontractor_id', $keyword);
        }

        return SubcontractorPriceListResource::collection($splQuery->orderBy('id', 'DESC')->paginate($limit));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        // get validation rules from getValidationRules method
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            // create new spl
            $spl = SubcontractorPriceList::create([
                'delivery_date' => $request->delivery_date,
                'user_id' => Auth::id(),
                'subcontractor_id' => $request->subcontractor_id,
                'discount' => $request->discount,
                'shipping_cost' => $request->shipping_cost,
                'note' => $request->note,
                'grand_total' => $request->grandTotal,
                'discount_type' => $request->discount_type,
            ]);
            if(isset($request->entry_date)){
                $spl->entry_date = $request->entry_date;
            }else {
                $spl->entry_date = date("Y-m-d");
            }
            $spl->save();
            // insert SubcontractorPriceList products
            if ($request->products) {
                $products = $request->products;
                if(count($products) > 0){
                    foreach ($products as $product) {
                        if (isset($product)) {
                            $splItem = new SubcontractorPriceListItems();
                            $splItem->subcontractor_price_list_id = $spl->id;
                            $splItem->product_id = $product['id'];
                            $splItem->quantity = $product['quantity'];
                            $splItem->price = $product['price'];
                            $splItem->discount = $product['discount'];
                            $splItem->total = $product['total'];
                            $splItem->save();
                        }
                    }
                }
            }
            return new SubcontractorPriceListResource($spl);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\SubcontractorPriceList  $subcontractorPriceList
     * @return \Illuminate\Http\Response
     */
    public function show(SubcontractorPriceList $subcontractorPriceList)
    {
        return new SubcontractorPriceListResource($subcontractorPriceList);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\SubcontractorPriceList  $subcontractorPriceList
     * @return \Illuminate\Http\Response
     */
    public function edit(SubcontractorPriceList $subcontractorPriceList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\SubcontractorPriceList  $subcontractorPriceList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubcontractorPriceList $subcontractorPriceList)
    {
        if ($subcontractorPriceList === null) {
            return response()->json(['error' => 'subcontractorPriceList not found'], 404);
        }

        $validator = Validator::make($request->all(), $this->getValidationRules(false));
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        }else{
            $subcontractorPriceList->entry_date = $request->get('entry_date');
            $subcontractorPriceList->subcontractor_id = $request->get('subcontractor_id');
            $subcontractorPriceList->discount = $request->get('discount');
            $subcontractorPriceList->shipping_cost = $request->get('shipping_cost');
            $subcontractorPriceList->note = $request->get('note');
            $subcontractorPriceList->grand_total = $request->get('grand_total');
            $subcontractorPriceList->discount_type = $request->get('discount_type');
            // update listItems 
            if ($request->get('listItems')) {
                $listItems = $request->get('listItems');
                if(count($listItems) > 0){
                    foreach ($listItems as $item) {
                        if (isset($item)) {
                            if (isset($item['id'])) {
                                // updating exsisting purchase items
                                $this->PItem = SubcontractorPriceListItems::find($item['id']);
                                $this->PItem->subcontractor_price_list_id = $subcontractorPriceList->id;
                                $this->PItem->product_id = $item['product_id'];
                                $this->PItem->quantity = $item['quantity'];
                                $this->PItem->price = $item['price'];
                                $this->PItem->discount = $item['discount'];
                                $this->PItem->total = $item['total'];
                                $this->PItem->save();
                            } else {
                                // insert new purchase item
                                $this->PItem = new SubcontractorPriceListItems();
                                $this->PItem->subcontractor_price_list_id = $subcontractorPriceList->id;
                                $this->PItem->product_id = $item['product']['id'];
                                $this->PItem->quantity = $item['quantity'];
                                $this->PItem->price = $item['product']['price'];
                                $this->PItem->discount = $item['discount'];
                                $this->PItem->total = $item['total'];
                                $this->PItem->save();
                            }
                        }
                    }
                }
            }
            $subcontractorPriceList->save();
            return new SubcontractorPriceListResource($subcontractorPriceList);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\SubcontractorPriceList  $subcontractorPriceList
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubcontractorPriceList $subcontractorPriceList)
    {
        //
    }

    private function getValidationRules($isNew = true)
    {
        return [
            'entry_date' => 'nullable|date',
            'delivery_date' => 'nullable|date',
            'subcontractor_id' => 'required',
        ];
    }
}
