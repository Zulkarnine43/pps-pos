<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\SaleRegisterResource;
use App\Laravue\Models\SaleRegister;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Auth;
use Carbon\Carbon;

class SaleRegisterController extends Controller
{
    const ITEM_PER_PAGE = 100;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $stateQuery = SaleRegister::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        // search query

        return SaleRegisterResource::collection($stateQuery->with('openByUser', 'closeByUser')->orderBy('id', 'desc')->paginate($limit));
    }

    public function checkOpenedRegister(){
        $saleRegister = SaleRegister::orderBy('id', 'desc')->whereNull('close_by')->first();

        return new SaleRegisterResource($saleRegister);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'opening_balance' => 'required|numeric',
        ]);
        
        $register = new SaleRegister();
        $register->opening_balance = $validated['opening_balance'];
        $register->open_by = Auth::id();
        $register->open_date_time = Carbon::now()->toDateTimeLocalString();
        $register->save();

        return new SaleRegisterResource($register);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\SaleRegister  $saleRegister
     * @return \Illuminate\Http\Response
     */
    public function show(SaleRegister $saleRegister)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\SaleRegister  $saleRegister
     * @return \Illuminate\Http\Response
     */
    public function edit(SaleRegister $saleRegister)
    {
        //
    }

    public function closeRegister(Request $request){
        $validated = $request->validate([
            'closing_balance' => 'required|numeric',
        ]);
        $saleRegister = SaleRegister::orderBy('id', 'desc')->where('close_by', null)->first();
        $saleRegister->closing_balance = $request->closing_balance;
        $saleRegister->note = $request->note;
        $saleRegister->close_by = Auth::id();
        $saleRegister->close_date_time = Carbon::now()->toDateTimeLocalString();
        $saleRegister->save();

        return new SaleRegisterResource($saleRegister);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\SaleRegister  $saleRegister
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SaleRegister $saleRegister)
    {
        $validated = $request->validate([
            'closing_balance' => 'required|numeric',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\SaleRegister  $saleRegister
     * @return \Illuminate\Http\Response
     */
    public function destroy(SaleRegister $saleRegister)
    {
        //
    }

    private function getValidationRules($isNew = true)
    {
        return [
            'opening_balance' => 'required',
        ];
    }
}
