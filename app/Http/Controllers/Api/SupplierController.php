<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\SupplierResource;
use App\Laravue\Models\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Validator;
use File;
use Illuminate\Support\Facades\Storage;

class SupplierController extends Controller
{


    const ITEM_PER_PAGE = 100;
 

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response|ResourceCollection
     */

    public function index(Request $request)
    {
        $searchParams = $request->all();
        $supplierQuery = Supplier::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        // search query
        if (!empty($keyword)) {
            $supplierQuery->where('name', 'LIKE', '%' . $keyword . '%');
            $supplierQuery->where('email', 'LIKE', '%' . $keyword . '%');
        }

        return SupplierResource::collection($supplierQuery->orderBy('id', 'desc')->paginate($limit));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // get active supplier
    public function isActive()
    {
        $suppliers = Supplier::where('is_active', 'active')->get();
        return response()->json(['suppliers' => $suppliers]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function getImage(Request $request){
       if($request->file('photo')){
        $file_name = time().'-'.$request->file('photo')->getClientOriginalName();
        $request->file('photo')->storePubliclyAs(
            'suppliers/images',
            $file_name,
            's3'
        );
        return response()->json(['imageName' => $file_name]);
       }
    }



    public function tradeLicence(Request $request){
        if($request->file('photo')){
        $file_name = time().'-'.$request->file('photo')->getClientOriginalName();
        $request->file('photo')->storePubliclyAs(
            'suppliers/trade',
            $file_name,
            's3'
        );
        return response()->json(['tradeLicence' => $file_name]);
    }
    }

    public function getTin(Request $request){
        if($request->file('photo')){
        $file_name = time().'-'.$request->file('photo')->getClientOriginalName();
        $request->file('photo')->storePubliclyAs(
            'suppliers/tin',
            $file_name,
            's3'
            
        );
        return response()->json(['tin' => $file_name]);
       }  
    }

    public function getBin(Request $request){
        if($request->file('photo')){
        $file_name = time().'-'.$request->file('photo')->getClientOriginalName();
        $request->file('photo')->storePubliclyAs(
            'suppliers/bin',
            $file_name,
            's3'
        );
        return response()->json(['bin' => $file_name]);
      }
    }

    public function getNid(Request $request){

        if($request->file('photo')){
        $file_name = time().'-'.$request->file('photo')->getClientOriginalName();
        $request->file('photo')->storePubliclyAs(
            'suppliers/nid',
             $file_name,
             's3'
        );
        return response()->json(['nid' => $file_name]);
       }
    }


    public function store(Request $request)
    {

        // get validation rules from getValidationRules method
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {

            if($request->image){
                $image = 'suppliers/images/'.$request->image;
            }
            else {
                $image = '';
            }
            
            if($request->trade_licence){
                $trade = 'suppliers/trade/'.$request->trade_licence;
            }
            else {
                $trade = '';
            }
            if($request->TIN){
                $tin = 'suppliers/tin/'.$request->TIN;
            }
            else {
                $tin = '';
            }
            if($request->BIN){
                $bin = 'suppliers/bin/'.$request->BIN;
            }
            else {
                $bin ='';
            }
            if($request->NID){
               $nid = 'suppliers/bin/'.$request->NID;
            }
            else {
                $nid  = '';
            }

            // create new supplier
            $supplier = Supplier::create([
                'name' => $request->name,
                'company' => $request->company,
                'email' => $request->email,
                'phone' => $request->phone,
                'country' => $request->country,
                'state' => $request->state,
                'district' => $request->district,
                'city' => $request->city,
                'zip_code' => $request->zip_code,
                'street' => $request->street,
                'image' => $image,
                'trade_licence' => $trade,
                'TIN' => $tin,
                'BIN' => $bin,
                'NID' => $nid,
            ]);
            return new SupplierResource($supplier);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  Supplier $supplier
     * @return SupplierResource|\Illuminate\Http\JsonResponse
     */
    public function show(Supplier $supplier)
    {
        return new SupplierResource($supplier);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function edit(Supplier $supplier)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */

 
    public function update(Request $request, Supplier $supplier)
    {
        $supplier_image = $supplier->image;
       if($request->image){
        if($supplier_image){

            Storage::disk('s3')->delete("/suppliers/images/".$supplier_image);
        }
       }
        $trade = $supplier->trade_licencence;
       if($request->trade_licence){
        if($trade){

            Storage::disk('s3')->delete("/suppliers/trade/".$trade);
        }
       }
        $tin = $supplier->TIN;
       if($request->TIN){
            if($tin){

                Storage::disk('s3')->delete("/suppliers/tin/".$tin);
            }
       }

        $bin = $supplier->BIN;
        if($request->BIN){
            if($bin){

                Storage::disk('s3')->delete("/suppliers/bin/".$bin);
            }
        }

        $nid = $supplier->NID;
        if($request->NID){
            if($nid){

                Storage::disk('s3')->delete("/suppliers/nid/".$nid);
            }
        }
        
        // check requested supplier
        if ($supplier === null) {
            return response()->json(['error' => 'Supplier not found'], 404);
        }


        $validator = Validator::make($request->all(), $this->getValidationRules(false));
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {

            

            $supplier->name = $request->get('name');
            $supplier->phone = $request->get('phone');
            $supplier->company = $request->get('company');
            $supplier->email = $request->get('email');
            $supplier->country = $request->get('country');
            $supplier->state = $request->get('state');
            $supplier->district = $request->get('district');
            $supplier->zip_code = $request->get('zip_code');
            $supplier->city = $request->get('city');
            $supplier->street = $request->get('street');
            if($request->image){$supplier->image ='suppliers/images/'.$request->get('image');}
            if($request->trade_licence){$supplier->trade_licence = 'suppliers/trade/'.$request->get('trade_licence');}
            if($request->TIN){$supplier->TIN = 'suppliers/tin/'.$request->get('TIN');}
            if($request->BIN){$supplier->BIN = 'suppliers/bin/'.$request->get('BIN');}
            if($request->NID){$supplier->NID = 'suppliers/nid/'.$request->get('NID');}
            $supplier->save();
            return new SupplierResource($supplier);
        }
    }

    // block subcontractor
    public function inActive(Request $request, Supplier $supplier){
        if ($supplier === null) {
            return response()->json(['error' => 'Supplier not found'], 404);
        }

        $supplier->is_active = 'inactive';

        $supplier->save();
        return new SupplierResource($supplier);
    }

    // unblock subcontractor
    public function active(Request $request, Supplier $supplier){
        if ($supplier === null) {
            return response()->json(['error' => 'Supplier not found'], 404);
        }

        $supplier->is_active = 'active';

        $supplier->save();
        return new SupplierResource($supplier);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function destroy(Supplier $supplier)
    {
        //
    }

    /**
     * getValidationRules.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	private
     * @param	boolean	$isNew	Default: true
     * @return	array
     */
    private function getValidationRules($isNew = true)
    {
        return [
            'name' => 'required',
            'email' => $isNew ? 'nullable|email|unique:suppliers' : 'nullable|email',
            'phone' => $isNew ? 'required|unique:suppliers' : 'required',
        ];
    }
}
