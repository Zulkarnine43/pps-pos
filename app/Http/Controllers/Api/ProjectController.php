<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProjectResource;
use App\Http\Resources\ProjectTypeEstimateResource;
use App\Laravue\Models\Milestone;
use App\Laravue\Models\Product;
use App\Laravue\Models\Project;
use App\Laravue\Models\ProjectFile;
use App\Laravue\Models\ProjectEstimate;
use App\Laravue\Models\ProjectMeta;
use App\Laravue\Models\ProjectMilestone;
use App\Laravue\Models\ProjectStage;
use App\Laravue\Models\ProjectTypeEstimate;
use App\Laravue\Models\ProjectTypeMeta;
use App\Laravue\Models\ProjectWbc;
use App\Laravue\Models\TypeStage;
use App\Laravue\Models\Zone;
use App\Laravue\Models\ZoneMilestone;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Validator;
use Auth;
use DateTime;
use Illuminate\Support\Facades\Storage;

class ProjectController extends Controller
{

    const ITEM_PER_PAGE = 100;
    public $current_milestone_id;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $projectQuery = Project::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        $user = Arr::get($searchParams, 'user', '');
        $status = Arr::get($searchParams, 'project_status_id', '');
        $start_date = Arr::get($searchParams, 'start_date', '');
        $zone_id = Arr::get($searchParams, 'zone_id', '');
        
        // search query
        if (!empty($keyword)) {
            $projectQuery
            ->where('name', 'LIKE', '%' . $keyword . '%')
            ->orwhere('location', 'LIKE', '%' . $keyword . '%')
            ->orwhere('site_code', 'LIKE', '%' . $keyword . '%');
        }
        // search according to user
        if (!empty($user)) {
            $projectQuery->where('coordinator', $user );
        }
        if (!empty($status)) {
            $projectQuery->where('project_status_id', $status );
        }
        if (!empty($zone_id)) {
            $projectQuery->where('zone_id', $zone_id );
        }

        if (!empty($start_date)) {
            $projectQuery->where('start_date', $start_date );
        }
        return ProjectResource::collection($projectQuery->orderBy('id', 'desc')->paginate($limit));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // check validation rules from getValidationRules method
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        }else{
            // creating new project
            $project = new Project();
            $project->coordinator = Auth::id();
            $project->country_id = $request->country_id;
            $project->state_id = $request->state_id;
            $project->city_id = $request->city_id;
            $project->district_id = $request->district_id;
            $project->location = $request->location;
            $project->details = $request->details;
            $project->site_code = $request->site_code;
            $project->name = $request->name;
            $project->zone_id = $request->zone_id;
            $project->deadline = $request->deadline;
            $project->start_date = $request->start_date;
            $project->approved_date = $request->approved_date;
            $project->project_amount = $request->project_amount;
            // $project->current_milestone_id = 1;
            $project->prepaired_by = Auth::id();
            if(isset($request->project_status_id)){
                $project->project_status_id = $request->project_status_id;
            }

            $project->save();

            /**
             * set project milestone form project zone method for set project estimate.
             */
            $this->setProjectMilestone($project); 



            $defaultMilestone = ProjectMilestone::where('project_id', $project->id)->first();
            if (isset($defaultMilestone)) {
                $project->current_milestone_id = $defaultMilestone->id;
                $project->save();
            }

            return new ProjectResource($project);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function setProjectMilestone($project){
        $zone = Zone::where('id', $project->zone_id)->first();
        $milestones = ZoneMilestone::where('zone_id', $zone->id)->with('milestone')->get();
        if(count($milestones) > 0){
            foreach ($milestones as $milestone) {
                $projectMilestone = new ProjectMilestone();
                $projectMilestone->project_id = $project->id;
                $projectMilestone->milestone_name = $milestone->milestone->name;
                $projectMilestone->work_done = $milestone->work_done;
                $projectMilestone->amount = 0;
                $projectMilestone->start_date = '';
                $projectMilestone->deadline = '';
                $projectMilestone->finish_date = '';
                $projectMilestone->save();
                $this->setMilestoneWbc($projectMilestone, $milestone);
            }
        }
    }

    public function setMilestoneWbc($projectMilestone, $milestone){
        $milestone = Milestone::where('id', $milestone->milestone->id)->with('wbcDatas')->first();
        foreach($milestone->wbcDatas as $wbc){
            $projectWbc = new ProjectWbc();
            $projectWbc->project_milestone_id = $projectMilestone->id;
            $projectWbc->project_id = $projectMilestone->project_id;
            $projectWbc->wbc_name = $wbc->name;
            $projectWbc->work_done = $wbc->work_done;
            $projectWbc->amount = 0;
            $projectWbc->start_date = '';
            $projectWbc->deadline = '';
            $projectWbc->finish_date = '';
            $projectWbc->save();
        };
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        // call project type meta for showing project type metas
        return new ProjectResource($project);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {

        // get validation rules from getValidationRules method
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules($project,$isNew = false),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        }else{
            // updating project data
            $project->state_id = $request->state_id;
            $project->city_id = $request->city_id;
            $project->district_id = $request->district_id;
            $project->location = $request->location;
            $project->details = $request->details;
            $project->name = $request->name;
            $project->deadline = $request->deadline;
            $project->start_date = $request->start_date;
            $project->approved_date = $request->approved_date;
            $project->project_amount = $request->project_amount;
            // $project->current_milestone_id = 1;
            if(isset($request->project_status_id)){
                $project->project_status_id = $request->project_status_id;
            }

            $project->save();
            return new ProjectResource($project);
        }

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        //
    }


    /**
     * getValidationRules.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	private
     * @param	boolean	$isNew	Default: true
     * @return	array
     */
    private function getValidationRules($project = null,$isNew = true)
    {
        return [
            'name' => 'nullable|string',
            'coordinator' => 'nullable|numeric',
            'project_amount' => 'nullable',
            'country_id' => 'nullable|numeric',
            'state_id' => 'nullable|numeric',
            'district_id' => 'nullable|numeric',
            'city_id' => 'nullable|numeric',
            'start_date' => 'nullable',
            'deadline' => 'nullable',
            'site_code' => $isNew ? 'nullable|unique:projects' : 'nullable|unique:projects,site_code,'.$project->id,
        ];
    }
}
