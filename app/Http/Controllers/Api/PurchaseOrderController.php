<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PurchaseOrderItemResource;
use App\Http\Resources\PurchaseOrderResource;
use App\Http\Resources\PurchaseRequisitionResource;
use App\Laravue\Models\PurchaseOrder;
use App\Laravue\Models\PurchaseOrderItem;
use App\Laravue\Models\PurchaseRequisition;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Validator;
use Auth;
use DateTime;
use Exception;

class PurchaseOrderController extends Controller
{
    const ITEM_PER_PAGE = 100;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
      $searchParams = $request->all();
      $purchaseOrderQuery = PurchaseOrder::query();
      // $purchaseOrderQuery = PurchaseOrder::with('fromWarehouse','fromProject','toWarehouse','toProject')->get();
      // $purchaseOrderQuery = PurchaseOrder::with('fromWarehouse','fromProject','toWarehouse','toProject','purchaseOrderItems.product');

      // $purchaseOrderQuery = PurchaseOrder::with('fromInventory','toInventory')->get();
      // $purchaseOrderQuery = PurchaseOrder::with('fromInventory')->get();
      // dd($purchaseOrderQuery);
      $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
      $keyword = Arr::get($searchParams, 'keyword', '');
      $date = Arr::get($searchParams, 'date', '');
      
      if (!empty($keyword)) {
          $purchaseOrderQuery->where('gatepass_number', 'LIKE', '%' . $keyword . '%');
      }
      if (!empty($date)) {
          $purchaseOrderQuery->where('purchaseOrder_date', 'LIKE', '%' . $date . '%');
      }

      return PurchaseOrderResource::collection($purchaseOrderQuery->orderBy('id', 'asc')->paginate($limit));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // try{

      // } catch(Exception $ex){

      // }
      //  get validation rules from getValidationRules method
      $validator = Validator::make(
        $request->all(),
        array_merge(
          $this->getValidationRules($isNew = true, $purchaseOrder = null),
        )
      );
      // dd('hi', $request->ItemRows, $request->all());

      if ($validator->fails()) {
        return response()->json(['errors' => $validator->errors()], 403);
      } else {
        // dd($request->all());
        $purchaseOrder = new PurchaseOrder();
        $purchaseOrder->created_by = Auth::id();
        $purchaseOrder->status = 'created';
        $this->savePurchaseOrderData($purchaseOrder, $request);
        return new PurchaseOrderItemResource($purchaseOrder);
      }
    }


    public function savePurchaseOrderData($purchaseOrder, $request)
    {
      $purchaseOrder->pon = $request->pon;
      $purchaseOrder->purchase_requisition_id = $request->purchase_requisition_id;
      $purchaseOrder->order_date = $request->order_date;
      $purchaseOrder->delivery_date = $request->delivery_date;
      $purchaseOrder->order_by = $request->order_by;
      $purchaseOrder->order_type = $request->order_type;
      $purchaseOrder->warehouse_id = $request->warehouse_id;
      $purchaseOrder->project_id = $request->project_id;
      $purchaseOrder->note = $request->note;
      // dd($purchaseOrder, $request->ItemRows,$request->purchase_requisition_id);
      // $purchaseOrder->from = $request->selectedFromId;
      $purchaseOrder->save();
      if(count($request->ItemRows)){
        $itemRows = $request->ItemRows;
        foreach($itemRows as $itemRow){
          // if(isset($itemRow['id'])){
            // $this->updatePurchaseOrderItem($itemRow);
          // } else {
            $this->savePurchaseOrderItem($itemRow, $purchaseOrder);
          // }
          
        }
      }
    }

    
    public function updatePurchaseOrderItem($itemRow)
    {
        $row = PurchaseOrderItem::find($itemRow['id']);
        $this->savePurchaseOrderItemData($itemRow, $row);
    }

    public function savePurchaseOrderItem($itemRow, $purchaseOrder)
    {
        $row = new PurchaseOrderItem();
        $row->purchase_order_id = $purchaseOrder->id;
        // dd('savePurchaseOrderItem',$row,$purchaseOrder);
        $this->savePurchaseOrderItemData($itemRow,$row);
    }

    public function savePurchaseOrderItemData($itemRow, $row)
    {
        // dd('savePurchaseOrderItemData',$row);
        $row->purchase_order_id = $row['purchase_order_id'];
        $row->product_id = $itemRow['product_id'];
        $row->unit_id = $itemRow['unit_id'];
        $row->quantity = $itemRow['quantity'];
        // dd($itemRow, $row);
        $row->save();
    }

    public function checkOrder($prid)
    {
      $purchaseOrder = PurchaseOrder::find($prid);
      $purchaseOrder->checked_by = Auth::id();
      $date = new DateTime();
      $purchaseOrder->checked_at = $date->format('Y-m-d');
      $purchaseOrder->status = 'checked';
      $purchaseOrder->save();
      return new PurchaseOrderResource($purchaseOrder);
    }

    public function approveOrder($prid)
    {
      $purchaseOrder = PurchaseOrder::find($prid);
      $purchaseOrder->approved_by = Auth::id();
      $date = new DateTime();
      $purchaseOrder->approved_at = $date->format('Y-m-d');
      $purchaseOrder->status = 'approved';
      $purchaseOrder->save();
      return new PurchaseOrderResource($purchaseOrder);
    }

    public function getRequisition($prid)
    {
      $purchaseRequisition = PurchaseRequisition::with('purchaseRequisitionItems')->find($prid);
      return new PurchaseRequisitionResource($purchaseRequisition);
    }
    public function getOrder($prid)
    {
      $purchaseOrder = PurchaseOrder::with('purchaseOrderItems')->find($prid);
      return new PurchaseOrderResource($purchaseOrder);
    }
    public function getActiveRequisitions()
    {
      $activeRequisitions = PurchaseRequisition::where('status','approved')->get();
      // dd($activeRequisitions);
      return $activeRequisitions;
    }
    public function getActiveOrders()
    {
      $activeOrders = PurchaseOrder::where('status','approved')->get();
      // dd($activeOrders);
      return $activeOrders;
    }

    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      // dd($id);
      $purchaseOrderQuery = PurchaseOrder::with('warehouse','project','purchaseOrderItems')->find($id);
      // $purchaseOrderQuery = PurchaseOrder::find($id);
      // dd($purchaseOrderQuery);
      return new PurchaseOrderResource($purchaseOrderQuery);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $purchaseOrder = PurchaseOrder::find($id);
        // dd('hello update',$request->ItemRows);
        // $this->savePurchaseOrderData($purchaseOrder, $request);
        if(count($request->ItemRows)){
          $itemRows = $request->ItemRows;
          foreach($itemRows as $itemRow){
            if(isset($itemRow['id'])){
              $this->updatePurchaseOrderItem($itemRow);
            } else {
              $this->savePurchaseOrderItem($itemRow, $purchaseOrder);
            }
            
          }
        }
        return new PurchaseOrderResource($purchaseOrder);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    public function getOrderItems($prid){
      $purchaseOrderItems = PurchaseOrderItem::where('purchase_order_id', $prid)->get();
      return PurchaseOrderItemResource::collection($purchaseOrderItems);
    }


    /**
     * getValidationRules.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	private
     * @param	boolean	$isNew	Default: true
     * @return	array
     */
    private function getValidationRules($isNew = true, $purchaseOrder)
    {
        return [
            'order_date' => 'required',
            'delivery_date' => 'required',
            'order_type' => 'required',
        ];
    }
}
