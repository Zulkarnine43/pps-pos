<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\DefaultGroupResource;
use App\Laravue\Models\DefaultGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Validator;

class DefaultGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $defaultGroupQuery = DefaultGroup::query();
        $limit = Arr::get($searchParams, 'limit');
        $keyword = Arr::get($searchParams, 'keyword', '');
        // search query
        if (!empty($keyword)) {
            $defaultGroupQuery->where('name', 'LIKE', '%' . $keyword . '%');
        }

        return DefaultGroupResource::collection($defaultGroupQuery->orderBy('id', 'desc')->paginate($limit));
    }

    public function getAll(){
        $groups = DefaultGroup::orderBy('name')->get();
        return DefaultGroupResource::collection($groups);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  get validation rules from getValidationRules method
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {

            // create new Department
            
            $defaultGroup = DefaultGroup::create([
                'name' => $request->name,
                'parent_group_id' => $request->parent_group_id,
            ]);
            return new DefaultGroupResource($defaultGroup);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\DefaultGroup  $defaultGroup
     * @return \Illuminate\Http\Response
     */
    public function show(DefaultGroup $defaultGroup)
    {
        return new DefaultGroupResource($defaultGroup);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\DefaultGroup  $defaultGroup
     * @return \Illuminate\Http\Response
     */
    public function edit(DefaultGroup $defaultGroup)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\DefaultGroup  $defaultGroup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DefaultGroup $defaultGroup)
    {
        // check requested department
        if ($defaultGroup === null) {
            return response()->json(['error' => 'Default Group not found'], 404);
        }

        $validator = Validator::make($request->all(), $this->getValidationRules(false));
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            $defaultGroup->name = $request->name;
            $defaultGroup->parent_group_id = $request->parent_group_id;
            $defaultGroup->save();

            return new DefaultGroupResource($defaultGroup);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\DefaultGroup  $defaultGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(DefaultGroup $defaultGroup)
    {
        //
    }

    private function getValidationRules($isNew = true)
    {
        return [
            
            'name' => $isNew ? 'required|unique:parent_groups' : 'required',
            'parent_group_id' => 'required',
        ];
    }
}
