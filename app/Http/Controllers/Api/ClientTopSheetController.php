<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Laravue\Models\ClientTopSheet;
use App\Laravue\Models\ClientBill;
use Illuminate\Http\Request;
use Validator;
use Auth;
use App\Http\Resources\ClientTopSheetResource;
use Illuminate\Support\Arr;

class ClientTopSheetController extends Controller
{
    const ITEM_PER_PAGE = 100;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $ctsQuery = ClientTopSheet::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        // search query
        if (!empty($keyword)) {
            $ctsQuery->where('topsheet_id', 'LIKE', '%' . $keyword . '%');
        }

        return ClientTopSheetResource::collection($ctsQuery->orderBy('id', 'desc')->paginate($limit));
    }

    public function getCanceledBills(){
        $bills = ClientTopSheet::where('status', 'canceled')->get();
        return ClientTopSheetResource::collection($bills);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // get validation rules from getValidationRules method
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            if(count($request->bills) > 0){
                $topsheet = new ClientTopSheet();

                $topsheet->topsheet_id = uniqid();
                $topsheet->supervised_by = $request->supervised_by;
                $topsheet->prepared_by = Auth::id();
                $topsheet->send_date = $request->send_date;
                $topsheet->estimated_payment_date = $request->estimated_payment_date;
                $topsheet->total_amount = $request->total;
                $topsheet->save();

                $bills = $request->bills;

                foreach($bills as $bill){
                    $cb = ClientBill::find($bill['id']);
                    $cb['client_top_sheet_id'] = $topsheet->id;
                    $cb['bill_status'] = 'pending';
                    $cb->save();
                }

                return new ClientTopSheetResource($topsheet);
            }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\ClientTopSheet  $clientTopSheet
     * @return \Illuminate\Http\Response
     */
    public function show(ClientTopSheet $clientTopSheet)
    {
        return new ClientTopSheetResource($clientTopSheet);
    }

    public function view($id){
        $cts = ClientTopSheet::find($id);
        return new ClientTopSheetResource($cts);
    }

    public function setBRNumber(Request $request, $id){
        $cts = ClientTopSheet::find($id);
        $cts->bill_received_number = $request->bill_received_number;
        $cts->save();
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\ClientTopSheet  $clientTopSheet
     * @return \Illuminate\Http\Response
     */
    public function edit(ClientTopSheet $clientTopSheet)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\ClientTopSheet  $clientTopSheet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cts = ClientTopSheet::find($id);
        if ($cts === null) {
            return response()->json(['error' => 'Client Top Sheet not found'], 404);
        } else {
            if(count($request->bills) > 0){
                $bills = $request->bills;
                foreach($bills as $bill){
                    $b = ClientBill::find($bill['id']);
                    $b->bill_received_number = $request->bill_received_number;
                    $b->payment_date = $request->payment_date;
                    $b->sd = $bill['sd'];
                    $b->vat = $bill['vat'];
                    $b->tax = $bill['tax'];
                    $b->deduction = $bill['deduction'];
                    $b->cheque_amount = $bill['cheque_amount'];
                    $b->bill_status = 'received';
                    $b->save();
                }
            }

            $cts->payment_received_date = $request->payment_date; 
            $cts->save();
            return new ClientTopSheetResource($cts);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\ClientTopSheet  $clientTopSheet
     * @return \Illuminate\Http\Response
     */
    public function destroy(ClientTopSheet $clientTopSheet)
    {
        
    }

    public function remove($id){

        $clientTopSheet = ClientTopSheet::find($id);
        
        if ($clientTopSheet === null) {
            return response()->json(['error' => 'Client Bill Item not found'], 404);
        } else {
            $clientTopSheet->status = 'canceled';
            $clientTopSheet->save();

            return new ClientTopSheetResource($clientTopSheet);
        }
    }

    private function getValidationRules($isNew = true)
    {
        return [
            'supervised_by' => 'required',
            'send_date' => 'required|date',
        ];
    }
}
