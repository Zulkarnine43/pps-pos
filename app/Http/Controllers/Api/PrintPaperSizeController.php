<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PrintPaperSizeResource;
use App\Laravue\Models\PrintPaperSize;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Validator;

class PrintPaperSizeController extends Controller
{
    const ITEM_PER_PAGE = 100;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $paperQuery = PrintPaperSize::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        // search query
        if (!empty($keyword)) {
            $paperQuery->where('name', 'LIKE', '%' . $keyword . '%');
        }

        return PrintPaperSizeResource::collection($paperQuery->orderBy('id', 'desc')->paginate($limit));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  get validation rules from getValidationRules method
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {

            // create new Department
            
            $paper = PrintPaperSize::create([
                'name' => $request->name,
                'dimension' => $request->dimension,
            ]);
            return new PrintPaperSizeResource($paper);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\PrintPaperSize  $printPaperSize
     * @return \Illuminate\Http\Response
     */
    public function show(PrintPaperSize $printPaperSize)
    {
        return new PrintPaperSizeResource($printPaperSize);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\PrintPaperSize  $printPaperSize
     * @return \Illuminate\Http\Response
     */
    public function edit(PrintPaperSize $printPaperSize)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\PrintPaperSize  $printPaperSize
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PrintPaperSize $printPaperSize)
    {
        // check requested department
        if ($printPaperSize === null) {
            return response()->json(['error' => 'print paper size not found'], 404);
        }

        $validator = Validator::make($request->all(), $this->getValidationRules(false));
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            //  update department data
            $printPaperSize->name = $request->get('name');
            $printPaperSize->dimension = $request->get('dimension');
            $printPaperSize->save();
            return new PrintPaperSizeResource($printPaperSize);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\PrintPaperSize  $printPaperSize
     * @return \Illuminate\Http\Response
     */
    public function destroy(PrintPaperSize $printPaperSize)
    {
        //
    }

    /**
     * getValidationRules.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	private
     * @param	boolean	$isNew	Default: true
     * @return	array
     */
    private function getValidationRules($isNew = true)
    {
        return [
            'dimension' => 'required',
            'name' => $isNew ? 'required|unique:print_paper_sizes' : 'nullable',
        ];
    }
}
