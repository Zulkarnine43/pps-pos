<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProjectTypeResource;
use App\Laravue\Models\ProjectStage;
use App\Laravue\Models\ProjectType;
use App\Laravue\Models\ProjectTypeMeta;
use App\Laravue\Models\TypeStage;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Validator;
use Illuminate\Support\Str;

class ProjectTypeController extends Controller
{
    const ITEM_PER_PAGE = 30;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $projectTypeQuery = ProjectType::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');

        if (!empty($keyword)) {
            $projectTypeQuery->where('name', 'LIKE', '%' . $keyword . '%');
        }

        return ProjectTypeResource::collection($projectTypeQuery->orderBy('id', 'desc')->paginate($limit));
    }

    public function getActiveItems(){
        $activeProTypes = ProjectType::where('status', 0)->orderBy('name', 'asc')->get();
        return ProjectTypeResource::collection($activeProTypes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // get validation rules form getValidationRules method
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            // create project type

            $projectType = new ProjectType();
            $projectType->name = $request->name;
            $projectType->code = $request->code;
            $stages = $request->stageDatas;

            // create project stage according to project type
            if(count($stages) == $request->stage ){
                $projectType->save();
                foreach ($stages as $stage) {
                    if (isset($stage)) {
                        $typeStage = TypeStage::create([
                            'project_type_id' => $projectType->id,
                            'name' => $stage['name'],
                            'image_required' => $stage['required']
                        ]);
                    }
                }
            }else {
                return response()->json(['error' => 'Please Insert Stage Data Properly '], 403);
            }

            // insert project type meta
            $projectMetas = $request->projectMetas;
            if(count($projectMetas) > 0){
                foreach ($projectMetas as $projectMeta) {
                    if (isset($projectMeta)) {
                        $projectMeta = ProjectTypeMeta::create([
                            'project_type_id' => $projectType->id,
                            'field_name' => $projectMeta['field_name'],
                            'input_name' => Str::slug($projectMeta['field_name']) . time(),
                            'input_type' => $projectMeta['input_type'],
                            'default_value' => $projectMeta['default_value'],
                            'is_required' => $projectMeta['is_required'],
                        ]);
                    }
                }
            }

            return new ProjectTypeResource($projectType);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\ProjectType  $projectType
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectType $projectType)
    {
        return new ProjectTypeResource($projectType);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\ProjectType  $projectType
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectType $projectType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\ProjectType  $projectType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProjectType $projectType)
    {
        // check requested project type
        if ($projectType === null) {
            return response()->json(['error' => 'Project Type not found'], 404);
        }


        $validator = Validator::make($request->all(), $this->getValidationRules(false));
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            // update project type data
            $projectType->name = $request->get('name');
            $projectType->code = $request->get('code');
            $projectType->save();

            $projectStages = $request->get('projectTypeStages');

            // update project type stage data
            if(count($projectStages) > 0){
                foreach ($projectStages  as $projectStage) {
                    if (isset($projectStage['id'])) {
                        $pStage = TypeStage::where('id', $projectStage['id'])->first();
                        $pStage->name = $projectStage['name'];
                        $pStage->image_required = $projectStage['image_required'];
                        $pStage->save();
                    }
                }
            }

            $projectTypeMetas = $request->get('projectTypeMetas');
            // dd($projectTypeMetas);
            if(count($projectTypeMetas) > 0){
                foreach ($projectTypeMetas as $projectTypeMeta) {
                    if (isset($projectTypeMeta['id'])) {
                        // update exsisting project meta
                        $PMeta = ProjectTypeMeta::where('id', $projectTypeMeta['id'])->first();
                        $PMeta->field_name = $projectTypeMeta['field_name'];
                        $PMeta->input_name = Str::slug($projectTypeMeta['field_name'], '_') . time();
                        $PMeta->input_type = $projectTypeMeta['input_type'];
                        $PMeta->default_value = $projectTypeMeta['default_value'];
                        $PMeta->is_required = $projectTypeMeta['is_required'];
                        $PMeta->save();
                    } else {
                        // creating new project meta
                        $pmeta = new ProjectTypeMeta();
                        $pmeta->project_type_id = $projectType->id;
                        $pmeta->field_name = $projectTypeMeta['field_name'];
                        $pmeta->input_name = Str::slug($projectTypeMeta['field_name'], '_') . time();
                        $pmeta->input_type = $projectTypeMeta['input_type'];
                        $pmeta->default_value = $projectTypeMeta['default_value'];
                        $pmeta->is_required = $projectTypeMeta['is_required'];
                        $pmeta->save();
                    };
                }
            }
            return new ProjectTypeResource($projectType);
        }
    }

    /**
     * This is for unmute requested muted project type.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	public
     * @param	mixed	$id
     * @return	mixed
     */
    public function unmute($id)
    {
        $projectType = ProjectType::find($id);
        if ($projectType === null) {
            return response()->json(['error' => 'Project Type not found'], 404);
        }

        $projectType->status = 0;

        $projectType->save();
        return new ProjectTypeResource($projectType);
    }

    /**
     * This is for mute requested active project type..
     *
     * @author	Unknown
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	public
     * @param	mixed	$id
     * @return	mixed
     */
    public function mute($id)
    {
        $projectType = ProjectType::find($id);
        if ($projectType === null) {
            return response()->json(['error' => 'Project Type not found'], 404);
        }
        // dd($projectType);
        $projectType->status = 1;

        $projectType->save();

        return new ProjectTypeResource($projectType);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\ProjectType  $projectType
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $projectTypeMeta = ProjectTypeMeta::find($id);
        $projectTypeMeta->delete();
        return response()->json('success', 200);
    }

    /**
     * getValidationRules.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	private
     * @param	boolean	$isNew	Default: true
     * @return	array
     */
    private function getValidationRules($isNew = true)
    {
        return [
            'name' => 'required',
            'code' => $isNew ? 'nullable|unique:project_types' : 'nullable',
        ];
    }
}
