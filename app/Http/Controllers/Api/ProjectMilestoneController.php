<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProjectMilestoneResource;
use App\Laravue\Models\ProjectMilestone;
use App\Laravue\Models\ProjectWbc;
use App\Laravue\Models\ProjectWbcImage;
use Illuminate\Http\Request;
use Validator;

class ProjectMilestoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\ProjectMilestone  $projectMilestone
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectMilestone $projectMilestone)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\ProjectMilestone  $projectMilestone
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectMilestone $projectMilestone)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\ProjectMilestone  $projectMilestone
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProjectMilestone $projectMilestone)
    {
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules($projectMilestone,$isNew = false),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        }else{
            $projectMilestone->update($request->all());
            $projectWbcs = $request->projectWbcs;
            foreach($projectWbcs as $projectWbc){
                $pWbc = ProjectWbc::find($projectWbc['id']);
                $pWbc->work_done = $projectWbc['work_done'];
                $pWbc->amount = $projectWbc['amount'];
                $pWbc->start_date = $projectWbc['start_date'];
                $pWbc->deadline = $projectWbc['deadline'];
                $pWbc->finish_date = $projectWbc['finish_date'];
                $pWbc->save();

                if(isset($projectWbc['images'])){
                    foreach($projectWbc['images'] as $image){
                        $wbcImage = new ProjectWbcImage();
                        $wbcImage->project_id = $projectMilestone->project_id;
                        $wbcImage->project_wbc_id = $pWbc->id;
                        $wbcImage->image_name = $image;
                        $wbcImage->save();
                    }
                }
                
            }
            return new ProjectMilestoneResource($projectMilestone);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\ProjectMilestone  $projectMilestone
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectMilestone $projectMilestone)
    {
        //
    }

    private function getValidationRules($project = null,$isNew = true)
    {
        return [
            'work_done' => 'required|string',
            'amount' => 'required|numeric',
        ];
    }
}
