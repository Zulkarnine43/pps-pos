<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\StateResource;
use App\Laravue\Models\State;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class StateController extends Controller
{
    const ITEM_PER_PAGE = 100;
    /**
     * index.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	public
     * @param	mixed	$country_id
     * @return	all the state list according to country_id
     */
    public function index(Request $request){
        $searchParams = $request->all();
        $stateQuery = State::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        // search query
        if (!empty($keyword)) {
            $stateQuery->where('name', 'LIKE', '%' . $keyword . '%');
        }

        return StateResource::collection($stateQuery->orderBy('name', 'asc')->paginate($limit));
    }
    public function stateByCountry($country_id){
        $states = State::where('country_id',$country_id)->orderBy('name','asc')->get();
        return response()->json(['states' => $states], 200);
    }

    // public function allState(){
    //     $states = State::orderBy('name','asc')->get();
    //     return response()->json(['states' => $states], 200);
    // }
}
