<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;
use App\Laravue\Models\Product;
use App\Laravue\Models\ProductMeta;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Validator;
use File;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    const ITEM_PER_PAGE = 100;
    public $image; //product image
    public $productId;
    public $product;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $productQuery = Product::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        // dd($keyword);
        // search query
        if (!empty($keyword)) {
            $productQuery->where('name', 'LIKE', '%' . $keyword . '%');
        }

        return ProductResource::collection($productQuery->orderBy('id', 'desc')->paginate($limit));
    }

    /**
     * Show all the active product.
     *
     * @return \Illuminate\Http\Response
     */
    public function isActive()
    {
        $products = Product::where('status', 0)->get();
        return response()->json(['products' => $products], 200);
    }


    public function getImage(Request $request){
        if($request->file('photo')){
         $file_name = time().'-'.$request->file('photo')->getClientOriginalName();
         $request->file('photo')->storePubliclyAs(
             'products/images',
             $file_name,
             's3'
         );
         return response()->json(['imageName' => $file_name]);
        }
     }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // check validation rules from getValidationRules method
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules($isNew = true, $product = null),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {

            if($request->image){
                $image = 'products/images/'.$request->image;
            }
            else {
                $image = '';
            }
           
            // product creating
            $product = Product::create([
                'category_id' => $request->category_id,
                'brand_id' => $request->brand_id,
                'unit_id' => $request->unit_id,
                'price' => $request->price,
                'discount_price' => $request->discount_price,
                'purchase_price' => $request->purchase_price,
                'name' => $request->name,
                'code' => $request->code,
                'description' => $request->description,
                'sku' => $request->sku,
                'alert_quantity' => $request->alert_quantity,
                'image' => $image,
            ]);
            return new ProductResource($product);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return new ProductResource($product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {

        $product_image = $product->image;
        if($request->image) {
            if($product_image){

                Storage::disk('s3')->delete("/products/images/".$product_image);
            }
        }
        // dd($request);
        // check requested product
        if ($product === null) {
            return response()->json(['error' => 'product not found'], 404);
        }

        // check validation rules form getValidationRules method
        $validator = Validator::make($request->all(), $this->getValidationRules($isNew = false, $product));
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
          

            // update requested product
            $product->category_id = $request->get('category_id');
            $product->brand_id = $request->get('brand_id');
            $product->unit_id = $request->get('unit_id');
            $product->name = $request->get('name');
            $product->price = $request->get('price');
            $product->purchase_price = $request->get('purchase_price');
            $product->discount_price = $request->get('discount_price');
            $product->code = $request->get('code');
            $product->description = $request->get('description');
            $product->sku = $request->get('sku');
            $product->alert_quantity = $request->get('alert_quantity');
            if($request->image){
                $product->image = 'products/images/'.$request->get('image');
            }

            $product->save();
            return new ProductResource($product);
        }
    }


    /**
     * unmute the muted product.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	public
     * @param	request	$request
     * @param	product	$product
     * @return	mixed
     */
    public function unmute(Request $request, Product $product)
    {
        if ($product === null) {
            return response()->json(['error' => 'product not found'], 404);
        }

        $product->status = 0;

        $product->save();
        return new ProductResource($product);
    }

    /**
     * mute the active product.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	public
     * @param	request	$request
     * @param	product	$product
     * @return	mixed
     */
    public function mute(Request $request, Product $product)
    {
        if ($product === null) {
            return response()->json(['error' => 'product not found'], 404);
        }

        $product->status = 1;

        $product->save();
        return new ProductResource($product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Pmeta = ProductMeta::find($id);
        $Pmeta->delete();
        return response()->json('success', 200);

    }

    /**
     * getValidationRules.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	private
     * @param	boolean	$isNew	Default: true
     * @return	array
     */
    private function getValidationRules($isNew, $product)
    {
        return [
            'name' => 'required',
            'brand_id' => 'required',
            'category_id' => 'required',
            'unit_id' => 'required',
            'price' => 'required',
            'code' => $isNew ? 'required|unique:products' : 'required|unique:products,code,'.$product->id,
            'sku' => $isNew ? 'nullable|unique:products' : 'nullable|unique:products,sku,'.$product->id,
        ];
    }
}
