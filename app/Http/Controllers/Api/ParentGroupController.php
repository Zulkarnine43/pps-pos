<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ParentGroupResource;
use App\Laravue\Models\ParentGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Validator;

class ParentGroupController extends Controller
{
    const ITEM_PER_PAGE = 100;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $parentgroupQuery = ParentGroup::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        // search query
        if (!empty($keyword)) {
            $parentgroupQuery->where('name', 'LIKE', '%' . $keyword . '%');
        }

        return ParentGroupResource::collection($parentgroupQuery->orderBy('id', 'desc')->paginate($limit));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  get validation rules from getValidationRules method
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {

            // create new Department
            
            $parentGroup = ParentGroup::create([
                'name' => $request->name,
            ]);
            return new ParentGroupResource($parentGroup);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\ParentGroup  $parentGroup
     * @return \Illuminate\Http\Response
     */
    public function show(ParentGroup $parentGroup)
    {
        return new ParentGroupResource($parentGroup);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\ParentGroup  $parentGroup
     * @return \Illuminate\Http\Response
     */
    public function edit(ParentGroup $parentGroup)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\ParentGroup  $parentGroup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ParentGroup $parentGroup)
    {
        // check requested department
        if ($parentGroup === null) {
            return response()->json(['error' => 'Parent Group not found'], 404);
        }

        $validator = Validator::make($request->all(), $this->getValidationRules(false));
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            $parentGroup->name = $request->name;
            $parentGroup->save();

            return new ParentGroupResource($parentGroup);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\ParentGroup  $parentGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(ParentGroup $parentGroup)
    {
        //
    }

    /**
     * getValidationRules.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	private
     * @param	boolean	$isNew	Default: true
     * @return	array
     */
    private function getValidationRules($isNew = true)
    {
        return [
            
            'name' => $isNew ? 'required|unique:parent_groups' : 'required',
        ];
    }
}
