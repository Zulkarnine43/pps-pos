<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PurchaseRequisitionItemResource;
use App\Http\Resources\PurchaseRequisitionResource;
use App\Laravue\Models\PurchaseRequisition;
use App\Laravue\Models\PurchaseRequisitionItem;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Validator;
use Auth;
use DateTime;
use Exception;

class PurchaseRequisitionController extends Controller
{
    const ITEM_PER_PAGE = 100;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
      $searchParams = $request->all();
      $purchaseRequisitionQuery = PurchaseRequisition::query();
      // $purchaseRequisitionQuery = PurchaseRequisition::with('fromWarehouse','fromProject','toWarehouse','toProject')->get();
      // $purchaseRequisitionQuery = PurchaseRequisition::with('fromWarehouse','fromProject','toWarehouse','toProject','purchaseRequisitionItems.product');

      // $purchaseRequisitionQuery = PurchaseRequisition::with('fromInventory','toInventory')->get();
      // $purchaseRequisitionQuery = PurchaseRequisition::with('fromInventory')->get();
      // dd($purchaseRequisitionQuery);
      $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
      $keyword = Arr::get($searchParams, 'keyword', '');
      $date = Arr::get($searchParams, 'date', '');
      
      if (!empty($keyword)) {
          $purchaseRequisitionQuery->where('gatepass_number', 'LIKE', '%' . $keyword . '%');
      }
      if (!empty($date)) {
          $purchaseRequisitionQuery->where('purchaseRequisition_date', 'LIKE', '%' . $date . '%');
      }

      return PurchaseRequisitionResource::collection($purchaseRequisitionQuery->orderBy('id', 'asc')->paginate($limit));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // try{

      // } catch(Exception $ex){

      // }
      //  get validation rules from getValidationRules method
      $validator = Validator::make(
        $request->all(),
        array_merge(
          $this->getValidationRules($isNew = true, $purchaseRequisition = null),
        )
      );
      // dd('hi', $request->ItemRows, $request->all());

      if ($validator->fails()) {
        return response()->json(['errors' => $validator->errors()], 403);
      } else {
        // dd($request->all());
        $purchaseRequisition = new PurchaseRequisition();
        $purchaseRequisition->created_by = Auth::id();
        $purchaseRequisition->status = 'created';
        $this->savePurchaseRequisitionData($purchaseRequisition, $request);
        return new PurchaseRequisitionItemResource($purchaseRequisition);
      }
    }


    public function savePurchaseRequisitionData($purchaseRequisition, $request)
    {
      $purchaseRequisition->prn = $request->prn;
      $purchaseRequisition->requisition_date = $request->requisition_date;
      $purchaseRequisition->delivery_date = $request->delivery_date;
      $purchaseRequisition->requisition_by = $request->requisition_by;
      $purchaseRequisition->requisition_type = $request->requisition_type;
      $purchaseRequisition->warehouse_id = $request->warehouse_id;
      $purchaseRequisition->project_id = $request->project_id;
      $purchaseRequisition->note = $request->note;
      // dd($purchaseRequisition, $request->ItemRows);
      // $purchaseRequisition->from = $request->selectedFromId;
      $purchaseRequisition->save();
      if(count($request->ItemRows)){
        $itemRows = $request->ItemRows;
        foreach($itemRows as $itemRow){
          // if(isset($itemRow['id'])){
            // $this->updatePurchaseRequisitionItem($itemRow);
          // } else {
            $this->savePurchaseRequisitionItem($itemRow, $purchaseRequisition);
          // }
          
        }
      }
    }

    
    public function updatePurchaseRequisitionItem($itemRow)
    {
        $row = PurchaseRequisitionItem::find($itemRow['id']);
        $this->savePurchaseRequisitionItemData($itemRow, $row);
    }

    public function savePurchaseRequisitionItem($itemRow, $purchaseRequisition)
    {
        $row = new PurchaseRequisitionItem();
        $row->purchase_requisition_id = $purchaseRequisition->id;
        // dd('savePurchaseRequisitionItem',$row,$purchaseRequisition);
        $this->savePurchaseRequisitionItemData($itemRow,$row);
    }

    public function savePurchaseRequisitionItemData($itemRow, $row)
    {
        // dd('savePurchaseRequisitionItemData',$row);
        $row->purchase_requisition_id = $row['purchase_requisition_id'];
        $row->product_id = $itemRow['product_id'];
        $row->unit_id = $itemRow['unit_id'];
        $row->quantity = $itemRow['quantity'];
        $row->save();
    }

    public function checkRequisition($prid)
    {
      $purchaseRequisition = PurchaseRequisition::find($prid);
      $purchaseRequisition->checked_by = Auth::id();
      $date = new DateTime();
      $purchaseRequisition->checked_at = $date->format('Y-m-d');
      $purchaseRequisition->status = 'checked';
      $purchaseRequisition->save();
      return new PurchaseRequisitionResource($purchaseRequisition);
    }

    public function approveRequisition($prid)
    {
      $purchaseRequisition = PurchaseRequisition::find($prid);
      $purchaseRequisition->approved_by = Auth::id();
      $date = new DateTime();
      $purchaseRequisition->approved_at = $date->format('Y-m-d');
      $purchaseRequisition->status = 'approved';
      $purchaseRequisition->save();
      return new PurchaseRequisitionResource($purchaseRequisition);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      // $purchaseRequisitionQuery = PurchaseRequisition::with('fromWarehouse','fromProject','toWarehouse','toProject','purchaseRequisitionItems.product')->find($purchaseRequisition->id);
      $purchaseRequisitionQuery = PurchaseRequisition::with('warehouse','project','purchaseRequisitionItems')->find($id);
      // dd($purchaseRequisitionQuery);
      // return $purchaseRequisitionQuery;
        return new PurchaseRequisitionResource($purchaseRequisitionQuery);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        dd('hello update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    public function getRequisitionItems($prid){
      $purchaseRequisitionItems = PurchaseRequisitionItem::where('purchase_requisition_id', $prid)->get();
      // dd($purchaseRequisitionItems);
      return PurchaseRequisitionItemResource::collection($purchaseRequisitionItems);
    }


    /**
     * getValidationRules.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	private
     * @param	boolean	$isNew	Default: true
     * @return	array
     */
    private function getValidationRules($isNew = true, $purchaseRequisition)
    {
        return [
            'requisition_date' => 'required',
            'delivery_date' => 'required',
            'requisition_type' => 'required',
        ];
    }
}
