<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\SaleResource;
use App\Laravue\Helpers\AppHelper;
use App\Laravue\Models\Sale;
use App\Laravue\Models\SaleItem;
use App\Laravue\Models\SalePayment;
use App\Laravue\Models\SaleRegister;
use App\Laravue\Models\WarehouseInventory;
use Illuminate\Http\Request;
use Validator;
use Auth;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class SaleController extends Controller
{
    const ITEM_PER_PAGE = 100;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $stateQuery = Sale::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        // search query
        if (!empty($keyword)) {
            $stateQuery->where('sale_id', 'LIKE', '%' . $keyword . '%');
        }

        return SaleResource::collection($stateQuery->orderBy('id', 'desc')->paginate($limit));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  get validation rules from getValidationRules method
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            try {
                DB::beginTransaction();
                $sale = new Sale();
                
                $sale->sale_by = Auth::id();
                
                $sale->sale_id = $request->sale_id_prefix.(Sale::orderBy('id', 'desc')->first() ? Sale::orderBy('id', 'desc')->first()->sale_id+1 : 1);
                $sale->warehouse_id = $request->warehouse_id;
                $sale->customer_id = $request->customer_id;
                $sale->sale_date = $request->sale_date;
                $sale->sale_time = $request->sale_time;
                // $sale->sale_time = Carbon::now()->timezone('Asia/Dhaka')->toTimeString();
                $sale->vat = $request->vat;
                $sale->tax = $request->tax;
                $sale->discount = $request->discount;
                $sale->pay_type = $request->pay_type;
                $sale->sub_total = $request->subTotal;
                $sale->grand_total = $request->grand_total;
                $sale->sale_register_id = SaleRegister::orderBy('id', 'desc')->whereNull('close_by')->first()->id;
                $sale->save();
                $saleItems = $request->saleItems;
                $paymentInfos = $request->paymentInfos;
                if (count($saleItems) > 0) {
                    foreach ($saleItems as $saleItem) {
                        $sItem = new SaleItem();
                        $sItem->sale_id = $sale->id;
                        $sItem->product_id = $saleItem['id'];
                        $sItem->quantity = $saleItem['quantity'];
                        $sItem->price = $saleItem['price'];
                        $sItem->vat = $saleItem['vat'];
                        $sItem->discount = $saleItem['discount_amount'];
                        $sItem->unit_name = $saleItem['unit_name'];
                        $sItem->sub_total = $saleItem['sub_total'];
                        $sItem->save();
                        $WIP = WarehouseInventory::where('product_id', $saleItem['id'])->first();
                        if(isset($WIP)){
                            AppHelper::updateWarehouseInventory($request->warehouse_id, $sItem, false);
                        } else{
                            return response()->json(['error' => 'Product not found in warehouse inventory'], 403);
                        }
                        
                    }
                }
                if (count($paymentInfos) > 0) {
                    foreach ($paymentInfos as $paymentInfo) {
                        $pay = new SalePayment();
                        $pay->sale_id = $sale->id;
                        $pay->payment_method_id = $paymentInfo['payment_method_id'];
                        $pay->amount = $paymentInfo['amount'];
                        $pay->date_time = Carbon::now()->toDateTimeLocalString();
                        $pay->save();
                    }
                }
                
                DB::commit();
                return new SaleResource($sale);
            } catch (Exception $e) {
                DB::rollBack();
                return response()->json(['errors' => $e->getMessage()], 403);
            }
        }
    }

    public function sale_report(Request $request)
    {
        $current_date = Carbon::now()->toDateString();
        $searchParams = $request->all();
        $saleQuery = Sale::query();
        $from = Arr::get($searchParams, 'from', '');
        $to = Arr::get($searchParams, 'to', '');
        $firstDate = "";
        $lastDate = "";

    if($from){
       $from=date('d-m-Y h:i:s a',strtotime($from));
    }
    if($to){
        $to=date('d-m-Y h:i:s a',strtotime($to));
    }
        // search query
        if (!empty($from) && !empty($to)) {
            $saleItem =$saleQuery->whereBetween('sale_date', [$from, $to]);
        }
        if (empty($to) && !empty($from)) {
            $saleItem =$saleQuery->whereBetween('sale_date', [$from, $current_date]);
        }

        if (!empty($from)) {
            $saleItem = Sale::where('sale_date', '>', $from)->get();
        }
        
        $saleItem = $saleQuery->orderBy('sale_date')->get();
  
        if(!$from && !$to){
        if(count($saleItem) > 1){
            $firstDate = $saleItem[0]->sale_date;
            $lastDate = $saleItem[count($saleItem)-1]->sale_date;
            $firstDate = date("Y-m-d", strtotime($firstDate));
            $lastDate = date("Y-m-d", strtotime($lastDate));
        }
    }
        return response()->json(['saleReport' => SaleResource::collection($saleItem),'firstDate' => $firstDate, 'lastDate' => $lastDate], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function show(Sale $sale)
    {
        return new SaleResource($sale);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function edit(Sale $sale)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sale $sale)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sale $sale)
    {
        //
    }

    private function getValidationRules($isNew = true)
    {
        return [
            'warehouse_id' => 'required',
            'customer_id' => 'required',
            'sale_date' => 'required',
            'grand_total' => 'required',
            // 'sale_id' => $isNew ? 'required|unique:sales' : 'nullable',
        ];
    }
}
