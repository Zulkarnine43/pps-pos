<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProjectRequisitionResource;
use App\Laravue\Models\Project;
use App\Laravue\Models\ProjectDirasa;
use App\Laravue\Models\ProjectRequisition;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Validator;

class ProjectRequisitionController extends Controller
{
    const ITEM_PER_PAGE = 100;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $requisitionQuery = ProjectRequisition::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        $status = Arr::get($searchParams, 'status', '');
        $subcontractor_id = Arr::get($searchParams, 'subcontractor_id', '');
        $project_type_id = Arr::get($searchParams, 'project_type_id', '');
        // search query
        
        if(!empty($subcontractor_id)){
            $requisitionQuery->where('subcontractor_id', $subcontractor_id);
        }
        if(!empty($project_type_id)){
            $requisitionQuery->where('project_type_id', $project_type_id);
        }
        if (!empty($keyword)) {
            $requisitionQuery->where('name', 'LIKE', '%' . $keyword . '%')
                ->orWhere('phone', 'LIKE', '%' . $keyword . '%')
                ->orWhere('prs_number', 'LIKE', '%' . $keyword . '%')
                ->orWhere('bn_address', 'LIKE', '%' . $keyword . '%')
                ->orWhere('name', 'LIKE', '%' . $keyword . '%')
                ->orWhere('nid_number', 'LIKE', '%' . $keyword . '%')
                ->orWhere('gps', 'LIKE', '%' . $keyword . '%')
                ->orWhere('site_code', 'LIKE', '%' . $keyword . '%')
                ->orWhere('location', 'LIKE', '%' . $keyword . '%');
        }

        return ProjectRequisitionResource::collection($requisitionQuery->orderBy('id', 'desc')->where('status', $status)->paginate($limit));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  get validation rules from getValidationRules method
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {

            // create new unit

            $requisition = new ProjectRequisition();
            $requisition->name = $request->name;
            $requisition->prs_number = $request->prs_number;
            $requisition->phone = $request->phone;
            $requisition->location = $request->location;
            $requisition->details = $request->details;
            $requisition->remark = $request->remark;
            $requisition->bn_address = $request->bn_address;
            $requisition->requisition_by = $request->requisition_by;
            $requisition->gps = $request->gps;
            $requisition->site_code = $request->site_code;
            $requisition->requisition_date = $request->requisition_date;
            $requisition->nid_number = $request->nid_number;
            $requisition->country_id = $request->country_id;
            $requisition->state_id = $request->state_id;
            $requisition->district_id = $request->district_id;
            $requisition->city_id = $request->city_id;
            

            if ($request->quantity) {
                $requisition->quantity = $request->quantity;
            }
            $requisition->project_type_id = $request->project_type_id;
            $requisition->save();
            return new ProjectRequisitionResource($requisition);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\ProjectRequisition  $projectRequisition
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectRequisition $projectRequisition)
    {
        return new ProjectRequisitionResource($projectRequisition);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\ProjectRequisition  $projectRequisition
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectRequisition $projectRequisition)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\ProjectRequisition  $projectRequisition
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProjectRequisition $projectRequisition)
    {
        if ($projectRequisition === null) {
            return response()->json(['error' => 'Requisition not found'], 404);
        }

        $validator = Validator::make($request->all(), $this->getValidationRules($isNew=false));
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            //  update unit data
            $projectRequisition->name = $request->name;
            $projectRequisition->phone = $request->phone;
            $projectRequisition->location = $request->location;
            $projectRequisition->details = $request->details;
            $projectRequisition->remark = $request->remark;
            $projectRequisition->bn_address = $request->bn_address;
            $projectRequisition->requisition_by = $request->requisition_by;
            $projectRequisition->gps = $request->gps;
            // 
            $projectRequisition->requisition_date = $request->requisition_date;
            $projectRequisition->nid_number = $request->nid_number;
            $projectRequisition->quantity = $request->quantity;
            $projectRequisition->project_type_id = $request->project_type_id;

            $projectRequisition->country_id = $request->country_id;
            $projectRequisition->state_id = $request->state_id;
            $projectRequisition->district_id = $request->district_id;
            $projectRequisition->city_id = $request->city_id;
            
            $projectRequisition->save();
            return new ProjectRequisitionResource($projectRequisition);
        }
    }

    public function updateProject($site_code, $requisition){
        $project = Project::where('site_code', $site_code)->first();
        if(isset($project)){
            $project->project_requisition_id = $requisition->id;
            $project->location = $requisition->location;
            $project->bn_address = $requisition->bn_address;
            $project->gps = $requisition->gps;
            $project->name = $requisition->name;
            $project->remark = $requisition->remark;
            $project->country_id = $requisition->country_id;
            $project->state_id = $requisition->state_id;
            $project->district_id = $requisition->district_id;
            $project->city_id = $requisition->city_id;
            
            $project->save();
        }else {
            return back();
        }
    }

    public function setSiteCode(Request $request, $id){
        if($request->site_code){
            $requisition = ProjectRequisition::find($id);
            if($requisition){
                $requisition->site_code = $request->site_code;
                $requisition->status = 'completed';
                $requisition->save();
                $this->updateProject($request->site_code, $requisition);
                $this->findProjectDirasa($requisition);

            }else {
                return response()->json(['error' => 'Requisition not found'], 404);
            }
        }
    }

    public function findProjectDirasa($requisition){
        $dirasa = ProjectDirasa::where('prs_number', $requisition->prs_number)->first();
        if(isset($dirasa)){
            $dirasa->site_code = $requisition->site_code;
            $dirasa->save();
        }
    }

    public function import(Request $request)
    {
        $requisitions = $request->all();

        if (count($requisitions) > 0) {
            $i = 0;
            foreach ($requisitions as $requi) {
                $i++;
                $validator = Validator::make(
                    $requi,
                    array_merge(
                        $this->getValidationRules($isNew = false),
                    )
                );

                if ($validator->fails()) {
                    return response()->json(['errors' => $validator->errors()], 403);
                } else {
                    $last = ProjectRequisition::orderBy('id', 'desc')->first();
                    $lastId = 0;
                    if (isset($last)) {
                        $lastId = $last->id;
                    }
                    $requisition = new ProjectRequisition();
                    $requisition->prs_number = 'PRS-' . ($lastId + 1);
                    $requisition->requisition_date = $requi['requisition_date'];
                    $requisition->requisition_by = $requi['requisition_by'];
                    $requisition->project_type_id = $requi['project_type_id'];
                    $requisition->nid_number = $requi['nid_number'];
                    $requisition->name = $requi['name'];
                    $requisition->phone = $requi['phone'];

                    if (isset($requi['quantity'])) {
                        $requisition->quantity = $requi['quantity'];
                    }
                    if (isset($requi['details'])) {
                        $requisition->details = $requi['details'];
                    }

                    if (isset($requi['country_id'])) {
                        $requisition->country_id = $requi['country_id'];
                    }
                    if (isset($requi['state_id'])) {
                        $requisition->state_id = $requi['state_id'];
                    }
                    if (isset($requi['district_id'])) {
                        $requisition->district_id = $requi['district_id'];
                    }
                    if (isset($requi['city_id'])) {
                        $requisition->city_id = $requi['city_id'];
                    }
                    if (isset($requi['location'])) {
                        $requisition->location = $requi['location'];
                    }
                    if (isset($requi['bn_address'])) {
                        $requisition->bn_address = $requi['bn_address'];
                    }

                    if (isset($requi['remark'])) {
                        $requisition->remark = $requi['remark'];
                    }
                    if (isset($requi['gps'])) {
                        $requisition->gps = $requi['gps'];
                    }

                    $requisition->save();
                }
            }
            return response()->json(['message' => 'Successfully Imported'], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\ProjectRequisition  $projectRequisition
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectRequisition $projectRequisition)
    {
        //
    }

    private function getValidationRules($isNew = true)
    {
        return [
            'name' => 'required',
            'phone' => 'required',
            'requisition_date' => 'required',
            'requisition_by' => 'required',
            'nid_number' => 'required',
            'project_type_id' => 'required',
            'quantity' => 'numeric|nullable',
            'prs_number' => $isNew ? 'required|unique:project_requisitions' : 'nullable',
        ];
    }
}
