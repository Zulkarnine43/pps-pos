<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\JournalResource;
use App\Laravue\Models\Journal;
use App\Laravue\Models\JournalItem;
use App\Laravue\Models\JournalItemCostCenter;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Validator;
use Auth;

class JournalController extends Controller
{
    const ITEM_PER_PAGE = 1000;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $journalQuery = Journal::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        $date = Arr::get($searchParams, 'date', '');
        // search query
        if (!empty($keyword)) {
            $journalQuery->where('voucher_no', 'LIKE', '%' . $keyword . '%');
        }
        if (!empty($date)) {
            $journalQuery->where('trans_date', 'LIKE', '%' . $date . '%');
        }

        return JournalResource::collection($journalQuery->orderBy('id', 'desc')->paginate($limit));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  get validation rules from getValidationRules method
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules($isNew = true, $journal = null),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            $journal = new Journal();
            $journal->voucher_no = $request->voucher_no;
            $journal->created_by = Auth::id();
            $this->saveJournalData($journal, $request);
            return new JournalResource($journal);
        }
    }

    public function saveJournalData($journal, $request){
        $journal->branch_name = $request->branch_name;
        $journal->trans_date = $request->trans_date;
        $journal->narration = $request->narration;
        $journal->net_amount = $request->net_amount;
        if(count($request->ItemRows) > 1){
            $itemRows = $request->ItemRows;
            $journal->ledger_name = $itemRows[0]['ledger_name'];
            $journal->ledger_id = $itemRows[0]['ledger_id'];
            $journal->save();
            foreach($itemRows as $itemRow){
                if(isset($itemRow['id'])){
                    $this->updateJournalItem($itemRow);
                } else {
                    $this->saveJournalItem($itemRow, $journal);
                }
                
            }
        }
    }

    public function saveJournalItem($itemRow, $journal){
        $row = new JournalItem();
        $row->journal_id = $journal->id;
        $this->saveJournalItemData($itemRow, $row);
    }

    public function updateJournalItem($itemRow){
        $row = JournalItem::find($itemRow['id']);
        $this->saveJournalItemData($itemRow, $row);
    }
    public function saveJournalItemData($itemRow, $row){
        $row->type = $itemRow['type'];
        $row->ledger_id = $itemRow['ledger_id'];
        $row->ledger_name = $itemRow['ledger_name'];
        $row->debit_amount = $itemRow['debit_amount'];
        $row->credit_amount = $itemRow['credit_amount'];
        if(isset($itemRow['cost_center'])){
            $row->cost_center = $itemRow['cost_center'];
        }
        
        $row->save();
        if($itemRow['cost_center'] === 1){
            foreach($itemRow['costCenters'] as $cost){
                if(isset($cost['id'])){
                    $this->updateJournalItemCostCenter($cost, $row);
                } else {
                    if(isset($cost['project_id'])){
                        $this->saveJournalItemCostCenter($cost, $row);
                    }
                    
                }
                
            }
        }
    }
    public function saveJournalItemCostCenter($cost, $row){

        $cc = new JournalItemCostCenter();
        $cc->journal_item_id = $row->id;
        $this->saveJournalItemCostCenterData($cost, $cc, $row);
        
    }

    public function updateJournalItemCostCenter($cost, $row){
        $cc = JournalItemCostCenter::find($cost['id']);
        $this->saveJournalItemCostCenterData($cost, $cc, $row);
    }

    public function saveJournalItemCostCenterData($cost, $cc, $row){
        $cc->project_type_id = $cost['project_type_id'];
        $cc->project_id = $cost['project_id'];
        $cc->site_code = $cost['site_code'];
        $cc->amount = $cost['amount'];
        $cc->cost_center_type = $row->type;
        $cc->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\Journal  $journal
     * @return \Illuminate\Http\Response
     */
    public function show(Journal $journal)
    {
        return new JournalResource($journal);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\Journal  $journal
     * @return \Illuminate\Http\Response
     */
    public function edit(Journal $journal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\Journal  $journal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Journal $journal)
    {
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules($isNew = false, $journal),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            $this->saveJournalData($journal, $request);
            return new JournalResource($journal);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\Journal  $journal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Journal $journal)
    {
        //
    }

    /**
     * getValidationRules.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	private
     * @param	boolean	$isNew	Default: true
     * @return	array
     */
    private function getValidationRules($isNew = true, $journal)
    {
        return [
            'voucher_no' => $isNew ? 'required|unique:journals' : 'required|unique:journals,voucher_no,' . $journal->id,
            'trans_date' => 'required',
            'branch_name' => 'required',
        ];
    }
}
