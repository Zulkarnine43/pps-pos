<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PurchaseResource;
use App\Laravue\Helpers\AppHelper;
use App\Laravue\Models\Product;
use App\Laravue\Models\Purchase;
use App\Laravue\Models\PurchaseItem;
use App\Laravue\Models\WarehouseInventory;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\This;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    const ITEM_PER_PAGE = 100;
    public $PItem;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $searchParams = $request->all();
        $purchaseQuery = Purchase::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');

        // search query
        if (!empty($keyword)) {
            $purchaseQuery->where('ref_number', 'LIKE', '%' . $keyword . '%');
        }

        return PurchaseResource::collection($purchaseQuery->orderBy('id', 'desc')->paginate($limit));
    }

    public function purchase_report(Request $request)
    {
        $current_date = Carbon::now()->toDateString();
        $searchParams = $request->all();
        $purchaseQuery = Purchase::query();
        $from = Arr::get($searchParams, 'from', '');
        $to = Arr::get($searchParams, 'to', '');
        $firstDate = "";
        $lastDate = "";
        // search query
        if (!empty($from) && !empty($to)) {
            $purchaseItem =$purchaseQuery->whereBetween('entry_date', [$from, $to]);
        }
        if (empty($to) && !empty($from)) {
            $purchaseItem =$purchaseQuery->whereBetween('entry_date', [$from, $current_date]);
        }

        if (!empty($from)) {
            $purchaseItem = Purchase::where('entry_date', '>', $from)->get();
        }
        
        $purchaseItem = Purchase::orderBy('entry_date')->get();
  
        if(!$from && !$to){
            if(count($purchaseItem) > 1){
                $firstDate = $purchaseItem[0]->entry_date;
                $lastDate = $purchaseItem[count($purchaseItem)-1]->entry_date;
            }
      }
        return response()->json(['purchaseReport' => PurchaseResource::collection($purchaseItem),'firstDate' => $firstDate, 'lastDate' => $lastDate], 200); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // get validation rules from getValidationRules method
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            try {
                // create new purchase
                DB::beginTransaction();
                $purchase = new Purchase();
                $purchase->created_by = Auth::id();
                $purchase->entry_date = Carbon::createFromDate($request->entry_date)->toDateString();
                $this->savePurchaseData($purchase, $request);
                // insert purchase products
                DB::commit();
                return new PurchaseResource($purchase);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(['errors' => $e->getMessage()], 403);
            }
        }
    }

    // save purchase data

    public function savePurchaseData($purchase, $request)
    {
        $purchase->ref_number = $request->get('ref_number');
        $purchase->warehouse_id = $request->get('warehouse_id');
        $purchase->supplier_id = $request->get('supplier_id');
        $purchase->discount = $request->get('discount');
        $purchase->shipping_cost = $request->get('shipping_cost');
        $purchase->note = $request->get('note');
        $purchase->payment_condition = $request->get('payment_condition');
        $purchase->grand_total = $request->get('grand_total');
        $purchase->discount_type = $request->get('discount_type');
        $purchase->save();
        if ($request->products) {
            $products = $request->products;
            if (count($products) > 0) {
                $this->SavePurchaseItemData($purchase, $products);
            }
        }
    }

    // save purchase items
    public function SavePurchaseItemData($purchase, $products)
    {
        foreach ($products as $product) {
            if ($product['id']) {
                $PItem = PurchaseItem::find($product['id']);
                $PItem->purchase_id = $purchase->id;
                $PItem->product_id = $product['product_id'];
                $PItem->quantity = $product['quantity'];
                $PItem->received_quantity = $product['quantity'];
                $PItem->price = $product['price'];
                $PItem->discount = $product['discount'];
                $PItem->total = $product['total'];
                $PItem->save();
                AppHelper::updateWarehouseInventory($purchase->warehouse_id, $product, true);
            } else {
                $purchaseItem = new PurchaseItem();
                $purchaseItem->purchase_id = $purchase->id;
                $purchaseItem->product_id = $product['product_id'];
                $purchaseItem->quantity = $product['quantity'];
                $purchaseItem->received_quantity = $product['quantity'];
                $purchaseItem->price = $product['price'];
                $purchaseItem->discount = $product['discount'];
                $purchaseItem->total = $product['total'];
                $purchaseItem->save();
                AppHelper::updateWarehouseInventory($purchase->warehouse_id, $product, true);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function show(Purchase $purchase)
    {
        return new PurchaseResource($purchase);
    }

    /**
     * Showing all the purchase products.
     *
     * @author	Unknown
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	public
     * @param	mixed	$purchase
     * @return	void
     */
    public function purchaseProduct($purchase)
    {
        $purchaseItems = $purchase->purchaseItems;
        if (count($purchaseItems) > 0) {
            foreach ($purchaseItems as $purchaseItem) {
                $purchaseProduct = Product::where('id', $purchaseItem->product_id)->get();
                foreach ($purchaseProduct as $product) {
                    $purchaseItem['name'] = $product->name;
                    $purchaseItem['code'] = $product->code;
                    $purchaseItem['itemId'] = $product->id;
                    $purchaseItem['product'] = $product;
                };
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function edit(Purchase $purchase)
    {
        return new PurchaseResource($purchase);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Purchase $purchase)
    {
        // check requested purchase;
        if ($purchase === null) {
            return response()->json(['error' => 'purchase not found'], 404);
        }

        $validator = Validator::make($request->all(), $this->getValidationRules(false));
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {

            // update purchase data

            $this->savePurchaseData($purchase, $request);

            return new PurchaseResource($purchase);
        }
    }

    // public function receivePurchase($purchaseItem, $purchase)
    // {
    //     $orderStatus = 'received';
    //     // foreach ($purchaseItems as $purchaseItem) {
    //         $PItem = PurchaseItem::find($purchaseItem['id']);
    //         if (isset($PItem)) {
    //             $PItem->received_quantity = $purchaseItem['received_quantity'];
    //             if ($PItem->quantity > $PItem->received_quantity && $PItem->received_quantity != 0) {
    //                 $orderStatus = 'partial received';
    //                 $this->updateWarehouseInventory($PItem, $purchase);
    //                 $PItem->save();
    //             } else if ($PItem->quantity == $PItem->received_quantity) {
    //                 $this->updateWarehouseInventory($PItem, $purchase);
    //                 $PItem->save();
    //             }
    //         }
    //     // }

    //     $purchase->status = $orderStatus;
    //     $purchase->save();

    //     return new PurchaseResource($purchase);
    // }

    public function updateWarehouseInventory($PItem, $purchase)
    {

        $WIproduct = WarehouseInventory::where('product_id', $PItem->product_id)->where('warehouse_id', $purchase->warehouse_id)->first();
        if (isset($WIproduct)) {
            $WIproduct->quantity = $WIproduct->quantity + $PItem->received_quantity;
            $WIproduct->save();
        } else {
            $WIP = new WarehouseInventory();
            $WIP->product_id = $PItem->product_id;
            $WIP->warehouse_id = $purchase->warehouse_id;
            $WIP->quantity = $PItem->received_quantity;
            $WIP->save();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function destroy(Purchase $purchase)
    {
        //
    }
    /**
     * getValidationRules.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	private
     * @param	boolean	$isNew	Default: true
     * @return	array
     */
    private function getValidationRules($isNew = true)
    {
        return [
            'entry_date' => 'required|date',
            'supplier_id' => 'required',
            'warehouse_id' => 'required',
            'price' => ' numeric',
            'quantity' => 'numeric',
            'discount' => 'nullable|numeric',
            'shipping_cost' => 'nullable|numeric',
        ];
    }
}
