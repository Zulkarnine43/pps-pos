<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Laravue\Models\Employee;
use App\Laravue\Models\EmployeeAddress;
use App\Http\Resources\EmployeeResource;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Validator;
use File;
use Illuminate\Support\Facades\Storage;

class EmployeeController extends Controller
{
    const ITEM_PER_PAGE = 100;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $employeeQuery = Employee::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        // search query
        if (!empty($keyword)) {
            $employeeQuery->where('name', 'LIKE', '%' . $keyword . '%');
        }

        return EmployeeResource::collection($employeeQuery->orderBy('id', 'desc')->paginate($limit));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function getImage(Request $request){
        if($request->file('photo')){
         $file_name = time().'-'.$request->file('photo')->getClientOriginalName();
         $request->file('photo')->storePubliclyAs(
             'employees/images',
             $file_name,
             's3'
         );
         return response()->json(['imageName' => $file_name]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            if($request->image){
                $image = 'employees/images/'.$request->image;
            }
            else {
                $image = '';
            }

            $employee = Employee::create([
                'name' => $request->name,
                'nid_number' => $request->nid_number,
                'employee_id' => $request->employee_id,
                'email' => $request->email,
                'phone' => $request->phone,
                'designation' => $request->designation,
                'blood_group' => $request->blood_group,
                'joining_date' => date('Y-m-d', strtotime($request->joining_date)),
                'office_phone' => $request->office_phone,
                'emergency_phone' => $request->emergency_phone,
                'image' => $image,
                'gender' => $request->gender,
            ]);

            $address = $request->address;
            foreach ($address as $ads) {
                if (isset($ads)) {
                    foreach ($ads as $ad) {
                        $adrs = EmployeeAddress::create([
                            'address_type' => $ad['type'],
                            'country' => $ad['country'],
                            'state' => $ad['state'],
                            'district' => $ad['district'],
                            'city' => $ad['city'],
                            'zip_code' => $ad['zip_code'],
                            'street' => $ad['street'],
                            'employee_id' => $employee->id,
                        ]);
                    }
                }
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        return new EmployeeResource($employee);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        if ($employee === null) {
            return response()->json(['error' => 'Employee not found'], 404);
        }

        $employee_image = $employee->image;
        if($request->image) {
            if($employee_image){
                Storage::disk('s3')->delete("/employees/images/".$employee_image);
            }
        }

        $validator = Validator::make($request->all(), $this->getValidationRules(false));
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            // update employee data
            $employee->name = $request->get('name');
            $employee->nid_number = $request->get('nid_number');
            $employee->employee_id = $request->get('employee_id');
            $employee->phone = $request->get('phone');
            $employee->office_phone = $request->get('office_phone');
            $employee->emergency_phone = $request->get('emergency_phone');
            $employee->email = $request->get('email');
            $employee->designation = $request->get('designation');
            $employee->blood_group = $request->get('blood_group');
            $employee->gender = $request->get('gender');
            if($request->image){
                $employee->image = 'employees/images/'.$request->get('image');
            }
            $employee->joining_date = date('Y-m-d', strtotime($request->get('joining_date')));

            // update employee present address
            $pAdrs = $request->get('present');
            if (isset($pAdrs['id'])) {
                $present = EmployeeAddress::where('employee_id', $employee->id)->where('address_type', 'present')->where('id', $pAdrs['id'])->first();
                $present->country = $pAdrs['country'];
                $present->state = $pAdrs['state'];
                $present->district = $pAdrs['district'];
                $present->city = $pAdrs['city'];
                $present->zip_code = $pAdrs['zip_code'];
                $present->street = $pAdrs['street'];
                $present->save();
            }

            // update employee permanent address
            $perAdrs = $request->get('permanent');
            if (isset($perAdrs['id'])) {
                $permanent = EmployeeAddress::where('employee_id', $employee->id)->where('address_type', 'permanent')->where('id', $perAdrs['id'])->first();
                $permanent->country = $perAdrs['country'];
                $permanent->state = $perAdrs['state'];
                $permanent->district = $perAdrs['district'];
                $permanent->city = $perAdrs['city'];
                $permanent->zip_code = $perAdrs['zip_code'];
                $permanent->street = $perAdrs['street'];
                $permanent->save();
            }

            $employee->save();
            return new EmployeeResource($employee);
        }
    }

    public function inactive($id)
    {
        $employee = Employee::find($id);
        $employee->employee_status = 'inactive';
        $employee->save();
        return response()->json(['success' => 'Employee Suspended'], 200);
        
    }


    public function active($id)
    {
        $employee = Employee::find($id);
        $employee->employee_status = 'active';
        $employee->save();

        return response()->json(['success' => 'Employee Actived'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        //
    }


    private function getValidationRules($isNew = true)
    {
        return [
            'name' => 'required',
            // 'image' => 'required',
            'emergency_phone' => 'required',
            'email' => $isNew ? 'required|email|unique:employees' : 'required|email',
            'phone' => $isNew ? 'required|unique:employees' : 'required',
            'office_phone' => $isNew ? 'nullable|unique:employees' : 'nullable',
        ];
    }
}
