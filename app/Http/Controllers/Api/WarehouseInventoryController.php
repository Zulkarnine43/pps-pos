<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Laravue\Models\WarehouseInventory;
use App\Http\Resources\WarehouseInventoryResource;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class WarehouseInventoryController extends Controller
{
    const ITEM_PER_PAGE = 100;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $WIQuery = WarehouseInventory::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        // search query
        if (!empty($keyword)) {
            $WIQuery->where('name', 'LIKE', '%' . $keyword . '%');
        }
        $WIQuery->where('quantity', '>', 0);

        return WarehouseInventoryResource::collection($WIQuery->orderBy('id', 'desc')->paginate($limit));
    }

    public function checkProductInWarehouse($product_id){
       // dd($product_id);
        $product = WarehouseInventory::where('product_id', $product_id)->first();
        if($product && $product->quantity > 0){
            return response()->json([
                'status' => 'success',
                'data' => $product
            ]);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Product not found'
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\WarehouseInventory  $warehouseInventory
     * @return \Illuminate\Http\Response
     */
    public function show(WarehouseInventory $warehouseInventory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\WarehouseInventory  $warehouseInventory
     * @return \Illuminate\Http\Response
     */
    public function edit(WarehouseInventory $warehouseInventory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\WarehouseInventory  $warehouseInventory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WarehouseInventory $warehouseInventory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\WarehouseInventory  $warehouseInventory
     * @return \Illuminate\Http\Response
     */
    public function destroy(WarehouseInventory $warehouseInventory)
    {
        //
    }
}
