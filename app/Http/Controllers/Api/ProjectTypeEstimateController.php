<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProjectTypeEstimateItemResource;
use App\Http\Resources\ProjectTypeEstimateResource;
use App\Laravue\Models\ProjectTypeEstimate;
use App\Laravue\Models\ProjectTypeEstimateItem;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Validator;

class ProjectTypeEstimateController extends Controller
{

    const ITEM_PER_PAGE = 100;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $searchParams = $request->all();
        $projectTypeEstimateQuery = ProjectTypeEstimate::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        // search query
        if (!empty($keyword)) {
            $projectTypeEstimateQuery->where('name', 'LIKE', '%' . $keyword . '%');
        }

        return ProjectTypeEstimateResource::collection($projectTypeEstimateQuery->orderBy('id', 'desc')->paginate($limit));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // check valodation rules form getValidationRules method
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            $pte = ProjectTypeEstimate::where('project_type_id', $request->project_type_id)->first();
            if (isset($pte)) {
                return response()->json(['error' => 'Estimate is already created for this type'], 403);
            } else {;
                // create new project type estimation
                $projectTypeEstimate = new ProjectTypeEstimate();
                $projectTypeEstimate->project_type_id = $request->project_type_id;
                $projectTypeEstimate->save();
                if (isset($request->projectTypeEstimateItems)) {
                    foreach ($request->projectTypeEstimateItems as $item) {
                        $pteItem = new ProjectTypeEstimateItem();
                        $pteItem->project_type_estimate_id = $projectTypeEstimate->id;
                        $pteItem->product_id = $item['product_id'];
                        $pteItem->unit_id = $item['unit_id'];
                        $pteItem->quantity = $item['quantity'];
                        $pteItem->save();
                    }
                }
                return new ProjectTypeEstimateResource($projectTypeEstimate);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\ProjectTypeEstimate  $projectTypeEstimate
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectTypeEstimate $projectTypeEstimate)
    {
        return new ProjectTypeEstimateResource($projectTypeEstimate);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\ProjectTypeEstimate  $projectTypeEstimate
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectTypeEstimate $projectTypeEstimate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\ProjectTypeEstimate  $projectTypeEstimate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProjectTypeEstimate $projectTypeEstimate)
    {
        // check requested project type estimation
        if ($projectTypeEstimate === null) {
            return response()->json(['error' => 'Project Type Estimate not found'], 404);
        }


        $validator = Validator::make($request->all(), $this->getValidationRules(false));
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            // update requested project type estimation
            if (isset($request->projectTypeEstimateItems)) {
                foreach ($request->projectTypeEstimateItems as $item) {
                    if(isset($item['id'])){
                        $pteItem = ProjectTypeEstimateItem::find($item['id']);
                        $pteItem->unit_id = $item['unit_id'];
                        $pteItem->quantity = $item['quantity'];
                        $pteItem->save();
                    }else {
                        $pteItem = new ProjectTypeEstimateItem();
                        $pteItem->project_type_estimate_id = $projectTypeEstimate->id;
                        $pteItem->product_id = $item['product_id'];
                        $pteItem->unit_id = $item['unit_id'];
                        $pteItem->quantity = $item['quantity'];
                        $pteItem->save();
                    };
                }
            }

            return new ProjectTypeEstimateResource($projectTypeEstimate);
        }
    }


    /**
     * Unmute requested muted project type estimation.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	public
     * @param	mixed	$id
     * @return	mixed
     */
    public function unmute($id)
    {
        $projectTypeEstimate = ProjectTypeEstimate::find($id);

        if ($projectTypeEstimate === null) {
            return response()->json(['error' => 'Project Type Estimate not found'], 404);
        }

        $projectTypeEstimate->status = 0;

        $projectTypeEstimate->save();
        return new ProjectTypeEstimateResource($projectTypeEstimate);
    }

    /**
     * Mute requested active project type estimation.
     *
     * @author	Unknown
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	public
     * @param	mixed	$id
     * @return	mixed
     */
    public function mute($id)
    {
        $projectTypeEstimate = ProjectTypeEstimate::find($id);
        if ($projectTypeEstimate === null) {
            return response()->json(['error' => 'Project Type Estimate not found'], 404);
        }
        // dd($projectType);
        $projectTypeEstimate->status = 1;

        $projectTypeEstimate->save();

        return new ProjectTypeEstimateResource($projectTypeEstimate);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\ProjectTypeEstimate  $projectTypeEstimate
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectTypeEstimate $projectTypeEstimate)
    {
        //
    }

    public function itemDelete($id){
        $item = ProjectTypeEstimateItem::find($id);
        if(isset($item)){
            $item->delete();
            return response()->json(['success' => 'item removed'], 200);
        } else {
            return response()->json(['error' => 'item not found'], 404);
        };
    }

    /**
     * getValidationRules.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	private
     * @param	boolean	$isNew	Default: true
     * @return	array
     */
    private function getValidationRules($isNew = true)
    {
        return [
            'project_type_id' => 'required|numeric',
        ];
    }
}
