<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Laravue\Models\Brand;
use App\Laravue\Models\Category;
use App\Laravue\Models\Client;
use App\Laravue\Models\Department;
use App\Laravue\Models\Product;
use App\Laravue\Models\Project;
use App\Laravue\Models\ProjectStage;
use App\Laravue\Models\ProjectStatus;
use App\Laravue\Models\ProjectType;
use App\Laravue\Models\Subcontractor;
use App\Laravue\Models\Supplier;
use App\Laravue\Models\Unit;
use App\Laravue\Models\User;
use App\Laravue\Models\Warehouse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use File;
use Illuminate\Support\Facades\Storage;

class SuperUserDashboard extends Controller
{
    public function index(){
        $clients = Client::count();
        $users = User::count();
        $suppliers = Supplier::count();
        $subcontractors = Subcontractor::count();
        return response()->json(['clients' => $clients, 'users' => $users, 'suppliers' => $suppliers, 'subcontractors' => $subcontractors], 200);
    }

    /**
     * List all the active items.
     *
     * @author	Unknown
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	public
     * @return	mixed
     */
    public function activeItems(){
        $suppliers = Supplier::where('is_active', 'active')->orderBy('name', 'asc')->get();
        $warehouses = Warehouse::where('status', '0')->orderBy('name', 'asc')->get();
        $users = User::where('is_active', 'active')->orderBy('name', 'asc')->get();
        $clients = Client::where('is_active', 'active')->orderBy('name', 'asc')->get();
        $products = Product::where('status',0)->orderBy('name', 'asc')->get();
        $categories = Category::where('status', 0)->orderBy('name', 'asc')->get();
        $brands = Brand::where('status', 0)->orderBy('name', 'asc')->get();
        $units = Unit::where('status', 0)->orderBy('name', 'asc')->get();
        $projectTypes = ProjectType::where('status', 0)->orderBy('name', 'asc')->get();
        // $proejctStage = ProjectStage::where('status', 0)->orderBy('stage_name', 'asc')->get();
        $subcontractors = Subcontractor::where('is_active', 'active')->orderBy('name', 'asc')->get();
        $projects = Project::orderBy('name', 'asc')->get();
        $departments = Department::orderBy('name', 'asc')->where('status', 0)->get();
        $statuses = ProjectStatus::orderBy('name', 'asc')->get();

        return response()->json(['statuses' => $statuses,'departments' => $departments,'products' => $products , 'suppliers' => $suppliers, 'warehouses' => $warehouses, 'categories'=> $categories, 'brands'=> $brands, 'units'=> $units, 'projectTypes' => $projectTypes , 'users' => $users, 'clients' => $clients, 'projects' => $projects,'subcontractors' => $subcontractors], 200);
    }

    public function getTableLastID($model){
        $mdl =  "App\Laravue\Models\'".$model;
        $mdl = Str::replaceFirst("'", '', $mdl);
        $data = $mdl::all();
        if(count($data) > 0){
            $data = $data->last()->id;
        } else {
            $data = 0;
        }
        return response()->json($data);
    }

    public function getDatas(Request $request){
        $modelsData = [];
        $models = $request->all();
        foreach($models as $model){
            $mdl =  "App\Laravue\Models\'".$model['model'];
            $mdl = Str::replaceFirst("'", '', $mdl);
            $data = $mdl::all();
            array_push($modelsData, [$model['name'] => $data]);
        }
        return response()->json($modelsData);
    }

    public function uploadImage(Request $request, $folder){
        if($request->file('photo')){
            $file_name = 'pps-'.uniqid().'.'.$request->file('photo')->extension();
            $path = $request->file('photo')->storePubliclyAs(
                'images/'.$folder,
                $file_name,
                's3'
            );
            return response()->json(['imageName' => 'images/'.$folder.'/'.$file_name]);
        }
    }
    public function removeUnsavedFile($file_group, $folder, $file_name){
        $img = $file_group.'/'.$folder.'/'.$file_name;
        if ($img){
            Storage::disk('s3')->delete($img);
        }
    }
}
