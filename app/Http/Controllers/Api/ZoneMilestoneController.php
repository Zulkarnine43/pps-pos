<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Laravue\Models\ZoneMilestone;
use Illuminate\Http\Request;

class ZoneMilestoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\ZoneMilestone  $zoneMilestone
     * @return \Illuminate\Http\Response
     */
    public function show(ZoneMilestone $zoneMilestone)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\ZoneMilestone  $zoneMilestone
     * @return \Illuminate\Http\Response
     */
    public function edit(ZoneMilestone $zoneMilestone)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\ZoneMilestone  $zoneMilestone
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ZoneMilestone $zoneMilestone)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\ZoneMilestone  $zoneMilestone
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $zoneMilestone = ZoneMilestone::find($id);
        if ($zoneMilestone) {
            $zoneMilestone->delete();
            return response()->json(['success' => true]);
        }
    }
}
