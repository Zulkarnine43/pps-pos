<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Laravue\Models\SubcontractorBill;
use App\Http\Resources\SubcontractorBillResource;
use App\Laravue\Models\SubcontractorBillItem;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Validator;
use Auth;

class SubcontractorBillController extends Controller
{
    const ITEM_PER_PAGE = 100;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $subcontractorBillQuery = SubcontractorBill::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        $subcontractor_id = Arr::get($searchParams, 'subcontractor_id', '');
        $bill_status = Arr::get($searchParams, 'bill_status', '');
        $date = Arr::get($searchParams, 'date', '');
        // search query
        
        if (!empty($keyword)) {
            $subcontractorBillQuery->where('bill_id', 'LIKE', '%' . $keyword . '%');
        }
        if (!empty($subcontractor_id)) {
            $subcontractorBillQuery->where('subcontractor_id', $subcontractor_id);
        }
        if (!empty($date)) {
            $subcontractorBillQuery->where('payment_date', 'LIKE', '%' . $date . '%')
                                   ->orwhere('entry_date', 'LIKE', '%' . $date . '%')
                                   ->orwhere('checked_date', 'LIKE', '%' . $date . '%')
                                   ->orwhere('approved_date', 'LIKE', '%' . $date . '%');
        }
        if (!empty($bill_status)) {
            if($bill_status !== 'all'){
                $subcontractorBillQuery->where('bill_status', $bill_status);
            }
        }

        return SubcontractorBillResource::collection($subcontractorBillQuery->orderBy('id', 'DESC')->paginate($limit));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            $bill = new SubcontractorBill();
            $bill->received_by = Auth::id();
            $bill->subcontractor_id = $request->subcontractor_id;
            $bill->bill_id = "BRS-" .$request->bill_id;
            $bill->bill_status = 'received';
            $bill->entry_date = $request->entry_date;
            $bill->submited_by = $request->submited_by;
            $bill->submited_by_phone_number = $request->submited_by_phone_number;
            $bill->total_receive_amount = $request->total_receive_amount;
            if(count($request->BillRows) > 0){
                $bill->save();
                $items = $request->BillRows;
                foreach($items as $item){
                    if($item['project_type'] !== null){
                        $billItem = new SubcontractorBillItem();
                        $billItem->subcontractor_bill_id = $bill->id;
                        $billItem->payment_details = $item['details'];
                        $billItem->work_order_id = $item['work_order_id'];
                        $billItem->remarks = $item['remarks'];
                        $billItem->amount = $item['amount'];
                        $billItem->stage_id = $item['stage_id'];
                        $billItem->project_id = $item['project_id'];
                        $billItem->project_subcontract_id = $item['project_subcontract_id'];
                        $billItem->save();
                    }
                }
            }else {
                return response()->json(['errors' => $validator->errors()], 403);
            }

            return new SubcontractorBillResource($bill);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\SubcontractorBill  $subcontractorBill
     * @return \Illuminate\Http\Response
     */
    public function show(SubcontractorBill $subcontractorBill)
    {
        return new SubcontractorBillResource($subcontractorBill);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\SubcontractorBill  $subcontractorBill
     * @return \Illuminate\Http\Response
     */
    public function edit(SubcontractorBill $subcontractorBill)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\SubcontractorBill  $subcontractorBill
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubcontractorBill $subcontractorBill)
    {
        $subcontractorBill->total_approve_amount = $request->total_approve_amount;
        $subcontractorBill->save();
        if(count($request->billItems) > 0){
            $items = $request->billItems;
            foreach($items as $item){
                $billItem = SubcontractorBillItem::find($item['id']);
                $billItem->payment_details = $item['details'];
                $billItem->sd = $item['sd'];
                $billItem->deduction = $item['deduction'];
                $billItem->cheque_amount = $item['cheque_amount'];
                $billItem->amount = $item['amount'];
                $billItem->save();
            }
            if($subcontractorBill->checked_by === null){
                $subcontractorBill->bill_status = 'checked';
                $subcontractorBill->checked_by = Auth::id();
                $subcontractorBill->checked_date = $request->checked_date;
                $subcontractorBill->save();
            } else {
                $subcontractorBill->bill_status = 'approved';
                $subcontractorBill->approved_by = Auth::id();
                $subcontractorBill->approved_date = $request->approved_date;
                $subcontractorBill->save();
            }
        }else {
            return response()->json('errors', 403);
        }
    }

    public function checked(Request $request, $id){
        dd($request);
        $bill = SubcontractorBill::find($id);

        $bill->bill_status = 'checked';
        $bill->checked_by = Auth::id();
        $bill->save();

        return response()->json('success', 200);
    }

    public function approved($id){
        $bill = SubcontractorBill::find($id);

        $bill->bill_status = 'approved';
        $bill->approved_by = Auth::id();
        $bill->save();

        return response()->json('success', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\SubcontractorBill  $subcontractorBill
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubcontractorBill $subcontractorBill)
    {
        if($subcontractorBill === null){
            return response()->json(['error' => 'subcontractor bill not found'], 404);
        } else {
            $subcontractorBill->bill_status = 'canceled';
            $subcontractorBill->save();

            return new SubcontractorBillResource($subcontractorBill);
        }
    }

    private function getValidationRules($isNew = true)
    {
        return [
            'subcontractor_id' => 'required',
        ];
    }
}
