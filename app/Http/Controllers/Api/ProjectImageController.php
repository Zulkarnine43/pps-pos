<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProjectStageResource;
use App\Laravue\Models\ProjectImage;
use Illuminate\Http\Request;
use File;

class ProjectImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * getImage.
     *
     * @author	Unknown
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	public
     * @param	mixed	$pid
     * @param	mixed	$sid
     * @return	all the project stage images
     */
    public function getImage($pid,$sid){
        $images = ProjectImage::where('project_id', $pid)->where('project_stage_id', $sid)->orderBy('id', 'desc')->get();
        return response()->json(['images' => $images], 200);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {



    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\ProjectImage  $projectImage
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectImage $projectImage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\ProjectImage  $projectImage
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectImage $projectImage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\ProjectImage  $projectImage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProjectImage $projectImage)
    {
        //
    }

    /**
     * Remove project stage image.
     *
     * @param  \App\Laravue\Models\ProjectImage  $projectImage
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        dd(public_path());
        $image = ProjectImage::find($id);
        
        if($image){
            $imgPath =  public_path() . '/images/project/';
            
            $img = $imgPath . $image->image;
            if (file_exists($img)) {
                @unlink($img);
            }
        }
        $image->delete();
        return new ProjectStageResource($image);
    }
}
