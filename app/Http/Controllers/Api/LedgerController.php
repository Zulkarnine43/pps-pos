<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\JournalItemResource;
use App\Laravue\Models\Ledger;
use Illuminate\Http\Request;
use App\Http\Resources\LedgerResource;
use App\Laravue\Models\JournalItem;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Validator;

class LedgerController extends Controller
{
    const ITEM_PER_PAGE = 1000;
    public $oldJournals = [];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $ledgerQuery = Ledger::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        // search query
        if (!empty($keyword)) {
            $ledgerQuery->where('name', 'LIKE', '%' . $keyword . '%');
        }

        return LedgerResource::collection($ledgerQuery->orderBy('id', 'desc')->paginate($limit));
    }

    public function viewReport(Request $request){

        $current_date = Carbon::now()->toDateTimeString();
        $searchParams = $request->all();
        $jorunalQuery = JournalItem::query();
        $from = Arr::get($searchParams, 'from', '');
        $to = Arr::get($searchParams, 'to', '');
        $openingBalance = ["balance" => 0, "type" => ""];
        $totalDr = 0;
        $totalCr = 0;
        // search query
        if (!empty($from) && !empty($to)) {
            $jorunalQuery->whereBetween('trans_date', [$from, $to]);
        }
        if (empty($to)) {
            $jorunalQuery->whereBetween('trans_date', [$from, $current_date]);
        }
        if (!empty($from)) {
            
            $ledgers = JournalItem::where('ledger_id', $request->ledger_id)->where('trans_date', '<', $from)->get();
            foreach($ledgers as $ledger){
                if($ledger->debit_amount === 0){
                    $totalCr = $totalCr + $ledger->credit_amount;
                } else {
                    $totalDr = $totalDr + $ledger->debit_amount;
                };
            }
            if($totalCr > $totalDr){
                $openingBalance['balance'] = $totalCr - $totalDr;
                $openingBalance['type'] = "Cr";
            } else {
                $openingBalance['balance'] = $totalDr - $totalCr;
                $openingBalance['type'] = "Dr";
            }
        }
        
        $journals = $jorunalQuery->where('ledger_id', $request->ledger_id)->orderBy('trans_date')->get();
        return response()->json(['journals' => JournalItemResource::collection($journals), 'openingBalance' =>$openingBalance], 200);

    }

    public function viewTrailBalanceReport(Request $request){
        $current_date = Carbon::now()->toDateString();
        $searchParams = $request->all();
        $jorunalQuery = JournalItem::query();
        $from = Arr::get($searchParams, 'from', '');
        $to = Arr::get($searchParams, 'to', '');
        $firstDate = "";
        $lastDate = "";
        // search query
        if (!empty($from) && !empty($to)) {
            $jorunalQuery->whereBetween('trans_date', [$from, $to]);
        }
        if (empty($to)) {
            $jorunalQuery->whereBetween('trans_date', [$from, $current_date]);
        }

        if (!empty($from)) {
            $this->oldJournals = JournalItem::where('trans_date', '<', $from)->get();
        }
        
        $journals = $jorunalQuery->orderBy('trans_date')->get();
        
        if(count($journals) > 1){
            $firstDate = $journals[0]->trans_date;
            $lastDate = $journals[count($journals)-1]->trans_date;
        }
        return response()->json(['journals' => JournalItemResource::collection($journals)->groupBy('ledger_id'), 'old_journals' => JournalItemResource::collection($this->oldJournals)->groupBy('ledger_id'), 'firstDate' => $firstDate, 'lastDate' => $lastDate], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  get validation rules from getValidationRules method
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules($isNew = true, $ledger = null),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            $ledger = new Ledger();
            $ledger->name = $request->name;
            $ledger->group_id = $request->group_id;
            $ledger->currency = $request->currency;
            $ledger->cost_center = $request->cost_center;
            $ledger->email = $request->email;
            $ledger->phone = $request->phone;
            $ledger->address = $request->address;
            $ledger->comments = $request->comments;
            $ledger->inactive = $request->inactive;
            $ledger->post_code = $request->post_code;
            $ledger->city = $request->city;
            $ledger->type = $request->type;
            $ledger->relation_id = $request->relation_id;
            $ledger->save();

            return new LedgerResource($ledger);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\Ledger  $ledger
     * @return \Illuminate\Http\Response
     */
    public function show(Ledger $ledger)
    {
        return new LedgerResource($ledger);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\Ledger  $ledger
     * @return \Illuminate\Http\Response
     */
    public function edit(Ledger $ledger)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\Ledger  $ledger
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ledger $ledger)
    {
        //  get validation rules from getValidationRules method
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules($isNew = false, $ledger),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {

            $ledger->name = $request->name;
            $ledger->group_id = $request->group_id;
            $ledger->currency = $request->currency;
            $ledger->cost_center = $request->cost_center;
            $ledger->email = $request->email;
            $ledger->phone = $request->phone;
            $ledger->address = $request->address;
            $ledger->comments = $request->comments;
            $ledger->inactive = $request->inactive;
            $ledger->post_code = $request->post_code;
            $ledger->city = $request->city;
            $ledger->type = $request->type;
            $ledger->relation_id = $request->relation_id;
            $ledger->save();

            return new LedgerResource($ledger);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\Ledger  $ledger
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ledger $ledger)
    {
        //
    }

    /**
     * getValidationRules.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	private
     * @param	boolean	$isNew	Default: true
     * @return	array
     */
    private function getValidationRules($isNew = true, $ledger)
    {
        return [
            'name' => $isNew ? 'required|unique:ledgers' : 'required|unique:ledgers,name,'.$ledger->id,
            'group_id' => 'required',
        ];
    }
}
