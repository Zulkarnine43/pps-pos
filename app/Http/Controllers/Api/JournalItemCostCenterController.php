<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\JournalItemCostCenterResource;
use App\Laravue\Models\JournalItemCostCenter;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class JournalItemCostCenterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $costCenters = [];
        $searchParams = $request->all();
        $keyword = $searchParams[0];
        if(!empty($keyword)){
            $costCenters = JournalItemCostCenter::where('site_code',  'LIKE', '%' . $keyword . '%')->orderBy('site_code')->get();
        }

        return JournalItemCostCenterResource::collection($costCenters);
    }

    public function viewReport(Request $request){
        $costCenters = [];
        $searchParams = $request->all();
        $site_code = Arr::get($searchParams, 'site_code', '');
        // search query
        if (!empty($site_code)) {
            $costCenters = JournalItemCostCenter::where('site_code', $site_code)->orderBy('id', 'desc')->get();
        }

        return JournalItemCostCenterResource::collection($costCenters);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\JournalItemCostCenter  $journalItemCostCenter
     * @return \Illuminate\Http\Response
     */
    public function show(JournalItemCostCenter $journalItemCostCenter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\JournalItemCostCenter  $journalItemCostCenter
     * @return \Illuminate\Http\Response
     */
    public function edit(JournalItemCostCenter $journalItemCostCenter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\JournalItemCostCenter  $journalItemCostCenter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, JournalItemCostCenter $journalItemCostCenter)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\JournalItemCostCenter  $journalItemCostCenter
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cost = JournalItemCostCenter::find($id);
        $cost->delete();
        return response()->json('success', 200);
    }
}
