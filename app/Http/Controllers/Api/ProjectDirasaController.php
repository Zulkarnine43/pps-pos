<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Laravue\Models\ProjectDirasa;
use Illuminate\Http\Request;
use App\Http\Resources\ProjectDirasaResource;
use App\Laravue\Models\Project;
use App\Laravue\Models\ProjectRequisition;
use Validator;
use Illuminate\Support\Arr;

class ProjectDirasaController extends Controller
{
    const ITEM_PER_PAGE = 100;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $requisitionQuery = ProjectDirasa::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        $client_id = Arr::get($searchParams, 'client_id', '');
        $project_type_id = Arr::get($searchParams, 'project_type_id', '');
        $status = Arr::get($searchParams, 'status', '');
        // search query

        if(!empty($project_type_id)){
            $requisitionQuery->where('project_type_id', $project_type_id);
        }
        if(!empty($client_id)){
            $requisitionQuery->where('client_id', $client_id);
        }
        if (!empty($keyword)) {
            $requisitionQuery->where('name', 'LIKE', '%' . $keyword . '%')
                            ->orWhere('phone', 'LIKE', '%' . $keyword . '%')
                            ->orWhere('pdc_number', 'LIKE', '%' . $keyword . '%')
                            ->orWhere('prs_number', 'LIKE', '%' . $keyword . '%')
                            ->orWhere('bn_address', 'LIKE', '%' . $keyword . '%')
                            ->orWhere('gps', 'LIKE', '%' . $keyword . '%')
                            ->orWhere('nid_number', 'LIKE', '%' . $keyword . '%')
                            ->orWhere('department_name', 'LIKE', '%' . $keyword . '%')
                            ->orWhere('site_code', 'LIKE', '%' . $keyword . '%')
                            ->orWhere('location', 'LIKE', '%' . $keyword . '%')
                            ->orWhere('requisition_by', 'LIKE', '%' . $keyword . '%');
        }

        return ProjectDirasaResource::collection($requisitionQuery->orderBy('id', 'desc')->where('status', $status)->paginate($limit));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {

            // create new dirasa

            $dirasa = new ProjectDirasa();
            $dirasa->name = $request->name;
            $dirasa->pdc_number = $request->pdc_number;
            $dirasa->prs_number = $request->prs_number;
            $dirasa->phone = $request->phone;
            $dirasa->location = $request->location;
            $dirasa->details = $request->details;
            $dirasa->remark = $request->remark;
            $dirasa->bn_address = $request->bn_address;
            $dirasa->requisition_by = $request->requisition_by;
            $dirasa->client_id = $request->client_id;
            $dirasa->department_name = $request->department_name;
            $dirasa->gps = $request->gps;
            $dirasa->site_code = $request->site_code;
            $dirasa->dirasa_date = $request->dirasa_date;
            $dirasa->nid_number = $request->nid_number;
            $dirasa->country_id = $request->country_id;
            $dirasa->state_id = $request->state_id;
            $dirasa->district_id = $request->district_id;
            $dirasa->city_id = $request->city_id;
            if($request->quantity){
                $dirasa->quantity = $request->quantity;
            }
            $dirasa->project_type_id = $request->project_type_id;
            $dirasa->save();
            return new ProjectDirasaResource($dirasa);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\ProjectDirasa  $projectDirasa
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectDirasa $projectDirasa)
    {
        return new ProjectDirasaResource($projectDirasa);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\ProjectDirasa  $projectDirasa
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectDirasa $projectDirasa)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\ProjectDirasa  $projectDirasa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProjectDirasa $projectDirasa)
    {
        if ($projectDirasa === null) {
            return response()->json(['error' => 'Dirasa not found'], 404);
        }

        $validator = Validator::make($request->all(), $this->getValidationRules(false));
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            //  update unit data
            $projectDirasa->name = $request->name;
            $projectDirasa->prs_number = $request->prs_number;
            $projectDirasa->client_id = $request->client_id;
            $projectDirasa->department_name = $request->department_name;
            $projectDirasa->phone = $request->phone;
            $projectDirasa->location = $request->location;
            $projectDirasa->details = $request->details;
            $projectDirasa->remark = $request->remark;
            $projectDirasa->bn_address = $request->bn_address;
            $projectDirasa->requisition_by = $request->requisition_by;
            $projectDirasa->gps = $request->gps;
            $projectDirasa->site_code = $request->site_code;
            $projectDirasa->dirasa_date = $request->dirasa_date;
            $projectDirasa->nid_number = $request->nid_number;
            $projectDirasa->quantity = $request->quantity;
            $projectDirasa->project_type_id = $request->project_type_id;
            $projectDirasa->country_id = $request->country_id;
            $projectDirasa->state_id = $request->state_id;
            $projectDirasa->district_id = $request->district_id;
            $projectDirasa->city_id = $request->city_id;
            $projectDirasa->save();
            return new ProjectDirasaResource($projectDirasa);
        }
    }

    public function setSiteCode(Request $request, $id){
        if($request->site_code){
            $dirasa = ProjectDirasa::find($id);
            if($dirasa){
                $dirasa->site_code = $request->site_code;
                $dirasa->status = 'completed';
                $dirasa->save();
                $project = Project::where('site_code', $dirasa->site_code)->first();
                if(isset($project)){
                    $project->project_dirasa_id = $dirasa->id;
                    $project->save();
                }
                if($dirasa->prs_number){
                    $this->updateProjectRequisition($dirasa);
                }
            }else {
                return response()->json(['error' => 'Requisition not found'], 404);
            }
        }
    }

    public function updateProjectRequisition($dirasa){
        $requisition = ProjectRequisition::where('prs_number', $dirasa->prs_number)->first();
            if($requisition){
                $requisition->site_code = $dirasa->site_code;
                $requisition->status = 'completed';
                $requisition->save();
                $project = Project::where('site_code', $dirasa->site_code)->first();
                if(isset($project)){
                    $project->project_requisition_id = $requisition->id;
                    $project->location = $requisition->location;
                    $project->bn_address = $requisition->bn_address;
                    $project->gps = $requisition->gps;
                    $project->name = $requisition->name;
                    $project->remark = $requisition->remark;
                    $project->country_id = $requisition->country_id;
                    $project->state_id = $requisition->state_id;
                    $project->district_id = $requisition->district_id;
                    $project->city_id = $requisition->city_id;
                    
                    $project->save();
                }
            }else {
                return response()->json(['error' => 'Requisition not found'], 404);
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\ProjectDirasa  $projectDirasa
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectDirasa $projectDirasa)
    {
        //
    }

    private function getValidationRules($isNew = true)
    {
        return [
            'name' => 'required',
            'phone' => 'required',
            'dirasa_date' => 'required',
            'requisition_by' => 'required',
            'nid_number' => 'required',
            'project_type_id' => 'required',
            'quantity' => 'numeric|nullable',
            'pdc_number' => $isNew ? 'required|unique:project_dirasas' : 'required',
        ];
    }
}
