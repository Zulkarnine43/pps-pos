<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Laravue\Models\ProjectMeta;
use Illuminate\Http\Request;


class ProjectMetaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\ProjectMeta  $projectMeta
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectMeta $projectMeta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\ProjectMeta  $projectMeta
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectMeta $projectMeta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\ProjectMeta  $projectMeta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProjectMeta $projectMeta)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\ProjectMeta  $projectMeta
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectMeta $projectMeta)
    {
        //
    }
}
