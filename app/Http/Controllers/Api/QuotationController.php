<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Laravue\Models\Quotation;
use App\Laravue\Models\QuotationItems;
use App\Http\Resources\QuotationResource;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Validator;
use Auth;

class QuotationController extends Controller
{
    const ITEM_PER_PAGE = 100;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $quatationQuery = Quotation::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        // search query
        if (!empty($keyword)) {
            $quatationQuery->where('client_id', $keyword);
        }

        return QuotationResource::collection($quatationQuery->orderBy('id', 'DESC')->paginate($limit));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // get validation rules from getValidationRules method
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            // create new quotation
            $quotation = Quotation::create([
                'delivery_date' => $request->delivery_date,
                'user_id' => Auth::id(),
                'client_id' => $request->client_id,
                'ref_number' => $request->ref_number,
                'discount' => $request->discount,
                'shipping_cost' => $request->shipping_cost,
                'note' => $request->note,
                'grand_total' => $request->grandTotal,
                'discount_type' => $request->discount_type,
            ]);
            if(isset($request->entry_date)){
                $quotation->entry_date = $request->entry_date;
            }else {
                $quotation->entry_date = date("Y-m-d");
            }
            $quotation->save();
            // insert quotation products
            if ($request->products) {
                $products = $request->products;
                if(count($products) > 0){
                    foreach ($products as $product) {
                        if (isset($product)) {
                            $quotationItem = new QuotationItems();
                            $quotationItem->quotation_id = $quotation->id;
                            $quotationItem->product_id = $product['id'];
                            $quotationItem->quantity = $product['quantity'];
                            $quotationItem->price = $product['price'];
                            $quotationItem->discount = $product['discount'];
                            $quotationItem->total = $product['total'];
                            $quotationItem->save();
                        }
                    }
                }
            }
            return new QuotationResource($quotation);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\Quotation  $quotation
     * @return \Illuminate\Http\Response
     */
    public function show(Quotation $quotation)
    {
        return new QuotationResource($quotation);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\Quotation  $quotation
     * @return \Illuminate\Http\Response
     */
    public function edit(Quotation $quotation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\Quotation  $quotation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Quotation $quotation)
    {
        if ($quotation === null) {
            return response()->json(['error' => 'quotation not found'], 404);
        }

        $validator = Validator::make($request->all(), $this->getValidationRules(false));
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        }else{
            $quotation->entry_date = $request->get('entry_date');
            $quotation->client_id = $request->get('client_id');
            $quotation->discount = $request->get('discount');
            $quotation->ref_number = $request->get('ref_number');
            $quotation->shipping_cost = $request->get('shipping_cost');
            $quotation->note = $request->get('note');
            $quotation->grand_total = $request->get('grand_total');
            $quotation->discount_type = $request->get('discount_type');
            // update listItems 
            if ($request->get('listItems')) {
                $listItems = $request->get('listItems');
                if(count($listItems) > 0){
                    foreach ($listItems as $item) {
                        if (isset($item)) {
                            if (isset($item['id'])) {
                                // updating exsisting purchase items
                                $this->PItem = QuotationItems::find($item['id']);
                                $this->PItem->quotation_id = $quotation->id;
                                $this->PItem->product_id = $item['product_id'];
                                $this->PItem->quantity = $item['quantity'];
                                $this->PItem->price = $item['price'];
                                $this->PItem->discount = $item['discount'];
                                $this->PItem->total = $item['total'];
                                $this->PItem->save();
                            } else {
                                // insert new purchase item
                                $this->PItem = new QuotationItems();
                                $this->PItem->quotation_id = $quotation->id;
                                $this->PItem->product_id = $item['product']['id'];
                                $this->PItem->quantity = $item['quantity'];
                                $this->PItem->price = $item['product']['price'];
                                $this->PItem->discount = $item['discount'];
                                $this->PItem->total = $item['total'];
                                $this->PItem->save();
                            }
                        }
                    }
                }
            }
            $quotation->save();
            return new QuotationResource($quotation);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\Quotation  $quotation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Quotation $quotation)
    {
        //
    }


    private function getValidationRules($isNew = true)
    {
        return [
            'entry_date' => 'nullable|date',
            'delivery_date' => 'nullable|date',
            'client_id' => 'required',
        ];
    }
}
