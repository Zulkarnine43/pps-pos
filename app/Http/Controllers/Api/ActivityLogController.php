<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ActivityLogResource;
use App\Laravue\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Spatie\Activitylog\Models\Activity;

/**
 * ActivityLogController.
 *
 * @author	bitbyte
 * @author	Unknown
 * @since	v0.0.1
 * @version	v1.0.0	Tuesday, February 9th, 2021.
 * @see		Controller
 * @global
 */
class ActivityLogController extends Controller{
    const ITEM_PER_PAGE = 100;
    public $causer;


    /**
     * index.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	public
     * @param	request	$request
     * @return	activity list
     */
    public function index(Request $request){
        $searchParams = $request->all();
        $activityQuery = Activity::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        $user = Arr::get($searchParams, 'user', '');
        $date = Arr::get($searchParams, 'date', '');
        // dd($date);

        /**
         * @var		mixed	!empty($dat
         * return search result
         */
        if (!empty($date)) {
            $from = $date['0'];
            $to = $date['1'];
            $activityQuery->whereBetween('updated_at', [$from, $to]);
        }
        if (!empty($user)) {
            $activityQuery->where('causer_id', 'LIKE', '%' . $user . '%');
        }
        if (!empty($keyword)) {
            $activityQuery->where('log_name', 'LIKE', '%' . $keyword . '%');
        }
        /**
         * ActivityLogResource::collection.
         *
         * @author	author
         * @since	v0.0.1
         * @version	v1.0.0	Tuesday, February 9th, 2021.
         * @param	mixed	$activityQuery->orderBY('id'
         * @param	mixed	DESC')->paginate($limit)
         * @return	void
         */
        return ActivityLogResource::collection($activityQuery->orderBY('id', 'DESC')->paginate($limit));
    }
}
