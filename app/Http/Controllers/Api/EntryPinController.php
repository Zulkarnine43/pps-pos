<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\EntryPinResource;
use App\Laravue\Models\EntryPin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class EntryPinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\EntryPin  $entryPin
     * @return \Illuminate\Http\Response
     * showing the entry pin
     */
    public function show($id)
    {
        $entryPin = EntryPin::find($id);
        return new EntryPinResource($entryPin);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\EntryPin  $entryPin
     * @return \Illuminate\Http\Response
     */
    public function edit(EntryPin $entryPin)
    {
        // return new EntryPinResource($entryPin);
    }

    /**
     * checkPin.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	public
     * @param	mixed	$givenpin
     * @return	void
     * check exsiting pin
     */
    public function checkPin($givenpin){
        $pin = EntryPin::first();
        if(Hash::check($givenpin, $pin->entry_pin)){
            return response()->json(['hash_pin' => $pin->entry_pin], 200);
        }else {
            return response()->json('Pin dose not found', 404);
        }
    }

    /**
     * getPin.
     *
     * @author	Unknown
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	public
     * @return	get the pin
     */
    public function getPin(){
        $pin = EntryPin::first();
        return response()->json(['hash_pin' => $pin->entry_pin], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\EntryPin  $entryPin
     * @return \Illuminate\Http\Response
     * update requested pin
     */
    public function update(Request $request, $id){
        $entryPin = EntryPin::find($id);
        if(Str::length($request->entry_pin) > 4){
            return response()->json('Pin Must Be Not More Than 4', 500);
        }else{
            $entryPin->entry_pin = Hash::make($request->entry_pin);
            $entryPin->save();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\EntryPin  $entryPin
     * @return \Illuminate\Http\Response
     */
    public function destroy(EntryPin $entryPin)
    {
        //
    }
}
