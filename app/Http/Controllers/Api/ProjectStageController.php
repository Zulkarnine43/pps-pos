<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProjectResource;
use App\Http\Resources\ProjectStageResource;
use App\Laravue\Models\Project;
use App\Laravue\Models\ProjectImage;
use App\Laravue\Models\ProjectStage;
use App\Laravue\Models\TypeStage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;
use Validator;

class ProjectStageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projectStageQuery = ProjectStage::query();
        return ProjectStageResource::collection($projectStageQuery->orderBy('id', 'desc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        dd($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\ProjectStage  $projectStage
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectStage $projectStage)
    {
        return new ProjectStageResource($projectStage);
    }


    public function getCurrentStage($sid){
        $current = ProjectStage::where('id', $sid)->first();
        return new ProjectStageResource($current);
        // return response()->json(['current' => $current], 200);
    }


    /**
     * Display the Stage accorfing to project
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	public
     * @param	mixed	$id
     * @return	mixed
     */
    public function getStage($id)
    {
        $stages = ProjectStage::where('project_id', $id)->get();
        return ProjectStageResource::collection($stages);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\ProjectStage  $projectStage
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectStage $projectStage)
    {
        return new ProjectStageResource($projectStage);
    }

    public function getImage(Request $request, $pid , $sid){

        if($request->file('photo')){
         $file_name = time().'-'.$request->file('photo')->getClientOriginalName();
         $request->file('photo')->storePubliclyAs(
            'stages/images',
            $file_name,
            's3'
         );
         $p_image = new ProjectImage();
         $p_image->project_id = $pid;
         $p_image->project_stage_id = $sid;
         $p_image->image = 'stages/images/'.$file_name;
         $p_image->save();
         return response()->json(['image' => $p_image], 200);
        }
     }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\ProjectStage  $projectStage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProjectStage $projectStage)
    {
        // check requested unit
        if ($projectStage === null) {
            return response()->json(['error' => 'project stage not found'], 404);
        }

        $validator = Validator::make($request->all(), $this->getValidationRules(false));
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            $projectStage->start_date = $request->start_date;
            $projectStage->end_date = $request->end_date;
            $projectStage->finished_date = $request->finished_date;
            $projectStage->stage_amount = $request->stage_amount;
            $projectStage->work_done = $request->work_done;
            // $projectStage->finished_date = $request->finished_date;
        
            $projectStage->save();
            return new ProjectStageResource($projectStage);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\ProjectStage  $projectStage
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectStage $projectStage)
    {
        //
    }

    /**
     * deleteImage.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	public
     * @param	projectstage	$projectStage
     * @param	mixed       	$pid
     * @param	mixed       	$ImageId
     * @return	mixed
     */
    public function deleteImage(ProjectStage $projectStage,$pid, $sid, $ImageId){
        // delete project stage image
        $image = ProjectImage::where('project_id', $pid)->where('project_stage_id', $sid)->where('id', $ImageId)->first();
        $imgName = $image->image;
        if ($image) {
            if (Storage::disk('s3')->exists($imgName)) {
                Storage::disk('s3')->delete($imgName);
                $image->delete();
            }
        }

        return new ProjectStageResource($projectStage);

    }
    /**
     * go next stage
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	public
     * @param	mixed	$pid
     * @param	mixed	$next_id
     * @param	mixed	$sid
     * @return	void
     */
    public function goNext($pid,$next_id, $sid){
        $project = Project::where('id', $pid)->first();
        $stage = ProjectStage::where('id', $sid)->first();
        if($stage->image_required == 'Yes'){
            // check stage image for go next stage
            $projectImages = ProjectImage::where('project_id', $pid)->where('project_stage_id', $sid)->get();
            if(count($projectImages) > 0){
                // if image found
                if ($project->stage_id < $next_id) {
                    $project->stage_id = $next_id;
                    $project->save();
                    return new ProjectResource($project);
                } else if ($project->stage_id == $next_id) {
                    // if project stage will be the last stage the change status for finish the project
                    $project->status = 1;
                    $project->save();
                    return new ProjectResource($project);
                }
            }else {
                // if image not found
                return response()->json(['error' => 'Please Upload Project Image For This Stage'], 422);
            }
        }else {
            // if image was not required for this stage
            if ($project->stage_id < $next_id) {
                $project->stage_id = $next_id;
                $project->save();
                return new ProjectResource($project);
            } else if ($project->stage_id == $next_id) {
                // if project stage will be the last stage the change status for finish the project
                $project->status = 1;
                $project->save();
                return new ProjectResource($project);
            }
        }
    }

    /**
     * getValidationRules.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	private
     * @param	boolean	$isNew	Default: true
     * @return	array
     */
    private function getValidationRules($isNew = true)
    {
        return [
            'start_date' => 'required',
            'end_date' => 'required',
        ];
    }
}
