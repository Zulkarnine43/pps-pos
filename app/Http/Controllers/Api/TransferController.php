<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\TransferResource;
use App\Laravue\Models\Delivery;
use App\Laravue\Models\ProjectInventory;
use App\Laravue\Models\Transfer;
use App\Laravue\Models\TransferItem;
use App\Laravue\Models\TransferItemCostCenter;
use App\Laravue\Models\WarehouseInventory;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Validator;
use Auth;
use Illuminate\Support\Facades\DB;

class TransferController extends Controller
{
    const ITEM_PER_PAGE = 100;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      // dd('hi');

      // $query = Transfer::leftJoin('transfer_items', 'transfers.id', '=', 'transfer_items.id')
      //         // ->leftJoinWhere('warehouses', 'transfers.from', '=', 'warehouses.id')
      //         // ->leftJoinWhere('warehouses', 'transfers.to', '=', 'warehouses.id')
      //         // ->leftJoinWhere('projects', 'transfers.from', '=', 'projects.id')
      //         // ->leftJoinWhere('projects', 'transfers.to', '=', 'projects.id')
      //         // ->leftJoin('warehouses', 'transfers.transfer_type', '=', 'warehouses.id')


      //         // ->leftJoin('warehouses', function($join){
      //         //   $join->on('transfers.from', '=', 'warehouses.id');
      //         //   $join->on('transfers.to', '=', 'warehouses.id');
      //         // })
      //         // ->leftJoin('projects', function($join){
      //         //   $join->on('transfers.from', '=', 'projects.id');
      //         //   $join->on('transfers.to', '=', 'projects.id');
      //         // })



      //         // ->leftJoin('warehouses', function($join){
      //         //   $join->on('transfers.from', '=', 'warehouses.id')->where('transfers.transfer_type',1)->orWhere('transfers.transfer_type',2);
      //         //   $join->on('transfers.to', '=', 'warehouses.id')->where('transfers.transfer_type',1)->orWhere('transfers.transfer_type',3);
      //         // })
      //         // ->leftJoin('projects', function($join){
      //         //   $join->on('transfers.from', '=', 'projects.id')->where('transfers.transfer_type',3)->orWhere('transfers.transfer_type',4);
      //         //   $join->on('transfers.to', '=', 'projects.id')->where('transfers.transfer_type',2)->orWhere('transfers.transfer_type',4);
      //         // })
      //         ->groupBy('transfer_items.transfer_id')
      //         ->select('transfers.id as id, transfers.ref_number, transfers.gatepass_number, transfers.transfer_type, 
      //                   transfers.transfered_by, transfers.transfered_by_phone ', DB::raw('sum(transfer_items.quantity) as quantity'))
      //         ->get();
      // // dd($query);
      // $keyword = $request->keyword;
      // $limit = $request->limit;
      // $page = $request->page;
      // return response()->json($query->paginate($limit,['page'=> $page]));


        $searchParams = $request->all();
        $transferQuery = Transfer::query();
        // $transferQuery = Transfer::with('fromWarehouse','fromProject','toWarehouse','toProject')->get();
        $transferQuery = Transfer::with('fromWarehouse','fromProject','toWarehouse','toProject','transferItems.product');

        // $transferQuery = Transfer::with('fromInventory','toInventory')->get();
        // $transferQuery = Transfer::with('fromInventory')->get();
        // dd($transferQuery);
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        $date = Arr::get($searchParams, 'date', '');
        
        if (!empty($keyword)) {
            $transferQuery->where('gatepass_number', 'LIKE', '%' . $keyword . '%');
        }
        if (!empty($date)) {
            $transferQuery->where('transfer_date', 'LIKE', '%' . $date . '%');
        }

        return TransferResource::collection($transferQuery->orderBy('id', 'asc')->paginate($limit));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  get validation rules from getValidationRules method
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules($isNew = true, $transfer = null),
            )
        );
        // dd('hi', $request->ItemRows, $request->all());

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
          // try{
            $transfer = new Transfer();
            $transfer->gatepass_number = $request->gatepass_number;
            $transfer->created_by = Auth::id();
            // DB::beginTransaction();
            $this->saveTransferData($transfer, $request);
            // DB::commit();
            return new TransferResource($transfer);
          // } catch(\Exception $e){
          //   DB::rollBack();
          // }
        }
    }

    public function saveTransferData($transfer, $request){
        $transfer->ref_number = $request->ref_number;
        $transfer->transfer_date = $request->transfer_date;
        $transfer->transfer_type = $request->transfer_type;
        $transfer->from = $request->selectedFromId;
        $transfer->to = $request->selectedToId;
        $transfer->transfered_by = $request->transfered_by;
        $transfer->transfered_by_phone = $request->transfered_by_phone;
        $transfer->save();
        if(count($request->ItemRows)){
            $itemRows = $request->ItemRows;
            foreach($itemRows as $itemRow){
                $this->moveFrom($itemRow, $transfer);
                $this->moveTo($itemRow, $transfer);


                if(isset($itemRow['id'])){
                    $this->updateTransferItem($itemRow);
                } else {
                    $this->saveTransferItem($itemRow, $transfer);
                }
                
            }
        }
    }

    public function saveTransferItem($itemRow, $transfer){
        $row = new TransferItem();
        $row->transfer_id = $transfer->id;
        // dd('saveTransferItem',$row,$transfer);
        $this->saveTransferItemData($itemRow, $row);
    }

    public function updateTransferItem($itemRow){
        $row = TransferItem::find($itemRow['id']);
        $this->saveTransferItemData($itemRow, $row);
    }

    public function saveTransferItemData($itemRow, $row){
        // dd('saveTransferItemData',$row);
        $row->transfer_id = $row['transfer_id'];
        $row->product_id = $itemRow['product_id'];
        $row->quantity = $itemRow['quantity'];
        $row->save();
    }



    public function moveFrom($itemRow, $transfer)
    {
      if($transfer->transfer_type ==1 || $transfer->transfer_type ==2){
        $inventory_product = WarehouseInventory::where('product_id', $itemRow['product_id'])->where('warehouse_id', $transfer->from)->first();
      } else {
        $inventory_product = ProjectInventory::where('product_id', $itemRow['product_id'])->where('project_id', $transfer->from)->first();
      } 
      $inventory_product->quantity = $inventory_product->quantity - (int) $itemRow['quantity'];
      // dd('moveFrom',$inventory_product, $transfer,$itemRow);
      $inventory_product->save();
    }


    public function moveTo($itemRow, $transfer)
    {
      if($transfer->transfer_type == 1 || $transfer->transfer_type == 3){
          $inventory_product = WarehouseInventory::where('product_id', $itemRow['product_id'])->where('warehouse_id', $transfer->to)->first();
      } else {
          $inventory_product = ProjectInventory::where('product_id', $itemRow['product_id'])->where('project_id', $transfer->to)->first();
      }
      if(isset($inventory_product)){
          // dd('moveTo if',$inventory_product);
          $inventory_product->quantity = $inventory_product->quantity + (int) $itemRow['quantity'];
          $inventory_product->save();
      } else {
          // dd('moveTo else',$inventory_product);
          if($transfer->transfer_type == 1 || $transfer->transfer_type == 3){
            $inventory_product = new WarehouseInventory();
            $inventory_product->warehouse_id = $transfer->to;
          } else {
            $inventory_product = new ProjectInventory();
            $inventory_product->project_id = $transfer->to;
          }
          $inventory_product->product_id = $itemRow['product_id'];
          $inventory_product->quantity = (int) $itemRow['quantity'];
          $inventory_product->save();
      }
    }



    public function getDeliveries()
    {
      $deliveries = Delivery::with('fromWarehouse','fromProject','toWarehouse','toProject','deliveryItems.product')->get();
      // dd('hu',$deliveryQuery);
      return $deliveries;
    }
 


    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\Transfer  $transfer
     * @return \Illuminate\Http\Response
     */
    public function show(Transfer $transfer)
    {
      // dd($transfer);
      // $transferQuery = Transfer::with('fromWarehouse','fromProject','toWarehouse','toProject','transferItems.product');
      $transferQuery = Transfer::with('fromWarehouse','fromProject','toWarehouse','toProject','transferItems.product')->find($transfer->id);
      // dd($transferQuery);
      // return $transferQuery;
        return new TransferResource($transferQuery);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\Transfer  $transfer
     * @return \Illuminate\Http\Response
     */
    public function edit(Transfer $transfer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\Transfer  $transfer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transfer $transfer)
    {
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules($isNew = false, $transfer),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            $this->saveTransferData($transfer, $request);
            return new TransferResource($transfer);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\Transfer  $transfer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transfer $transfer)
    {
        //
    }

    /**
     * getValidationRules.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	private
     * @param	boolean	$isNew	Default: true
     * @return	array
     */
    private function getValidationRules($isNew = true, $transfer)
    {
        return [
            'gatepass_number' => $isNew ? 'required|unique:transfers' : 'required|unique:transfers,gatepass_number,' . $transfer->id,
            'transfer_date' => 'required',
            'transfer_type' => 'required',
            'selectedFromId' => 'required',
            'selectedToId' => 'required',
        ];
    }
}
