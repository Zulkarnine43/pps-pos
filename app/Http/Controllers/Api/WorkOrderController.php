<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\WorkOrderResource;
use App\Laravue\Models\WorkOrder;
use App\Laravue\Models\WorkOrderItem;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Validator;

class WorkOrderController extends Controller
{
    const ITEM_PER_PAGE = 100;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $woQuery = WorkOrder::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        $subcontractor = Arr::get($searchParams, 'subcontractor', '');
        $supplier = Arr::get($searchParams, 'supplier', '');
        $date = Arr::get($searchParams, 'date', '');
        $project = Arr::get($searchParams, 'project', '');
        // search query
        if (!empty($keyword)) {
            $woQuery
            ->orWhere('order_no', 'LIKE', '%' . $keyword . '%')
            ->orWhere('contract_ref', 'LIKE', '%' . $keyword . '%')
            ->orWhere('payable_amount', 'LIKE', '%' . $keyword . '%');
            
        }
        // search according to user

        if (!empty($subcontractor)) {
            $woQuery->where('subcontractor_id', $subcontractor );
        }
        if (!empty($supplier)) {
            $woQuery->where('supplier_id', $supplier );
        }

        if (!empty($date)) {
            $woQuery->orwhere('issue_date', 'LIKE', '%' . $date . '%')
                    ->orwhere('po_issuance_date', 'LIKE', '%' . $date . '%')
                    ->orwhere('deadline', 'LIKE', '%' . $date . '%');
        }
    

        return WorkOrderResource::collection($woQuery->orderBy('id', 'desc')->paginate($limit));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            $wo = new WorkOrder();

            $wo->subcontractor_id = $request->subcontractor_id;
            $wo->supplier_id = $request->supplier_id;
            $wo->task_details = $request->task_details;
            $wo->order_no = $request->order_no;
            $wo->contract_ref = $request->contract_ref;
            $wo->department_id = $request->department_id;
            $wo->payable_amount = $request->payable_amount;
            $wo->issue_date = $request->issue_date;
            $wo->po_issuance_date = $request->po_issuance_date;
            $wo->deadline = $request->deadline;
            $wo->delivery_point = $request->delivery_point;
            $wo->delivery_address = $request->delivery_address;
            $wo->delivery_contract_number = $request->delivery_contract_number;
            $wo->supply_item = $request->supply_item;
            $wo->payment_terms = $request->payment_terms;
            $wo->payment_methods = $request->payment_methods;
            $wo->remarks = $request->remarks;
            $wo->work_order_for = $request->work_order_for;
            $wo->warranty_terms = $request->warranty_terms;
            $wo->delay_terms = $request->delay_terms;
            $wo->other_terms = $request->other_terms;
            $wo->scope_of_work = $request->scope_of_work;
            $wo->service_scope = $request->service_scope;
            $wo->equipment_scope = $request->equipment_scope;
            $wo->contract_referance = $request->contract_referance;

            if(count($request->ItemRows) > 0){
                $wo->save();
                $woItems = $request->ItemRows;
                foreach($woItems as $woItem){
                    $item = new WorkOrderItem();
                    $item->work_order_id = $wo->id;
                    if(isset($woItem['project_id'])){
                        $item->project_id = $woItem['project_id'];
                        $item->project_stage_id = $woItem['project_stage_id'];
                    }
                    $item->work_order_amount = $woItem['work_order_amount'];
                    $item->remarks = $woItem['remarks'];
                    $item->details = $woItem['details'];
                    $item->quantity = $woItem['quantity'];
                    $item->unit_price = $woItem['unit_price'];
                    $item->unit_id = $woItem['unit_id'];
                    $item->site_location = $woItem['site_location'];
                    $item->save();
                }
            }

            return new WorkOrderResource($wo);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\WorkOrder  $workOrder
     * @return \Illuminate\Http\Response
     */
    public function show(WorkOrder $workOrder)
    {
        return new WorkOrderResource($workOrder);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\WorkOrder  $workOrder
     * @return \Illuminate\Http\Response
     */
    public function edit(WorkOrder $workOrder)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\WorkOrder  $workOrder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WorkOrder $workOrder)
    {
        // dd($request);
        if ($workOrder === null) {
            return response()->json(['error' => 'Work Order not found'], 404);
        } else {

            $workOrder->task_details = $request->task_details;
            $workOrder->contract_ref = $request->contract_ref;
            $workOrder->department_id = $request->department_id;
            $workOrder->payable_amount = $request->payable_amount;
            $workOrder->issue_date = $request->issue_date;
            $workOrder->po_issuance_date = $request->po_issuance_date;
            $workOrder->deadline = $request->deadline;
            $workOrder->warranty_terms = $request->warranty_terms;
            $workOrder->delay_terms = $request->delay_terms;
            $workOrder->other_terms = $request->other_terms;
            $workOrder->scope_of_work = $request->scope_of_work;
            $workOrder->service_scope = $request->service_scope;
            $workOrder->equipment_scope = $request->equipment_scope;

            if(count($request->ItemRows) > 0){
                $workOrder->save();
                $woItems = $request->ItemRows;
                foreach($woItems as $woItem){
                    if(isset($woItem['work_order_id'])){
                        $item = WorkOrderItem::find($woItem['work_order_id']);
                        $item->work_order_amount = $woItem['work_order_amount'];
                        $item->remarks = $woItem['remarks'];
                        $item->details = $woItem['details'];
                        $item->quantity = $woItem['quantity'];
                        $item->unit_price = $woItem['unit_price'];
                        $item->unit_id = $woItem['unit_id'];
                        $item->site_location = $woItem['site_location'];
                        $item->save();
                    } else {
                        $item = new WorkOrderItem();
                        $item->work_order_id = $workOrder->id;
                        $item->project_id = $woItem['project_id'];
                        $item->project_stage_id = $woItem['project_stage_id'];
                        $item->work_order_amount = $woItem['work_order_amount'];
                        $item->remarks = $woItem['remarks'];
                        $item->details = $woItem['details'];
                        $item->quantity = $woItem['quantity'];
                        $item->unit_price = $woItem['unit_price'];
                        $item->unit_id = $woItem['unit_id'];
                        $item->site_location = $woItem['site_location'];
                        $item->save();
                    }
                    
                }
            }
            return new WorkOrderResource($workOrder);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\WorkOrder  $workOrder
     * @return \Illuminate\Http\Response
     */
    public function destroy(WorkOrder $workOrder)
    {
        //
    }

    /**
     * getValidationRules.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	private
     * @param	boolean	$isNew	Default: true
     * @return	array
     */
    private function getValidationRules($isNew = true)
    {
        return [
            // 'subcontractor_id' => 'required|numeric',
            // 'payable_amount' => 'required',        
        ];
    }
}
