<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ClientResource;
use App\Laravue\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Validator;
use File;
use Illuminate\Support\Facades\Storage;

class ClientController extends Controller
{

    const ITEM_PER_PAGE = 100;
    public $image; //client image
    public $tradeLicence; //tradelicence image
    public $TIN; //tin image
    public $BIN; // bin image
    public $NID; // nid image

    /**
     * Display a listing of the resource.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	public
     * @param	request	$request
     * @return	all the client list
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $clientQuery = Client::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        // search query
        if (!empty($keyword)) {
            $clientQuery->where('name', 'LIKE', '%' . $keyword . '%');
            $clientQuery->where('email', 'LIKE', '%' . $keyword . '%');
        }

        return ClientResource::collection($clientQuery->orderBy('id', 'DESC')->paginate($limit));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	public
     * @param	request	$request
     * @return	void
     */




    public function getImage(Request $request){
       if($request->file('photo')){
        $file_name = time().'-'.$request->file('photo')->getClientOriginalName();
        $request->file('photo')->storePubliclyAs(
            'clients/images',
            $file_name,
            's3'
        );
        return response()->json(['imageName' => $file_name]);
       }
    }

    public function tradeLicence(Request $request){
       if($request->file('photo')){
        $file_name = time().'-'.$request->file('photo')->getClientOriginalName();
        $request->file('photo')->storePubliclyAs(
            'clients/trade',
            $file_name,
            's3'
        );
        return response()->json(['tradeLicence' => $file_name]);
       }
    }

    public function getTin(Request $request){
        if($request->file('photo')){
            $file_name = time().'-'.$request->file('photo')->getClientOriginalName();
            $request->file('photo')->storePubliclyAs(
            'clients/tin',
            $file_name,
            's3'
        );
        return response()->json(['tin' => $file_name]);
        }
    }

    public function getBin(Request $request){
        if($request->file('photo')){
        $file_name = time().'-'.$request->file('photo')->getClientOriginalName();
        $request->file('photo')->storePubliclyAs(
            'clients/bin',
            $file_name,
            's3'
        );
        return response()->json(['bin' => $file_name]);
       }
    }

    public function getNid(Request $request){
        if($request->file('photo')){
            $file_name = time().'-'.$request->file('photo')->getClientOriginalName();
            $request->file('photo')->storePubliclyAs(
            'clients/nid',
             $file_name,
             's3'
        );
        return response()->json(['nid' => $file_name]);
        }
    }



    public function store(Request $request)
    {
        // check validation rules form getValidationRules method
      
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
            )
        );

     

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {

            if($request->image){
                $image = 'clients/images/'.$request->image;
            }
            else {
                $image = '';
            }
            
            if($request->trade_licence){
                $trade = 'clients/trade/'.$request->trade_licence;
            }
            else {
                $trade = '';
            }
            if($request->TIN){
                $tin = 'clients/tin/'.$request->TIN;
            }
            else {
                $tin = '';
            }
            if($request->BIN){
                $bin = 'clients/bin/'.$request->BIN;
            }
            else {
                $bin ='';
            }
            if($request->NID){
               $nid = 'clients/bin/'.$request->NID;
            }
            else {
                $nid  = '';
            }
            // client creating
            $client = Client::create([
                'name' => $request->name,
                'company' => $request->company,
                'email' => $request->email,
                'phone' => $request->phone,
                'country' => $request->country,
                'state' => $request->state,
                'district' => $request->district,
                'city' => $request->city,
                'zip_code' => $request->zip_code,
                'street' => $request->street,
                'image' => $image,
                'trade_licence' => $trade,
                'TIN' => $tin,
                'BIN' => $bin,
                'NID' => $nid,
            ]);

            return new ClientResource($client);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        return new ClientResource($client);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	public
     * @param	request	$request
     * @param	client 	$client
     * @return	void
     */



    public function update(Request $request, Client $client)
    {
        
        $client_image = $client->image;
        if($request->image) {
            if($client_image){

                Storage::disk('s3')->delete("/clients/images/".$client_image);
            }
        }
        $trade = $client->trade_licencence;
        if($request->trade_licence){
            if($trade){

                Storage::disk('s3')->delete("/clients/trade/".$trade);
            }
        }
        $tin = $client->TIN;
        if($request->TIN){
            if($tin){

                Storage::disk('s3')->delete("/clients/tin/".$tin);
            }
        }

        $bin = $client->BIN;
        if($request->BIN){
            if($bin){

                Storage::disk('s3')->delete("/clients/bin/".$bin);
            }
        }

        $nid = $client->NID;
        if($request->NID){
            if($nid){

                Storage::disk('s3')->delete("/clients/nid/".$nid);
            }
        }

        // check requested client
        if ($client === null) {
            return response()->json(['error' => 'Client not found'], 404);
        }

        // check validation rules form getValidationRules method
        $validator = Validator::make($request->all(), $this->getValidationRules(false));
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
     

            // update requested client data
            $client->name = $request->get('name');
            $client->phone = $request->get('phone');
            $client->company = $request->get('company');
            $client->email = $request->get('email');
            $client->country = $request->get('country');
            $client->state = $request->get('state');
            $client->district = $request->get('district');
            $client->zip_code = $request->get('zip_code');
            $client->city = $request->get('city');
            $client->street = $request->get('street');
            if($request->image){
                $client->image = 'clients/images/'.$request->get('image');
            }
            if($request->trade_licence){
                $client->trade_licence = 'clients/trade/'.$request->get('trade_licence');
            }
            if($request->TIN){
                $client->TIN = 'clients/tin/'.$request->get('TIN');
            }
            if($request->BIN){
                $client->BIN = 'clients/bin/'.$request->get('BIN');
            }
            if($request->NID){
                $client->NID = 'clients/nid/'.$request->get('NID'); 
            }
            $client->save();
            return new ClientResource($client);
        }
    }

    /**
     * InActive requested active client.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	public
     * @param	request	$request
     * @param	client 	$client
     * @return	mixed
     */
    public function inActive(Request $request, Client $client){
        // check requested client
        if ($client === null) {
            return response()->json(['error' => 'Client not found'], 404);
        }

        $client->is_active = 'inactive';

        $client->save();
        return new ClientResource($client);
    }

    /**
     * Active inactive requested client.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	public
     * @param	request	$request
     * @param	client 	$client
     * @return	mixed
     */
    public function active(Request $request, Client $client){
        // check requested client
        if ($client === null) {
            return response()->json(['error' => 'Client not found'], 404);
        }

        $client->is_active = 'active';

        $client->save();
        return new ClientResource($client);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        //
    }

    /**
     * getValidationRules.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	private
     * @param	boolean	$isNew	Default: true
     * @return	array
     */
    private function getValidationRules($isNew = true)
    {
        return [
            'name' => 'required',
            'email' => $isNew ? 'nullable|email|unique:clients' : 'nullable|email',
            'phone' => $isNew ? 'required|unique:clients' : 'required',
        ];
    }
}
