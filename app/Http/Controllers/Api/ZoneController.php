<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ZoneResource;
use App\Laravue\Models\Zone;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Validator;

class ZoneController extends Controller
{
    const ITEM_PER_PAGE = 100;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $zoneQuery = Zone::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        // search query
        if (!empty($keyword)) {
            $zoneQuery->where('name', 'LIKE', '%' . $keyword . '%');
        }

        return ZoneResource::collection($zoneQuery->orderBy('id', 'desc')->paginate($limit));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  get validation rules from getValidationRules method
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {

            // create new Department
            
            $zone = Zone::create([
                'name' => $request->name,
            ]);

            $milestones = $request->milestones;
            if(count($milestones) > 0){
                foreach($milestones as $milestone){
                    $zone->zoneMilestones()->create([
                        'zone_id' => $zone->id,
                        'milestone_id' => $milestone['milestone_id'],
                        'work_done' => $milestone['work_done'],
                    ]);
                }
            }
            return new ZoneResource($zone);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\Zone  $zone
     * @return \Illuminate\Http\Response
     */
    public function show(Zone $zone)
    {
        return new ZoneResource($zone);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\Zone  $zone
     * @return \Illuminate\Http\Response
     */
    public function edit(Zone $zone)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\Zone  $zone
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Zone $zone)
    {
        if ($zone === null) {
            return response()->json(['error' => 'Parent Group not found'], 404);
        }

        $validator = Validator::make($request->all(), $this->getValidationRules(false));
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            $zone->name = $request->name;
            $zone->save();

            $milestones = $request->milestones;
            if(count($milestones) > 0){
                foreach($milestones as $milestone){
                    if(isset($milestone['id'])){
                        $zone->zoneMilestones()->where('id', $milestone['id'])->update([
                            'milestone_id' => $milestone['milestone_id'],
                            'work_done' => $milestone['work_done'],
                        ]);

                    }else{
                    $zone->zoneMilestones()->create([
                        'zone_id' => $zone->id,
                        'milestone_id' => $milestone['milestone_id'],
                        'work_done' => $milestone['work_done'],
                    ]);
                }
                }
            }

            return new ZoneResource($zone);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\Zone  $zone
     * @return \Illuminate\Http\Response
     */
    public function destroy(Zone $zone)
    {
        //
    }

    /**
     * getValidationRules.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	private
     * @param	boolean	$isNew	Default: true
     * @return	array
     */
    private function getValidationRules($isNew = true)
    {
        return [
            
            'name' => $isNew ? 'required|unique:zones' : 'required',
        ];
    }
}
