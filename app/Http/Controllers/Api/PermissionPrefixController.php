<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Laravue\Models\PermissionPrefix;
use Illuminate\Http\Request;
use App\Http\Resources\PermisionPrefixResource;

class PermissionPrefixController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $prefixQuery = PermissionPrefix::orderBy('name', 'asc')->get();
        return PermisionPrefixResource::collection($prefixQuery);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\PermissionPrefix  $permissionPrefix
     * @return \Illuminate\Http\Response
     */
    public function show(PermissionPrefix $permissionPrefix)
    {
        return new PermisionPrefixResource($permissionPrefix);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\PermissionPrefix  $permissionPrefix
     * @return \Illuminate\Http\Response
     */
    public function edit(PermissionPrefix $permissionPrefix)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\PermissionPrefix  $permissionPrefix
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PermissionPrefix $permissionPrefix)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\PermissionPrefix  $permissionPrefix
     * @return \Illuminate\Http\Response
     */
    public function destroy(PermissionPrefix $permissionPrefix)
    {
        //
    }
}
