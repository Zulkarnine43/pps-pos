<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Laravue\Models\ChartOfAccount;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\ChartOfAccountResource;

class ChartOfAccountController extends Controller
{
    const ITEM_PER_PAGE = 100;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $chartOfAccountData = ChartOfAccount::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        // search query
        if (!empty($keyword)) {
            $chartOfAccountData->where('title', 'LIKE', '%' . $keyword . '%');
        }

        return ChartOfAccountResource::collection($chartOfAccountData->orderBy('id', 'desc')->paginate($limit));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // check validation rules from getValidationRules method
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            // dd($request);
            // if ($request->image) {
            //     $image = 'categories/images/' . $request->image;
            // } else {
            //     $image = '';
            // }
            // category creating
            if ($request->cost_center === true) {
                $costCenter = 1;
            } else {
                $costCenter = 0;
            }
            $data = ChartOfAccount::create([
                'title' => $request->title,
                'code' => $request->code,
                'cost_center' => (int)$costCenter,
            ]);
            return new ChartOfAccountResource($data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\ChartOfAccount  $chartOfAccount
     * @return \Illuminate\Http\Response
     */
    public function show(ChartOfAccount $chartOfAccount)
    {
        return new ChartOfAccountResource($chartOfAccount);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\ChartOfAccount  $chartOfAccount
     * @return \Illuminate\Http\Response
     */
    public function edit(ChartOfAccount $chartOfAccount)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\ChartOfAccount  $chartOfAccount
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ChartOfAccount $chartOfAccount)
    {
        // check requested bank
        if ($chartOfAccount === null) {
            return response()->json(['error' => 'Chart Of Account not found'], 404);
        }
        // check validation rules from getValidationRules method
        $validator = Validator::make($request->all(), $this->getValidationRules(false));
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {

            //update requested category
            $chartOfAccount->title = $request->get('title');
            $chartOfAccount->code = $request->get('code');
            $chartOfAccount->cost_center = $request->get('cost_center');
            // if ($request->image) {
            //     $category->image = 'categories/images/' . $request->get('image');
            // }
            $chartOfAccount->save();
            return new ChartOfAccountResource($chartOfAccount);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\ChartOfAccount  $chartOfAccount
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChartOfAccount $chartOfAccount)
    {
        //
    }
    private function getValidationRules($isNew = true)
    {
        return [
            'title' => $isNew ? 'required' : 'nullable',
            'code' => $isNew ? 'required|unique:chart_of_accounts' : 'required',
            // 'cost_center' => $isNew ? 'required' : 'nullable',
        ];
    }
}
