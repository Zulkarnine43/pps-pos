<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\StockResource;
use App\Laravue\Models\PurchaseItem;
use App\Laravue\Models\WarehouseInventory;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class StockController extends Controller
{
    const ITEM_PER_PAGE = 2;
    public function index(Request $request)
    {
        // dd($request->all());
        $query = WarehouseInventory::groupBy('product_id')
            ->leftJoin('products', 'warehouse_inventories.product_id', '=', 'products.id')
            ->select('products.name as product_name', DB::raw('sum(quantity) as total'))
            ->where('warehouse_inventories.quantity','>',0);

        // dd($query);
        // dd($request->all());
        $keyword = $request->keyword;
        $limit = $request->limit;
        $page = $request->page;
        // search query
        if (!empty($keyword)) {
            $query->where('name', 'LIKE', '%' . $keyword . '%');
        }

        // dd($query,response()->json($query));
        // dd($query,response()->json($query), $limit, $page, $query->paginate($limit));
        
        return response()->json($query->paginate($limit,['page'=> $page]));
        
        // return response()->json($query);
        // return $query;
    }




    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getItems(Request $request)
    {
      // dd($request->all());
      $query = PurchaseItem::groupBy('product_id')
          ->leftJoin('products', 'purchase_items.product_id', '=', 'products.id')
          ->select('products.name as product_name', DB::raw('sum(received_quantity) as total'))
          ->where('purchase_items.received_quantity','>',0);

      // dd($query);
      // dd($request->all());
      $keyword = $request->keyword;
      $limit = $request->limit;
      $page = $request->page;
      // search query
      if (!empty($keyword)) {
          $query->where('name', 'LIKE', '%' . $keyword . '%');
      }

      // dd($query,response()->json($query));
      // dd($query,response()->json($query), $limit, $page, $query->paginate($limit));
      
      return response()->json($query->paginate($limit,['page'=> $page]));
    }
}
