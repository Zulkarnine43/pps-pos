<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Arr;
use App\Laravue\Models\Bank;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\BankResource;
use Illuminate\Support\Facades\Validator;

class BankController extends Controller
{
    const ITEM_PER_PAGE = 100;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $bankQuery = Bank::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        // search query
        if (!empty($keyword)) {
            $bankQuery->where('name', 'LIKE', '%' . $keyword . '%');
        }

        return BankResource::collection($bankQuery->orderBy('id', 'desc')->paginate($limit));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // check validation rules from getValidationRules method
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {

            // if ($request->image) {
            //     $image = 'categories/images/' . $request->image;
            // } else {
            //     $image = '';
            // }
            // category creating
            $bank = Bank::create([
                'name' => $request->name,
                'account_number' => $request->account_number,
                'branch' => $request->branch,
            ]);
            return new BankResource($bank);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function show(Bank $bank)
    {
        return new BankResource($bank);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function edit(Bank $bank)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bank $bank)
    {
        // check requested bank
        if ($bank === null) {
            return response()->json(['error' => 'bank not found'], 404);
        }
        // check validation rules from getValidationRules method
        $validator = Validator::make($request->all(), $this->getValidationRules(false));
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {

            //update requested category
            $bank->name = $request->get('name');
            $bank->account_number = $request->get('account_number');
            $bank->branch = $request->get('branch');
            // if ($request->image) {
            //     $category->image = 'categories/images/' . $request->get('image');
            // }
            $bank->save();
            return new BankResource($bank);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bank $bank)
    {
        //
    }
    public function mute(Request $request, Bank $bank)
    {
        if ($bank === null) {
            return response()->json(['error' => 'Bank not found'], 404);
        }

        $bank->status = 1;

        $bank->save();
        return new BankResource($bank);
    }
    public function unmute(Request $request, Bank $bank)
    {
        if ($bank === null) {
            return response()->json(['error' => 'Bank not found'], 404);
        }

        $bank->status = 0;

        $bank->save();
        return new BankResource($bank);
    }
    private function getValidationRules($isNew = true)
    {
        return [
            'name' => $isNew ? 'required' : 'nullable',
            'account_number' => $isNew ? 'required|unique:banks' : 'required',
            'branch' => $isNew ? 'required' : 'nullable',
        ];
    }
}
