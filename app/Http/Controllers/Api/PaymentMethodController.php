<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PaymentMethodResource;
use App\Laravue\Models\PaymentMethod;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Validator;

class PaymentMethodController extends Controller
{
    const ITEM_PER_PAGE = 100;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $paymentMethodQuery = PaymentMethod::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        // search query
        if (!empty($keyword)) {
            $paymentMethodQuery->where('name', 'LIKE', '%' . $keyword . '%');
        }

        return PaymentMethodResource::collection($paymentMethodQuery->orderBy('id', 'desc')->paginate($limit));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules($isNew = true, $paymentMethod = null),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {

            // create new Department
            
            $paymentMethod = PaymentMethod::create([
                'name' => $request->name,
                'allaw_change_due_amount' => $request->allaw_change_due_amount,
            ]);
            return new PaymentMethodResource($paymentMethod);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\PaymentMethod  $paymentMethod
     * @return \Illuminate\Http\Response
     */
    public function show(PaymentMethod $paymentMethod)
    {
        return new PaymentMethodResource($paymentMethod);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\PaymentMethod  $paymentMethod
     * @return \Illuminate\Http\Response
     */
    public function edit(PaymentMethod $paymentMethod)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\PaymentMethod  $paymentMethod
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PaymentMethod $paymentMethod)
    {
        // check requested department
        if ($paymentMethod === null) {
            return response()->json(['error' => 'Parent Group not found'], 404);
        }

        $validator = Validator::make($request->all(), $this->getValidationRules($isNew = false, $paymentMethod));
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            $paymentMethod->name = $request->name;
            $paymentMethod->allaw_change_due_amount = $request->allaw_change_due_amount;
            $paymentMethod->save();

            return new PaymentMethodResource($paymentMethod);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\PaymentMethod  $paymentMethod
     * @return \Illuminate\Http\Response
     */
    public function destroy(PaymentMethod $paymentMethod)
    {
        //
    }

    /**
     * getValidationRules.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	private
     * @param	boolean	$isNew	Default: true
     * @return	array
     */
    private function getValidationRules($isNew, $paymentMethod)
    {
        return [
            
            'name' => $isNew ? 'required|unique:payment_methods' : 'required|unique:payment_methods,name,'.$paymentMethod->id,
        ];
    }
}
