<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\OfficeInfoResource;
use App\Laravue\Models\OfficeInfo;
use Illuminate\Http\Request;
use Validator;

class OfficeInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\OfficeInfo  $officeInfo
     * @return \Illuminate\Http\Response
     */
    public function show(OfficeInfo $officeInfo)
    {
        return new OfficeInfoResource($officeInfo);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\OfficeInfo  $officeInfo
     * @return \Illuminate\Http\Response
     */
    public function edit(OfficeInfo $officeInfo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\OfficeInfo  $officeInfo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OfficeInfo $officeInfo)
    {
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules($isNew = true, $ledger = null),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            $officeInfo->name = $request->name;
            $officeInfo->company_name = $request->company_name;
            $officeInfo->phone = $request->phone;
            $officeInfo->email = $request->email;
            $officeInfo->telephone = $request->telephone;
            $officeInfo->office_address = $request->office_address;
            $officeInfo->designation = $request->designation;
            $officeInfo->website_url = $request->website_url;
            $officeInfo->save();

            return new OfficeInfoResource($officeInfo);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\OfficeInfo  $officeInfo
     * @return \Illuminate\Http\Response
     */
    public function destroy(OfficeInfo $officeInfo)
    {
        //
    }

    private function getValidationRules($isNew = true, $ledger)
    {
        return [
            'name' => 'required',
            'company_name' => 'required',
            'telephone' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'designation' => 'required',
            'office_address' => 'required',
        ];
    }
}
