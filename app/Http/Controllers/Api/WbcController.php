<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Laravue\Models\Wbc;
use Illuminate\Http\Request;

class WbcController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\Wbc  $wbc
     * @return \Illuminate\Http\Response
     */
    public function show(Wbc $wbc)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\Wbc  $wbc
     * @return \Illuminate\Http\Response
     */
    public function edit(Wbc $wbc)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\Wbc  $wbc
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Wbc $wbc)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\Wbc  $wbc
     * @return \Illuminate\Http\Response
     */
    public function destroy(Wbc $wbc)
    {
        //
    }
}
