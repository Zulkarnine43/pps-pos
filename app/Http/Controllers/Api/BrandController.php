<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\BrandResource;
use App\Laravue\Models\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Validator;
use File;
use Illuminate\Support\Facades\Storage;

class BrandController extends Controller
{

    const ITEM_PER_PAGE = 100;



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * index.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	public
     * @param	request	$request
     * @return	all brand list
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $brandQuery = Brand::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');

        if (!empty($keyword)) {
            $brandQuery->where('name', 'LIKE', '%' . $keyword . '%');
        }

        return BrandResource::collection($brandQuery->orderBy('id', 'desc')->paginate($limit));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function getImage(Request $request){
        if($request->file('photo')){
         $file_name = time().'-'.$request->file('photo')->getClientOriginalName();
         $request->file('photo')->storePubliclyAs(
             'brands/images',
             $file_name,
             's3'
         );
         return response()->json(['imageName' => $file_name]);
        }
     }

    /**
     * Store a newly created resource in storage.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	public
     * @param	request	$request
     * @return	void
     */
    public function store(Request $request)
    {

        // dd($request);
        // check validation getValidationRules method
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
           
            if($request->image){
                $image = 'brands/images/'.$request->image;
            }
            else {
                $image = '';
            }
            // create new brand
            $brand = Brand::create([
                'name' => $request->name,
                'code' => $request->code,
                'description' => $request->description,
                'image' => $image,
            ]);
            /**
             * @var		new	BrandResource($brand)
             */
            return new BrandResource($brand);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        return new BrandResource($brand);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brand $brand)
    {
        $brand_image = $brand->image;
        if($request->image) {
            if($brand_image){
                Storage::disk('s3')->delete("/brands/images/".$brand_image);
            }
        }
        // check requested brand
        if ($brand === null) {
            return response()->json(['error' => 'brand not found'], 404);
        }
//      check validation rules from getValidationRules method
        $validator = Validator::make($request->all(), $this->getValidationRules(false));
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {

            // update brand
            $brand->name = $request->get('name');
            $brand->code = $request->get('code');
            $brand->description = $request->get('description');
            if($request->image){
                $brand->image = 'brands/images/'.$request->get('image');
            }
            $brand->save();
            return new BrandResource($brand);
        }
    }

    /**
     * unmute.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	public
     * @param	request	$request
     * @param	brand  	$brand
     * @return	mixed
     * for unmute muted brands
     */
    public function unmute(Request $request, Brand $brand){
        if ($brand === null) {
            return response()->json(['error' => 'brand not found'], 404);
        }

        $brand->status = 0;

        $brand->save();
        return new BrandResource($brand);
    }

    /**
     * mute.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	public
     * @param	request	$request
     * @param	brand  	$brand
     * @return	mixed
     * for mute active brands
     */
    public function mute(Request $request, Brand $brand){
        if ($brand === null) {
            return response()->json(['error' => 'brand not found'], 404);
        }

        $brand->status = 1;

        $brand->save();
        return new BrandResource($brand);
    }


    /**
     * getValidationRules.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	private
     * @param	boolean	$isNew	Default: true
     * @return	array
     * check validation rules
     */
    private function getValidationRules($isNew = true)
    {
        return [
            'name' => 'required',
            'code' => $isNew ? 'nullable|unique:brands' : 'nullable',
        ];
    }
}
