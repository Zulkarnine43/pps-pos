<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Laravue\Models\ProjectStatus;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\ProjectStatusResource;

class ProjectStatusController extends Controller
{
    const ITEM_PER_PAGE = 100;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $data = ProjectStatus::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        // search query
        if (!empty($keyword)) {
            $data->where('name', 'LIKE', '%' . $keyword . '%');
        }

        return ProjectStatusResource::collection($data->orderBy('id', 'desc')->paginate($limit));
    }

    public function getAll(){
        $statuses = ProjectStatus::all();
        return ProjectStatusResource::collection($statuses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // check validation rules from getValidationRules method
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            // dd($request);
            // if ($request->image) {
            //     $image = 'categories/images/' . $request->image;
            // } else {
            //     $image = '';
            // }
            // category creating
            // if ($request->cost_center === true) {
            //     $costCenter = 1;
            // } else {
            //     $costCenter = 0;
            // }
            $data = ProjectStatus::create([
                'name' => $request->name,
            ]);
            return new ProjectStatusResource($data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\ProjectStatus  $projectStatus
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectStatus $projectStatus)
    {
        return new ProjectStatusResource($projectStatus);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\ProjectStatus  $projectStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectStatus $projectStatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\ProjectStatus  $projectStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProjectStatus $projectStatus)
    {
        // check requested bank
        if ($projectStatus === null) {
            return response()->json(['error' => 'Chart Of Account not found'], 404);
        }
        // check validation rules from getValidationRules method
        $validator = Validator::make($request->all(), $this->getValidationRules(false));
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {

            //update requested category
            $projectStatus->name = $request->get('name');
            // $projectStatus->code = $request->get('code');
            // $projectStatus->cost_center = $request->get('cost_center');
            // if ($request->image) {
            //     $category->image = 'categories/images/' . $request->get('image');
            // }
            $projectStatus->save();
            return new ProjectStatusResource($projectStatus);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\ProjectStatus  $projectStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectStatus $projectStatus)
    {
        //
    }
    private function getValidationRules($isNew = true)
    {
        return [
            'name' => $isNew ? 'required|unique:project_statuses' : 'nullable',
            // 'code' => $isNew ? 'required|unique:chart_of_accounts' : 'required',
            // 'cost_center' => $isNew ? 'required' : 'nullable',
        ];
    }
}
