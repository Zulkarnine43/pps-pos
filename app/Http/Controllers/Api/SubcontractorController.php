<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\SubcontractorResource;
use App\Laravue\Models\Subcontractor;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Validator;
use File;
use Illuminate\Support\Facades\Storage;

class SubcontractorController extends Controller
{

    const ITEM_PER_PAGE = 100;


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $searchParams = $request->all();
        $subcontractorQuery = Subcontractor::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        // search query
        if (!empty($keyword)) {
            $subcontractorQuery->where('name', 'LIKE', '%' . $keyword . '%');
            $subcontractorQuery->where('email', 'LIKE', '%' . $keyword . '%');
        }

        return SubcontractorResource::collection($subcontractorQuery->orderBy('id', 'desc')->paginate($limit));
    }


    public function activeSubcontractors(){
        $ac = Subcontractor::where('status', 0)->orderBy('name','asc')->get();
        return SubcontractorResource::collection($ac);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function getImage(Request $request){
        if($request->file('photo')){
            $file_name = time().'-'.$request->file('photo')->getClientOriginalName();
            $request->file('photo')->storePubliclyAs(
            'subcontractors/images',
            $file_name,
            's3'
        );
        return response()->json(['imageName' => $file_name]);
        }
    }

    public function tradeLicence(Request $request){
        if($request->file('photo')){
        $file_name = time().'-'.$request->file('photo')->getClientOriginalName();
        $request->file('photo')->storePubliclyAs(
            'subcontractors/trade',
            $file_name,
            's3'
        );
        return response()->json(['tradeLicence' => $file_name]);
      }
    }

    public function getTin(Request $request){
        if($request->file('photo')){
        $file_name = time().'-'.$request->file('photo')->getClientOriginalName();
        $request->file('photo')->storePubliclyAs(
            'subcontractors/tin',
            $file_name,
            's3'
        );
        return response()->json(['tin' => $file_name]);
      }
    }

    public function getBin(Request $request){
        if($request->file('photo')){
        $file_name = time().'-'.$request->file('photo')->getClientOriginalName();
        $request->file('photo')->storePubliclyAs(
            'subcontractors/bin',
            $file_name,
            's3'
        );
        return response()->json(['bin' => $file_name]);
    }
    }

    public function getNid(Request $request){
        if($request->file('photo')){
        $file_name = time().'-'.$request->file('photo')->getClientOriginalName();
        $request->file('photo')->storePubliclyAs(
            'subcontractors/nid',
             $file_name,
             's3'
        );
        return response()->json(['nid' => $file_name]);
       }
    }

    public function store(Request $request)
    {
        // check validation rules from getValidationRules method
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {

            if($request->image){
                $image = 'subcontractors/images/'.$request->image;
            }
            else {
                $image = '';
            }
            
            if($request->trade_licence){
                $trade = 'subcontractors/trade/'.$request->trade_licence;
            }
            else {
                $trade = '';
            }
            if($request->TIN){
                $tin = 'subcontractors/tin/'.$request->TIN;
            }
            else {
                $tin = '';
            }
            if($request->BIN){
                $bin = 'subcontractors/bin/'.$request->BIN;
            }
            else {
                $bin ='';
            }
            if($request->NID){
               $nid = 'subcontractors/bin/'.$request->NID;
            }
            else {
                $nid  = '';
            }
            

            // create new Subcontractor
            $subcontractor = Subcontractor::create([
                'name' => $request->name,
                'company' => $request->company,
                'email' => $request->email,
                'phone' => $request->phone,
                'country' => $request->country,
                'state' => $request->state,
                'district' => $request->district,
                'city' => $request->city,
                'zip_code' => $request->zip_code,
                'street' => $request->street,
                'image' =>$image,
                'trade_licence' => $trade,
                'TIN' => $tin,
                'BIN' => $bin,
                'NID' => $nid,
            ]);
            return new SubcontractorResource($subcontractor);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\Subcontractor  $subcontractor
     * @return \Illuminate\Http\Response
     */
    public function show(Subcontractor $subcontractor)
    {
        return new SubcontractorResource($subcontractor);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\Subcontractor  $subcontractor
     * @return \Illuminate\Http\Response
     */
    public function edit(Subcontractor $subcontractor)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\Subcontractor  $subcontractor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subcontractor $subcontractor)
    {

        $subcontractor_image = $subcontractor->image;
        if($request->image){
            if($subcontractor_image){

                Storage::disk('s3')->delete("/subcontractors/images/".$subcontractor_image);
            }
        }
        $trade = $subcontractor->trade_licencence;
        if($request->trade_licence){
            if($trade){

                Storage::disk('s3')->delete("/subcontractors/trade/".$trade);
            }
        }
        $tin = $subcontractor->TIN;
        if($request->TIN){
            if($tin){

                Storage::disk('s3')->delete("/subcontractors/tin/".$tin);
            }
        }

        $bin = $subcontractor->BIN;
        if($request->BIN){
            if($bin){

                Storage::disk('s3')->delete("/subcontractors/bin/".$bin);
            }
        }

        $nid = $subcontractor->NID;
        if($request->NID){
            if($nid){

                Storage::disk('s3')->delete("/subcontractors/nid/".$nid);
            }
        }
        // check requested Subcontractor
        if ($subcontractor === null) {
            return response()->json(['error' => 'Subcontractor not found'], 404);
        }


        $validator = Validator::make($request->all(), $this->getValidationRules(false));
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {

           
            // update requested subcontractor data
            $subcontractor->name = $request->get('name');
            $subcontractor->phone = $request->get('phone');
            $subcontractor->company = $request->get('company');
            $subcontractor->email = $request->get('email');
            $subcontractor->country = $request->get('country');
            $subcontractor->state = $request->get('state');
            $subcontractor->district = $request->get('district');
            $subcontractor->zip_code = $request->get('zip_code');
            $subcontractor->city = $request->get('city');
            $subcontractor->street = $request->get('street');
            if($request->image){$subcontractor->image = 'subcontractors/images/'.$request->get('image');}
            if($request->trade_licence){$subcontractor->trade_licence = 'subcontractors/trade/'.$request->get('trade_licence');}
            if($request->TIN){$subcontractor->TIN = 'subcontractors/tin/'.$request->get('TIN');}
            if($request->BIN){$subcontractor->BIN = 'subcontractors/bin/'.$request->get('BIN');}
            if($request->NID){$subcontractor->NID = 'subcontractors/nid/'.$request->get('NID');}
            $subcontractor->save();

            return new SubcontractorResource($subcontractor);
        }
    }

    // block subcontractor
    public function inActive(Request $request, Subcontractor $subcontractor){
        if ($subcontractor === null) {
            return response()->json(['error' => 'Subcontractor not found'], 404);
        }

        $subcontractor->is_active = 'inactive';

        $subcontractor->save();
        return new SubcontractorResource($subcontractor);
    }

    // unblock subcontractor
    public function active(Request $request, Subcontractor $subcontractor){
        if ($subcontractor === null) {
            return response()->json(['error' => 'subcontractor not found'], 404);
        }

        $subcontractor->is_active = 'active';

        $subcontractor->save();
        return new SubcontractorResource($subcontractor);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\Subcontractor  $subcontractor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subcontractor $subcontractor)
    {
        //
    }

    /**
     * getValidationRules.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	private
     * @param	boolean	$isNew	Default: true
     * @return	array
     */
    private function getValidationRules($isNew = true)
    {
        return [
            'name' => 'required',
            'email' => $isNew ? 'required|email|unique:subcontractors' : 'required|email',
            'phone' => $isNew ? 'required|unique:subcontractors' : 'required',
        ];
    }



    public function bulkEdit(Request $request, $subcontractor_ids)
    {
          $s_ids = explode(',' , $subcontractor_ids);
        //   foreach ($s_ids as $id) {
        //         $subcontractor = Subcontractor::where('id' , $id)->first();

        //         dd($subcontractor);
        //   }

        dd($request);

    }
}
