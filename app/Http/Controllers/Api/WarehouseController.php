<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\WarehouseResource;
use App\Laravue\Models\Warehouse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Validator;

class WarehouseController extends Controller
{

    const ITEM_PER_PAGE = 100;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $searchParams = $request->all();
        $warehouseQuery = Warehouse::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');

        // search query
        if (!empty($keyword)) {
            $warehouseQuery->where('name', 'LIKE', '%' . $keyword . '%')
                           ->orWhere('address', 'LIKE', '%' . $keyword . '%')
                           ->orWhere('phone', 'LIKE', '%' . $keyword . '%')
                           ->orWhere('email', 'LIKE', '%' . $keyword . '%');

        }

        return WarehouseResource::collection($warehouseQuery->orderBy('id', 'desc')->paginate($limit));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // get validation rules from getValidationRules method
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            // create new warehouse
            $warehouse = Warehouse::create([
                'name' => $request->name,
                'code' => $request->code,
                'email' => $request->email,
                'phone' => $request->phone,
                'address' => $request->address,
            ]);
            return new WarehouseResource($warehouse);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\Warehouse  $warehouse
     * @return \Illuminate\Http\Response
     */
    public function show(Warehouse $warehouse)
    {
        return new WarehouseResource($warehouse);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\Warehouse  $warehouse
     * @return \Illuminate\Http\Response
     */
    public function edit(Warehouse $warehouse)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\Warehouse  $warehouse
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Warehouse $warehouse)
    {
        // check requested warehouse
        if ($warehouse === null) {
            return response()->json(['error' => 'warehouse not found'], 404);
        }


        $validator = Validator::make($request->all(), $this->getValidationRules(false));
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {

            // update warehouse data
            $warehouse->name = $request->get('name');
            $warehouse->code = $request->get('code');
            $warehouse->email = $request->get('email');
            $warehouse->phone = $request->get('phone');
            $warehouse->address = $request->get('address');
            $warehouse->save();
            return new WarehouseResource($warehouse);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\Warehouse  $warehouse
     * @return \Illuminate\Http\Response
     */
    public function destroy(Warehouse $warehouse)
    {
        //
    }


    /**
     * unmute warehouse.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	public
     * @param	request  	$request
     * @param	warehouse	$warehouse
     * @return	mixed
     */
    public function unmute(Request $request, Warehouse $warehouse){
        if ($warehouse === null) {
            return response()->json(['error' => 'warehouse not found'], 404);
        }

        $warehouse->status = 0;

        $warehouse->save();
        return new WarehouseResource($warehouse);
    }

    /**
     * mute warehouse.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	public
     * @param	request  	$request
     * @param	warehouse	$warehouse
     * @return	mixed
     */
    public function mute(Request $request, Warehouse $warehouse){
        if ($warehouse === null) {
            return response()->json(['error' => 'brand not found'], 404);
        }

        $warehouse->status = 1;

        $warehouse->save();
        return new WarehouseResource($warehouse);
    }

    /**
     * getValidationRules.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	private
     * @param	boolean	$isNew	Default: true
     * @return	array
     */
    private function getValidationRules($isNew = true)
    {
        return [
            'name' => 'required',
            'code' => $isNew ? 'nullable|unique:warehouses' : 'nullable',
            'phone' => 'required',
            'email' => 'nullable',
            'address' => 'required',
        ];
    }

    /**
     * Display a listing of the resource depending on ID
     *
     * @return \Illuminate\Http\Response
     */
    public function getInventoryProducts($id)
    {
      // $warehouse = Warehouse::with('warehouseInventories.product')
      //           ->whereHas('warehouseInventories',function($query){
      //             $query->where('quantity', '>', 0);
      //           })->find($id);
      $warehouse = Warehouse::with('warehouseInventories.product')->find($id);
      $products = array();
      foreach($warehouse->warehouseInventories as $inventory){
        if($inventory->quantity){
          $inventory_info = [
            'product' => $inventory->product,
            'quantity' => $inventory->quantity
          ];
          array_push($products, $inventory_info );  
        }
      }
      return $products;
    }
}
