<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Laravue\Models\JournalItem;
use App\Laravue\Models\JournalItemCostCenter;
use Illuminate\Http\Request;

class JournalItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\JournalItem  $journalItem
     * @return \Illuminate\Http\Response
     */
    public function show(JournalItem $journalItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\JournalItem  $journalItem
     * @return \Illuminate\Http\Response
     */
    public function edit(JournalItem $journalItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\JournalItem  $journalItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, JournalItem $journalItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\JournalItem  $journalItem
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = JournalItem::find($id);
        if(isset($item)){
            $costs = JournalItemCostCenter::where('journal_item_id', $item->id);
            if(isset($costs)){
                foreach($costs as $cost){
                    $cost->delete();
                }
            }
        }
        $item->delete();

        return response()->json('success', 200);
    }
}
