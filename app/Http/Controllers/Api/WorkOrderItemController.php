<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\WorkOrderItemResource;
use App\Laravue\Models\WorkOrderItem;
use Illuminate\Http\Request;

class WorkOrderItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\WorkOrderItem  $workOrderItem
     * @return \Illuminate\Http\Response
     */
    public function show(WorkOrderItem $workOrderItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\WorkOrderItem  $workOrderItem
     * @return \Illuminate\Http\Response
     */
    public function edit(WorkOrderItem $workOrderItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\WorkOrderItem  $workOrderItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WorkOrderItem $workOrderItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\WorkOrderItem  $workOrderItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(WorkOrderItem $workOrderItem)
    {
        if ($workOrderItem === null) {
            return response()->json(['error' => 'Work Order Item not found'], 404);
        } else {
            $workOrderItem->status = 'canceled';
            $workOrderItem->save();

            return new WorkOrderItemResource($workOrderItem);
        }
    }
}
