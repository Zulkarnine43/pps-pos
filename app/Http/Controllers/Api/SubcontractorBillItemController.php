<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Laravue\Models\SubcontractorBillItem;
use Illuminate\Http\Request;
use App\Http\Resources\SubcontractorBillItemResource;

class SubcontractorBillItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function getCanceledBills(){
        $bills = SubcontractorBillItem::where('status', 'canceled')->get();
        return SubcontractorBillItemResource::collection($bills);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\SubcontractorBillItem  $subcontractorBillItem
     * @return \Illuminate\Http\Response
     */
    public function show(SubcontractorBillItem $subcontractorBillItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\SubcontractorBillItem  $subcontractorBillItem
     * @return \Illuminate\Http\Response
     */
    public function edit(SubcontractorBillItem $subcontractorBillItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\SubcontractorBillItem  $subcontractorBillItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubcontractorBillItem $subcontractorBillItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\SubcontractorBillItem  $subcontractorBillItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubcontractorBillItem $subcontractorBillItem)
    {
        if($subcontractorBillItem === null){
            return response()->json(['error' => 'Client bill item not found'], 404);
        } else {
            $subcontractorBillItem->status = 'canceled';
            $subcontractorBillItem->save();
            return new SubcontractorBillItemResource($subcontractorBillItem);
        }
    }
}
