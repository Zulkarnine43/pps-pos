<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\NewProjectAssignItemResource;
use App\Laravue\Models\NewProjectAssignItem;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class NewProjectAssignItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $data = NewProjectAssignItem::query();
        $keyword = Arr::get($searchParams, 'keyword', '');
        $status = Arr::get($searchParams, 'status', '');
        // search query
        if (!empty($status)) {
            $data->where('status', $status);
        }
        if (!empty($keyword)) {
            $data->where('reference_number', 'LIKE', '%' . $keyword . '%');
        }

        return NewProjectAssignItemResource::collection($data->orderBy('id', 'desc')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\NewProjectAssignItem  $newProjectAssignItem
     * @return \Illuminate\Http\Response
     */
    public function show(NewProjectAssignItem $newProjectAssignItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\NewProjectAssignItem  $newProjectAssignItem
     * @return \Illuminate\Http\Response
     */
    public function edit(NewProjectAssignItem $newProjectAssignItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\NewProjectAssignItem  $newProjectAssignItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NewProjectAssignItem $newProjectAssignItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\NewProjectAssignItem  $newProjectAssignItem
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = NewProjectAssignItem::find($id);
        $project->delete();

        return new NewProjectAssignItemResource($project);
    }
}
