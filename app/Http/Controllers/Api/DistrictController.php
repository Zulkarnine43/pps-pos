<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\DistrictResource;
use App\Laravue\Models\District;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class DistrictController extends Controller
{
    const ITEM_PER_PAGE = 100;
    /**
     * index.
     *
     * @author	Display a listing of the resource.
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	public
     * @param	mixed	$state_id
     * @return	all the district list
     */
    public function index(Request $request){
        $searchParams = $request->all();
        $districtQuery = District::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        // search query
        if (!empty($keyword)) {
            $districtQuery->where('name', 'LIKE', '%' . $keyword . '%');
        }

        return DistrictResource::collection($districtQuery->orderBy('name', 'asc')->paginate($limit));
    }

    public function districtByState($state_id){
        $districts = District::where('state_id', $state_id)->orderBy('name', 'asc')->get();
        return response()->json(['districts' => $districts], 200);
    }

    // public function allDistrict(){
    //     $districts = District::orderBy('name', 'asc')->get();
    //     return response()->json(['districts' => $districts], 200);
    // }
}
