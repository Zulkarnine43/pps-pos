<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\DeliveryResource;
use App\Laravue\Models\ProjectInventory;
use App\Laravue\Models\Delivery;
use App\Laravue\Models\DeliveryItem;
use App\Laravue\Models\WarehouseInventory;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Validator;
use Auth;
use Illuminate\Support\Facades\DB;

class DeliveryController extends Controller
{
    const ITEM_PER_PAGE = 100;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      // dd('hi');


        $searchParams = $request->all();
        $deliveryQuery = Delivery::query();
        $deliveryQuery = Delivery::with('fromWarehouse','fromProject','toWarehouse','toProject','deliveryItems.product');

        // $deliveryQuery = Delivery::with('fromInventory','toInventory')->get();
        // $deliveryQuery = Delivery::with('fromInventory')->get();
        // dd($deliveryQuery);
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        $date = Arr::get($searchParams, 'date', '');
        
        if (!empty($keyword)) {
            $deliveryQuery->where('gatepass_number', 'LIKE', '%' . $keyword . '%');
        }
        if (!empty($date)) {
            $deliveryQuery->where('delivery_date', 'LIKE', '%' . $date . '%');
        }

        return DeliveryResource::collection($deliveryQuery->orderBy('id', 'asc')->paginate($limit));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  get validation rules from getValidationRules method
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules($isNew = true, $delivery = null),
            )
        );
        // dd('hi', $request->ItemRows, $request->all());

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
          // try{
            $delivery = new Delivery();
            $delivery->gatepass_number = $request->gatepass_number;
            $delivery->created_by = Auth::id();
            // DB::beginTransaction();
            $this->saveDeliveryData($delivery, $request);
            // DB::commit();
            return new DeliveryResource($delivery);
          // } catch(\Exception $e){
          //   DB::rollBack();
          // }
        }
    }

    public function saveDeliveryData($delivery, $request){
        $delivery->delivery_date = $request->delivery_date;
        $delivery->delivery_type = $request->delivery_type;
        $delivery->from = $request->selectedFromId;
        $delivery->to = $request->selectedToId;
        $delivery->delivered_by = $request->delivered_by;
        $delivery->delivered_by_phone = $request->delivered_by_phone;
        $delivery->save();
        if(count($request->ItemRows)){
            $itemRows = $request->ItemRows;
            foreach($itemRows as $itemRow){
                if(isset($itemRow['id'])){
                    $this->updateDeliveryItem($itemRow);
                } else {
                    $this->saveDeliveryItem($itemRow, $delivery);
                }   
            }
        }
    }

    public function saveDeliveryItem($itemRow, $delivery){
        $row = new DeliveryItem();
        $row->delivery_id = $delivery->id;
        // dd('saveDeliveryItem',$row,$delivery);
        $this->saveDeliveryItemData($itemRow, $row);
    }

    public function updateDeliveryItem($itemRow){
        $row = DeliveryItem::find($itemRow['id']);
        $this->saveDeliveryItemData($itemRow, $row);
    }

    public function saveDeliveryItemData($itemRow, $row){
        // dd('saveDeliveryItemData',$row);
        $row->delivery_id = $row['delivery_id'];
        $row->product_id = $itemRow['product_id'];
        $row->quantity = $itemRow['quantity'];
        $row->save();
    }
 

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\Delivery  $delivery
     * @return \Illuminate\Http\Response
     */
    public function show(Delivery $delivery)
    {
      $deliveryQuery = Delivery::with('fromWarehouse','fromProject','toWarehouse','toProject','deliveryItems.product')->find($delivery->id);
      return new DeliveryResource($deliveryQuery);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\Delivery  $delivery
     * @return \Illuminate\Http\Response
     */
    public function edit(Delivery $delivery)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\Delivery  $delivery
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Delivery $delivery)
    {
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules($isNew = false, $delivery),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            $this->saveDeliveryData($delivery, $request);
            return new DeliveryResource($delivery);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\Delivery  $delivery
     * @return \Illuminate\Http\Response
     */
    public function destroy(Delivery $delivery)
    {
        //
    }

    /**
     * getValidationRules.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	private
     * @param	boolean	$isNew	Default: true
     * @return	array
     */
    private function getValidationRules($isNew = true, $delivery)
    {
        return [
            'gatepass_number' => $isNew ? 'required|unique:deliveries' : 'required|unique:deliveries,gatepass_number,' . $delivery->id,
            'delivery_date' => 'required',
            'delivery_type' => 'required',
            'selectedFromId' => 'required',
            'selectedToId' => 'required',
        ];
    }
}
