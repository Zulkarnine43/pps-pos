<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Laravue\Models\NewProjectAssign;
use App\Http\Resources\NewProjectAssignResource;
use App\Laravue\Models\NewProjectAssignItem;
use App\Laravue\Models\Project;
use Validator;
use Auth;

class NewProjectAssignController extends Controller
{
    const ITEM_PER_PAGE = 100;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $data = NewProjectAssign::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        $status = Arr::get($searchParams, 'status', '');
        $subcontractor_id = Arr::get($searchParams, 'subcontractor_id', '');
        // search query
        if (!empty($status)) {
            $data->where('status', $status);
        }
        if (!empty($subcontractor_id)) {
            $data->where('subcontractor_id', $subcontractor_id);
        }
        if (!empty($keyword)) {
            $data->where('reference_number', 'LIKE', '%' . $keyword . '%');
        }

        return NewProjectAssignResource::collection($data->orderBy('id', 'desc')->paginate($limit));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        }else{
            $new_assaign = new NewProjectAssign();
            $new_assaign->reference_number = $request->reference_number;
            $new_assaign->prepared_date = $request->prepared_date;
            $new_assaign->subcontractor_id = $request->subcontractor_id;
            $new_assaign->department_id = $request->department_id;
            $new_assaign->prepared_by = Auth::id();

            if(count($request->newProjects) > 0){
                $assaignItems = $request->newProjects;
                $new_assaign->save();
                foreach($assaignItems as $item){
                    $npi = new NewProjectAssignItem();
                    $npi->new_project_assign_id = $new_assaign->id;
                    if(isset($item['project_id'])){
                        $npi->project_id = $item['project_id'];
                        $npi->status = 'assigned';
                        $this->updateProject($item, $new_assaign, $npi);
                    }
                    $npi->client_id = $item['client_id'];
                    $npi->project_type_id = $item['project_type_id'];
                    $npi->details = $item['details'];
                    $npi->remark = $item['remark'];
                    $npi->quantity = $item['quantity'];
                    $npi->save();
                    
                }
            }
            $this->updateStatus($new_assaign);
            return new NewProjectAssignResource($new_assaign);
        }
        
    }

    public function updateStatus($new_assaign){
        $new_assaign->status = 'all assaigned';
        foreach($new_assaign->projects as $project){
            if(empty($project->project_id)){
                $new_assaign->status = 'partial assaigned';
            }
        }
        $new_assaign->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\NewProjectAssign  $newProjectAssign
     * @return \Illuminate\Http\Response
     */
    public function show(NewProjectAssign $newProjectAssign)
    {
        return new NewProjectAssignResource($newProjectAssign);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\NewProjectAssign  $newProjectAssign
     * @return \Illuminate\Http\Response
     */
    public function edit(NewProjectAssign $newProjectAssign)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\NewProjectAssign  $newProjectAssign
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NewProjectAssign $newProjectAssign)
    {
        // dd($request);
        if ($newProjectAssign === null) {
            return response()->json(['error' => 'Assaigned Project not found'], 404);
        } else {
            $newProjectAssign->department_id = $request->department_id;
            $newProjectAssign->save();
            if(count($request->projects) > 0){
                foreach($request->projects as $project){
                    if(isset($project['new_project_assign_id'])){
                        $pro = NewProjectAssignItem::find($project['id']);
                        $pro->details = $project['details'];
                        $pro->quantity = $project['quantity'];
                        $pro->remark = $project['remark'];
                        if($project['project_id']){
                            $pro->status = 'assigned';
                            $this->updateProject($project, $newProjectAssign, $pro);
                        }
                        $pro->save();
                    }else {
                        $pro = new NewProjectAssignItem();
                        $pro->new_project_assign_id = $newProjectAssign->id;
                        if($project['project_id']){
                            $pro->status = 'assigned';
                            $this->updateProject($project, $newProjectAssign, $pro);
                        }
                        $pro->details = $project['details'];
                        $pro->quantity = $project['quantity'];
                        $pro->remark = $project['remark'];
                        $pro->save();
                    }
                }
            }
            $this->updateStatus($newProjectAssign);
            return new NewProjectAssignResource($newProjectAssign);
        }
    }

    public function updateProject($project, $new, $pro){
        $prjct = Project::find($project['project_id']);
        if(empty($prjct->new_project_assign_id)){
            $pro->project_id = $project['project_id'];
            $pro->client_id = $project['client_id'];
            $pro->project_type_id = $project['project_type_id'];
            $prjct->new_project_assign_id = $new->id;
        
            $prjct->save();
        } else {
            return response()->json(['msg' => 'project '. $prjct->site_code. ' is already assaigned']);
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\NewProjectAssign  $newProjectAssign
     * @return \Illuminate\Http\Response
     */
    public function destroy(NewProjectAssign $newProjectAssign)
    {
        //
    }

    private function getValidationRules($project = null,$isNew = true)
    {
        return [
            'subcontractor_id' => 'required',
            'department_id' => 'required',
            'prepared_date' => 'required',
            'reference_number' => $isNew ? 'required|unique:new_project_assigns' : 'required|unique:new_project_assigns,reference_number,'.$project->id,
        ];
    }
}
