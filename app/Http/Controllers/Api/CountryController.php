<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CountryResource;
use App\Laravue\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class CountryController extends Controller{

    const ITEM_PER_PAGE = 100;
    /**
     * Display a listing of the resource.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	public
     * @return	all the country list
     *
     */
    public function index(Request $request){
        $searchParams = $request->all();
        $countryQuery = Country::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        // search query
        if (!empty($keyword)) {
            $countryQuery->where('name', 'LIKE', '%' . $keyword . '%');
        }

        return CountryResource::collection($countryQuery->where('status', 1)->orderBy('name', 'asc')->paginate($limit));
    }

    public function activeCountry(){
        $countries = Country::where('status',1)->orderBy('name', 'asc')->get();
        return response()->json(['countries' => $countries], 200);
    }
}
