<?php

namespace App\Http\Controllers\Api;

use File;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Laravue\Models\Category;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\CategoryResource;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    const ITEM_PER_PAGE = 100;
    public $image;
    /**
     * Display a listing of the resource.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	public
     * @param	request	$request
     * @return	all category list
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $categoryQuery = Category::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        // search query
        if (!empty($keyword)) {
            $categoryQuery->where('name', 'LIKE', '%' . $keyword . '%');
        }

        return CategoryResource::collection($categoryQuery->orderBy('id', 'desc')->paginate($limit));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function getImage(Request $request)
    {
        if ($request->file('photo')) {
            $file_name = time() . '-' . $request->file('photo')->getClientOriginalName();
            $request->file('photo')->storePubliclyAs(
                'categories/images',
                $file_name,
                's3'
            );
            return response()->json(['imageName' => $file_name]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	public
     * @param	request	$request
     * @return	void
     */
    public function store(Request $request)
    {
        // check validation rules from getValidationRules method
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {

            if ($request->image) {
                $image = 'categories/images/' . $request->image;
            } else {
                $image = '';
            }
            // category creating
            $category = Category::create([
                'name' => $request->name,
                'slug' => Str::slug($request->name),
                'code' => $request->code,
                'description' => $request->description,
                'image' => $image,
            ]);
            return new CategoryResource($category);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        return new CategoryResource($category);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	public
     * @param	request 	$request
     * @param	category	$category
     * @return	void
     */
    public function update(Request $request, Category $category)
    {
        // check requested category
        if ($category === null) {
            return response()->json(['error' => 'category not found'], 404);
        }

        $category_image = $category->image;
        if ($request->image) {
            if ($category_image) {

                Storage::disk('s3')->delete("/categories/images/" . $category_image);
            }
        }

        // check validation rules from getValidationRules method
        $validator = Validator::make($request->all(), $this->getValidationRules(false));
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {

            //update requested category
            $category->name = $request->get('name');
            $category->code = $request->get('code');
            $category->description = $request->get('description');
            if ($request->image) {
                $category->image = 'categories/images/' . $request->get('image');
            }
            $category->save();
            return new CategoryResource($category);
        }
    }

    /**
     * For unmute the muted category.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	public
     * @param	request 	$request
     * @param	category	$category
     * @return	mixed
     */
    public function unmute(Request $request, Category $category)
    {
        if ($category === null) {
            return response()->json(['error' => 'Category not found'], 404);
        }

        $category->status = 0;

        $category->save();
        return new CategoryResource($category);
    }

    /**
     * For mute the active category.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	public
     * @param	request 	$request
     * @param	category	$category
     * @return	mixed
     */
    public function mute(Request $request, Category $category)
    {
        if ($category === null) {
            return response()->json(['error' => 'Category not found'], 404);
        }

        $category->status = 1;

        $category->save();
        return new CategoryResource($category);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }

    /**
     * getValidationRules.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	private
     * @param	boolean	$isNew	Default: true
     * @return	array
     */
    private function getValidationRules($isNew = true)
    {
        return [
            'code' => $isNew ? 'nullable|unique:categories' : 'nullable',
            'name' => $isNew ? 'required|unique:categories' : 'required',
        ];
    }
}
