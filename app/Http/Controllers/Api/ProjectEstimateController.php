<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProjectEstimateResource;
use App\Laravue\Models\Product;
use App\Laravue\Models\ProjectEstimate;
use Illuminate\Http\Request;
use Validator;

class ProjectEstimateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projectEstimateQuery = ProjectEstimate::query();

        return ProjectEstimateResource::collection($projectEstimateQuery->orderBy('id', 'desc')->get());
    }

    public function projectEstimations($pid){
        $estimations = ProjectEstimate::where('project_id', $pid)->get();
        return ProjectEstimateResource::collection($estimations);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // check validation rules from getValidationRules method
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            // creating new project estimate
            $product = Product::where('id', $request->product_id)->first();
            $pEstimate = new ProjectEstimate();
            $pEstimate->project_id = $request->project_id;
            $pEstimate->product_id = $request->product_id;
            $pEstimate->quantity = $request->quantity;
            $pEstimate->unit_id = $request->unit_id;
            $pEstimate->price = $product->price;
            $pEstimate->total = $product->price * $request->quantity;
            $pEstimate->save();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\ProjectEstimate  $projectEstimate
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectEstimate $projectEstimate)
    {
        return new ProjectEstimateResource($projectEstimate);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\ProjectEstimate  $projectEstimate
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectEstimate $projectEstimate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\ProjectEstimate  $projectEstimate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProjectEstimate $projectEstimate)
    {
        $projectEstimate->price = $request->price;
        $projectEstimate->quantity = $request->quantity;
        $projectEstimate->unit_id = $request->unit_id;
        $projectEstimate->total = $request->price * $request->quantity;

        $projectEstimate->save();
        return new ProjectEstimateResource($projectEstimate);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\ProjectEstimate  $projectEstimate
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectEstimate $projectEstimate)
    {
        if($projectEstimate){
            $projectEstimate->delete();
            return new ProjectEstimateResource($projectEstimate);
        }
    }

    private function getValidationRules()
    {
        return [
            'product_id' => 'required',
            'quantity' => 'required',
        ];
    }
}
