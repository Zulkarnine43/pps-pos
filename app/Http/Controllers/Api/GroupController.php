<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Laravue\Models\Group;
use Illuminate\Http\Request;
use App\Http\Resources\GroupResource;
use Illuminate\Support\Arr;
use Validator;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $groupQuery = Group::query();
        $limit = Arr::get($searchParams, 'limit');
        $keyword = Arr::get($searchParams, 'keyword', '');
        // search query
        if (!empty($keyword)) {
            $groupQuery->where('name', 'LIKE', '%' . $keyword . '%');
        }

        return GroupResource::collection($groupQuery->orderBy('id', 'desc')->paginate($limit));
    }

    public function getAll(){
        $groups = Group::orderBy('name')->get();
        return GroupResource::collection($groups);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  get validation rules from getValidationRules method
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules($isNew = true, $group = null),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            $group = new Group();
            $group->name = $request->name;
            $group->parent_group_id = $request->parent_group_id;
            $group->default_group_id = $request->default_group_id;
            $group->nature = $request->nature;
            $group->cash_flow_type = $request->cash_flow_type;
            $group->effect_gross_profit = $request->effect_gross_profit;
            $group->save();

            return new GroupResource($group);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function show(Group $group)
    {
        return new GroupResource($group);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function edit(Group $group)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Group $group)
    {
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules($isNew = false,$group),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            $group->name = $request->name;
            $group->parent_group_id = $request->parent_group_id;
            $group->default_group_id = $request->default_group_id;
            $group->nature = $request->nature;
            $group->cash_flow_type = $request->cash_flow_type;
            $group->effect_gross_profit = $request->effect_gross_profit;
            $group->save();

            return new GroupResource($group);
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function destroy(Group $group)
    {
        //
    }

    /**
     * getValidationRules.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	private
     * @param	boolean	$isNew	Default: true
     * @return	array
     */
    private function getValidationRules($isNew = true, $group)
    {
        return [
            'name' => $isNew ? 'required|unique:groups' : 'required|unique:groups,name,'.$group->id,
        ];
    }
}
