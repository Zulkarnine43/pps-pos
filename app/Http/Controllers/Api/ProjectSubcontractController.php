<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProjectSubcontractResource;
use App\Laravue\Models\ProjectSubcontract;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Validator;
use App\Laravue\Models\Project;

class ProjectSubcontractController extends Controller
{
    const ITEM_PER_PAGE = 100;

    public function index(Request $request)
    {
        $searchParams = $request->all();
        $projectSubQuery = ProjectSubcontract::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        $subcontractor = Arr::get($searchParams, 'subcontractor', '');
        $date = Arr::get($searchParams, 'date', '');
        $project = Arr::get($searchParams, 'project', '');
        // search query
        if (!empty($keyword)) {
            $projectSubQuery
            ->where('code', 'LIKE', '%' . $keyword . '%')
            ->orWhere('order_no', 'LIKE', '%' . $keyword . '%')
            ->orWhere('contract_ref', 'LIKE', '%' . $keyword . '%')
            ->orWhere('payable_amount', 'LIKE', '%' . $keyword . '%');
            
        }
        // search according to user


        if (!empty($project)) {
            $projectSubQuery->where('project_id', $project );
        }

     
        if (!empty($subcontractor)) {
            $projectSubQuery->where('subcontractor_id', $subcontractor );
        }

       

        if (!empty($date)) {
            $projectSubQuery->where('issue_date', $date );
        }
    

        return ProjectSubcontractResource::collection($projectSubQuery->orderBy('id', 'desc')->paginate($limit));
    }

    /**
     * getProSub.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Wednesday, March 3rd, 2021.
     * @access	public
     * @param	mixed	$pid
     * @return	project wise subcontract list
     */
    public function getProSub($pid){
        $proSub = ProjectSubcontract::where('project_id', $pid)->orderBy('id', 'desc')->get();
        return ProjectSubcontractResource::collection($proSub);
    }


    public function getAllSubcontract()
    {
        $allSubcontract = ProjectSubcontract::all();
        return ProjectSubcontractResource::collection($allSubcontract);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->project_id);
        // get validation rules from getValidationRules method
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            // dd($request);
            // creating project subcontract
            $projectSub = ProjectSubcontract::create([
                'project_id' => $request->project_id,
                'subcontractor_id' => $request->subcontractor_id,
                'department_id' => $request->department_id,
                'order_no' => $request->order_no,
                'contract_ref' => $request->contract_ref,
                'code' => $request->code,
                'task_details' => $request->task_details,
                'payable_amount' => $request->payable_amount,
                'project_stage_id' => $request->project_stage_id,
                'issue_date' => date('Y-m-d', strtotime($request->issue_date)),
                'deadline' => date('Y-m-d', strtotime($request->deadline)),
                'finished_date' => date('Y-m-d', strtotime($request->finished_date)),
            ]);

            return new ProjectSubcontractResource($projectSub);
        }
    }


    public function bulkStore(Request $request)
    {
        // dd($request[1]);
        $p_ids = [];
        $subs = $request[0];
        foreach($subs as $sub){
            array_push($p_ids, $sub['id']);
        }
        foreach ($p_ids as $id) {
        // creating project subcontract
            $projectSub = new ProjectSubcontract();
            if(isset($request[1]['contract_ref'])){
                $projectSub->contract_ref = $request[1]['contract_ref'];
            }
            if(isset($request[1]['department_id'])){
                $projectSub->department_id = $request[1]['department_id'];
            }
            if(isset($request[1]['subcontractor_id'])){
                $projectSub->subcontractor_id = $request[1]['subcontractor_id'];
            }
            if(isset($request[1]['task_details'])){
                $projectSub->task_details = $request[1]['task_details'];
            }
            if(isset($request[1]['payable_amount'])){
                $projectSub->payable_amount = $request[1]['payable_amount'];
            }
            if(isset($id)){
                $projectSub->project_id = $id;
            }
            if(isset($sub['stage_id'])){
                $projectSub->project_stage_id = $sub['stage_id'];
            }
            if(isset($request[1]['issue_date'])){
                $projectSub->issue_date = $request[1]['issue_date'];
            }
            if(isset($request[1]['deadline'])){
                $projectSub->deadline = $request[1]['deadline'];
            }
            if(isset($request[1]['finished_date'])){
                $projectSub->finished_date = $request[1]['finished_date'];
            }

            $projectSub->save();
        }

        return new ProjectSubcontractResource($projectSub);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\ProjectSubcontract  $projectSubcontract
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectSubcontract $projectSubcontract)
    {
        return new ProjectSubcontractResource($projectSubcontract);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\ProjectSubcontract  $projectSubcontract
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectSubcontract $projectSubcontract)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\ProjectSubcontract  $projectSubcontract
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProjectSubcontract $projectSubcontract)
    {
        // check project subcontract
        if ($projectSubcontract === null) {
            return response()->json(['error' => 'Project Subcontract not found'], 404);
        }

        $validator = Validator::make($request->all(), $this->getValidationRules(false));
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            // update project subcontract
            $projectSubcontract->subcontractor_id = $request->get('subcontractor_id');
            $projectSubcontract->code = $request->get('code');
            $projectSubcontract->department_id = $request->get('department_id');
            $projectSubcontract->order_no = $request->get('order_no');
            $projectSubcontract->contract_ref = $request->get('contract_ref');
            $projectSubcontract->task_details = $request->get('task_details');
            $projectSubcontract->payable_amount = $request->get('payable_amount');
            $projectSubcontract->issue_date = date('Y-m-d', strtotime($request->get('issue_date')));
            $projectSubcontract->deadline = date('Y-m-d', strtotime($request->get('deadline')));
            $projectSubcontract->finished_date = date('Y-m-d', strtotime($request->get('finished_date')));
            $projectSubcontract->save();
            return new ProjectSubcontractResource($projectSubcontract);
        }
    }


    public function done($id){
        $projectSubcontract = ProjectSubcontract::find($id);
        $projectSubcontract->status = 1;
        $projectSubcontract->save();
        return new ProjectSubcontractResource($projectSubcontract);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\ProjectSubcontract  $projectSubcontract
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectSubcontract $projectSubcontract)
    {
        //
    }

    /**
     * getValidationRules.
     *
     * @author	bitbyte
     * @since	v0.0.1
     * @version	v1.0.0	Tuesday, February 9th, 2021.
     * @access	private
     * @param	boolean	$isNew	Default: true
     * @return	array
     */
    private function getValidationRules($isNew = true)
    {
        return [
            'project_id' => 'required|numeric',
            'subcontractor_id' => 'required|numeric',
            'task_details' => 'required',
            'payable_amount' => 'required',
            'issue_date' => 'required',
            'deadline' => 'required',
        ];
    }
}
