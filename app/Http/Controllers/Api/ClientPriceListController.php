<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Laravue\Models\ClientPriceList;
use App\Laravue\Models\ClientPriceListItems;
use App\Http\Resources\ClientPriceListResource;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Validator;
use Auth;

class ClientPriceListController extends Controller
{
    const ITEM_PER_PAGE = 100;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $cplQuery = ClientPriceList::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        // search query
        if (!empty($keyword)) {
            $cplQuery->where('client_id', $keyword);
        }

        return ClientPriceListResource::collection($cplQuery->orderBy('id', 'DESC')->paginate($limit));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // get validation rules from getValidationRules method
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            // create new Cpl
            $cpl = ClientPriceList::create([
                'delivery_date' => $request->delivery_date,
                'user_id' => Auth::id(),
                'client_id' => $request->client_id,
                'discount' => $request->discount,
                'shipping_cost' => $request->shipping_cost,
                'note' => $request->note,
                'grand_total' => $request->grandTotal,
                'discount_type' => $request->discount_type,
            ]);
            if(isset($request->entry_date)){
                $cpl->entry_date = $request->entry_date;
            }else {
                $cpl->entry_date = date("Y-m-d");
            }
            $cpl->save();
            // insert clientPriceList products
            if ($request->products) {
                $products = $request->products;
                if(count($products) > 0){
                    foreach ($products as $product) {
                        if (isset($product)) {
                            $cplItem = new ClientPriceListItems();
                            $cplItem->client_price_list_id = $cpl->id;
                            $cplItem->product_id = $product['id'];
                            $cplItem->quantity = $product['quantity'];
                            $cplItem->price = $product['price'];
                            $cplItem->discount = $product['discount'];
                            $cplItem->total = $product['total'];
                            $cplItem->save();
                        }
                    }
                }
            }
            return new ClientPriceListResource($cpl);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Laravue\Models\ClientPriceList  $clientPriceList
     * @return \Illuminate\Http\Response
     */
    public function show(ClientPriceList $clientPriceList)
    {
        return new ClientPriceListResource($clientPriceList);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Laravue\Models\ClientPriceList  $clientPriceList
     * @return \Illuminate\Http\Response
     */
    public function edit(ClientPriceList $clientPriceList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Laravue\Models\ClientPriceList  $clientPriceList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClientPriceList $clientPriceList)
    {
        
        // dd($request);

        if ($clientPriceList === null) {
            return response()->json(['error' => 'clientPriceList not found'], 404);
        }

        $validator = Validator::make($request->all(), $this->getValidationRules(false));
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        }else{
            $clientPriceList->entry_date = $request->get('entry_date');
            $clientPriceList->client_id = $request->get('client_id');
            $clientPriceList->discount = $request->get('discount');
            $clientPriceList->shipping_cost = $request->get('shipping_cost');
            $clientPriceList->note = $request->get('note');
            $clientPriceList->grand_total = $request->get('grand_total');
            $clientPriceList->discount_type = $request->get('discount_type');
            // update listItems 
            if ($request->get('listItems')) {
                $listItems = $request->get('listItems');
                if(count($listItems) > 0){
                    foreach ($listItems as $item) {
                        if (isset($item)) {
                            if (isset($item['id'])) {
                                // updating exsisting purchase items
                                $this->PItem = ClientPriceListItems::find($item['id']);
                                $this->PItem->client_price_list_id = $clientPriceList->id;
                                $this->PItem->product_id = $item['product_id'];
                                $this->PItem->quantity = $item['quantity'];
                                $this->PItem->price = $item['price'];
                                $this->PItem->discount = $item['discount'];
                                $this->PItem->total = $item['total'];
                                $this->PItem->save();
                            } else {
                                // insert new purchase item
                                $this->PItem = new ClientPriceListItems();
                                $this->PItem->client_price_list_id = $clientPriceList->id;
                                $this->PItem->product_id = $item['product']['id'];
                                $this->PItem->quantity = $item['quantity'];
                                $this->PItem->price = $item['product']['price'];
                                $this->PItem->discount = $item['discount'];
                                $this->PItem->total = $item['total'];
                                $this->PItem->save();
                            }
                        }
                    }
                }
            }
            $clientPriceList->save();
            return new ClientPriceListResource($clientPriceList);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Laravue\Models\ClientPriceList  $clientPriceList
     * @return \Illuminate\Http\Response
     */
    public function destroy(ClientPriceList $clientPriceList)
    {
        //
    }

    private function getValidationRules($isNew = true)
    {
        return [
            'entry_date' => 'nullable|date',
            'delivery_date' => 'nullable|date',
            'client_id' => 'required',
        ];
    }
}
