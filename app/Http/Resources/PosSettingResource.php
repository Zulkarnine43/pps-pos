<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PosSettingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'vat' => $this->vat,
            'vat_status' => $this->vat_status,
            'tax' => $this->tax,
            'vat_registration_number' => $this->vat_registration_number,
            'tax_registration_number' => $this->tax_registration_number,
            'warehouse_id' => $this->warehouse_id,
            'warehouse' => $this->warehouse,
            'customer_id' => $this->customer_id,
            'customer' => $this->customer,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'shop_name' => $this->shop_name,
            'address' => $this->address,
            'office_phone' => $this->office_phone,
            'mushak' => $this->mushak,
            'bill_footer' => $this->bill_footer,
            'logo' => $this->logo,
            'print_paper_size_id' => $this->print_paper_size_id,
            'currency' => $this->currency,
            'sale_id_prefix' => $this->sale_id_prefix,
            'print_page_top_margin' => $this->print_page_top_margin,
            'print_page_bottom_margin' => $this->print_page_bottom_margin,
            'print_page_left_margin' => $this->print_page_left_margin,
            'print_page_right_margin' => $this->print_page_right_margin,
        ];
    }
}
