<?php

namespace App\Http\Resources;

use App\Laravue\Models\Journal;
use Illuminate\Http\Resources\Json\JsonResource;

class JournalItemCostCenterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $journal = Journal::where('id', $this->journalItem->journal_id)->first();
        return [
            'id' => $this->id,
            'amount' => $this->amount,
            'cost_center_type' => $this->cost_center_type,
            'journal_item_id' => $this->journal_item_id,
            'journalItem' => $this->journalItem,
            'journal' => $journal,
            'project_id' => $this->project_id,
            'site_code' => $this->site_code,
            'project' => $this->project,
            'project_type_id' => $this->project_type_id,
            'created_at' => $this->created_at->format('d-m-Y'),
        ];
    }
}
