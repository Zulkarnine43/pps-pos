<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProjectStageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {   
        
        return [
            'id' => $this->id,
            'stage_name' => $this->stage_name,
            'project_id' => $this->project_id,
            'work_done' => $this->work_done,
            'finished_date' => $this->finished_date,
            'stage_amount' => $this->stage_amount,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'type_stage_id' => $this->type_stage_id,
            'image_required' => $this->image_required,
            'images' => $this->images,
            'subcontractorBill' => $this->subcontractorBill,
            'clientBill' => $this->clientBill,
            'project' => $this->project,
        ];
    }
}
