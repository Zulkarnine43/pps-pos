<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProjectRequisitionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'prs_number' => $this->prs_number,
            'name' => $this->name,
            'requisition_date' => $this->requisition_date,
            'nid_number' => $this->nid_number,
            'location' => $this->location,
            'bn_address' => $this->bn_address,
            'gps' => $this->gps,
            'site_code' => $this->site_code,
            'project_type_id' => $this->project_type_id,
            'requisition_by' => $this->requisition_by,
            'quantity' => $this->quantity,
            'details' => $this->details,
            'remark' => $this->remark,
            'status' => $this->status,
            'phone' => $this->phone,
            'projectType' => $this->projectType,
            'country_id' => $this->country_id,
            'state_id' => $this->state_id,
            'district_id' => $this->district_id,
            'city_id' => $this->city_id,
        ];
    }
}
