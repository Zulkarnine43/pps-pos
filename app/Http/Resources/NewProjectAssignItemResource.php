<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Laravue\Models\Project;

class NewProjectAssignItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // dd('hello');
        $site_code = "";
        $project_amount = 0;
        $project_stage_id = "";
        $unit_price = 0;
        $site_location = '';
        if(isset($this->project->site_code)){
            $site_code = $this->project->site_code;
            $project_amount = $this->project->project_amount;
            $project_stage_id = $this->project->stage_id;
            $unit_price = $this->project->unit_price;

            $state_name = "";
            $district_name = "";
            $city_name = "";
            if($this->project->state){
                $state_name = $this->project->state->name;
            }
            if($this->project->district){
                $district_name = $this->project->district->name;
            }
            if($this->project->city){
                $city_name = $this->project->city->name;
            }

            $site_location = $this->project->location . ' ,'. $city_name. ' ,'. $district_name. ' ,'. $state_name;
        }
        return [
            'id' => $this->id,
            'new_project_assign_id' => $this->new_project_assign_id,
            'project_id' => $this->project_id,
            'details' => $this->details,
            'remark' => $this->remark,
            'quantity' => $this->quantity,
            'status' => $this->status,
            'site_code' => $site_code,
            'project_amount' => $project_amount,
            'project_stage_id' => $project_stage_id,
            'unit_price' => $unit_price,
            'client_name' => $this->client->name,
            'project_type_name' => $this->projectType->name,
            'client_id' => $this->client_id,
            'project_type_id' => $this->project_type_id,
            'site_location' => $site_location,
        ];
    }
}
