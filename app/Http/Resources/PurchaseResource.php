<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\PurchaseItemResource;

class PurchaseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // dd($this->purchaseProduct);
        return [
            'id' => $this->id,
            'entry_date' => $this->entry_date,
            'created_by' => $this->created_by,
            'creator' => $this->creator->name,
            'warehouse_id' => $this->warehouse_id,
            'warehouse' => $this->warehouse->name,
            'supplier_id' => $this->supplier_id,
            'supplier' => $this->supplier->name,
            'ref_number' => $this->ref_number,
            'discount' => $this->discount,
            'discount_type' => $this->discount_type,
            'shipping_cost' => $this->shipping_cost,
            'grand_total' => $this->grand_total,
            'status' => $this->status,
            'purchaseItems' => PurchaseItemResource::collection($this->purchaseItems),
            'products' => $this->purchaseItems,
            'note' => $this->note,
            'payment_condition' => $this->payment_condition,
            // 'purchaseProduct' => $this->purchaseProduct,
        ];
    }
}
