<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'category_id' => $this->category_id,
            'category_name' => $this->category->name,
            'brand_id' => $this->brand_id,
            'brand_name' => $this->brand->name,
            'unit_id' => $this->unit_id,
            'unit_name' => $this->unit->name,
            'price' => $this->price,
            'purchase_price' => $this->purchase_price,
            'discount_price' => $this->discount_price,
            'code' => $this->code,
            'image' => $this->image,
            'description' => $this->description,
            'status' => $this->status,
            'product_id' => $this->product_id,
            'sku' => $this->sku,
            'alert_quantity' => $this->alert_quantity,
            'quantity' => 1,
        ];
    }
}
