<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\NewProjectAssignItemResource;

class NewProjectAssignResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // $projects = [];
        // if(count($this->projects) > 0){}
        
        return [
            'id' => $this->id,
            'department_id' => $this->department_id,
            'reference_number' => $this->reference_number,
            'subcontractor_id' => $this->subcontractor_id,
            'prepared_by' => $this->prepared_by,
            'prepared_date' => $this->prepared_date,
            'checked_by' => $this->checked_by,
            'checked_date' => $this->checked_date,
            'approved_by' => $this->approved_by,
            'approved_date' => $this->approved_date,
            'status' => $this->status,
            'subcontractor' => $this->subcontractor,
            'department' => $this->department,
            'projects' => NewProjectAssignItemResource::collection($this->projects),
        ];
    }
}
