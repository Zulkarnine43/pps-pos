<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProjectEstimateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'project_id' => $this->project_id,
            'product_id' => $this->product_id,
            'unit_id' => $this->unit_id,
            'unit' => $this->unit,
            'product' => $this->product,
            'productName' => $this->product->name,
            'price' => $this->price,
            'quantity' => $this->quantity,
            'total' => $this->total,
            'id' => $this->id,
            'status' => $this->status,
        ];
    }
}
