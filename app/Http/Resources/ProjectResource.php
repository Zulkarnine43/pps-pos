<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProjectResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $totalWorkDone = 0;
        foreach ($this->doneMilestones as $milestone) {
            $totalWorkDone += substr_replace($milestone->work_done ,"",-1);
        }
        $state_name = "";
        $district_name = "";
        $city_name = "";
        if($this->state){
            $state_name = $this->state->name;
        }
        if($this->district){
            $district_name = $this->district->name;
        }
        if($this->city){
            $city_name = $this->city->name;
        }

        $site_location = $this->location . ' ,'. $city_name. ' ,'. $district_name. ' ,'. $state_name;
        
        return [
            'id'  => $this->id,
            'name' => $this->name,
            'site_code' => $this->site_code,
            'country' => $this->country,
            'country_id' => $this->country_id,
            'state' => $this->state,
            'state_id' => $this->state_id,
            'district' => $this->district,
            'district_id' => $this->district_id,
            'city' => $this->city,
            'city_id' => $this->city_id,
            'coordinator' => $this->coordinator,
            'location' => $this->location,
            'details' => $this->details,
            'prepaired_by' => $this->prepaired_by,
            'project_amount' => $this->project_amount,
            'deadline' => $this->deadline,
            'start_date' => $this->start_date,
            'finished_date' => $this->finished_date,
            'approved_date' => $this->approved_date,
            'status' => $this->status,
            'project_status_id' => $this->project_status_id,
            'projectStatus' => $this->projectStatus,
            'site_location' => $site_location,
            'totalWorkDone' => $totalWorkDone,
            'zone_id' => $this->zone_id,
            'milestones' => ProjectMilestoneResource::collection($this->milestones),
        ];
    }
}
