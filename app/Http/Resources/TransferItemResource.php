<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TransferItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'transfer_id' => $this->transfer_id,
            'quantity' => $this->quantity,
            'transfer' => $this->transfer,
            'product' =>  new ProductResource($this->product),
            'created_at' => $this->created_at->format('d-m-Y'),
        ];
    }
}
