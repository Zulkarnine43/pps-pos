<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\JournalItemResource;

class JournalResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'branch_name' => $this->branch_name,
            'created_by' => $this->created_by,
            'ledger_name' => $this->ledger_name,
            'ledger_id' => $this->ledger_id,
            'narration' => $this->narration,
            'net_amount' => $this->net_amount,
            'trans_date' => $this->trans_date,
            'voucher_no' => $this->voucher_no,
            'ItemRows' => JournalItemResource::collection($this->ItemRows),
        ];
    }
}
