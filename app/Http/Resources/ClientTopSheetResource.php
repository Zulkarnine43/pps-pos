<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\ClientBillResource;
use App\Laravue\Models\ClientBill;

class ClientTopSheetResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $clientBill = ClientBill::where('id', $this->client_bill_id)->with('client')->first();
        
        return [
            'id' => $this->id,
            'client_bill_id' => $this->client_bill_id,
            'project_id' => $this->project_id,
            'stage_id' => $this->stage_id,
            'amount' => $this->amount,
            'vat' => $this->vat,
            'tax' => $this->tax,
            'sd' => $this->sd,
            'deduction' => $this->deduction,
            'cheque_amount' => $this->cheque_amount,
            'remarks' => $this->remarks,
            'details' => $this->details,
            'status' => $this->status,
            'project' => $this->project,
            'clientBill' => $clientBill,
        ];
    }
}
