<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SaleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // dd($this->paymentInfos);
        return [
            'id' => $this->id,
            'vat' => $this->vat,
            'tax' => $this->tax,
            'sale_id' => $this->sale_id,
            'sale_date' => $this->sale_date,
            'sale_time' => $this->sale_time,
            'discount' => $this->discount,
            'grand_total' => $this->grand_total,
            'subTotal' => $this->sub_total,
            'sale_by' => $this->sale_by,
            'customer_id' => $this->customer_id,
            'sale_register_id' => $this->sale_register_id,
            'warehouse_id' => $this->warehouse_id,
            'note' => $this->note,
            'saleItems' => SaleItemResource::collection($this->saleItems),
            'paymentInfos' => $this->paymentInfos,
            'warehouse_name' => $this->warehouse->name,
            'customer_name' => $this->customer->name,
            'customer_tax_number' => $this->customer->tax_number,
            'pay_type' => $this->pay_type,
        ];
    }
}
