<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProjectTypeEstimateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'project_type_id' => $this->project_type_id,
            'status' => $this->status,
            'projectType' => $this->projectType,
            'projectTypeEstimateItems' => ProjectTypeEstimateItemResource::collection($this->projectTypeEstimateItems),
        ];
    }
}
