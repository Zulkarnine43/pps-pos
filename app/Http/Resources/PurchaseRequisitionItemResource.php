<?php

namespace App\Http\Resources;

use App\Laravue\Models\PurchaseRequisitionItem;
use Illuminate\Http\Resources\Json\JsonResource;

class PurchaseRequisitionItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'product_id' => $this->product_id,
            'unit_id' => $this->unit_id,
            'purchase_requisition_id' => $this->purchase_requisition_id,
            'quantity' => $this->quantity,
            'purchase_requisition' => $this->purchase_requisition,
            'product' => $this->product,
            'unit' => $this->unit,
            // 'product' =>  new ProductResource($this->product),
            // 'unit' =>  new UnitResource($this->unit),
            'created_at' => $this->created_at->format('d-m-Y'),
        ];
    }
}
