<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class WarehouseInventoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        
        return [
            'id' => $this->id,
            'quantity' => $this->quantity,
            'warehouse_id' => $this->warehouse_id,
            'product_id' => $this->product_id,
            'product' => $this->product,
            'warehouse' => $this->warehouse,
        ];
    }
}
