<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\ClientTopSheetResource;

class ClientBillResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'bill_id' => $this->bill_id,
            'client_id' => $this->client_id,
            'bill_status' => $this->bill_status,
            'payment_details' => $this->payment_details,
            'supervised_by' => $this->supervised_by,
            'prepared_by' => $this->prepared_by,
            'prepared_date' => $this->prepared_date,
            'send_date' => $this->send_date,
            'estimated_payment_date' => $this->estimated_payment_date,
            'bill_received_number' => $this->bill_received_number,
            'bill_received_date' => $this->bill_received_date,
            'bill_submission_number' => $this->bill_submission_number,
            'bill_submission_date' => $this->bill_submission_date,
            'status' => $this->status,
            'client' => $this->client,
            'preparedBy' => $this->preparedBy,
            'supervisedBy' => $this->supervisedBy,
            'subject' => $this->subject,
            'bill_format' => $this->bill_format,
            'msg1' => $this->msg1,
            'msg2' => $this->msg2,
            'to' => $this->to,
            'from' => $this->from,
            'vat' => $this->vat,
            'uom' => $this->uom,
            'total_cheque_amount' => $this->total_cheque_amount,
            'total_bill_amount' => $this->total_bill_amount,
            'attached' => $this->attached,
            'work_order_id' => $this->work_order_id,
            'site_code' => $this->site_code,
            'BillRows' => ClientTopSheetResource::collection($this->billItems),
        ];
    }
}
