<?php

namespace App\Http\Resources;
use App\Http\Resources\SubcontractorBillItemResource;

use Illuminate\Http\Resources\Json\JsonResource;

class SubcontractorBillResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'bill_id' => $this->bill_id,
            'bill_status' => $this->bill_status,
            'entry_date' => $this->entry_date,
            'subcontractor_id' => $this->subcontractor_id,
            'subcontractor' => $this->subcontractor,
            'received_by' => $this->received_by,
            'receivedBy' => $this->receivedBy,
            'submited_by' => $this->submited_by,
            'submited_by_phone_number' => $this->submited_by_phone_number,
            'submitedBy' => $this->submitedBy,
            'checked_by' => $this->checked_by,
            'checked_date' => $this->checked_date,
            'checkedBy' => $this->checkedBy,
            'approved_by' => $this->approved_by,
            'approved_date' => $this->approved_date,
            'approvedBy' => $this->approvedBy,
            'total_receive_amount' => $this->total_receive_amount,
            'total_approve_amount' => $this->total_approve_amount,
            'billItems' => SubcontractorBillItemResource::collection($this->billItems),
        ];
    }
}
