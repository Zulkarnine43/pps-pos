<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SubcontractorPriceListItemsResource;

class SubcontractorPriceListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'entry_date' => $this->entry_date,
            'delivery_date' => $this->delivery_date,
            'note' => $this->note,
            'discount_type' => $this->discount_type,
            'discount' => $this->discount,
            'shipping_cost' => $this->shipping_cost,
            'grand_total' => $this->grand_total,
            'subcontractor_id' => $this->subcontractor_id,
            'user_id' => $this->user_id,
            'user' => $this->user,
            'subcontractor' => $this->subcontractor,
            'listItems' => SubcontractorPriceListItemsResource::collection($this->listItems),
        ];
    }
}
