<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GroupResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        
        return [
            'id' => $this->id,
            'name' => $this->name,
            'parent_group_id' => $this->parent_group_id,
            'default_group_id' => $this->default_group_id,
            'nature' => $this->nature,
            'cash_flow_type' => $this->cash_flow_type,
            'effect_gross_profit' => $this->effect_gross_profit,
            'status' => $this->status,
            'default' => $this->default,
            'parent' => $this->parent,
        ];
    }
}
