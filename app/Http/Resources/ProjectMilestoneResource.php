<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProjectMilestoneResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'project_id' => $this->project_id,
            'amount' => $this->amount,
            'milestone_name' => $this->milestone_name,
            'work_done' => $this->work_done,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'finish_date' => $this->finish_date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'projectWbcs' => ProjectWbcResource::collection($this->projectWbcs),
        ];
    }
}
