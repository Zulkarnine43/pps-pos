<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TransferResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        // dd($this);
        $total_quantity = 0;
        foreach($this->transferItems as $transferItem){
          $total_quantity += $transferItem->quantity;
        }
        return [
          'id' => $this->id,
          'created_by' => $this->created_by,
          'transfer_date' => $this->transfer_date,
          'gatepass_number' => $this->gatepass_number,
          'ref_number' => $this->ref_number,
          'transfer_type' => $this->transfer_type,
          'from' => $this->from,
          'to' => $this->to,
          'transfered_by' => $this->transfered_by,
          'transfered_by_phone' => $this->transfered_by_phone,
          'quantity' => $total_quantity,
          'fromInventory' => ($this->transfer_type==1 || $this->transfer_type == 2) ? $this->fromWarehouse->name : $this->fromProject->site_code,
          'toInventory' => ($this->transfer_type==1 || $this->transfer_type == 3) ? $this->toWarehouse->name : $this->toProject->site_code,
          'transferItems' => TransferItemResource::collection($this->transferItems),
        ];
    }
}
