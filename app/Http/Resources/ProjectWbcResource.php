<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProjectWbcResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'project_id' => $this->project_id,
            'project_milestone_id' => $this->project_milestone_id,
            'wbc_name' => $this->wbc_name,
            'work_done' => $this->work_done,
            'amount' => $this->amount,
            'start_date' => $this->start_date,
            'deadline' => $this->deadline,
            'finish_date' => $this->finish_date,
            'images' => $this->images,
        ];
    }
}
