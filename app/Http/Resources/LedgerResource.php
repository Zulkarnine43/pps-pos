<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LedgerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'city' => $this->city,
            'address' => $this->address,
            'post_code' => $this->post_code,
            'email' => $this->email,
            'phone' => $this->phone,
            'comments' => $this->comments,
            'group_id' => $this->group_id,
            'group' => $this->group,
            'group_name' => $this->group->name,
            'inactive' => $this->inactive,
            'currency' => $this->currency,
            'cost_center' => $this->cost_center,
            'relation_id' => $this->relation_id,
            'type' => $this->type,
            'journalItems' => $this->journalItems,
        ];
    }
}
