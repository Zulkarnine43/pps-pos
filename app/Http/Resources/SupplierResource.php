<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SupplierResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'company' => $this->company,
            'email' => $this->email,
            'phone' => $this->phone,
            'country' => $this->country,
            'state' => $this->state,
            'district' => $this->district,
            'city' => $this->city,
            'zip_code' => $this->zip_code,
            'street' => $this->street,
            'image' => $this->image,
            'is_active' => $this->is_active,
            'country_name' => $this->countryName,
            'state_name' => $this->stateName,
            'district_name' => $this->districtName,
            'trade_licence' => $this->trade_licence,
            'TIN' => $this->TIN,
            'BIN' => $this->BIN,
            'NID' => $this->NID,
        ];
    }
}
