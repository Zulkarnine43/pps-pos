<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\QuotationItemsResource;

class QuotationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'entry_date' => $this->entry_date,
            'delivery_date' => $this->delivery_date,
            'ref_number' => $this->ref_number,
            'note' => $this->note,
            'discount_type' => $this->discount_type,
            'discount' => $this->discount,
            'shipping_cost' => $this->shipping_cost,
            'grand_total' => $this->grand_total,
            'client_id' => $this->client_id,
            'user_id' => $this->user_id,
            'user' => $this->user,
            'client' => $this->client,
            'listItems' => QuotationItemsResource::collection($this->listItems),
        ];
    }
}
