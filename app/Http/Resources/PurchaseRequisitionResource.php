<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PurchaseRequisitionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      // return parent::toArray($request);
      return [
        'id' => $this->id,
        'created_by' => $this->created_by,
        'prn' => $this->prn,
        'requisition_date' => $this->requisition_date,
        'delivery_date' => $this->delivery_date,
        'requisition_type' => $this->requisition_type,
        'note' => $this->note,
        'project_id' => $this->project_id,
        'warehouse_id' => $this->warehouse_id,
        'warehouse' => ($this->requisition_type==1 ) ? $this->warehouse->name : null,
        'project' => ($this->requisition_type==2 ) ? $this->project->site_code : null,
        'requisition_by' => $this->requisition_by,
        'status' => $this->status,
        'checked_by' => $this->checked_by,
        'checked_at' => $this->checked_at,
        'approved_by' => $this->approved_by,
        'approved_at' => $this->approved_at,
        'ItemRows' => PurchaseRequisitionItemResource::collection($this->purchaseRequisitionItems),
        // 'quantity' => $total_quantity,
      ];
    }
}
