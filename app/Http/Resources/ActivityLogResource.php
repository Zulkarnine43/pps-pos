<?php

namespace App\Http\Resources;

use App\Laravue\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class ActivityLogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $causer = User::where('id', $this->causer_id)->first();

        return [
            'id'  => $this->id,
            'name'  => $this->log_name,
            'description' => $this->description,
            'subject_type' => $this->subject_type,
            'subject_id' => $this->subject_id,
            'causer_type' => $this->causer_type,
            'causer_id' => $this->causer_id,
            'updated_at' => $this->updated_at,
            'properties' => $this->properties,
            'causer_name' => $causer->name,
        ];
    }
}
