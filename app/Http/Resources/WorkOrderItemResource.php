<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\WorkOrderResource;

class WorkOrderItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // dd($this);
        return [
            'id' => $this->id,
            'work_order_id' => $this->work_order_id,
            'project_id' => $this->project_id,
            'project_stage_id' => $this->project_stage_id,
            'work_order_amount' => $this->work_order_amount,
            'remarks' => $this->remarks,
            'details' => $this->details,
            'status' => $this->status,
            'project' => $this->whenLoaded('project'),
            // 'site_code' => $this->project->site_code,
            'quantity' => $this->quantity,
            'unit_price' => $this->unit_price,
            'unit_id' => $this->unit_id,
            'unit' => $this->unit,
            'workorder' => new WorkOrderResource($this->workOrder),
        ];
    }
}
