<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SubcontractorBillItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'project_id' => $this->project_id,
            'work_order_id' => $this->work_order_id,
            'project' => $this->project,
            'subcontractor_bill_id' => $this->subcontractor_bill_id,
            'sd' => $this->sd,
            'deduction' => $this->deduction,
            'cheque_amount' => $this->cheque_amount,
            'details' => $this->payment_details,
            'remarks' => $this->remarks,
            'project_subcontract_id' => $this->project_subcontract_id,
            'stage_id' => $this->stage_id,
            'amount' => $this->amount,
            'status' => $this->status,
            'subcontractorBill' => $this->subcontractorBill,
            'workOrder' => $this->workOrder,
        ];
    }
}
