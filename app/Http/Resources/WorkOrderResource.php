<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\WorkOrderItemResource;
use App\Laravue\Models\WorkOrderItem;

class WorkOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {   
        // dd('hi');
        $person = null;
        $totalSubmitedAmount = 0;
        $totalPenaltyAmount = 0;
        $totalSDAmount = 0;
        $totalPaymentAmount = 0;
        if(isset($this->subcontractor)){
            $person = $this->subcontractor;
        } else if(isset($this->supplier)){
            $person = $this->supplier;
        }
        $woItem = WorkOrderItem::where('work_order_id', $this->id)->with('project', 'unit')->get();
        if(isset($this->bills)){
            foreach($this->bills as $bill){
                $totalSubmitedAmount += $bill->amount;
                $totalPenaltyAmount += $bill->deduction;
                $totalSDAmount += $bill->sd;
                $totalPaymentAmount += $bill->cheque_amount;
            }
        }
        return [
            'id' => $this->id,
            'order_no' => $this->order_no,
            'subcontractor_id' => $this->subcontractor_id,
            'person' => $person,
            'supplier_id' => $this->supplier_id,
            'task_details' => $this->task_details,
            'contract_ref' => $this->contract_ref,
            'department_id' => $this->department_id,
            'payable_amount' => $this->payable_amount,
            'issue_date' => $this->issue_date,
            'po_issuance_date' => $this->po_issuance_date,
            'deadline' => $this->deadline,
            'department' => $this->department,
            'delivery_point' => $this->delivery_point,
            'delivery_address' => $this->delivery_address,
            'delivery_contract_number' => $this->delivery_contract_number,
            'supply_item' => $this->supply_item,
            'payment_terms' => $this->payment_terms,
            'warranty_terms' => $this->warranty_terms,
            'delay_terms' => $this->delay_terms,
            'other_terms' => $this->other_terms,
            'scope_of_work' => $this->scope_of_work,
            'service_scope' => $this->service_scope,
            'equipment_scope' => $this->equipment_scope,
            'contract_referance' => $this->contract_referance,
            'payment_methods' => $this->payment_methods,
            'remarks' => $this->remarks,
            'work_order_for' => $this->work_order_for,
            'ItemRows' =>$woItem,
            'totalSubmitedAmount' => $totalSubmitedAmount,
            'totalPenaltyAmount' => $totalPenaltyAmount,
            'totalSDAmount' => $totalSDAmount,
            'totalPaymentAmount' => $totalPaymentAmount,
        ];
    }
}
