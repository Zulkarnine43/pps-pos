<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SubcontractorPriceListItemsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'subcontractor_price_list_id' => $this->subcontractor_price_list_id,
            'discount' => $this->discount,
            'price' => $this->price,
            'product_id' => $this->product_id,
            'quantity' => $this->quantity,
            'total' => $this->total,
            'product' => $this->product,
            'status' => $this->status,
        ];
    }
}
