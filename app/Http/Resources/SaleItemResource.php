<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SaleItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'sale_id' => $this->sale_id,
            'product_id' => $this->product_id,
            'quantity' => $this->quantity,
            'price' => $this->price,
            'discount_amount' => $this->discount,
            'sub_total' => $this->sub_total,
            'name' => $this->product->name,
            'code' => $this->product->code,
            'unit_name' => $this->unit_name,
            'vat' => $this->vat,
            'sub_total' => $this->sub_total,
        ];
    }
}
