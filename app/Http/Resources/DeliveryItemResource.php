<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DeliveryItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'delivery_id' => $this->delivery_id,
            'quantity' => $this->quantity,
            'delivery' => $this->delivery,
            'product' =>  new ProductResource($this->product),
            'created_at' => $this->created_at->format('d-m-Y'),
        ];
    }
}
