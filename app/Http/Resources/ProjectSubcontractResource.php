<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Laravue\Models\SubcontractorBill;
use App\Http\Resources\ProjectResource;
use App\Laravue\Models\ProjectType;

use function PHPSTORM_META\map;

class ProjectSubcontractResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $paidAmount = 0;
        
        $projectType = ProjectType::where('id', $this->project->project_type)->first();
        $this->project->projectType = $projectType;
        return [
            'id' => $this->id,
            'project_id' => $this->project_id,
            'code' => $this->code,
            'order_no' => $this->order_no,
            'department_id' => $this->department_id,
            'department_name' => $this->department->name,
            'contract_ref' => $this->contract_ref,
            'project_name' => $this->project->name,
            'subcontractor_id' => $this->subcontractor_id,
            'subcontractor_name' => $this->subcontractor->name,
            'subcontractor' => $this->subcontractor,
            'task_details' => $this->task_details,
            'payable_amount' => $this->payable_amount,
            'deadline' => $this->deadline,
            'issue_date' => $this->issue_date,
            'finished_date' => $this->finished_date,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'project' => $this->project,
            'projectState' => $this->project->state,
            'projectDistrict' => $this->project->district,
            'city' => $this->project->city,
            'subcontractorCountry' => $this->subcontractor->countryName,
            'subcontractorState' => $this->subcontractor->stateName,
            'subcontractorDistrict' => $this->subcontractor->districtName,
            'paidAmount' => $paidAmount,
        ];
    }
}
