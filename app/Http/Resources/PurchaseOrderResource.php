<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PurchaseOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
        'id' => $this->id,
        'created_by' => $this->created_by,
        'pon' => $this->pon,
        'purchase_requisition_id' => $this->purchase_requisition_id,
        'order_date' => $this->order_date,
        'delivery_date' => $this->delivery_date,
        'order_type' => $this->order_type,
        'note' => $this->note,
        'project_id' => $this->project_id,
        'warehouse_id' => $this->warehouse_id,
        'warehouse' => ($this->order_type==1 ) ? $this->warehouse->name : null,
        'project' => ($this->order_type==2 ) ? $this->project->site_code : null,
        'order_by' => $this->order_by,
        'status' => $this->status,
        'checked_by' => $this->checked_by,
        'checked_at' => $this->checked_at,
        'approved_by' => $this->approved_by,
        'approved_at' => $this->approved_at,
        'ItemRows' => PurchaseOrderItemResource::collection($this->purchaseOrderItems),
      ];
    }
}
