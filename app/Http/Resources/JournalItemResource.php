<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\JournalItemCostCenterResource;
use App\Laravue\Models\JournalItem;

class JournalItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $journalItems = JournalItem::where('journal_id', $this->journal_id)->get();
        return [
            'id' => $this->id,
            'cost_center' => $this->cost_center,
            'credit_amount' => $this->credit_amount,
            'debit_amount' => $this->debit_amount,
            'journal_id' => $this->journal_id,
            'ledger_id' => $this->ledger_id,
            'ledger_name' => $this->ledger_name,
            'type' => $this->type,
            'trans_date' => $this->trans_date,
            'journal' => $this->journal,
            'journalItems' => $journalItems,
            'costCenters' => JournalItemCostCenterResource::collection($this->costCenters),
            'ledger' => $this->ledger,
            'created_at' => $this->created_at->format('d-m-Y'),
        ];
    }
}
