<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\ProjectResource;

class ClientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $countryName = $this->countryName->name;
        $stateName = $this->stateName;
        // $cityName = $this->cityName->name;
        return [
            'id' => $this->id,
            'name' => $this->name,
            'company' => $this->company,
            'email' => $this->email,
            'phone' => $this->phone,
            'country' => $this->country,
            'state' => $this->state,
            'city' => $this->city,
            'zip_code' => $this->zip_code,
            'street' => $this->street,
            'image' => $this->image,
            'is_active' => $this->is_active,
            'country_name' => $countryName,
            'state_name' => $stateName,
            'tax_number' => $this->tax_number,
        ];
    }
}
