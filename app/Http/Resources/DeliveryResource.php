<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DeliveryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        // dd($this);
        $total_quantity = 0;
        foreach($this->deliveryItems as $deliveryItem){
          $total_quantity += $deliveryItem->quantity;
        }
        return [
          'id' => $this->id,
          'created_by' => $this->created_by,
          'delivery_date' => $this->delivery_date,
          'gatepass_number' => $this->gatepass_number,
          'ref_number' => $this->ref_number,
          'delivery_type' => $this->delivery_type,
          'from' => $this->from,
          'to' => $this->to,
          'delivered_by' => $this->delivered_by,
          'delivered_by_phone' => $this->delivered_by_phone,
          'quantity' => $total_quantity,
          'fromInventory' => ($this->delivery_type==1 || $this->delivery_type == 2) ? $this->fromWarehouse->name : $this->fromProject->site_code,
          'toInventory' => ($this->delivery_type==1 || $this->delivery_type == 3) ? $this->toWarehouse->name : $this->toProject->site_code,
          'deliveryItems' => DeliveryItemResource::collection($this->deliveryItems),
        ];
    }
}
