<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EmployeeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'employee_id' => $this->employee_id,
            'email' => $this->email,
            'phone' => $this->phone,
            'office_phone' => $this->office_phone,
            'emergency_phone' => $this->emergency_phone,
            'image' => $this->image,
            'present' => $this->presentAddress,
            'permanent' => $this->permanentAddress,
            'nid_number' => $this->nid_number,
            'designation' => $this->designation,
            'blood_group' => $this->blood_group,
            'joining_date' => $this->joining_date,
            'gender' => $this->gender,
            'employee_status' => $this->employee_status,
        ];
    }
}
