<?php

namespace App\Laravue\Helpers;

use App\Laravue\Models\WarehouseInventory;

class AppHelper
{
    public static function updateWarehouseInventory($warehouseId, $product,$status){
        
        $WI = WarehouseInventory::where('warehouse_id',$warehouseId)->where('product_id', $product['product_id'])->first();
        if($status === true){
            if(isset($WI)){
                AppHelper::increaseQuantity($WI,$product['quantity']);
            } else {
                AppHelper::insertWarehouseInventory($warehouseId,$product);
            }
        } else {
            if(isset($WI)){
                AppHelper::decreaseQuantity($WI,$product['quantity']);
            } else {
                return back()->with('error', 'Product not found in warehouse');
            }
        }
        
    }
    public static function increaseQuantity($WI, $Qty){
        // update warehouse inventory
        $WI->quantity += $Qty;
        $WI->save();
    }

    public static function insertWarehouseInventory($warehouseId, $product){
        // insert warehouse inventory
        $whInventory = new WarehouseInventory();
        $whInventory->product_id = $product['product_id'];
        $whInventory->quantity = $product['quantity'];
        $whInventory->warehouse_id = $warehouseId;
        $whInventory->save();
    }

    public static function decreaseQuantity($WI, $Qty)
    {
        $WI->quantity -= $Qty;
        $WI->save();
    }
}
