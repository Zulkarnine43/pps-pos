<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class State extends Model
{
    use HasFactory;
    use LogsActivity;


    // activity log start
    protected static $logAttributes = ['name', 'country_id', 'status', 'created_at', 'updated_at'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "State has been {$eventName} ";
    }

    protected static $logName = 'State';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log ends
}
