<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Milestone extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'name', 'zone_id', 'work_done'
    ];


    // activity log start
    protected static $logAttributes = ['name', 'work_done', 'status', 'created_at', 'updated_at'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Milestone has been {$eventName} ";
    }

    protected static $logName = 'Milestone';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    public function wbcDatas(){
        return $this->hasMany(Wbc::class);
    }
}
