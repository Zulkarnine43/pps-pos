<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeliveryItem extends Model
{
    use HasFactory;
    protected $guard_name = 'api';

    protected $fillable = [
        'delivery_id', 'product_id', 'quantity', 'status'
    ];

    public function product(){
      return $this->belongsTo(Product::class)->withDefault();
    }

    // activity log start
    protected static $logAttributes = ['delivery_id', 'product_id', 'quantity', 'status', 'created_at', 'updated_at'];

    public function getDescriptionForEvent(string $eventName): string
    {
        return "DeliveryItem has been {$eventName} ";
    }

    protected static $logName = 'DeliveryItem';
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;
    // activity log ends

}
