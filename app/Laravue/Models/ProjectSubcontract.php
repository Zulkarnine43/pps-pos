<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Http\Resources\ProjectResource;

class ProjectSubcontract extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'project_id', 'project_stage_id', 'code', 'finished_date','department_id', 'order_no', 'contract_ref', 'subcontractor_id', 'task_details', 'payable_amount', 'issue_date', 'deadline'
    ];




    // activity log start

    protected static $logAttributes = ['project_id', 'project_stage_id', 'finished_date', 'code', 'department_id', 'order_no', 'contract_ref', 'subcontractor_id', 'task_details', 'payable_amount', 'issue_date', 'deadline', 'status', 'created_at', 'updated_at'];

    public function getDescriptionForEvent(string $eventName): string
    {
        return "Project Subcontract has been {$eventName} ";
    }

    protected static $logName = 'Project Subcontract';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log ends

    public function project(){
        return $this->belongsTo(Project::class);
    }

    public function department(){
        return $this->belongsTo(Department::class);
    }

    public function subcontractor(){
        return $this->belongsTo(Subcontractor::class);
    }

}
