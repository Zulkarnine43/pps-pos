<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class EmployeeAddress extends Model
{
    use HasFactory, LogsActivity;


    protected $fillable = [
        'employee_id', 'address_type', 'country', 'state', 'district', 'city', 'zip_code', 'street'
    ];

    // activity log start
    protected static $logAttributes = ['employee_id', 'address_type', 'country', 'state', 'district', 'city', 'zip_code', 'street', 'created_at', 'updated_at'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Employee Address been {$eventName} ";
    }

    protected static $logName = 'Employee Address';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log ends
}
