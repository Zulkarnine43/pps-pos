<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Product extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'name', 'image', 'code', 'sku', 'alert_quantity', 'description', 'category_id', 'brand_id', 'unit_id', 'price','purchase_price', 'discount_price',
    ];

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function brand(){
        return $this->belongsTo(Brand::class);
    }

    public function unit(){
        return $this->belongsTo(Unit::class);
    }

    // activity log start
    protected static $logAttributes = ['name', 'image', 'sku', 'alert_quantity','code', 'description', 'category_id', 'brand_id', 'unit_id', 'price', 'purchase_price', 'discount_price','status', 'created_at', 'updated_at'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Product has been {$eventName} ";
    }

    protected static $logName = 'Product';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log ends
}
