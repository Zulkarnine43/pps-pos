<?php
/**
 * File Permission.php
 *
 * @author Tuan Duong <bacduong@gmail.com>
 * @package Laravue
 * @version 1.0
 */

namespace App\Laravue\Models;
use App\Laravue\Acl;
use Illuminate\Database\Query\Builder;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class Permission
 *
 * @package App\Laravue\Models
 */
class Permission extends \Spatie\Permission\Models\Permission
{
    use LogsActivity;
    public $guard_name = 'api';

    /**
     * To exclude permission management from the list
     *
     * @param $query
     * @return Builder
     */
    public function scopeAllowed($query)
    {
        return $query->where('name', '!=', Acl::PERMISSION_PERMISSION_MANAGE);
    }

    // activity log starts
    protected static $logAttributes = ['name', 'guard_name', 'updated_at', 'created_at'];

    // protected static $recordEvents = ['create', 'update'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Permission has been {$eventName} ";
    }

    protected static $logName = 'Permission';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log ends
}
