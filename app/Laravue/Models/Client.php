<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Client extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'name', 'company', 'email','phone','country','state','city','zip_code','street','image', 'tax_number'
    ];

    public function countryName(){
        return $this->belongsTo(Country::class, 'country', 'id');
    }

    public function stateName(){
        return $this->belongsTo(State::class, 'state', 'id');
    }


    // activity log start

    protected static $logAttributes = ['name', 'company', 'email', 'phone', 'country', 'state', 'city', 'zip_code', 'street', 'image', 'is_active', 'tax_number', 'status', 'updated_at', 'created_at'];

    // protected static $recordEvents = ['create', 'update'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Client has been {$eventName} ";
    }

    protected static $logName = 'Client';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log end
}
