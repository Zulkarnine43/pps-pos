<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class NewProjectAssign extends Model
{
    use HasFactory, LogsActivity;
    protected $fillable = [
        'department_id', 'reference_number',  'subcontractor_id', 'prepared_by', 'prepared_date', 'checked_by', 'checked_date','approved_by', 'approved_date'
    ];

    // activity log start
    protected static $logAttributes = ['department_id', 'reference_number',  'subcontractor_id', 'prepared_by', 'prepared_date', 'checked_by','checked_date','approved_by', 'approved_date', 'status', 'created_at', 'updated_at'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "New project assaign has been {$eventName} ";
    }

    protected static $logName = 'New project assaign';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log ends
    public function department()
    {
        return $this->belongsTo(Department::class);
    }
    public function subcontractor()
    {
        return $this->belongsTo(Subcontractor::class);
    }

    public function projects(){
        return $this->hasMany(NewProjectAssignItem::class);
    }
}
