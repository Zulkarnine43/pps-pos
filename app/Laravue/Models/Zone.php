<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Zone extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'name', 
    ];


    // activity log start
    protected static $logAttributes = ['name', 'status', 'created_at', 'updated_at'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Zone has been {$eventName} ";
    }

    protected static $logName = 'Zone';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    public function zoneMilestones(){
        return $this->hasMany(ZoneMilestone::class);
    }

    public function projects(){
        return $this->hasMany(Project::class);
    }
}
