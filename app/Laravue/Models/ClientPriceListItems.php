<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ClientPriceListItems extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'client_price_list_id', 'product_id', 'price','quantity', 'discount',
    ];

    // activity log start

    protected static $logAttributes = ['client_price_list_id', 'product_id', 'price','quantity', 'discount', 'status', 'updated_at', 'created_at'];

    // protected static $recordEvents = ['create', 'update'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Client Price List Items has been {$eventName} ";
    }

    protected static $logName = 'Client Price List Items';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log end

    public function product(){
        return $this->belongsTo(Product::class);
    }
}
