<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjectFile extends Model
{
    use HasFactory;

    protected $guard_name = 'api';

    protected $fillable = [
        'user_id', 'file_name', 'extension'
    ];


    // activity log start

    protected static $logAttributes = ['user_id', 'file_name', 'extension', 'status', 'created_at', 'updated_at'];

    // protected static $recordEvents = ['create', 'update'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Project File has been {$eventName} ";
    }

    protected static $logName = 'Project File';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;
}
