<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ProjectWbcImage extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'project_id', 'project_wbc_id', 'image_name',
    ];


    // activity log start
    protected static $logAttributes = ['project_id', 'project_wbc_id','image_name', 'status', 'created_at', 'updated_at'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Project WBC has been {$eventName} ";
    }

    protected static $logName = 'Milestone';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;
}
