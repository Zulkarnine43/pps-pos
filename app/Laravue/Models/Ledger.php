<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Ledger extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'name', 'group_id', 'relation_id', 'type', 'cost_center', 'currency', 'city', 'address', 'post_code', 'phone', 'email', 'comments' , 'inactive',
    ];


    // activity log start
    protected static $logAttributes = ['name', 'group_id', 'relation_id', 'type', 'cost_center', 'currency', 'city', 'address', 'post_code', 'phone', 'email', 'comments' , 'inactive', 'status', 'created_at', 'updated_at'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Ledger has been {$eventName} ";
    }

    protected static $logName = 'Ledger';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log ends

    public function group(){
        return $this->belongsTo(Group::class);
    }

    public function journalItems(){
        return $this->hasMany(JournalItem::class);
    }
}
