<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Journal extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'voucher_no', 'ledger_id', 'ledger_name', 'net_amount', 'trans_date', 'created_by', 'branch_name', 'narration',
    ];


    // activity log start
    protected static $logAttributes = ['voucher_no', 'ledger_id', 'ledger_name', 'net_amount', 'trans_date', 'created_by', 'branch_name', 'narration', 'status', 'created_at', 'updated_at'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Journal has been {$eventName} ";
    }

    protected static $logName = 'Journal';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log ends

    public function ItemRows(){
        return $this->hasMany(JournalItem::class);
    }
}
