<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Sale extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $casts = [
        'sale_date	' => 'string',
    ];

    protected $fillable = [
        'sale_id', 'sale_date', 'sale_time','discount', 'tax', 'vat', 'sub_total','grand_total', 'sale_by', 'customer_id', 'sale_register_id', 'warehouse_id', 'note', 'pay_type'
    ];


    // activity log start
    protected static $logAttributes = ['sale_id', 'sale_date','sale_time', 'discount', 'tax', 'vat', 'sub_total', 'grand_total', 'sale_by', 'customer_id', 'sale_register_id', 'warehouse_id', 'pay_type', 'note', 'status', 'created_at', 'updated_at'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Sale has been {$eventName} ";
    }

    protected static $logName = 'Sale';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log ends

    public function saleItems(){
        return $this->hasMany(SaleItem::class)->with('product');
    }
    public function paymentInfos(){
        return $this->hasMany(SalePayment::class);
    }
    public function warehouse(){
        return $this->belongsTo(Warehouse::class);
    }
    public function customer(){
        return $this->belongsTo(Client::class,'customer_id', 'id');
    }
}
