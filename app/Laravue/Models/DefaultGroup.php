<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class DefaultGroup extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'name', 'parent_group_id'
    ];


    // activity log start
    protected static $logAttributes = ['name', 'parent_group_id','status', 'created_at', 'updated_at'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Default Group has been {$eventName} ";
    }

    protected static $logName = 'Default Group';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log ends

    public function parent(){
        return $this->belongsTo(ParentGroup::class, 'parent_group_id', 'id');
    }
}
