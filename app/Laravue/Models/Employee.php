<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Employee extends Model
{
    use HasFactory, LogsActivity;

    protected $fillable = [
        'name', 'nid_number', 'employee_id', 'gender', 'designation', 'blood_group', 'joining_date', 'email', 'office_phone', 'emergency_phone', 'phone','image'
    ];

    // activity log start
    protected static $logAttributes = ['name', 'gender', 'designation', 'blood_group', 'joining_date', 'nid_number', 'email', 'is_active','phone','office_phone', 'emergency_phone', 'country','state','district','city','zip_code','street','image', 'remember_token', 'created_at', 'updated_at'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Employee has been {$eventName} ";
    }

    protected static $logName = 'Employee';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log ends


    public function presentAddress(){
        return $this->hasOne(EmployeeAddress::class, 'employee_id', 'id')->where('address_type', '=', 'present');
    }

    public function permanentAddress()
    {
        return $this->hasOne(EmployeeAddress::class, 'employee_id', 'id')->where('address_type', '=', 'permanent');
    }
}
