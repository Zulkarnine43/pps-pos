<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PurchaseRequisitionItem extends Model
{
    use HasFactory;
    protected $guard_name = 'api';
    protected $fillable = [
      'product_id', 'unit_id', 'quantity', 'remarks', 'created_by', 
    ];

    public function product(){
      return $this->belongsTo(Product::class)->withDefault();
    }
    public function unit(){
      return $this->belongsTo(Unit::class)->withDefault();
    }


    // activity log start
    protected static $logAttributes = ['product_id', 'unit_id', 'quantity', 'remarks', 'created_by', 'created_at', 'updated_at'];
    public function getDescriptionForEvent(string $eventName): string
    {
        return "Purchase Requisition has been {$eventName} ";
    }
    protected static $logName = 'Purchase Requisition';
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;
    // activity log ends
}
