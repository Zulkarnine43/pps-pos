<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class WarehouseInventory extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'product_id', 'quantity', 'warehouse_id'
    ];


    // activity log start
    protected static $logAttributes = ['product_id', 'quantity', 'warehouse_id', 'status', 'created_at', 'updated_at'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "WarehouseInventory has been {$eventName} ";
    }

    protected static $logName = 'WarehouseInventory';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log ends

    public function product(){
        return $this->belongsTo(Product::class);
    }
    public function warehouse(){
        return $this->belongsTo(Warehouse::class);
    }
}
