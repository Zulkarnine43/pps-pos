<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    use HasFactory;

    protected $connection = 'mysql2';

    protected $table = 'project_management_activitylog.activity_log';
    
    public function user(){
        return $this->belongsTo(User::class, 'causer_id', 'id');
    }
}
