<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ProjectTypeEstimateItem extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
       'product_id', 'quantity', 'unit_id',
    ];


    // activity log start
    protected static $logAttributes = ['product_id', 'quantity', 'unit_id', 'status', 'created_at', 'updated_at'];

    // protected static $recordEvents = ['create', 'update'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Project Type Estimate Item has been {$eventName} ";
    }

    protected static $logName = 'Project Type Estimate Item';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log ends

    public function product(){
        return $this->belongsTo(Product::class);
    }
    public function unit(){
        return $this->belongsTo(Unit::class);
    }
}
