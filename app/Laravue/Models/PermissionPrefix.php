<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PermissionPrefix extends Model
{
    use HasFactory;
    use LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'name',
    ];


    // activity log start
    protected static $logAttributes = ['name', 'status', 'created_at', 'updated_at'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Permission Prefix has been {$eventName} ";
    }

    protected static $logName = 'Permission Prefix';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log ends

    public function permission(){
        return $this->hasMany(Permission::class);
    }
}
