<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Warehouse extends Model
{
    use HasFactory, LogsActivity;
    protected $guard_name = 'api';

    protected $fillable = [
        'name', 'code', 'email', 'phone', 'address'
    ];


    // activity log start
    protected static $logAttributes = ['name', 'code', 'email', 'phone', 'address', 'status', 'created_at', 'updated_at'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Warehouse has been {$eventName} ";
    }

    protected static $logName = 'Warehouse';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;
    // activity log ends
    

    public function warehouseInventories(){
      return $this->hasMany(WarehouseInventory::class);
    }
}
