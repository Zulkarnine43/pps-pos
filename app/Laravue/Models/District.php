<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class District extends Model
{
    use HasFactory;
    use LogsActivity;

    // activity log start

    protected static $logAttributes = ['name', 'state_id', 'status', 'updated_at', 'created_at'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "District has been {$eventName} ";
    }

    protected static $logName = 'District';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log ends
}
