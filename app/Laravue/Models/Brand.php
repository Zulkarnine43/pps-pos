<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Brand extends Model
{

    use HasFactory;
    use LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'name', 'image', 'code', 'description', 'status'
    ];

    // activity log start
    protected static $logAttributes = ['name', 'image', 'code', 'description', 'status','updated_at', 'created_at'];

    // protected static $recordEvents = ['create', 'update'];




    // activity log end
}
