<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class SalePayment extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'sale_id', 'amount', 'change_due_amount', 'date_time', 'payment_method_id'
    ];


    // activity log start
    protected static $logAttributes = ['sale_id', 'payment_method_id', 'amount', 'change_due_amount', 'date_time','status', 'created_at', 'updated_at'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Sale Payment has been {$eventName} ";
    }

    protected static $logName = 'Sale Payment';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;
}
