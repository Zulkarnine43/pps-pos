<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ProjectMilestone extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'project_id', 'milestone_name', 'work_done', 'amount', 'start_date', 'deadline', 'finish_date',
    ];


    // activity log start
    protected static $logAttributes = ['project_id', 'milestone_name', 'work_done', 'amount', 'start_date', 'deadline', 'finish_date', 'status', 'created_at', 'updated_at'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Milestone has been {$eventName} ";
    }

    protected static $logName = 'Milestone';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    public function projectWbcs(){
        return $this->hasMany(ProjectWbc::class);
    }
}
