<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Bank extends Model
{
    use HasFactory, LogsActivity;
    
    protected $guard_name = 'api';

    protected $fillable = [
        'name', 'account_number', 'branch'
    ];
    // activity log start
    protected static $logAttributes = ['name', 'account_number', 'branch', 'status', 'updated_at', 'created_at'];

    // protected static $recordEvents = ['create', 'update'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Bank has been {$eventName} ";
    }

    protected static $logName = 'Bank';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log end
}
