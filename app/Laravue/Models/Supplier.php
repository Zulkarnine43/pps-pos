<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Supplier extends Model
{
    use HasFactory;
    use LogsActivity;
    protected $guard_name = 'api';

    protected $fillable = [
        'name', 'company', 'email','phone','country','state','district','city','zip_code','street','image', 'trade_licence', 'TIN', 'BIN', 'NID'
    ];

    public function countryName()
    {
        return $this->belongsTo(Country::class, 'country', 'id');
    }

    public function stateName()
    {
        return $this->belongsTo(State::class, 'state', 'id');
    }
    public function districtName()
    {
        return $this->belongsTo(District::class, 'district', 'id');
    }


    // activity log start
    protected static $logAttributes = ['name', 'company', 'email', 'phone', 'country', 'state', 'district', 'city', 'zip_code', 'street', 'image', 'isActive', 'trade_licence', 'TIN', 'BIN', 'NID', 'status', 'created_at', 'updated_at'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Supplier has been {$eventName} ";
    }

    protected static $logName = 'Supplier';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log ends
}
