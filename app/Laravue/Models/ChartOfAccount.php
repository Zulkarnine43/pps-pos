<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ChartOfAccount extends Model
{
    use HasFactory;
    use LogsActivity;
    protected $guard_name = 'api';

    protected $fillable = [
        'title', 'code', 'cost_center'
    ];
    // activity log start
    protected static $logAttributes = ['title', 'code', 'cost_center', 'updated_at', 'created_at'];
    public function getDescriptionForEvent(string $eventName): string
    {
        return "Chart Of List has been {$eventName} ";
    }

    protected static $logName = 'Chart Of List';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log end
}
