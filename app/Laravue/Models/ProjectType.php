<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ProjectType extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'name', 'code', 'status'
    ];

    public function projectTypeMetas(){
        return $this->hasMany(ProjectTypeMeta::class);
    }

    public function projectStages(){
        return $this->hasMany(TypeStage::class);
    }


    // activity log start

    protected static $logAttributes = ['name', 'code', 'status', 'created_at', 'updated_at'];

    // protected static $recordEvents = ['create', 'update'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Project Type has been {$eventName} ";
    }

    protected static $logName = 'Project Type';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log ends
}
