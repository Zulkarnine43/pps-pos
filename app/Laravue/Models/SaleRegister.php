<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class SaleRegister extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'open_by', 'close_by', 'open_date_time', 'close_date_time', 'opening_balance', 'closing_balance', 'note'
    ];


    // activity log start
    protected static $logAttributes = ['open_by', 'close_by', 'open_date_time', 'close_date_time', 'opening_balance', 'closing_balance','note','status', 'created_at', 'updated_at'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Sale Register has been {$eventName} ";
    }

    protected static $logName = 'Sale Register';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    public function openByUser(){
        return $this->belongsTo(User::class, 'open_by', 'id');
    }
    public function closeByUser(){
        return $this->belongsTo(User::class, 'close_by', 'id');
    }
}
