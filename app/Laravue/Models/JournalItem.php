<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class JournalItem extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'type', 'ledger_id', 'ledger_name', 'journal_id', 'debit_amount', 'credit_amount', 'cost_center',
    ];


    // activity log start
    protected static $logAttributes = ['type', 'ledger_id', 'ledger_name', 'journal_id', 'debit_amount', 'credit_amount', 'cost_center', 'status', 'created_at', 'updated_at'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Journal Item has been {$eventName} ";
    }

    protected static $logName = 'Journal Item';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log ends

    public function costCenters(){
        return $this->hasMany(JournalItemCostCenter::class);
    }

    public function ledger(){
        return $this->belongsTo(Ledger::class);
    }

    public function journal(){
        return $this->belongsTo(Journal::class);
    }
}
