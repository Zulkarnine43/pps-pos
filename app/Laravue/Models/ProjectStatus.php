<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ProjectStatus extends Model
{
    use HasFactory;
    use LogsActivity;
    protected $guard_name = 'api';

    protected $fillable = [
        'name'
    ];
    // activity log start
    protected static $logAttributes = ['name', 'updated_at', 'created_at'];
    public function getDescriptionForEvent(string $eventName): string
    {
        return "Project Status has been {$eventName} ";
    }

    protected static $logName = 'Project Status';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log end
}
