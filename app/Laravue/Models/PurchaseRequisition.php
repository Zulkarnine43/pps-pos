<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PurchaseRequisition extends Model
{
    use HasFactory;
    protected $guard_name = 'api';
    protected $fillable = [
        'prn', 'requisition_date', 'delivery_date', 'requisition_by', 'requisition_type', 'project_id', 'warehouse_id', 'note', 'status', 'checked_by', 'checked_at', 'approved_by', 'approved_at', 'created_by'
    ];

    public function purchaseRequisitionItems(){
        return $this->hasMany(PurchaseRequisitionItem::class);
    }

    public function warehouse(){
        return $this->belongsTo(Warehouse::class,'warehouse_id')->withDefault();
    }
    public function project(){
        return $this->belongsTo(Project::class,'project_id')->withDefault();
    }


    // activity log start
    protected static $logAttributes = ['prn', 'requisition_date', 'delivery_date', 'requisition_by', 'requisition_type', 'project_id', 'warehouse_id', 'note', 'status', 'checked_by', 'checked_at', 'approved_by', 'approved_at', 'created_by', 'created_at', 'updated_at'];
    public function getDescriptionForEvent(string $eventName): string
    {
        return "Purchase Requisition has been {$eventName} ";
    }
    protected static $logName = 'Purchase Requisition';
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;
    // activity log ends
}
