<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class WorkOrder extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'project_id', 'project_stage_id', 'work_order_for', 'code', 'po_issuance_date','department_id', 'order_no', 'contract_ref', 'subcontractor_id', 'supplier_id', 'delivery_point', 'delivery_address', 'delivery_contract_number', 'supply_item', 'payment_terms', 'payment_methods', 'remarks', 'task_details', 'payable_amount', 'issue_date', 'deadline', 'contract_referance', 'equipment_scope', 'service_scope', 'scope_of_work', 'other_terms', 'delay_terms', 'warranty_terms',
    ];




    // activity log start

    protected static $logAttributes = ['project_id', 'project_stage_id', 'work_order_for', 'po_issuance_date', 'code', 'department_id', 'order_no', 'contract_ref', 'subcontractor_id', 'supplier_id', 'delivery_point', 'delivery_address', 'delivery_contract_number', 'supply_item', 'payment_terms', 'payment_methods', 'remarks', 'task_details', 'payable_amount', 'issue_date', 'deadline', 'contract_referance', 'equipment_scope', 'service_scope', 'scope_of_work', 'other_terms', 'delay_terms', 'warranty_terms', 'status', 'created_at', 'updated_at'];

    public function getDescriptionForEvent(string $eventName): string
    {
        return "Work Order has been {$eventName} ";
    }

    protected static $logName = 'Work Order';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log ends

    // public function project(){
    //     return $this->belongsTo(Project::class);
    // }

    public function department(){
        return $this->belongsTo(Department::class);
    }

    public function subcontractor(){
        return $this->belongsTo(Subcontractor::class);
    }

    public function supplier(){
        return $this->belongsTo(Supplier::class);
    }

    public function workOrderItems(){
        return $this->hasMany(WorkOrderItem::class);
    }

    public function bills(){
        return $this->hasMany(SubcontractorBillItem::class);
    }
}
