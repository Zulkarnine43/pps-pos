<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class SaleItem extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'sale_id', 'product_id', 'quantity', 'price', 'discount', 'sub_total', 'unit_name', 'vat',
    ];


    // activity log start
    protected static $logAttributes = ['sale_id', 'product_id', 'quantity', 'price', 'discount', 'sub_total', 'unit_name', 'vat', 'status', 'created_at', 'updated_at'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Sale Item has been {$eventName} ";
    }

    protected static $logName = 'Sale Item';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    public function product(){
        return $this->belongsTo(Product::class);
    }
}
