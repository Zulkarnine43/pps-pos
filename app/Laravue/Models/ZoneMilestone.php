<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ZoneMilestone extends Model
{
   
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'name', 'milestone_id', 'work_done',
    ];


    // activity log start
    protected static $logAttributes = ['zone_id', 'milestone_id', 'work_done','status', 'created_at', 'updated_at'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Zone Milestone has been {$eventName} ";
    }

    protected static $logName = 'Zone Milestone';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    public function milestone(){
        return $this->belongsTo('App\Laravue\Models\Milestone');
    }
}
