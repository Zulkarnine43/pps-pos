<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    use HasFactory;

    protected $guard_name = 'api';

    protected $fillable = [
        'delivery_date', 'created_by', 'ref_number', 'gatepass_number', 'delivery_type', 'from', 'to', 'status', 'delivered_by', 'delivered_by_phone', 'note'
    ];

    public function warehouse(){
        return $this->belongsTo(Warehouse::class,'from');
    }
    public function project(){
        return $this->belongsTo(Project::class,'from');
    }
    public function deliveryItems(){
        return $this->hasMany(DeliveryItem::class);
    }

    public function fromWarehouse(){
        return $this->belongsTo(Warehouse::class,'from')->withDefault();
    }

    public function fromProject(){
        return $this->belongsTo(Project::class,'from')->withDefault();
    }
    public function toWarehouse(){
        return $this->belongsTo(Warehouse::class,'to')->withDefault();
    }
    public function toProject(){
        return $this->belongsTo(Project::class,'to')->withDefault();
    }


    // activity log start
    protected static $logAttributes = ['product_id', 'quantity', 'project_id', 'status', 'created_at', 'updated_at'];

    public function getDescriptionForEvent(string $eventName): string
    {
        return "Delivery has been {$eventName} ";
    }

    protected static $logName = 'Delivery';
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;
    // activity log ends
}
