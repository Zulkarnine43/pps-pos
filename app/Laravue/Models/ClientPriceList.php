<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ClientPriceList extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'client_id', 'user_id', 'entry_date','delivery_date', 'discount_type', 'discount', 'shipping_cost', 'note', 'grand_total'
    ];

    // activity log start

    protected static $logAttributes = ['client_id', 'user_id', 'entry_date','delivery_date', 'status', 'updated_at', 'discount_type', 'discount', 'shipping_cost', 'note', 'grand_total','created_at'];

    // protected static $recordEvents = ['create', 'update'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Client Price List has been {$eventName} ";
    }

    protected static $logName = 'Client Price List';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log end

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function client(){
        return $this->belongsTo(Client::class);
    }

    public function listItems(){
        return $this->hasMany(ClientPriceListItems::class,);
    }
}
