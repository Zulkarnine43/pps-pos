<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ProjectMeta extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'project_id', 'project_type_meta_id', 'field_name', 'input_name', 'input_value', 'input_type'
    ];



    // activity log start

    protected static $logAttributes = ['project_id', 'project_type_meta_id', 'field_name', 'input_name', 'input_value', 'status', 'created_at', 'updated_at', 'input_type'];

    // protected static $recordEvents = ['create', 'update'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Project Meta has been {$eventName} ";
    }

    protected static $logName = 'Project Meta';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;


    // activity log ends
}
