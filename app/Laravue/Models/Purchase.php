<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Purchase extends Model
{
    use HasFactory;
    use LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'entry_date', 'created_by', 'ref_number', 'warehouse_id', 'status', 'supplier_id', 'attachment', 'discount', 'discount_type', 'shipping_cost', 'note', 'payment_condition', 'grand_total'
    ];

    public function warehouse(){
        return $this->belongsTo(Warehouse::class);
    }
    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id','id');
    }
    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function purchaseItems(){
        return $this->hasMany(PurchaseItem::class, 'purchase_id','id');
    }


    // activity log start
    protected static $logAttributes = ['entry_date', 'created_by', 'ref_number', 'warehouse_id', 'status', 'supplier_id', 'attachment', 'discount', 'discount_type', 'shipping_cost', 'note', 'payment_condition', 'grand_total', 'created_at', 'updated_at'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Purchase has been {$eventName} ";
    }

    protected static $logName = 'Purchase';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log ends
}
