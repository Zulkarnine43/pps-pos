<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjectInventory extends Model
{
    use HasFactory;

    protected $guard_name = 'api';

    protected $fillable = [
        'product_id', 'quantity', 'project_id', 'status'
    ];

    // activity log start
    protected static $logAttributes = ['product_id', 'quantity', 'project_id', 'status', 'created_at', 'updated_at'];

    public function getDescriptionForEvent(string $eventName): string
    {
        return "ProjectInventory has been {$eventName} ";
    }

    protected static $logName = 'ProjectInventory';
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;
    // activity log ends


    public function product(){
        return $this->belongsTo(Product::class);
    }
    public function project(){
        return $this->belongsTo(Project::class);
    }
}
