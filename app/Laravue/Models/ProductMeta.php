<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductMeta extends Model
{
    use HasFactory;
    protected $guard_name = 'api';

    protected $fillable = [
        'product_id', 'key', 'value'
    ];



    // activity log start

    protected static $logAttributes = ['product_id', 'key', 'value', 'status', 'created_at', 'updated_at'];

    // protected static $recordEvents = ['create', 'update'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Product Meta has been {$eventName} ";
    }

    protected static $logName = 'Product Meta';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log ends
}
