<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Project extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'name', 'prepaired_by', 'deadline', 'finished_date', 'start_date', 'site_code', 'coordinator', 'country_id', 'state_id', 'district_id', 'city_id', 'project_amount', 'project_status_id', 'current_milestone_id', 'zone_id'
    ];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }
    public function state()
    {
        return $this->belongsTo(State::class);
    }
    public function district()
    {
        return $this->belongsTo(District::class);
    }
    public function coordinatorName()
    {
        return $this->belongsTo(User::class, 'coordinator', 'id');
    }

    public function city(){
        return $this->belongsTo(City::class, 'city_id', 'id');
    }

    // activity log start


    protected static $logAttributes = ['name', 'prepaired_by', 'deadline', 'finished_date', 'start_date', 'site_code', 'coordinator', 'country_id', 'state_id', 'district_id', 'city_id', 'project_amount', 'project_status_id', 'current_milestone_id', 'zone_id', 'status' ,'created_at', 'updated_at'];

    // protected static $recordEvents = ['create', 'update'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Project has been {$eventName} ";
    }

    protected static $logName = 'Project';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log ends

    public function milestones()
    {
        return $this->hasMany(ProjectMilestone::class);
    }
    public function doneMilestones()
    {
        return $this->hasMany(ProjectMilestone::class)->where('status', 1);
    }
}
