<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class SubcontractorBill extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
         'received_by', 'received_date', 'checked_date', 'checked_by', 'approved_by', 'submited_by', 'submited_by_phone_number', 'subcontractor_id', 'bill_id','payment_send_number', 'payment_date', 'bill_status', 'entry_date','total_receive_amount','total_approve_amount',
    ];
    // activity log start

    protected static $logAttributes = [
        'received_by', 'received_date', 'checked_date', 'checked_by', 'approved_by', 'submited_by', 'submited_by_phone_number', 'subcontractor_id', 'bill_id', 'payment_send_number', 'payment_date', 'bill_status', 'entry_date','total_receive_amount','total_approve_amount', 'status', 'updated_at', 'created_at'
    ];

    // protected static $recordEvents = ['create', 'update'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Subcontractor Bill has been {$eventName} ";
    }

    protected static $logName = 'Subcontractor Bill';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    public function subcontractor(){
        return $this->belongsTo(Subcontractor::class);
    }

    public function receivedBy(){
        return $this->belongsTo(User::class, 'received_by', 'id');
    }
    public function approvedBy(){
        return $this->belongsTo(User::class, 'approved_by', 'id');
    }
    public function checkedBy(){
        return $this->belongsTo(User::class, 'checked_by', 'id');
    }

    public function billItems(){
        return $this->hasMany(SubcontractorBillItem::class);
    }
}
