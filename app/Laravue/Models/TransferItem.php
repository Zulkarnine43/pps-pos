<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransferItem extends Model
{
    use HasFactory;
    protected $guard_name = 'api';

    protected $fillable = [
        'transfer_id', 'product_id', 'quantity', 'status'
    ];

    public function product(){
      return $this->belongsTo(Product::class)->withDefault();
    }

    // activity log start
    protected static $logAttributes = ['transfer_id', 'product_id', 'quantity', 'status', 'created_at', 'updated_at'];

    public function getDescriptionForEvent(string $eventName): string
    {
        return "TransferItem has been {$eventName} ";
    }

    protected static $logName = 'TransferItem';
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;
    // activity log ends

}
