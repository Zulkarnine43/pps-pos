<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ProjectTypeMeta extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'project_type_id', 'field_name', 'input_type', 'is_required', 'input_name', 'default_value'
    ];


    // activity log start
    protected static $logAttributes = ['project_type_id', 'field_name', 'input_name', 'input_type', 'default_value', 'is_required', 'created_at', 'updated_at'];

    // protected static $recordEvents = ['create', 'update'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Project Type Meta has been {$eventName} ";
    }

    protected static $logName = 'Project Type Meta';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log ends
}
