<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Group extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'name', 'parent_group_id', 'default_group_id', 'nature', 'effect_gross_profit', 'cash_flow_type',
    ];


    // activity log start
    protected static $logAttributes = ['name', 'parent_group_id', 'default_group_id', 'nature', 'effect_gross_profit', 'cash_flow_type', 'status', 'created_at', 'updated_at'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Group has been {$eventName} ";
    }

    protected static $logName = 'Group';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log ends
    public function parent(){
        return $this->belongsTo(ParentGroup::class, 'parent_group_id', 'id');
    }
    public function default(){
        return $this->belongsTo(DefaultGroup::class, 'default_group_id', 'id');
    }
}
