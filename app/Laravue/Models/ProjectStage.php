<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ProjectStage extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'stage_name', 'project_id', 'work_done', 'finished_date', 'stage_amount', 'start_date', 'end_date', 'type_stage_id',  'image_required'
    ];


    // activity log start
    protected static $logAttributes = ['stage_name', 'work_done', 'finished_date', 'project_id', 'stage_amount', 'start_date', 'end_date', 'image_required', 'type_stage_id', 'status', 'created_at', 'updated_at'];

    // protected static $recordEvents = ['create', 'update'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Project Stage has been {$eventName} ";
    }

    protected static $logName = 'Project Stage';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log ends


    public function images(){
        return $this->hasMany(ProjectImage::class, 'project_stage_id', 'id');
    }

    public function clientBill(){
        return $this->hasMany(ClientTopSheet::class, 'stage_id','id');
    }

    public function subcontractorBill(){
        return $this->hasMany(SubcontractorBillItem::class, 'stage_id','id');
    }

    public function project(){
        return $this->belongsTo(Project::class)->with('client');
    }
}
