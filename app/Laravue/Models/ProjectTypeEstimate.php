<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ProjectTypeEstimate extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'project_type_id',
    ];


    // activity log start
    protected static $logAttributes = ['project_type_id', 'status', 'created_at', 'updated_at'];

    // protected static $recordEvents = ['create', 'update'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Project Type Estimate has been {$eventName} ";
    }

    protected static $logName = 'Project Type Estimate';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log ends

    public function projectType(){
        return $this->belongsTo(ProjectType::class, 'project_type_id', 'id');
    }

    public function projectTypeEstimateItems(){
        return $this->hasMany(ProjectTypeEstimateItem::class);
    }
}
