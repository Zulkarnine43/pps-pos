<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class QuotationItems extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'quotation_id', 'product_id', 'price','quantity', 'discount',
    ];

    // activity log start

    protected static $logAttributes = ['quotation_id', 'product_id', 'price','quantity', 'discount', 'status', 'updated_at', 'created_at'];

    // protected static $recordEvents = ['create', 'update'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Quotation Items has been {$eventName} ";
    }

    protected static $logName = 'Quotation Items';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log end

    public function product(){
        return $this->belongsTo(Product::class);
    }
}
