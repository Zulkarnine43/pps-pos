<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ProjectDirasa extends Model
{
    use HasFactory, LogsActivity;
    protected $guard_name = 'api';

    protected $fillable = [
        'name', 'prs_number', 'phone', 'location', 'bn_address', 'country_id', 'city_id', 'state_id', 'district_id', 'gps', 'site_code', 'requisition_by', 'project_type_id', 'quantity', 'details', 'dirasa_date', 'remark', 'nid_number', 'client_id', 'department_name', 'prs_number',
    ];


    // activity log start
    protected static $logAttributes = ['name', 'prs_number', 'phone', 'location', 'bn_address', 'country_id', 'city_id', 'state_id', 'district_id', 'gps', 'site_code', 'requisition_by', 'project_type_id', 'quantity', 'details', 'dirasa_date', 'remark', 'nid_number', 'client_id', 'department_name', 'prs_number', 'status', 'created_at', 'updated_at'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Project Dirasa For Client has been {$eventName} ";
    }

    protected static $logName = 'Project Dirasa';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log ends


    public function projectType(){
        return $this->belongsTo(ProjectType::class);
    }
    public function client(){
        return $this->belongsTo(Client::class);
    }
}
