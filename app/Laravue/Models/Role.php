<?php
/**
 * File Role.php
 *
 * @author Tuan Duong <bacduong@gmail.com>
 * @package Laravue
 * @version
 */
namespace App\Laravue\Models;

use App\Laravue\Acl;
use Spatie\Permission\Models\Permission;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class Role
 *
 * @property Permission[] $permissions
 * @property string $name
 * @package App\Laravue\Models
 */
class Role extends \Spatie\Permission\Models\Role
{
    use LogsActivity;
    public $guard_name = 'api';

    /**
     * Check whether current role is admin
     * @return bool
     */
    public function isSuperUser(): bool
    {
        return $this->name === Acl::ROLE_SUPER_USER;
    }


    // activity log start
    protected static $logAttributes = ['name', 'guard_name', 'created_at', 'updated_at'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Role has been {$eventName} ";
    }

    protected static $logName = 'Role';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log ends
}
