<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class JournalItemCostCenter extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'journal_item_id', 'project_type_id', 'project_id', 'site_code', 'amount', 'cost_center_type',
    ];


    // activity log start
    protected static $logAttributes = ['journal_item_id', 'project_type_id', 'project_id', 'site_code', 'amount', 'cost_center_type', 'status', 'created_at', 'updated_at'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Journal Item Cost Center has been {$eventName} ";
    }

    protected static $logName = 'Journal Item Cost Center';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log ends

    public function project(){
        return $this->belongsTo(Project::class);
    }

    public function journalItem(){
        return $this->belongsTo(JournalItem::class);
    }
}
