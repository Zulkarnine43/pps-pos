<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PosSetting extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'vat', 'vat_status','tax', 'vat_registration_number', 'tax_registration_number', 'warehouse_id', 'customer_id', 'shop_name', 'address', 'office_phone', 'mushak', 'bill_footer', 'logo', 'print_paper_size_id', 'currency', 'sale_id_prefix','print_page_top_margin', 'print_page_bottom_margin', 'print_page_left_margin', 'print_page_right_margin'
    ];


    // activity log start
    protected static $logAttributes = ['vat', 'vat_status', 'tax', 'vat_registration_number', 'tax_registration_number', 'warehouse_id', 'customer_id', 'shop_name', 'address', 'office_phone', 'mushak', 'bill_footer', 'logo', 'print_paper_size_id', 'currency', 'sale_id_prefix', 'print_page_top_margin', 'print_page_bottom_margin', 'print_page_left_margin', 'print_page_right_margin', 'status', 'created_at', 'updated_at'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Pos Setting has been {$eventName} ";
    }

    protected static $logName = 'Pos Setting';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    public function warehouse(){
        return $this->belongsTo('App\Laravue\Models\Warehouse');
    }

    public function customer(){
        return $this->belongsTo('App\Laravue\Models\Client', 'customer_id', 'id');
    }

    public function printPaperSize(){
        return $this->belongsTo('App\Laravue\Models\PrintPaperSize');
    }

}
