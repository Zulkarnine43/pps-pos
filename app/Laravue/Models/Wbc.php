<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Wbc extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'name', 'milestone_id', 'work_done'
    ];


    // activity log start
    protected static $logAttributes = ['name', 'milestone_id', 'work_done', 'status', 'created_at', 'updated_at'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "WBC has been {$eventName} ";
    }

    protected static $logName = 'WBC';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;
}
