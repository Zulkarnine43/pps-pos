<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\WorkOrderResource;
use Spatie\Activitylog\Traits\LogsActivity;

class WorkOrderItem extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'project_id', 'project_stage_id', 'work_order_id', 'remarks', 'details', 'work_order_amount', 'quantity', 'unit_price', 'unit_id', 'site_location',
    ];




    // activity log start

    protected static $logAttributes = ['project_id', 'project_stage_id', 'work_order_id', 'remarks', 'details', 'work_order_amount', 'quantity', 'unit_price', 'unit_id', 'site_location', 'status', 'created_at', 'updated_at'];

    public function getDescriptionForEvent(string $eventName): string
    {
        return "Work Order Item has been {$eventName} ";
    }

    protected static $logName = 'Work Order Item';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log ends

    // public function project(){
    //     return $this->belongsTo(Project::class);
    // }

    public function project(){
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }

    public function workOrder(){
        return $this->belongsTo(WorkOrder::class);
    }
    public function unit(){
        return $this->belongsTo(Unit::class, 'unit_id', 'id');
    }

}
