<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class SubcontractorBillItem extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'project_id', 'work_order_id', 'bill_details', 'remarks', 'stage_id', 'project_subcontract_id', 'amount', 'sd', 'deduction','bill_id', 'payment_details', 'entry_date'
    ];
    // activity log start

    protected static $logAttributes = [
        'project_id', 'work_order_id', 'bill_details', 'remarks', 'stage_id', 'project_subcontract_id', 'amount', 'sd', 'deduction', 'bill_id', 'payment_details', 'entry_date', 'status', 'updated_at', 'created_at'
    ];

    // protected static $recordEvents = ['create', 'update'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Subcontractor Bill Item has been {$eventName} ";
    }

    protected static $logName = 'Subcontractor Bill Item';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    public function project(){
        return $this->belongsTo(Project::class);
    }

    public function subcontractorBill(){
        return $this->belongsTo(SubcontractorBill::class);
    }

    public function workOrder(){
        return $this->belongsTo(WorkOrder::class);
    }
}
