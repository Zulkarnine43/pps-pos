<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class NewProjectAssignItem extends Model
{
    use HasFactory, LogsActivity;

    protected $fillable = [
        'new_project_assign_id', 'project_id', 'client_id', 'project_type_id', 'details', 'remark', 'quantity', 'remark',
    ];

    // activity log start
    protected static $logAttributes = ['new_project_assign_id', 'project_id', 'client_id', 'project_type_id', 'details', 'remark', 'quantity', 'remark',  'status', 'created_at', 'updated_at'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "New project assaign item has been {$eventName} ";
    }

    protected static $logName = 'New project assaign item';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log ends

    public function project(){
        return $this->belongsTo(Project::class);
    }
    public function client(){
        return $this->belongsTo(Client::class);
    }
    public function projectType(){
        return $this->belongsTo(ProjectType::class);
    }
    
}
