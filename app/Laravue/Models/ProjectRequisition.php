<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ProjectRequisition extends Model
{
    use HasFactory, LogsActivity;


    protected $guard_name = 'api';

    protected $fillable = [
        'name', 'prs_number', 'phone', 'location', 'country_id', 'city_id', 'state_id', 'district_id', 'bn_address', 'gps', 'site_code', 'requisition_by', 'project_type_id', 'quantity', 'details', 'requisition_date', 'remark', 'nid_number',
    ];


    // activity log start
    protected static $logAttributes = ['name', 'prs_number', 'phone', 'location', 'country_id', 'city_id', 'state_id', 'district_id', 'bn_address', 'gps', 'site_code', 'requisition_by', 'project_type_id', 'quantity', 'details', 'requisition_date', 'remark', 'nid_number', 'status', 'created_at', 'updated_at'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Project Requisition For Subcontract has been {$eventName} ";
    }

    protected static $logName = 'Project Requisition';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log ends

    public function projectType(){
        return $this->belongsTo(ProjectType::class);
    }
}
