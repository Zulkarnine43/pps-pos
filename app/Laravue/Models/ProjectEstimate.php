<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ProjectEstimate extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'project_id', 'product_id', 'unit_id', 'quantity', 'price', 'total'
    ];


    // activity log start

    protected static $logAttributes = ['project_id', 'product_id', 'unit_id', 'quantity', 'price', 'total', 'status', 'created_at', 'updated_at'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Project Estimate has been {$eventName} ";
    }

    protected static $logName = 'Project Estimate';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;


    public function product(){
        return $this->belongsTo(Product::class);
    }

    public function unit(){
        return $this->belongsTo(Unit::class);
    }
}
