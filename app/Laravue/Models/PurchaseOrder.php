<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PurchaseOrder extends Model
{
    use HasFactory;
    protected $guard_name = 'api';
    protected $fillable = [
        'pon', 'prn', 'order_date', 'delivery_date', 'order_by', 'order_type', 'project_id', 'warehouse_id', 'note', 'status', 'checked_by', 'checked_at', 'approved_by', 'approved_at', 'created_by'
    ];

    public function purchaseOrderItems(){
        return $this->hasMany(PurchaseOrderItem::class);
    }

    public function warehouse(){
        return $this->belongsTo(Warehouse::class,'warehouse_id')->withDefault();
    }
    public function project(){
        return $this->belongsTo(Project::class,'project_id')->withDefault();
    }


    // activity log start
    protected static $logAttributes = ['pon', 'prn', 'order_date', 'delivery_date', 'order_by', 'order_type', 'project_id', 'warehouse_id', 'note', 'status', 'checked_by', 'checked_at', 'approved_by', 'approved_at', 'created_by', 'created_at', 'updated_at'];
    public function getDescriptionForEvent(string $eventName): string
    {
        return "Purchase Order has been {$eventName} ";
    }
    protected static $logName = 'Purchase Order';
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;
    // activity log ends
}
