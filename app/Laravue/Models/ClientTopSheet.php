<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ClientTopSheet extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'client_bill_id', 'project_id', 'stage_id', 'amount', 'vat', 'tax', 'sd', 'deduction', 'cheque_amount', 'details', 'remarks',
    ];
    // activity log start

    protected static $logAttributes = ['client_bill_id', 'project_id', 'stage_id', 'amount', 'vat', 'tax', 'sd', 'deduction', 'cheque_amount', 'details', 'remarks', 'status', 'updated_at', 'created_at'];

    // protected static $recordEvents = ['create', 'update'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Client Bill Topsheet has been {$eventName} ";
    }

    protected static $logName = 'Client Bill Topsheet';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    public function project(){
        return $this->belongsTo(Project::class);
    }

    public function clientBill(){
        return $this->belongsTo(ClientBill::class);
    }
    
}
