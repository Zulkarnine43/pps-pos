<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ClientBill extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'client_id','bill_id', 'bill_submission_date', 'bill_submission_number', 'work_order_id', 'site_code', 'bill_status', 'prepared_by', 'prepared_date', 'send_date', 'bill_received_number',  'supervised_by', 'send_date', 'estimated_payment_date', 'payment_received_date', 'payment_details', 'subject', 'bill_format','msg1', 'msg2', 'to', 'from', 'uom', 'vat', 'total_bill_amount', 'total_cheque_amount', 'attached'
    ];
    // activity log start

    protected static $logAttributes = ['client_id', 'bill_submission_date', 'bill_submission_number', 'bill_id', 'work_order_id', 'site_code', 'bill_status', 'prepared_by', 'prepared_date', 'send_date', 'bill_received_number', 'supervised_by', 'send_date', 'estimated_payment_date', 'payment_received_date', 'payment_details', 'subject', 'bill_format','msg1', 'msg2', 'to', 'from', 'uom', 'vat', 'total_bill_amount', 'total_cheque_amount', 'attached','status', 'updated_at', 'created_at'];

    // protected static $recordEvents = ['create', 'update'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Client Bill has been {$eventName} ";
    }

    protected static $logName = 'Client Bill';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    public function client(){
        return $this->belongsTo(Client::class);
    }

    public function preparedBy(){
        return $this->belongsTo(User::class, 'prepared_by', 'id');
    }
    public function supervisedBy(){
        return $this->belongsTo(User::class, 'supervised_by', 'id');
    }

    public function billItems(){
        return $this->hasMany(ClientTopSheet::class);
    }
}
