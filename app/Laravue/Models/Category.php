<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Category extends Model
{
    use HasFactory;
    use LogsActivity;
    protected $guard_name = 'api';

    protected $fillable = [
        'name', 'slug', 'image', 'code', 'description'
    ];


    // activity log start
    protected static $logAttributes = ['name', 'slug', 'image', 'code', 'description', 'status', 'updated_at', 'created_at'];

    // protected static $recordEvents = ['create', 'update'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Category has been {$eventName} ";
    }

    protected static $logName = 'Category';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log end
}
