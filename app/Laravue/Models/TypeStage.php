<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class TypeStage extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'project_type_id','name', 'image_required'
    ];


    // activity log start
    protected static $logAttributes = ['project_type_id', 'name', 'image_required', 'status', 'created_at', 'updated_at'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Project Type Stage has been {$eventName} ";
    }

    protected static $logName = 'Project Type Stage';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log ends
}
