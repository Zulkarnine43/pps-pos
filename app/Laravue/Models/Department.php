<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Department extends Model
{
    use HasFactory, LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'name', 'code'
    ];


    // activity log start
    protected static $logAttributes = ['name', 'code', 'status', 'created_at', 'updated_at'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "Department has been {$eventName} ";
    }

    protected static $logName = 'Department';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log ends
}
