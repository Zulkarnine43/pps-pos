<?php

namespace App\Laravue\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PurchaseItem extends Model
{
    use HasFactory;
    use LogsActivity;

    protected $guard_name = 'api';

    protected $fillable = [
        'product_id', 'quantity', 'received_quantity', 'price', 'discount', 'total', 'purchase_id'
    ];

    // activity log start
    protected static $logAttributes = ['product_id', 'quantity', 'received_quantity', 'price', 'discount', 'total', 'purchase_id', 'status', 'created_at', 'updated_at'];


    public function getDescriptionForEvent(string $eventName): string
    {
        return "PurchaseItem has been {$eventName} ";
    }

    protected static $logName = 'PurchaseItem';

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    // activity log ends

    // public function warehouse(){
    //     return $this->hasOneThrough(Purchase::class,Warehouse::class);
    // }

    public function product(){
        return $this->belongsTo(Product::class);
    }
    public function purchase(){
        return $this->belongsTo(Purchase::class);
    }
}
