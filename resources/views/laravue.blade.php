<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="auth_user" content="{{ Auth::user() }}">
    <title class="site-description">Pro Projects Solutions</title>

    <link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
    <link rel="manifest" href="/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <style>
        
        .warning-row {
            background: #ffb7b7 !important;
            color: #000;
        }

        .success-row {
            background: green !important;
            color: #000;
        }
        .el-radio__input.is-disabled + span.el-radio__label,.el-radio__input.is-disabled.is-checked .el-radio__inner::after {
            color: #000 !important;
        }
        .el-radio__input.is-disabled.is-checked .el-radio__inner {
            background-color: #555 !important;
            border-color: #999 !important;
        }
         #work-order .el-checkbox__input.is-checked + .el-checkbox__label{
          color: #000 !important;
        }

        /* print css start from here */
        @media print {
            @page {
                size: {{ $setting->printPaperSize->dimension }} || '';
                font-size: 16px !important;
                transform: scale(.5) !important;
                margin-top: {{ $setting->print_page_top_margin }}px;
                margin-bottom: {{ $setting->print_page_bottom_margin }}px;
                margin-left: {{ $setting->print_page_left_margin }}px;
                margin-right: {{ $setting->print_page_right_margin }}px;
            }
            #qr_code, #invoice_logo {
                text-align: center !important;
                margin: 0 auto !important;
            }

            #work-order, #work-order-item {
                font-size: 18px;
            }
            .p-hide{
                display: none;
                background-color: red;
            }
            #info_top b,
            #info_top span {
                font-size: 14px !important;
            }

            #info_top .grid-content {
                height: 140px !important;
                line-height: 15px !important;
            }

            #info_top #contact_info .grid-content,
            #info_top #sub_info .grid-content {
                line-height: 20px !important;
                font-size: 14px !important;
            }

            #app .main-container {
                width: 100% !important;
                margin: 0 auto !important;
            }

            .sidebar-container,
            .navbar,
            #tags-view-container,
            .site-description,
            footer,
            #intro,
            #print {
                display: none !important;
            }

            .el-tabs__header, .el-button, #pe_action {
                display: none !important;
            }

            #projectEstimation .el-table__header .el-table_1_column_6 {
                display: none !important;
            }

            a[href]:after {
                display: none !important;
                visibility: hidden !important;
            }
            #bill_print {
                font-size: 14px !important;
            }
            #bill_print .p-hide , .p-hide{
                display: none;
                background-color: red;
            }

            #mceu_70,#mceu_321, #mceu_338, #mceu_127, #mceu_144, #mceu_472, #mceu_489,#mceu_166,#mceu_183,#mceu_205, div#mceu_54,div#mceu_71, div#mceu_16, div#mceu_129,div#mceu_146, div#mceu_93,div#mceu_90,div#mceu_107, div#mceu_110, div#mceu_100, div#mceu_109,div#mceu_72,div#mceu_55, div#mceu_33, div#mceu_61, #div#mceu_33-body , div#mceu_17, div#mceu_33, .el-message-box{
                display: none !important;
            }
            #mceu_70,#mceu_321, #mceu_338, #mceu_127, #mceu_144, #mceu_472, #mceu_489,#mceu_166,#mceu_183,#mceu_205, div#mceu_54,div#mceu_71,div#mceu_32,div#mceu_129,div#mceu_146,div#mceu_145,div#mceu_90,div#mceu_107,div#mceu_106,div#mceu_93,div#mceu_92, div#mceu_100, div#mceu_109, div#mceu_71,div#mceu_72,div#mceu_55, div#mceu_52, div#mceu_110, div#mceu_61, div#mceu_15, div#mceu_33, div#mceu_17, #bill_print textarea.el-textarea__inner, #bill_print input.el-input__inner {
                border: none !important;
                border-width: none;
            }
            table {
                border-collapse: collapse !important;
            }

            #mceu_33, #mceu_52, div#mceu_32, #mceu_15, .mce-panel {
                border-width: 0 !important;
                border: none !important;
            }
            
            /* @page page-2 {
                size: auto;
                margin-top: 5%;
            } */

            /* div#page-2 {
                page: page-2;
            }

            @page conditions {
                size: auto;
            } */

            /* @page

            #page-2{
                page: conditions;
            } */

            /* @page sincerely{
                margin-bottom: 0%;
            }

            div#page-2 .sincerely{
                page: sincerely;
            } */

        }

        /* print css ends here */
        #view input.el-input__inner {
            border: none !important;
            pointer-events: none !important;
        }

        #view .el-radio,
        #view .el-select {
            border: none !important;
            pointer-events: none !important;
        }

        #view input[type="file"] {
            display: none !important;
        }

        #view #extra_info .new_image_label {
            display: none !important;
        }

        #trade_licence,
        #BIN,
        #TIN,
        #NID {
            display: block !important;
        }

        .el-form-item.el-form-item--medium {
            width: 100%;
            display: flex;
            flex-direction: column !important;
            justify-content: flex-start !important;
            align-items: flex-start !important;
        }

        .el-form-item__content {
            width: 100%;
        }

        .el-date-editor.el-input,
        .el-date-editor.el-input__inner {
            width: 100% !important;
        }

        .el-form-item__content {
            margin-left: 0 !important;
        }

        .el-select.el-select--medium {
            width: 100%;
        }

        label.el-form-item__label {
            width: 250px !important;
            text-align: start !important;
        }

        select {
            width: 240px;
        }


        #view .el-button--small {
            display: none !important;
        }

        table {
            width: 100% !important;
        }

        #primary_select_user,
        #primary_select_client,
        #primary_select_p_type,
        #primary_select_status,
        #primary_project,
        #primary_select_subcontractor {
            background: #1682e6;
            border-color: #1682e6;
            color: #FFFFFF;
            outline: none;
            font-size: 14px;
        }

        #project_search,
        #search_date {
            border-color: #1682e6;
        }

        #project_list_top .el-date-editor.el-input,
        .el-date-editor.el-input__inner {
            width: 18% !important;
            top: -4px !important;
        }

        #general-countdown thead {
            display: none !important;
        }

        #general-countdown td.digit {
            text-align: left !important;
        }

        .preview {
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .preview img {
            max-width: 200px;
            max-height: 200px;
            object-fit: cover;
        }

        .modal {

            position: fixed;
            /* Stay in place */
            z-index: 9999;
            /* Sit on top */
            left: 0;
            top: 0;
            width: 100%;
            /* Full width */
            height: 100%;
            /* Full height */
            overflow-y: auto;
            background-color: rgb(0, 0, 0);
            /* Fallback color */
            background-color: rgba(0, 0, 0, 0.9);
            /* Black w/ opacity */
        }

        .lightBox {
            z-index: 9;
            display: flex;
            position: relative;
            padding: 100px 0 100px 0;

        }

        .lightBox img {
            display: block;
            text-align: center;
            margin: auto;
            max-height: 800px;
        }

        .close {
            z-index: 999;
            position: absolute;
            top: 15px;
            right: 35px;
            color: #f1f1f1;
            font-size: 40px;
            font-weight: bold;
            transition: 0.3s;
        }

        .close:hover,
        .close:focus {
            color: #bbb;
            text-decoration: none;
            cursor: pointer;
        }

        .el-scrollbar__bar.is-vertical {
            background: #46a5fc;
            width: 5px !important;
        }

        #ledgerReportForm .el-input__inner {
            width: 200% !important;
        }

        .el-input.is-disabled .el-input__inner {
            background-color: #F5F7FA;
            border-color: #dfe4ed;
            color: #000 !important;
            cursor: not-allowed;
        }

    </style>
</head>

<body>
    {{-- {{ $setting->print_page_right_margin }} --}}
    <div id="app">
        <app></app>
    </div>

    <script src=/static/tinymce4.7.5/tinymce.min.js></script>
    <script src="{{ mix('js/vendor.js') }}"></script>
    <script src="{{ mix('js/manifest.js') }}"></script>
    <script src="{{ mix('js/app.js') }}"></script>
</body>

</html>