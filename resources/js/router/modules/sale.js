/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const saleRoutes = {
  path: '/sales',
  component: Layout,
  redirect: '/sales/list',
  name: 'Sales',
  alwaysShow: true,
  meta: {
    title: 'Sales',
    icon: 'admin',
    permissions: ['view menu sales'],
  },
  children: [
    /** Settings managements */

    {
      path: 'list',
      component: () => import('@/views/sale/List'),
      name: 'saleList',
      meta: {
        title: 'Sale List',
        icon: 'list',
        permissions: ['view sale list'],
      },
    },
    {
      path: 'create',
      component: () => import('@/views/sale/Create'),
      name: 'createList',
      meta: { title: 'Create Sale', icon: 'list', permissions: ['add sale'] },
    },
    {
      path: 'view/:id(\\d+)',
      component: () => import('@/views/sale/View'),
      name: 'ViewSale',
      meta: {
        title: 'View Sale',
        noCache: true,
        permissions: ['add sale'],
      },
      hidden: true,
    },

    {
      path: 'register/list',
      component: () => import('@/views/sale/RegisterList'),
      name: 'RegisterList',
      meta: {
        title: 'Sale Register List',
        icon: 'list',
        permissions: ['view sale list'],
      },
    },
    {
      path: 'sale-report',
      component: () => import('@/views/sale/SaleReport'),
      name: 'saleReport',
      meta: {
        title: 'Sale Report',
        icon: 'list',
        permissions: ['view sale list'],
      },
      hidden: true,
    },
    // clients routes
    {
      path: 'clients',
      component: () => import('@/views/client/List'),
      name: 'ClientList',
      meta: { title: 'clientList', icon: 'list' },
    },
    {
      path: 'clients/edit/:id(\\d+)',
      component: () => import('@/views/client/components/ClientEdit'),
      name: 'EditClient',
      meta: {
        title: 'clientProfile',
        noCache: true,
        permissions: ['manage client'],
      },
      hidden: true,
    },
  ],
};

export default saleRoutes;
