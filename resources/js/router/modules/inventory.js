/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const inventoryRoutes = {
  path: '/inventory',
  component: Layout,
  redirect: '/inventory/users',
  name: 'Inventory',
  alwaysShow: true,
  meta: {
    title: 'inventory',
    icon: 'boxes',
    permissions: ['view menu inventory'],
  },
  children: [
    /** inventory managements */
    {
      path: 'categories/list',
      component: () => import('@/views/categories/List'),
      name: 'categoryList',
      meta: { title: 'categoryList', icon: 'list' },
    },
    {
      path: 'categories/edit/:id(\\d+)',
      component: () => import('@/views/categories/components/CategoryEdit'),
      name: 'EditCategory',
      meta: { title: 'EditCategory', noCache: true, permissions: ['manage category'] },
      hidden: true,
    },
    // brand routes
    {
      path: 'brands/list',
      component: () => import('@/views/brand/List'),
      name: 'brandList',
      meta: { title: 'brandList', icon: 'list' },
    },
    {
      path: 'brands/edit/:id(\\d+)',
      component: () => import('@/views/brand/components/BrandEdit'),
      name: 'EditBrand',
      meta: { title: 'EditBrand', noCache: true, permissions: ['manage brand'] },
      hidden: true,
    },
    // unit routes
    {
      path: 'units/list',
      component: () => import('@/views/unit/List'),
      name: 'unitList',
      meta: { title: 'unitList', icon: 'list' },
    },
    {
      path: 'units/edit/:id(\\d+)',
      component: () => import('@/views/unit/components/UnitEdit'),
      name: 'EditUnit',
      meta: { title: 'EditUnit', noCache: true, permissions: ['manage unit'] },
      hidden: true,
    },

    // product routes
    {
      path: 'product/list',
      component: () => import('@/views/product/List'),
      name: 'ProductList',
      meta: { title: 'productList', icon: 'list' },
    },
    {
      path: 'products/edit/:id(\\d+)',
      component: () => import('@/views/product/components/ProductEdit'),
      name: 'ProductEdit',
      meta: { title: 'ProductEdit', noCache: true, permissions: ['manage product'] },
      hidden: true,
    },
    // warehouse routes
    {
      path: 'warehouses/list',
      component: () => import('@/views/warehouse/List'),
      name: 'warehouseList',
      meta: { title: 'warehouseList', icon: 'list', permissions: ['manage warehouse'] },
    },
    {
      path: 'warehouses/edit/:id(\\d+)',
      component: () => import('@/views/warehouse/components/WarehouseEdit'),
      name: 'EditWarehouse',
      meta: { title: 'EditWarehouse', noCache: true, permissions: ['manage warehouse'] },
      hidden: true,
    },
    // warehouse inventory routes
    {
      path: 'warehouse-inventory/list',
      component: () => import('@/views/warehouse-inventory/List'),
      name: 'warehouseInventoryList',
      meta: { title: 'warehouseInventoryList', icon: 'list', permissions: ['manage warehouse inventory'] },
    },
    {
      path: 'warehouse-inventory/pending-list',
      component: () => import('@/views/warehouse-inventory/PendingList'),
      name: 'warehouseInventoryPendingList',
      meta: { title: 'warehouseInventoryPendingList', icon: 'list', permissions: ['manage warehouse inventory'] },
      hidden: true,
    },
    {
      path: 'receive-product/:id(\\d+)',
      component: () => import('@/views/warehouse-inventory/ReceiveProduct'),
      name: 'warehouseInventoryReceiveProduct',
      meta: { title: 'warehouseInventoryReceiveProduct', icon: 'list', permissions: ['manage warehouse inventory'] },
      hidden: true,
    },
    {
      path: 'stock/list',
      component: () => import('@/views/stock/List'),
      name: 'stockList',
      meta: { title: 'stockList', icon: 'list', permissions: ['manage warehouse inventory'] },
    },


    // Transfer routes
    {
      path: 'transfer/list',
      component: () => import('@/views/transfer/List'),
      name: 'transferList',
      meta: { title: 'transferList', icon: 'list', permissions: ['view transfer list'] },
    },
    {
      path: 'transfers/Create',
      component: () => import ('@/views/transfer/Create'),
      name: 'transferCreate',
      meta: { title: 'transferCreate', icon: 'list', permissions: ['add transfer']  },
      hidden: true,
    },
    {
      path: 'transfers/view/:id(\\d+)',
      component: () => import ('@/views/transfer/View'),
      name: 'transferView',
      meta: { title: 'transferView', icon: 'list', permissions: ['view transfer']  },
      hidden: true,
    },


    // Delivery routes
    {
      path: 'delivery/list',
      component: () => import('@/views/delivery/List'),
      name: 'deliveryList',
      meta: { title: 'deliveryList', icon: 'list', permissions: ['view delivery list'] },
    },
    {
      path: 'deliveries/Create',
      component: () => import ('@/views/delivery/Create'),
      name: 'deliveryCreate',
      meta: { title: 'deliveryCreate', icon: 'list', permissions: ['add delivery']  },
      hidden: true,
    },
    {
      path: 'deliveries/view/:id',
      component: () => import ('@/views/delivery/View'),
      name: 'deliveryView',
      meta: { title: 'deliveryView', icon: 'list', permissions: ['view delivery']  },
      hidden: true,
    },
    

    // // Quotation routes
    {
      path: 'quotation/list',
      component: () => import('@/views/quotation/List'),
      name: 'quotationList',
      meta: { title: 'quotationList', icon: 'list', permissions: ['manage quotation'] },
    },
    {
      path: 'quotation/create',
      component: () => import('@/views/quotation/Create'),
      name: 'quotationEdit',
      meta: { title: 'quotationEdit', icon: 'list', permissions: ['add quotation'] },
      hidden: true,
    },
    {
      path: 'quotation/view/:id(\\d+)',
      component: () => import('@/views/quotation/View'),
      name: 'quotationEdit',
      meta: { title: 'quotationEdit', noCache: true, permissions: ['update quotation'] },
      hidden: true,
    },
  ],
};

export default inventoryRoutes;
