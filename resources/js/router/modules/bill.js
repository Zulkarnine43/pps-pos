/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const billRoutes = {
  path: '/bill',
  component: Layout,
  redirect: '/bill',
  name: 'Bill',
  alwaysShow: true,
  meta: {
    title: 'Bill',
    icon: 'cart',
    permissions: ['view menu bill'],
  },
  children: [
    /** Client Bill managements */
    // {
    //   path: 'client-bill/list',
    //   component: () => import('@/views/bill/ClientBill/List'),
    //   name: 'clientBill',
    //   meta: { title: 'clientBillList', icon: 'list' },
    // },

    // {
    //   path: 'client-bill/create',
    //   component: () => import('@/views/bill/ClientBill/Create'),
    //   name: 'createBill',
    //   meta: { title: 'createBill', icon: 'edit', noCache: true, permissions: ['manage bill'] },
    //   hidden: true,
    // },
    // // {
    // //   path: 'client-bill/generate-topsheet',
    // //   component: () => import('@/views/bill/ClientBill/Topsheet'),
    // //   name: 'viewBill',
    // //   meta: { title: 'viewBill', icon: 'edit', noCache: true, permissions: ['manage bill'] },
    // //   hidden: true,
    // // },
    // {
    //   path: 'client-bill/view/:id(\\d+)',
    //   component: () => import('@/views/bill/ClientBill/View'),
    //   name: 'viewBill',
    //   meta: { title: 'viewBill', icon: 'edit', noCache: true, permissions: ['view client bill'] },
    //   hidden: true,
    // },

    // // {
    // //   path: 'client-bill/topsheet/list',
    // //   component: () => import('@/views/bill/ClientBillTopsheet/List'),
    // //   name: 'ClientBillTopsheetList',
    // //   meta: { title: 'ClientBillTopsheetList', icon: 'list', noCache: true, permissions: ['manage bill'] },
    // // },
    // {
    //   path: 'topsheet/view/:id(\\d+)',
    //   component: () => import('@/views/bill/ClientBillTopsheet/View'),
    //   name: 'ClientBillTopsheetView',
    //   meta: { title: 'ClientBillTopsheetView', icon: 'list', noCache: true, permissions: ['manage bill'] },
    //   hidden: true,
    // },
    // {
    //   path: 'client-bill/receive',
    //   component: () => import('@/views/bill/ClientBill/ReceiveBills'),
    //   name: 'ClientBillTopsheetReceive',
    //   meta: { title: 'ClientBillTopsheetReceive', icon: 'list', noCache: true, permissions: ['manage bill'] },
    //   hidden: true,
    // },

    // {
    //   path: 'client-bill/receive/:id(\\d+)',
    //   component: () => import('@/views/bill/ClientBillTopsheet/BillReceive'),
    //   name: 'ClientBillReceive',
    //   meta: { title: 'ClientBillReceive', icon: 'list', noCache: true, permissions: ['receive client bill'] },
    //   hidden: true,
    // },

    // /** Subcontractor Bill managements */
    // {
    //   path: 'subcontractor-bill/list',
    //   component: () => import('@/views/bill/SubcontractorBill/List'),
    //   name: 'subcontractorBill',
    //   meta: { title: 'subcontractorBillList', icon: 'list' },
    // },
    // {
    //   path: 'subcontractor-bill/create',
    //   component: () => import('@/views/bill/SubcontractorBill/Create'),
    //   name: 'subcontractorCreateBill',
    //   meta: { title: 'Subcontractor Bill Create', icon: 'edit', noCache: true, permissions: ['manage bill'] },
    //   hidden: true,
    // },
    // {
    //   path: 'subcontractor-bill/view/:id(\\d+)',
    //   component: () => import('@/views/bill/SubcontractorBill/View'),
    //   name: 'subcontractorViewBill',
    //   meta: { title: 'Subcontractor Bill View', icon: 'edit', noCache: true, permissions: ['view subcontractor bill'] },
    //   hidden: true,
    // },
    // {
    //   path: 'subcontractor-bill/check/:id(\\d+)',
    //   component: () => import('@/views/bill/SubcontractorBill/Check'),
    //   name: 'checkBill',
    //   meta: { title: 'checkBill', icon: 'edit', noCache: true, permissions: ['check subcontractor bill'] },
    //   hidden: true,
    // },
    // {
    //   path: 'subcontractor-bill/approve/:id(\\d+)',
    //   component: () => import('@/views/bill/SubcontractorBill/Approve'),
    //   name: 'approveill',
    //   meta: { title: 'approveBill', icon: 'edit', noCache: true, permissions: ['approve subcontractor bill'] },
    //   hidden: true,
    // },

    // // bill format
    // {
    //   path: 'bill-format',
    //   component: () => import('@/views/bill/billformat/Index'),
    //   name: 'billformat',
    //   meta: { title: 'billformat', icon: 'list' },
    //   hidden: true,
    // },
    // {
    //   path: 'bill-format/view/general-format/:id(\\d+)',
    //   component: () => import('@/views/bill/billformat/General/GeneralFormatView'),
    //   name: 'General Format View',
    //   meta: { title: 'General Format List', icon: 'edit' },
    //   hidden: true,
    // },
    // {
    //   path: 'bill-format/receive/general-format/:id(\\d+)',
    //   component: () => import('@/views/bill/billformat/General/GeneralFormatReceive'),
    //   name: 'GeneralFormatView',
    //   meta: { title: 'General Format Bill View', icon: 'edit' },
    //   hidden: true,
    // },
    // {
    //   path: 'bill-format/general-format',
    //   component: () => import('@/views/bill/billformat/General/General'),
    //   name: 'generalFormatBillEdit',
    //   meta: { title: 'General Format Bill Edit', icon: 'list' },
    //   hidden: true,
    // },
    // {
    //   path: 'bill-format/ksr-format',
    //   component: () => import('@/views/bill/billformat/KsrFormat/KsrFormat'),
    //   name: 'ksrFormatBillList',
    //   meta: { title: 'KSR Format Bill List', icon: 'list' },
    //   hidden: true,
    // },
    // {
    //   path: 'bill-format/view/ksr-format/:id(\\d+)',
    //   component: () => import('@/views/bill/billformat/KsrFormat/KsrFormatView'),
    //   name: 'ksrFormatBillView',
    //   meta: { title: 'KSR Format Bill View', icon: 'list' },
    //   hidden: true,
    // },
    // {
    //   path: 'bill-format/receive/ksr-format/:id(\\d+)',
    //   component: () => import('@/views/bill/billformat/KsrFormat/KsrFormatReceive'),
    //   name: 'ksrFormatBillEdit',
    //   meta: { title: 'KSR Format Bill Edit', icon: 'list' },
    //   hidden: true,
    // },
    // {
    //   path: 'bill-format/gp-format',
    //   component: () => import('@/views/bill/billformat/GpFormat/GpFormat'),
    //   name: 'gpFormatBillList',
    //   meta: { title: 'GP Format Bill List', icon: 'list' },
    //   hidden: true,
    // },
    // {
    //   path: 'bill-format/view/gp-format/:id(\\d+)',
    //   component: () => import('@/views/bill/billformat/GpFormat/GpFormatView'),
    //   name: 'gpFormatBillView',
    //   meta: { title: 'GP Format Bill View', icon: 'list' },
    //   hidden: true,
    // },
    // {
    //   path: 'bill-format/receive/gp-format/:id(\\d+)',
    //   component: () => import('@/views/bill/billformat/GpFormat/GpFormatReceive'),
    //   name: 'gpFormatBillEdit',
    //   meta: { title: 'GP Format Bill Edit', icon: 'list' },
    //   hidden: true,
    // },
    // {
    //   path: 'bill-format/edotco-format',
    //   component: () => import('@/views/bill/billformat/EdotcoFormat/EdotcoFormat'),
    //   name: 'edotcoFormatBillList',
    //   meta: { title: 'Edotco Format Bill List', icon: 'list' },
    //   hidden: true,
    // },
    // {
    //   path: 'bill-format/view/edotco-format/:id(\\d+)',
    //   component: () => import('@/views/bill/billformat/EdotcoFormat/EdotcoFormatView'),
    //   name: 'edotcoFormatBillView',
    //   meta: { title: 'Edotco Format Bill View', icon: 'list' },
    //   hidden: true,
    // },
    // {
    //   path: 'bill-format/receive/edotco-format/:id(\\d+)',
    //   component: () => import('@/views/bill/billformat/EdotcoFormat/EdotcoFormatReceive'),
    //   name: 'edotcoFormatBillEdit',
    //   meta: { title: 'Edotco Format Bill Edit', icon: 'list' },
    //   hidden: true,
    // },
  ],
};

export default billRoutes;
