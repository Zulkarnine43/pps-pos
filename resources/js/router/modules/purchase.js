/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const purchaseRoutes = {
  path: '/purchase',
  component: Layout,
  redirect: '/purchase',
  name: 'Purchase',
  alwaysShow: true,
  meta: {
    title: 'purchase',
    icon: 'cart',
    permissions: ['view menu purchase'],
  },
  children: [
    /** Purchase managements */
    {
      path: 'purchases/list',
      component: () => import('@/views/purchase/List'),
      name: 'purchaseList',
      meta: { title: 'purchaseList', icon: 'list' },
    },

    {
      path: 'purchases/create',
      component: () => import('@/views/purchase/Create'),
      name: 'createPurchase',
      meta: { title: 'createPurchase', icon: 'edit', noCache: true, permissions: ['manage purchase'] },
    },
    {
      path: 'purchases/view/:id(\\d+)',
      component: () => import('@/views/purchase/View'),
      name: 'viewPurchase',
      meta: { title: 'viewPurchase', icon: 'edit', noCache: true, permissions: ['manage purchase'] },
      hidden: true,
    },
    {
      path: 'purchases/edit/:id(\\d+)',
      component: () => import('@/views/purchase/Edit'),
      name: 'editPurchase',
      meta: { title: 'editPurchase', icon: 'edit', noCache: true, permissions: ['manage purchase'] },
      hidden: true,
    },
    {
      path: 'purchase-report',
      component: () => import('@/views/purchase/purchase-report'),
      name: 'purchaseReport',
      meta: {
        title: 'purchase Report',
        icon: 'list',
       // permissions: ['view sale list'],
      },
      hidden: true,
    },

    // Requisition routes
    // {
    //   path: 'requisitions/list',
    //   component: () => import('@/views/purchase-requisition/List'),
    //   name: 'purchaseRequisitionList',
    //   meta: { title: 'purchaseRequisitionList', icon: 'list', permissions: ['manage purchase'] },
    // },
    // {
    //   path: 'requisitions/Create',
    //   component: () => import ('@/views/purchase-requisition/Create'),
    //   name: 'purchaseRequisitionCreate',
    //   meta: { title: 'purchaseRequisitionCreate', icon: 'list', permissions: ['manage purchase']  },
    //   hidden: true,
    // },
    // {
    //   path: 'requisitions/view/:id',
    //   component: () => import ('@/views/purchase-requisition/View'),
    //   name: 'purchaseRequisitionView',
    //   meta: { title: 'purchaseRequisitionView', icon: 'list', permissions: ['manage purchase']  },
    //   hidden: true,
    // },
  ],
};

export default purchaseRoutes;
