/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const projectStatusRoutes = {
    path: '/project-status',
    component: Layout,
    name: 'projectStatus',
    alwaysShow: true,
    meta: {
        title: 'projectStatus',
        icon: 'admin',
        permissions: ['view menu project status'],
    },
    children: [
        /** projectStatus managements */
        {
            path: 'list',
            component: () =>
                import ('@/views/projectstatus/List'),
            name: 'projectStatusList',
            meta: { title: 'projectStatus', icon: 'list', permissions: ['manage project status'] },
        },
        {
            path: 'view/:id(\\d+)',
            component: () =>
                import ('@/views/projectstatus/components/Edit'),
            name: 'projectstatusEdit',
            meta: { title: 'projectstatusEdit', noCache: true, permissions: ['manage project status'] },
            hidden: true,
        },
        // client-price-list routes
        {
            path: 'client-price-list/list',
            component: () =>
                import ('@/views/client-price-list/List'),
            name: 'clientPriceList',
            meta: { title: 'clientPriceList', icon: 'list', permissions: ['view client price list'] },
        },
        {
            path: 'client-price-list/create',
            component: () =>
                import ('@/views/client-price-list/Create'),
            name: 'clientPriceCreate',
            meta: { title: 'clientPriceCreate', icon: 'edit', permissions: ['view client price list'] },
            hidden: true,
        },
        {
            path: 'client-price-list/view/:id(\\d+)',
            component: () =>
                import ('@/views/client-price-list/View'),
            name: 'clientPriceEdit',
            meta: { title: 'clientPriceEdit', noCache: true, permissions: ['view client price list'] },
            hidden: true,
        },

        // subcontractor-price-list routes
        {
            path: 'subcontractor-price-list/list',
            component: () =>
                import ('@/views/subcontractor-price-list/List'),
            name: 'subcontractorPriceList',
            meta: { title: 'subcontractorPriceList', icon: 'list', permissions: ['view subcontractor price list'] },
        },
        {
            path: 'subcontractor-price-list/create',
            component: () =>
                import ('@/views/subcontractor-price-list/Create'),
            name: 'subcontractorPriceCreate',
            meta: { title: 'subcontractorPriceCreate', icon: 'edit', permissions: ['view subcontractor price list'] },
            hidden: true,
        },
        {
            path: 'subcontractor-price-list/view/:id(\\d+)',
            component: () =>
                import ('@/views/subcontractor-price-list/View'),
            name: 'subcontractorPriceEdit',
            meta: { title: 'subcontractorPriceEdit', noCache: true, permissions: ['view subcontractor price list'] },
            hidden: true,
        },
    ],
};

export default projectStatusRoutes;