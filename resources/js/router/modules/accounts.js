/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const accountsRoutes = {
  path: '/accounts',
  component: Layout,
  redirect: '/accounts/users',
  name: 'accounts',
  alwaysShow: true,
  meta: {
    title: 'Accounts',
    icon: 'el-icon-s-data',
    permissions: ['view menu accounts'],
  },
  children: [
     /** Purchase managements */
  {
      path: 'chart-of-account/list',
      component: () => import ('@/views/chartered/charteredAccount'),
      name: 'chartlist',
      meta: { title: 'charteredAccountList', icon: 'list' },
  },
  {
      path: 'chart-of-account/view/:id(\\d+)',
      component: () => import ('@/views/chartered/components/charteredAccountEdit'),
      name: 'chartedit',
      meta: { title: 'charteredAccountListEdit', icon: 'list' },
      hidden: true,
  },
  {
    path: 'groups/list',
    component: () => import ('@/views/group/List'),
    name: 'groupList',
    meta: { title: 'groupList', icon: 'list',permissions: ['add group']  },
  },
  {
    path: 'groups/edit/:id(\\d+)',
    component: () => import ('@/views/group/Edit'),
    name: 'groupUpdate',
    meta: { title: 'groupUpdate', icon: 'list', permissions: ['update group']  },
    hidden: true,
  },
  {
    path: 'groups/Create',
    component: () => import ('@/views/group/Create'),
    name: 'groupCreate',
    meta: { title: 'groupCreate', icon: 'list', permissions: ['view group list']  },
    hidden: true,
  },
  {
    path: 'parent-group/list',
    component: () => import ('@/views/parent-group/List'),
    name: 'parentGroupList',
    meta: { title: 'parentGroupList', icon: 'list',permissions: ['view parent group list']  },
    hidden: true,
  },
  
  {
    path: 'default-group/list',
    component: () => import ('@/views/default-group/List'),
    name: 'defaultGroupList',
    meta: { title: 'defaultGroupList', icon: 'list',permissions: ['view default group list']  },
    hidden: true,
  },
  {
    path: 'ledgers/list',
    component: () => import ('@/views/ledger/List'),
    name: 'ledgerList',
    meta: { title: 'ledgerList', icon: 'list',permissions: ['view ledger list']  },
  },
  {
    path: 'ledgers/edit/:id(\\d+)',
    component: () => import ('@/views/ledger/Edit'),
    name: 'ledgerpUpdate',
    meta: { title: 'ledgerUpdate', icon: 'list', permissions: ['update ledger']  },
    hidden: true,
  },
  {
    path: 'ledgers/Create',
    component: () => import ('@/views/ledger/Create'),
    name: 'ledgerCreate',
    meta: { title: 'ledgerCreate', icon: 'list', permissions: ['add ledger']  },
    hidden: true,
  },

  {
    path: 'journals/list',
    component: () => import ('@/views/journal/List'),
    name: 'journalList',
    meta: { title: 'journalList', icon: 'list',permissions: ['view journal list']  },
  },
  {
    path: 'journals/Create',
    component: () => import ('@/views/journal/Create'),
    name: 'journalCreate',
    meta: { title: 'journalCreate', icon: 'list', permissions: ['add journal']  },
    hidden: true,
  },
  {
    path: 'journals/view/:id(\\d+)',
    component: () => import ('@/views/journal/View'),
    name: 'journalView',
    meta: { title: 'journalView', icon: 'list', permissions: ['view journal']  },
    hidden: true,
  },
  {
    path: 'journals/edit/:id(\\d+)',
    component: () => import ('@/views/journal/Edit'),
    name: 'journalUpdate',
    meta: { title: 'journalUpdate', icon: 'list', permissions: ['update journal']  },
    hidden: true,
  },
  {
    path: 'reports',
    component: () => import ('@/views/accounts/Report'),
    name: 'accountsReport',
    meta: { title: 'Reports', icon: 'list', permissions: ['view reports']  },
  },
  {
    path: 'view/cost-center-report',
    component: () => import ('@/views/accounts/costCenterReport'),
    name: 'viewCostCenterReport',
    meta: { title: 'viewCostCenterReport', icon: 'list', permissions: ['view reports']  },
    hidden: true,
  },
  {
    path: 'view/ledger-report',
    component: () => import ('@/views/accounts/ledgerReport'),
    name: 'viewLedgerReport',
    meta: { title: 'viewLedgerReport', icon: 'list', permissions: ['view reports']  },
    hidden: true,
  },
  {
    path: 'view/trial-balance-report',
    component: () => import ('@/views/accounts/trialBalanceReport'),
    name: 'viewTrialBalanceReport',
    meta: { title: 'View Trial Balance Report', icon: 'list', permissions: ['view reports']  },
    hidden: true,
  },
  ],
};

export default accountsRoutes;
