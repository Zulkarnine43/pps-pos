/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const settingsRoutes = {
  path: '/settings',
  component: Layout,
  redirect: '/settings',
  name: 'Settings',
  alwaysShow: true,
  meta: {
    title: 'settings',
    icon: 'admin',
    permissions: ['view menu settings'],
  },
  children: [
    /** Settings managements */

    {
      path: 'pos-settings/update/1',
      component: () => import('@/views/pos-setting/Update'),
      name: 'PosSettingUpdate',
      meta: { title: 'Update Pos Setting', icon: 'edit', permissions: ['view country list'] },
    },
    {
      path: 'office-info/update/1',
      component: () => import('@/views/office-info/Update'),
      name: 'OfficeInfoUpdate',
      meta: { title: 'Office Info Update', icon: 'edit', permissions: ['update entry pin'] },
    },
    // {
    //   path: 'entry-pin/update/1',
    //   component: () => import('@/views/entry-pin/Update'),
    //   name: 'entryPinUpdate',
    //   meta: { title: 'entryPinUpdate', icon: 'edit', permissions: ['update entry pin'] },
    // },
    // {
    //   path: 'country/list',
    //   component: () => import('@/views/country/List'),
    //   name: 'countryList',
    //   meta: { title: 'countryList', icon: 'list', permissions: ['view country list'] },
    // },
    {
      path: 'zone/list',
      component: () => import('@/views/zone/List'),
      name: 'zoneList',
      meta: { title: 'Zone List', icon: 'list', permissions: ['view state list'] },
    },
    {
      path: 'milestone/list',
      component: () => import('@/views/milestone/List'),
      name: 'milestoneList',
      meta: { title: 'Milestone List', icon: 'list', permissions: ['view state list'] },
    },
    {
      path: 'milestone/create',
      component: () => import('@/views/milestone/Create'),
      name: 'milestoneCreate',
      meta: { title: 'Milestone Create', icon: 'list', permissions: ['view state list'] },
      hidden: true,
    },
    {
      path: 'milestone/view/:id(\\d+)',
      component: () => import('@/views/milestone/View'),
      name: 'viewMilestone',
      meta: { title: 'View Milestone', icon: 'edit', noCache: true, permissions: ['manage purchase'] },
      hidden: true,
    },
    {
      path: 'state/list',
      component: () => import('@/views/state/List'),
      name: 'stateList',
      meta: { title: 'stateList', icon: 'list', permissions: ['view state list'] },
    },
    {
      path: 'district/list',
      component: () => import('@/views/district/List'),
      name: 'districtList',
      meta: { title: 'districtList', icon: 'list', permissions: ['view district list'] },
    },
    {
      path: 'city/list',
      component: () => import('@/views/city/List'),
      name: 'cityList',
      meta: { title: 'cityList', icon: 'list', permissions: ['view city list'] },
    },
  ],
};

export default settingsRoutes;
