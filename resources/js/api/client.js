import request from '@/utils/request';
import Resource from '@/api/resource';

/**
 * All Client Api call from here.
 *
 * @author	bitbyte
 * @since	v0.0.1
 * @version	v1.0.0	Wednesday, February 10th, 2021.
 * @see		Resource
 * @global
 */
export class ClientResource extends Resource {
  constructor() {
    super('clients');
  }
}

/**
 * a single client api call from here
 * @var		export	functio
 * @global
 */
export function fetchClient(id) {
  return request({
    url: '/clients/' + id,
    method: 'get',
  });
}

/** client inactive api call from here
 * @var		export	functio
 * @global
 */
export function inActive(id){
  console.log(id);
  return request({
    url: '/clients/inactive/' + id,
    method: 'put',
  });
}

/**
 * client active api call from here
 * @var		export	functio
 * @global
 */
export function active(id){
  console.log(id);
  return request({
    url: '/clients/active/' + id,
    method: 'put',
  });
}
