import request from '@/utils/request';
import Resource from '@/api/resource';

/**
 * All Department Api call from here.
 *
 * @author	bitbyte
 * @since	v0.0.1
 * @version	v1.0.0	Wednesday, February 10th, 2021.
 * @see		Resource
 * @global
 */
export class DepartmentResource extends Resource {
  constructor() {
    super('departments');
  }
}

/** '
 * a single unit api call from here
 * @var		export	functio
 * @global
 */
export function fetchDepartment(id) {
  return request({
    url: '/departments/' + id,
    method: 'get',
  });
}

