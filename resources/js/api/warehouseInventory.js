import request from '@/utils/request';
import Resource from '@/api/resource';

/**
 * All Warehouse Api call from here.
 *
 * @author	bitbyte
 * @since	v0.0.1
 * @version	v1.0.0	Wednesday, February 10th, 2021.
 * @see		Resource
 * @global
 */
export class WarehouseResource extends Resource {
  constructor() {
    super('warehouse-inventory');
  }
}

/**
 * a sinle warehouse api call from here
 * @var		export	functio
 * @global
 */
export function fetchWarehouse(id) {
  return request({
    url: '/warehouse-inventory/' + id,
    method: 'get',
  });
}
