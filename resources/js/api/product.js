import request from '@/utils/request';
import Resource from '@/api/resource';

/**
 * All Product api call from here.
 *
 * @author	bitbyte
 * @since	v0.0.1
 * @version	v1.0.0	Wednesday, February 10th, 2021.
 * @see		Resource
 * @global
 */
export class ProductResource extends Resource {
  constructor() {
    super('products');
  }
}

/**
 * a single product api call from here
 * @var		export	functio
 * @global
 */
export function fetchProduct(id) {
  return request({
    url: '/products/' + id,
    method: 'get',
  });
}

/**
 * product mute api call from here
 * @var		export	functio
 * @global
 */
export function mute(id){
  console.log(id);
  return request({
    url: '/product/mute/' + id,
    method: 'put',
  });
}

/**
 * product unmute api call from here
 * @var		export	functio
 * @global
 */
export function unmute(id){
  console.log(id);
  return request({
    url: '/product/unmute/' + id,
    method: 'put',
  });
}
