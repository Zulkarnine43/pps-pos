import request from '@/utils/request';

/**
 * order api call from here
 * @var		export	functio
 * @global
 */
export function fetchList(query) {
  return request({
    url: '/orders',
    method: 'get',
    params: query,
  });
}
