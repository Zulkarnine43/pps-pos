import request from '@/utils/request';
import Resource from '@/api/resource';

/**
 * All Role Api call from here.
 *
 * @author	bitbyte
 * @since	v0.0.1
 * @version	v1.0.0	Wednesday, February 10th, 2021.
 * @see		Resource
 * @global
 */
class RoleResource extends Resource {
  constructor() {
    super('roles');
  }

  /**
   * update user permission api call from here
   * @var		mixed	permissions(i
   */
  permissions(id) {
    return request({
      url: '/' + this.uri + '/' + id + '/permissions',
      method: 'get',
    });
  }
}

export { RoleResource as default };
