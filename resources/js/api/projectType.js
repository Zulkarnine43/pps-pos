import request from '@/utils/request';
import Resource from '@/api/resource';

/**
 * All ProjectType Api call from here.
 *
 * @author	Unknown
 * @since	v0.0.1
 * @version	v1.0.0	Wednesday, February 10th, 2021.
 * @see		Resource
 * @global
 */
export class ProjectTypeResource extends Resource {
  constructor() {
    super('project-type');
  }
}

/**
 * a single project type api call from here
 * @var		export	functio
 * @global
 */
export function fetchProjectType(id) {
  return request({
    url: '/project-type/' + id,
    method: 'get',
  });
}

/**
 * project type mute api call from here
 * @var		export	functio
 * @global
 */
export function mute(id){
  return request({
    url: '/project-type/mute/' + id,
    method: 'put',
  });
}

/**
 * project type unmute api call form here
 * @var		export	functio
 * @global
 */
export function unmute(id){
  return request({
    url: '/project-type/unmute/' + id,
    method: 'put',
  });
}
