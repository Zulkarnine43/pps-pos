import request from '@/utils/request';
import Resource from '@/api/resource';

/**
 * All PermisionPrefix Api call from here.
 *
 * @author	bitbyte
 * @since	v0.0.1
 * @version	v1.0.0	Wednesday, February 10th, 2021.
 * @see		Resource
 * @global
 */
export class PermisionPrefixResource extends Resource {
  constructor() {
    super('permission-prefix');
  }
}

/** '
 * a single unit api call from here
 * @var		export	functio
 * @global
 */
export function fetchPermisionPrefix(id) {
  return request({
    url: '/permission-prefix/' + id,
    method: 'get',
  });
}
