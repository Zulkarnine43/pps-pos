import request from '@/utils/request';
import Resource from '@/api/resource';

/**
 * All Purchase Api call from here.
 *
 * @author	Unknown
 * @since	v0.0.1
 * @version	v1.0.0	Wednesday, February 10th, 2021.
 * @see		Resource
 * @global
 */
export class PurchaseResource extends Resource {
  constructor() {
    super('purchases');
  }
}

/**
 * a single purchase api call from here
 * @var		export	functio
 * @global
 */
export function fetchPurchase(id) {
  return request({
    url: '/purchases/' + id,
    method: 'get',
  });
}

/**
 * a single purchase mute api call from here
 * @var		export	functio
 * @global
 */
export function mute(id){
  return request({
    url: '/purchase/mute/' + id,
    method: 'put',
  });
}

/**
 * a single purchase umute api call from here
 * @var		export	functio
 * @global
 */
export function unmute(id){
  console.log(id);
  return request({
    url: '/purchase/unmute/' + id,
    method: 'put',
  });
}
