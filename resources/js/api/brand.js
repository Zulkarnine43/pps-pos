import request from '@/utils/request';
import Resource from '@/api/resource';

/**
 * All brand api call from here.
 *
 * @author	bitbyte
 * @since	v0.0.1
 * @version	v1.0.0	Wednesday, February 10th, 2021.
 * @see		Resource
 * @global
 */
export class BrandResource extends Resource {
  constructor() {
    super('brands');
  }
}

/**
 * get a single brand api call from here
 * @var		export	functio
 * @global
 */
export function fetchBrand(id) {
  return request({
    url: '/brands/' + id,
    method: 'get',
  });
}

/**
 * brand mute api call from here
 * @var		export	functio
 * @global
 */
export function mute(id){
  console.log(id);
  return request({
    url: '/brand/mute/' + id,
    method: 'put',
  });
}

/**
 * brand unmute api call from here
 * @var		export	functio
 * @global
 */
export function unmute(id){
  console.log(id);
  return request({
    url: '/brand/unmute/' + id,
    method: 'put',
  });
}
