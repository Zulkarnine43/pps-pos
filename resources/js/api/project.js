import request from '@/utils/request';
import Resource from '@/api/resource';

/**
 * All Project Api call from here.
 *
 * @author	bitbyte
 * @since	v0.0.1
 * @version	v1.0.0	Wednesday, February 10th, 2021.
 * @see		Resource
 * @global
 */
export class ProjectResource extends Resource {
  constructor() {
    super('projects');
  }
}

/**
 * a single project api call from here
 * @var		export	functio
 * @global
 */
export function fetchProject(id) {
  return request({
    url: '/projects/' + id,
    method: 'get',
  });
}

/**
 * project mute api call from here
 * @var		export	functio
 * @global
 */
export function mute(id){
  return request({
    url: '/project/mute/' + id,
    method: 'put',
  });
}

/**
 * project unmute api call from here
 * @var		export	functio
 * @global
 */
export function unmute(id){
  return request({
    url: '/project/unmute/' + id,
    method: 'put',
  });
}
