import request from '@/utils/request';
import Resource from '@/api/resource';

/**
 * All EntryPin Api call from here.
 *
 * @author	bitbyte
 * @since	v0.0.1
 * @version	v1.0.0	Wednesday, February 10th, 2021.
 * @see		Resource
 * @global
 */
export class EntryPinResource extends Resource {
  constructor() {
    super('entry-pin/update');
  }
}

/**
 * get pin api call from here
 * @var		export	functio
 * @global
 */
export function fetchEntryPin(id) {
  return request({
    url: '/entry-pin/update/' + id,
    method: 'get',
  });
}
