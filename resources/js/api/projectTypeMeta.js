import request from '@/utils/request';
import Resource from '@/api/resource';

/**
 * All ProjectTypeMeta Api call from here.
 *
 * @author	Unknown
 * @since	v0.0.1
 * @version	v1.0.0	Wednesday, February 10th, 2021.
 * @see		Resource
 * @global
 */
export class ProjectTypeMetaResource extends Resource {
  constructor() {
    super('project-type-meta');
  }
}

/**
 * a single project type meta api call from here
 * @var		export	functio
 * @global
 */
export function fetchProjectTypeMeta(id) {
  return request({
    url: '/project-type-meta/' + id,
    method: 'get',
  });
}
