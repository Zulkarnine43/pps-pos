import request from '@/utils/request';
import Resource from '@/api/resource';

/**
 * All Unit Api call from here.
 *
 * @author	bitbyte
 * @since	v0.0.1
 * @version	v1.0.0	Wednesday, February 10th, 2021.
 * @see		Resource
 * @global
 */
export class UnitResource extends Resource {
  constructor() {
    super('units');
  }
}

/** '
 * a single unit api call from here
 * @var		export	functio
 * @global
 */
export function fetchUnit(id) {
  return request({
    url: '/units/' + id,
    method: 'get',
  });
}

/**
 * a single mute api call from here
 * @var		export	functio
 * @global
 */
export function mute(id){
  console.log(id);
  return request({
    url: '/unit/mute/' + id,
    method: 'put',
  });
}

/**
 * a single unmute api call from here
 * @var		export	functio
 * @global
 */
export function unmute(id){
  console.log(id);
  return request({
    url: '/unit/unmute/' + id,
    method: 'put',
  });
}
