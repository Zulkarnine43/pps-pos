import request from '@/utils/request';
import Resource from '@/api/resource';

/**
 * All ProjectTypeEstimate Api call form here.
 *
 * @author	bitbyte
 * @since	v0.0.1
 * @version	v1.0.0	Wednesday, February 10th, 2021.
 * @see		Resource
 * @global
 */
export class ProjectTypeEstimateResource extends Resource {
  constructor() {
    super('project-type-estimate');
  }
}

/**
 * a single project type estimate api call from here
 * @var		export	functio
 * @global
 */
export function fetchProjectTypeEstimate(id) {
  return request({
    url: '/project-type-estimate/' + id,
    method: 'get',
  });
}

/**
 * project type estimate mute api call from here
 * @var		export	functio
 * @global
 */
export function mute(id){
  return request({
    url: '/project-type-estimate/mute/' + id,
    method: 'put',
  });
}

/**
 * project type estimate unmute api call from here
 * @var		export	functio
 * @global
 */
export function unmute(id){
  console.log(id);
  return request({
    url: '/project-type-estimate/unmute/' + id,
    method: 'put',
  });
}
