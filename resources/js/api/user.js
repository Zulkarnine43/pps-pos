import Resource from '@/api/resource';
import request from '@/utils/request';

/**
 * All User Api call from here.
 *
 * @author	Unknown
 * @since	v0.0.1
 * @version	v1.0.0	Wednesday, February 10th, 2021.
 * @see		Resource
 * @global
 */
class UserResource extends Resource {
  constructor() {
    super('users');
  }

  /**
   * user permission api call from here
   * @var		mixed	permissions(i
   */
  permissions(id) {
    return request({
      url: '/' + this.uri + '/' + id + '/permissions',
      method: 'get',
    });
  }

  /**
   * update user permission call from here
   * @var		mixed	updatePermission(id
   *//**
   * @var		mixed	permission
   */
  updatePermission(id, permissions) {
    return request({
      url: '/' + this.uri + '/' + id + '/permissions',
      method: 'put',
      data: permissions,
    });
  }
}

export { UserResource as default };
