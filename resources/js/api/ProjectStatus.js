import request from '@/utils/request';
import Resource from '@/api/resource';

/**
 * All brand api call from here.
 *
 * @author	bitbyte
 * @since	v0.0.1
 * @version	v1.0.0	Wednesday, February 10th, 2021.
 * @see		Resource
 * @global
 */
export class ProjectStatusResource extends Resource {
    constructor() {
        super('project-status');
    }
}

/**
 * get a single brand api call from here
 * @var		export	functio
 * @global
 */
export function fetchdata(id) {
    // console.log(id);
    return request({
        url: '/project-status/' + id,
        method: 'get',
    });
}