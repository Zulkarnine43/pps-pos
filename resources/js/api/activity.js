import request from '@/utils/request';
import Resource from '@/api/resource';

/**
 * Activity Log All Api Call from here.
 *
 * @author	bitbyte
 * @since	v0.0.1
 * @version	v1.0.0	Wednesday, February 10th, 2021.
 * @see		Resource
 * @global
 */
export class ActivityLogResource extends Resource {
  constructor() {
    super('activity-log');
  }
}
