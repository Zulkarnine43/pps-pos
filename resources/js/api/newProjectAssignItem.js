import request from '@/utils/request';
import Resource from '@/api/resource';

/**
 * All brand api call from here.
 *
 * @author	bitbyte
 * @since	v0.0.1
 * @version	v1.0.0	Wednesday, February 10th, 2021.
 * @see		Resource
 * @global
 */
export class NewProjectAssignItemResource extends Resource {
    constructor() {
        super('new-project-assign-item');
    }
}


