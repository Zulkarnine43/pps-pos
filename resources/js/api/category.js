import request from '@/utils/request';
import Resource from '@/api/resource';

/**
 * All Category api call from here.
 *
 * @author	bitbyte
 * @since	v0.0.1
 * @version	v1.0.0	Wednesday, February 10th, 2021.
 * @see		Resource
 * @global
 */
export class CategoryResource extends Resource {
  constructor() {
    super('categories');
  }
}

/**
 * get a single category api call from here
 * @var		export	functio
 * @global
 */
export function fetchCategory(id) {
  return request({
    url: '/categories/' + id,
    method: 'get',
  });
}

/**
 * category mute api call from here
 * @var		export	functio
 * @global
 */
export function mute(id){
  console.log(id);
  return request({
    url: '/categories/mute/' + id,
    method: 'put',
  });
}

/**
 * category unmute api call from here
 * @var		export	functio
 * @global
 */
export function unmute(id){
  console.log(id);
  return request({
    url: '/categories/unmute/' + id,
    method: 'put',
  });
}
