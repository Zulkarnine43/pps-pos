import request from '@/utils/request';
import Resource from '@/api/resource';

/**
 * All Subcontractor Api call from here.
 *
 * @author	bitbyte
 * @since	v0.0.1
 * @version	v1.0.0	Wednesday, February 10th, 2021.
 * @see		Resource
 * @global
 */
export class SubcontractorResource extends Resource {
  constructor() {
    super('subcontractors');
  }
}

/**
 * a single subcontractor api call from here
 * @var		export	functio
 * @global
 */
export function fetchSubcontractor(id) {
  return request({
    url: '/subcontractors/' + id,
    method: 'get',
  });
}

/**
 * a single subcontractor inactve api call from here
 * @var		export	functio
 * @global
 */
export function inActive(id){
  console.log(id);
  return request({
    url: '/subcontractors/inactive/' + id,
    method: 'put',
  });
}

/**
 * a single subcontractor active api call from here
 * @var		export	functio
 * @global
 */
export function active(id){
  console.log(id);
  return request({
    url: '/subcontractors/active/' + id,
    method: 'put',
  });
}
