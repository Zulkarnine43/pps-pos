import request from '@/utils/request';
import Resource from '@/api/resource';

/**
 * Supplier.
 *
 * @author	bitbyte
 * @since	v0.0.1
 * @version	v1.0.0	Wednesday, February 10th, 2021.
 * @see		Resource
 * @global
 */
export class SupplierResource extends Resource {
  constructor() {
    super('suppliers');
  }
}

/**
 * a single supplier api call from here
 * @var		export	functio
 * @global
 */
export function fetchSupplier(id) {
  return request({
    url: '/suppliers/' + id,
    method: 'get',
  });
}

/**
 * a supplier inactive api call from here
 * @var		export	functio
 * @global
 */
export function inActive(id){
  console.log(id);
  return request({
    url: '/suppliers/inactive/' + id,
    method: 'put',
  });
}

/**
 * a single active api call from here
 * @var		export	functio
 * @global
 */
export function active(id){
  console.log(id);
  return request({
    url: '/suppliers/active/' + id,
    method: 'put',
  });
}
