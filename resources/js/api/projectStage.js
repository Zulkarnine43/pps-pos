import request from '@/utils/request';
import Resource from '@/api/resource';

/**
 * All ProjectStage Api call from here.
 *
 * @author	Unknown
 * @since	v0.0.1
 * @version	v1.0.0	Wednesday, February 10th, 2021.
 * @see		Resource
 * @global
 */
export class ProjectStageResource extends Resource {
  constructor() {
    super('project-stage');
  }
}

/**
 * a single project stage api call from here
 * @var		export	functio
 * @global
 */
export function fetchProjectStage(id) {
  return request({
    url: 'project-stage/' + id,
    method: 'get',
  });
}
