import request from '@/utils/request';
import Resource from '@/api/resource';

/**
 * All Client Api call from here.
 *
 * @author	bitbyte
 * @since	v0.0.1
 * @version	v1.0.0	Wednesday, February 10th, 2021.
 * @see		Resource
 * @global
 */
export class SubcontractorPriceListResource extends Resource {
  constructor() {
    super('subcontractor-price-list');
  }
}

/**
 * a single client api call from here
 * @var		export	functio
 * @global
 */
export function fetchSubcontractorPriceList(id) {
  return request({
    url: '/subcontractor-price-list/' + id,
    method: 'get',
  });
}
