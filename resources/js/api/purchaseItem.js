import request from '@/utils/request';
import Resource from '@/api/resource';

/**
 * All Purchase Api call from here.
 *
 * @author	Unknown
 * @since	v0.0.1
 * @version	v1.0.0	Wednesday, February 10th, 2021.
 * @see		Resource
 * @global
 */
export class PurchaseItemResource extends Resource {
  constructor() {
    super('purchase-item');
  }
}

/**
 * a single purchase api call from here
 * @var		export	functio
 * @global
 */
// export function fetchPurchase(id) {
//   return request({
//     url: '/purchase-item/' + id,
//     method: 'get',
//   });
// }

