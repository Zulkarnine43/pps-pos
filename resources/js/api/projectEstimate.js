import request from '@/utils/request';
import Resource from '@/api/resource';

/**
 * All ProjectEstimate Api call form here.
 *
 * @author	bitbyte
 * @since	v0.0.1
 * @version	v1.0.0	Wednesday, February 10th, 2021.
 * @see		Resource
 * @global
 */
export class ProjectEstimateResource extends Resource {
  constructor() {
    super('project-estimate');
  }
}

/**
 * a single project estimate api call from here
 * @var		export	functio
 * @global
 */
export function fetchProjectEstimate(id) {
  return request({
    url: '/project-estimate/' + id,
    method: 'get',
  });
}
