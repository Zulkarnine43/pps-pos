import request from '@/utils/request';
import Resource from '@/api/resource';

/**
 * All ProjectSubcontract Api call from here.
 *
 * @author	Unknown
 * @since	v0.0.1
 * @version	v1.0.0	Wednesday, February 10th, 2021.
 * @see		Resource
 * @global
 */
export class ProjectSubcontractResource extends Resource {
  constructor() {
    super('project-subcontracts');
  }
}

/**
 * a single project subcontract api call from here
 * @var		export	functio
 * @global
 */
export function fetchProjectSubcontract(id) {
  return request({
    url: '/project-subcontracts/' + id,
    method: 'get',
  });
}
