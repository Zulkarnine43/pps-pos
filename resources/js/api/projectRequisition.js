import request from '@/utils/request';
import Resource from '@/api/resource';

/**
 * All ProjectRequisitionResource Api call form here.
 *
 * @author	bitbyte
 * @since	v0.0.1
 * @version	v1.0.0	Wednesday, February 10th, 2021.
 * @see		Resource
 * @global
 */
export class ProjectRequisitionResource extends Resource {
  constructor() {
    super('project-requisition');
  }
}

/**
 * a single project requisition api call from here
 * @var		export	functio
 * @global
 */
export function fetchProjectRequisition(id) {
  return request({
    url: '/project-requisition/' + id,
    method: 'get',
  });
}
