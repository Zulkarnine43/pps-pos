import request from '@/utils/request';
import Resource from '@/api/resource';

/**
 * All employees Api call from here.
 *
 * @author	bitbyte
 * @since	v0.0.1
 * @version	v1.0.0	Wednesday, February 10th, 2021.
 * @see		Resource
 * @global
 */
export class EmployeeResource extends Resource {
  constructor() {
    super('employees');
  }
}

/** '
 * a single unit api call from here
 * @var		export	functio
 * @global
 */
export function fetchEmployee(id) {
  return request({
    url: '/employees/' + id,
    method: 'get',
  });
}

/**
 * a single inactive api call from here
 * @var		export	functio
 * @global
 */
export function inactive(id){
  console.log(id);
  return request({
    url: '/employee/inactive/' + id,
    method: 'put',
  });
}

/**
 * a single active api call from here
 * @var		export	functio
 * @global
 */
export function active(id){
  console.log(id);
  return request({
    url: '/employee/active/' + id,
    method: 'put',
  });
}
