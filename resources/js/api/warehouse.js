import request from '@/utils/request';
import Resource from '@/api/resource';

/**
 * All Warehouse Api call from here.
 *
 * @author	bitbyte
 * @since	v0.0.1
 * @version	v1.0.0	Wednesday, February 10th, 2021.
 * @see		Resource
 * @global
 */
export class WarehouseResource extends Resource {
  constructor() {
    super('warehouses');
  }
}

/**
 * a sinle warehouse api call from here
 * @var		export	functio
 * @global
 */
export function fetchWarehouse(id) {
  return request({
    url: '/warehouses/' + id,
    method: 'get',
  });
}

/**
 * a single warehouse mute api call from here
 * @var		export	functio
 * @global
 */
export function mute(id){
  return request({
    url: '/warehouse/mute/' + id,
    method: 'put',
  });
}

/**
 * a sinle warehouse unmute api call from here
 * @var		export	functio
 * @global
 */
export function unmute(id){
  return request({
    url: '/warehouse/unmute/' + id,
    method: 'put',
  });
}
