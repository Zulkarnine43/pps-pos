import request from '@/utils/request';

/**
 * search api
 * @var		export	functio
 * @global
 */
export function userSearch(name) {
  return request({
    url: '/search/user',
    method: 'get',
    params: { name },
  });
}
