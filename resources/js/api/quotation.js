import request from '@/utils/request';
import Resource from '@/api/resource';

/**
 * All Quotation Api call from here.
 *
 * @author	bitbyte
 * @since	v0.0.1
 * @version	v1.0.0	Wednesday, February 10th, 2021.
 * @see		Resource
 * @global
 */
export class QuotationResource extends Resource {
  constructor() {
    super('quotation');
  }
}

/**
 * a single client api call from here
 * @var		export	functio
 * @global
 */
export function fetchQuotation(id) {
  return request({
    url: '/quotation/' + id,
    method: 'get',
  });
}
