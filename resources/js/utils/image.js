export function removeUnsavedImage(image){
  console.log('removeUnsavedImage', image);
  axios.delete('/api/remove-unsaved-file/'+ image).then(res => {
      return res;
  })
}
export async function UploadImage(folder, event){
  let fileName = '';
  const image = event.target.files[0];
  const data = new FormData();
  data.append('photo', image);
  await axios.post('/api/upload-image/'+ folder, data).then(res => {
    fileName = res.data.imageName;
  });

  return fileName;
}