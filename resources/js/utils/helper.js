export function getCurrentDate() {
  const current = new Date();
  return `${current.getFullYear()}-${current.getMonth() +
    1}-${current.getDate()}`;
}

export async function getItemLastId(model) {
  let lastID = null;
  await axios.get('/api/get-table-last-id/'+model).then(res => {
    lastID = res.data + 1;
  });
  return lastID;
}

export function calcProductVat($product){

}
