export default {
  checkPin() {
    this.$prompt('Please Type Entry Pin', {
      confirmButtonText: 'OK',
      cancelButtonText: 'Cancel',
      inputErrorMessage: 'Invalid Pin',
    }).then(({ value }) => {
      this.$message({
        type: 'success',
        message: 'Your email is:' + value,
      });
    }).catch(() => {
      this.$message({
        type: 'info',
        message: 'Input canceled',
      });
    });
  },
};
